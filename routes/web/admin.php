<?php

$routePrefix = config('common.backend-route-prefix');

$router->group(['namespace' => 'Admin', 'prefix' => $routePrefix, 'middleware' => ['web']], function () use ($router, $routePrefix) {

    $router->get('login', 'LoginController@showLoginForm')->name('admin.login');
    $router->post('login', 'LoginController@login')->name('admin.login.post');

});

$router->group(['namespace' => 'Admin', 'prefix' => $routePrefix, 'middleware' => ['web', 'auth.admin', 'admin']], function () use ($router, $routePrefix) {

    $router->post('logout', 'LoginController@logout')->name('admin.logout');

    $router->get('/', function () {
        return redirect()->route('admin.dashboard');
    });

    $router->get('dashboard', 'DashboardController@index')->name('admin.dashboard');

    $router->post('cache/clear', 'DashboardController@clearCache')->name('admin.cache.clear');

    /*
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    */
    $router->resource('users', 'UserController', ['as' => $routePrefix, 'except' => ['create', 'show']]);
    $router->get('users/search', 'UserController@search');
    $router->get('users/invite', 'UserController@getInvite');
    $router->get('users/switch/{id}', 'UserController@switchToUser');
    $router->post('users/invite', 'UserController@postInvite');

    /*
    |--------------------------------------------------------------------------
    | Customers
    |--------------------------------------------------------------------------
    */
    $router->get('customers/search', 'CustomerController@search');
    $router->resource('customers', 'CustomerController', ['as' => $routePrefix, 'except' => ['create', 'edit']]);

    /*
    |--------------------------------------------------------------------------
    | Roles
    |--------------------------------------------------------------------------
    */
    $router->resource('roles', 'RoleController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->get('roles/search', 'RoleController@search');

    /*
    |--------------------------------------------------------------------------
    | Categories
    |--------------------------------------------------------------------------
    */
    $router->resource('categories', 'CategoryController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->get('categories/search', 'CategoryController@search');
    $router->post('categories/main_images/{id}', 'CategoryController@deleteMainImage')->name('admin.categories.main_image.delete');
    $router->post('categories/{id}/status', 'CategoryController@postStatus')->name('admin.categories.status');
    $router->post('categories/{id}/delete', 'CategoryController@destroy')->name('admin.categories.delete');

    /*
    |--------------------------------------------------------------------------
    | Manufacturers
    |--------------------------------------------------------------------------
    */
    $router->resource('manufacturers', 'ManufacturerController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->get('manufacturers/search', 'ManufacturerController@search');
    $router->post('manufacturers/main_images/{id}', 'ManufacturerController@deleteMainImage')->name('admin.manufacturers.main_image.delete');
    $router->post('manufacturers/{id}/status', 'ManufacturerController@postStatus')->name('admin.manufacturers.active');
    $router->post('manufacturers/{id}/delete', 'ManufacturerController@destroy')->name('admin.manufacturers.delete');

    /*
    |--------------------------------------------------------------------------
    | Shipments
    |--------------------------------------------------------------------------
    */
    $router->resource('shipments', 'ShipmentController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('shipments/main_images/{id}', 'ShipmentController@deleteMainImage')->name('admin.shipments.main_image.delete');
    $router->post('shipments/{id}/status', 'ShipmentController@postStatus')->name('admin.shipments.active');
    $router->post('shipments/{id}/delete', 'ShipmentController@destroy')->name('admin.shipments.delete');

    /*
    |--------------------------------------------------------------------------
    | Payments
    |--------------------------------------------------------------------------
    */
    $router->resource('payments', 'PaymentController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('payments/main_images/{id}', 'PaymentController@deleteMainImage')->name('admin.payments.main_image.delete');
    $router->post('payments/{id}/status', 'PaymentController@postStatus')->name('admin.payments.active');
    $router->post('payments/{id}/delete', 'PaymentController@destroy')->name('admin.payments.delete');

    /*
    |--------------------------------------------------------------------------
    | Attributes and groups of attributes
    |--------------------------------------------------------------------------
    */
    $router->resource('attributes/groups', 'AttributeGroupController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->resource('attributes', 'AttributeController', ['as' => $routePrefix, 'except' => ['show']]);

    /*
    |--------------------------------------------------------------------------
    | Products
    |--------------------------------------------------------------------------
    */
    $router->resource('products', 'ProductController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->get('products/search', 'ProductController@search');
    $router->get('products/delete_all','ProductController@deleteAll')->name('admin.products.all.delete');
    $router->post('products/delete_filters','ProductController@deleteFilteredProducts')->name('admin.products.delete.filter');
    $router->post('products/main_images/{id}', 'ProductController@deleteMainImage')->name('admin.product.main_image.delete');
    $router->post('products/images/{id}', 'ProductController@deleteImage')->name('admin.product.image.delete');
    $router->post('products/{id}/delete', 'ProductController@destroy')->name('admin.products.delete');
    $router->post('products/{id}/status', 'ProductController@postStatus')->name('admin.products.status');
    $router->post('products/{id}/attribute', 'ProductController@addAttribute')->name('admin.products.attribute.add');
    $router->delete('products/{id}/attribute', 'ProductController@deleteAttribute')->name('admin.products.attribute.delete');


    /*
    |--------------------------------------------------------------------------
    | Discounts
    |--------------------------------------------------------------------------
    */
    $router->resource('discounts', 'DiscountController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('discounts/{id}/status', 'DiscountController@postStatus')->name('admin.discounts.active');
    $router->post('discounts/{id}/delete', 'DiscountController@destroy')->name('admin.discounts.delete');

    /*
    |--------------------------------------------------------------------------
    | Languages
    |--------------------------------------------------------------------------
    */
    $router->resource('languages', 'LanguageController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('languages/{id}/active', 'LanguageController@postActive')->name('admin.languages.active');
    $router->post('languages/{id}/image', 'LanguageController@deleteImage')->name('admin.languages.image.delete');

    /*
    |--------------------------------------------------------------------------
    | Currencies
    |--------------------------------------------------------------------------
    */
    $router->resource('currencies', 'CurrencyController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('currencies/{id}/active', 'CurrencyController@postActive')->name('admin.currencies.status');
    $router->post('currencies/{id}/delete', 'CurrencyController@destroy')->name('admin.currencies.delete');

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    */

    $router->get('settings', 'SettingController@index')->name('admin.settings');
    $router->post('settings', 'SettingController@save')->name('admin.settings.save');

    /*
    |--------------------------------------------------------------------------
    | Menus
    |--------------------------------------------------------------------------
    */
    $router->resource('menus', 'MenuController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('menus/{id}/active', 'MenuController@active')->name('admin.menus.active');
    $router->get('menus/{id}/link', 'MenuController@linkCreate')->name('admin.menus.link.create');
    $router->post('menus/{id}/sort', 'MenuController@linkSort')->name('admin.menus.link.sort');
    $router->post('menus/{id}/link', 'MenuController@linkStore')->name('admin.menus.link.store');
    $router->post('menus/{id}/link/active', 'MenuController@linkActive')->name('admin.menus.link.active');
    $router->get('menus/{id}/link/{link_id}', 'MenuController@linkEdit')->name('admin.menus.link.edit');
    $router->patch('menus/{id}/link/{link_id}', 'MenuController@linkUpdate')->name('admin.menus.link.update');
    $router->delete('menus/{id}/link/{link_id}/delete', 'MenuController@linkDelete')->name('admin.menus.link.delete');

    /*
    |--------------------------------------------------------------------------
    | Pages
    |--------------------------------------------------------------------------
    */
    $router->resource('pages', 'PageController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('pages/{id}/status', 'PageController@postStatus')->name('admin.pages.active');
    $router->post('pages/{id}/delete', 'PageController@destroy')->name('admin.pages.delete');
    $router->post('pages/{id}/upload', 'PageController@upload')->name('admin.pages.upload');

    /*
    |--------------------------------------------------------------------------
    | Reviews
    |--------------------------------------------------------------------------
    */
    $router->resource('reviews', 'ReviewController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('reviews/{id}/status', 'ReviewController@postStatus')->name('admin.reviews.active');
    $router->post('reviews/{id}/delete', 'ReviewController@destroy')->name('admin.reviews.delete');

    /*
    |--------------------------------------------------------------------------
    | Blog
    |--------------------------------------------------------------------------
    */
    $router->resource('blog', 'BlogController', ['as' => $routePrefix, 'except' => ['show']]);
    $router->post('blog/{id}/status', 'BlogController@postStatus')->name('admin.blog.active');
    $router->post('blog/{id}/delete', 'BlogController@destroy')->name('admin.blog.delete');
    $router->post('blog/{id}/upload', 'BlogController@upload')->name('admin.blog.upload');
    $router->post('blog/main_images/{id}', 'BlogController@deleteMainImage')->name('admin.blog.main_image.delete');

    /*
    |--------------------------------------------------------------------------
    | Modules
    |--------------------------------------------------------------------------
    */
    $router->resource('modules', 'ModuleController', ['as' => $routePrefix, 'except' => ['show', 'create', 'destroy', 'edit']]);
    $router->get('modules/{code}', 'ModuleController@config')->name('admin.modules.config');
    $router->post('modules/{code}', 'ModuleController@update')->name('admin.modules.update');
    $router->post('modules/{id}/status', 'ModuleController@postStatus')->name('admin.modules.active');
    $router->post('modules/{id}/cache', 'ModuleController@postCache')->name('admin.modules.cache');
    $router->post('modules/{code}/clear', 'ModuleController@clearCache')->name('admin.modules.clear');
    $router->get('modules/{code}/ajax/{key?}', 'ModuleController@ajax')->name('admin.modules.ajax');
    $router->post('modules/{code}/file/{id}/delete', 'ModuleController@deleteFile')->name('admin.modules.file.delete');


    /*
    |--------------------------------------------------------------------------
    | Orders
    |--------------------------------------------------------------------------
    */
    $router->resource('orders', 'OrderController', ['as' => $routePrefix, 'except' => ['edit', 'update', 'create', 'store']]);
    $router->post('orders/{id}/status', 'OrderController@postStatus')->name('admin.orders.status');
    $router->post('orders/{id}/delete', 'OrderController@destroy')->name('admin.orders.delete');
    $router->post('orders/delete_all', 'OrderController@deleteAll')->name('admin.orders.delete.all');

    $router->get('subscriptions', 'SubscriptionController@index')->name('admin.subscriptions');

});