<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a given Closure or controller and enjoy the fresh air.
|
*/

$router->group(['namespace' => 'Shop', 'middleware' => ['web', 'locale']], function () use ($router) {

    $router->get('/', 'MainController@index')->name('shop.main');
    $router->get('/product_search', 'MainController@productSearch');

    /*
     * Cart
     */

    $router->get('cart', 'CartController@index')->name('shop.cart');
    $router->post('cart/add', 'CartController@add')->name('shop.cart.add');
    $router->post('cart/delete', 'CartController@delete')->name('shop.cart.delete');
    $router->post('cart/quantity', 'CartController@quantity')->name('shop.cart.quantity');
    $router->post('cart/payments', 'CartController@getPayments')->name('shop.cart.payments');

    $router->post('checkout', 'CartController@postCheckout')->name('shop.checkout');
    $router->get('checkout', 'CartController@getCheckout')->name('shop.checkout.success');

    /*
     * Catalog
     */

    $router->get('catalog', 'CatalogController@index')->name('shop.catalog');
    $router->get('catalog/popular', 'CatalogController@popular')->name('shop.catalog.popular');
    $router->get('catalog/latest', 'CatalogController@latest')->name('shop.catalog.latest');
    $router->post('catalog/substance', 'CatalogController@ajax')->name('shop.substance');
    $router->post('catalog/oneclick', 'CatalogController@quickBuy')->name('shop.oneclick');

    $router->get('product/{slug}/{tab?}', 'ProductController@index')->name('shop.product');
    $router->post('product/{slug}/review', 'ProductController@postReview')->name('shop.review.post');

    $router->get('page/{slug}', 'PageController@index')->name('shop.page');
    $router->get('blog', 'BlogController@index')->name('shop.blog');
    $router->get('blog/{slug}', 'BlogController@item')->name('shop.blog.post');

    /*
     * Helpers
     */

    $router->get('language/{code}', 'MainController@language')->name('shop.language.change');
    $router->get('currency/{code}', 'MainController@currency')->name('shop.currency.change');

    $router->get('images/{disk}/{id}/{name}|{w}x{h}', 'ImageController@get')->name('shop.image.get');
    $router->get('images/{disk}/no-photo|{w}x{h}', 'ImageController@not')->name('shop.image.not');

    $router->get('{slug}', 'CatalogController@category')->name('shop.category');

});

