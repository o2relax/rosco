<?php

$router->group(['namespace' => 'Auth', 'middleware' => ['guest', 'locale']], function () use ($router) {

    /*
    |--------------------------------------------------------------------------
    | Login/ Logout/ Password
    |--------------------------------------------------------------------------
    */
    $router->get('login', 'LoginController@showLoginForm')->name('login');
    $router->post('login', 'LoginController@login')->name('login.post');

    /*
    |--------------------------------------------------------------------------
    | Registration & Activation
    |--------------------------------------------------------------------------
    */
    $router->get('register', 'RegisterController@showRegistrationForm')->name('register');
    $router->post('register', 'RegisterController@register')->name('register.post');

    /*
    |--------------------------------------------------------------------------
    | Forgot password
    |--------------------------------------------------------------------------
    */
    $router->get('profile/forgot/password', 'ForgotPasswordController@index')->name('forgot.password');
    $router->post('profile/forgot/password', 'ForgotPasswordController@post')->name('forgot.password.post');

});


$router->group(['prefix' => 'profile', 'namespace' => 'User'], function () use ($router) {

    $router->post('subscribe', 'ProfileController@subscribe')->name('shop.subscribe');

});

/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
*/
$router->group(['namespace' => 'Auth', 'middleware' => ['auth', 'locale']], function () use ($router) {

    $router->post('logout', 'LoginController@logout')->name('logout');

});

$router->group(['middleware' => ['auth', 'locale']], function () use ($router) {

    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    */
    $router->group(['prefix' => 'profile', 'namespace' => 'User'], function () use ($router) {
        $router->get('', 'ProfileController@index')->name('shop.profile');
        $router->get('orders', 'ProfileController@orders')->name('shop.profile.orders');
        $router->post('password', 'ProfileController@password')->name('shop.profile.password.update');
    });

});