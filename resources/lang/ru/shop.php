<?php

return [
    'common' => [
        'enter' => 'Войти',
        'buttons' => [
            'add_to_cart' => 'В корзину'
        ]
    ],
    'cart' => [
        'heading' => 'Моя корзина'
    ],
    'blog' => [
        'heading' => 'Блог',
        'read_more' => 'Читать дальше'
    ],
    'menus' => [
        'account' => 'Моя учетная запись',
        'information' => 'Информация'
    ],
    'footer' => [
        'contacts' => 'Контакты ООО “Роско Груп”'
    ],
    'modules' => [
        'slider' => [
            'btn' => 'Перейти в каталог'
        ]
    ],
    'catalog' => [
        'quick_buy' => 'Купити cейчас',
        'client_name' => 'Имя',
        'client_phone' => 'Номер телефона',
        'get' => 'Заказать'
    ]

];