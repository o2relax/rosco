<?php

return [
    'order' => [
        'new' => [
            'subject' => 'Новый заказ #:order_id',
            'customer_data' => 'Вы оставили данные',
            'office' => 'Подробнее в <a href=":link">личном кабинете</a>',
            'sub' => 'Вы всегда можете написать нам по любым вопросам на :email.',
            'text' => 'Вы оформили заказа #:order_id на :site_name',
        ]
    ],
    'register' => [
        'subject' => 'Регистрация аккаунта',
        'text' => 'Вы оформили заказа #:order_id на :site_name',
        'name' => 'Имя',
        'email' => 'Почта',
        'password' => 'Пароль',
    ],
    'password_change' => [
        'subject' => 'Изменение пароля',
        'text' => 'Вы изменили пароль на :site_name',
        'password' => 'Новый пароль',
    ]
];