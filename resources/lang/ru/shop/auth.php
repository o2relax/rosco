<?php

return [
    'headings' => [
        'login' => 'Вход в аккаунт',
        'register' => 'Регистрация аккаунта',
        'forgot' => 'Восстановление пароля',
    ],
    'remember' => 'Запомнить меня',
    'buttons' => [
        'enter' => 'Войти',
        'register' => 'Зарегистрировать аккаунт',
        'forgot' => 'Забыли пароль?',
        'forgot_post' => 'Сбросить'
    ],
    'placeholders' => [
        'email' => 'E-mail',
        'password' => 'Пароль',
        'password_confirmation' => 'Подтверждение пароля',
        'name' => 'Ваше имя'
    ]
];