<?php

return [
    'or' => 'или',
    'enter' => 'Войти',
    'register' => 'Регистрация',
    'reviews' => [
        'rules' => 'Для того, чтобы оставить отзывы вам необходимо <a href=":REGISTER">зарегистрироваться</a> или <a href=":LOGIN">войти</a> в свой аккаунт',
        'btn' => 'Оставить отзыв'
    ],
    'main_page' => 'Главная',
    'catalog_page' => 'Каталог',
    'buttons' => [
        'add_to_cart' => 'В корзину',
        'delete_from_cart' => '&#043;',
        'added_to_cart' => 'Добавлено',
        'deleted_from_cart' => 'удалено'
    ]
];