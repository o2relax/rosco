<?php

return [
    'heading' => 'Моя корзина',
    'th' => [
        'title' => 'Товар',
        'manufacturer' => 'Производитель',
        'price' => 'Цена',
        'quantity' => 'Кол-во',
        'total' => 'Итого',
    ],
    'button' => 'Оформить заказ',
    'total' => 'Итого:',
    'empty' => 'Ваша корзина пуста',
    'checkout' => [
        'shipment' => 'Доставка <span class="text-red">*</span>',
        'payment' => 'Оплата <span class="text-red">*</span>',
        'name' => 'Ф.И.О <span class="text-red">*</span>',
        'email' => 'Email <span class="text-red">*</span>',
        'phone' => 'Телефон <span class="text-red">*</span>',
        'address' => 'Адрес <span class="text-red">*</span>',
        'validation_notice' => 'Поля отмеченные <span class="text-red">*</span> обязательны для заполнения',
        'name_placeholder' => 'Иванов Иван Иванович'
    ],
    'errors' => [
        'quantity' => 'доступно :stock'
    ]
];