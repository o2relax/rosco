<?php

return [
    'heading' => 'Фильтры',
    'any'     => 'Любой',

    'substance' => [
        'title'        => 'Действующее вещество:',
        'placeholders' => 'Введите вещество',
    ],

    'titles'       => [
        'manufacturer' => 'Производитель:',
        'price'        => 'Диапазон цен, <span>:currency</span>',
    ],
    'placeholders' => [
        'to'   => 'до',
        'from' => 'от',
    ],
    'sorts'        => [
        'title' => ' Сортировать по:',
        'date_add_desc' => 'От новых к старым',
        'date_add_asc'  => 'От старых к новым',
        'price_asc'     => 'По возрастанию цены',
        'price_desc'    => 'По убыванию цены',
        'name_asc'      => 'По названию А-Я',
        'name_desc'     => 'По названию Я-А',
        'stock_desc'    => 'По наличию (уменьшение)',
        'stock_asc'     => 'По наличию (увеличение)',
    ],
    'per_page' => 'Показывать:'
];