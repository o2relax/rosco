<?php

return [
    'headings' => [
        'main' => 'Личный кабинет',
        'password' => 'Изменение пароля',
        'orders' => 'Мои заказы',
        'exit' => 'Выход'
    ],
    'buttons' => [
        'change_password' => 'Сменить пароль',
    ],
    'placeholders' => [
        'old_password' => 'Текущий пароль',
        'password' => 'Новый пароль',
        'password_confirmation' => 'Подтверждение пароля',
    ],
    'heads' => [
        'products' => 'Инфо',
        'total' => 'Сумма',
        'status' => 'Статус',
        'date' => 'Дата',
    ],
    'messages' => [
        'change_password' => 'Ваш пароль изменен',
        'change_password_fail' => 'При изменении пароля произошла ошибка. Свяжитесь с администрацией',
        'forgot_password' => 'Ваш новый пароль отправлен на :email',
        'forgot_password_fail' => 'При изменении пароля произошла ошибка. Свяжитесь с администрацией',
        'subscribe' => 'Подписка успешно оформлена',
        'subscribe_fail' => 'Произошла ошибка. Свяжитесь с администрацией'
    ],
    'product_deleted' => '<span class="status_6">Товар не доступен</span>'
];