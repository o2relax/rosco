<?php

return [
    'image' => [
        'main_alt' => 'главное фото',
        'ext_alt' => 'дополнительное фото'
    ],
    'substance' => 'Действующие вещества:',
    'article' => 'Артикул:',
    'quantity' => 'Количество',
    'description' => 'Описание',
    'reviews' => 'Отзывы',
    'attributes' => 'Характеристики',
    'attribute' => [
        'title' => 'Наименование',
        'value' => 'Значение',
    ],
    'empty_reviews' => 'отзывов пока нет',
    'related_products' => 'C этим товаром покупают'
];