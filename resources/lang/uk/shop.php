<?php

return [
    'common' => [
        'enter' => 'Увійти',
        'buttons' => [
            'add_to_cart' => 'У кошик'
        ]
    ],
    'cart' => [
        'heading' => 'Мій кошик'
    ],
    'blog' => [
        'heading' => 'Блог',
        'read_more' => 'Читати далі'
    ],
    'menus' => [
        'account' => 'Мій обліковий запис',
        'information' => 'Інформація'
    ],
    'footer' => [
        'contacts' => 'Контакти ООО “Роско Груп”'
    ],
    'modules' => [
        'slider' => [
            'btn' => 'Перейти у каталог'
        ]
    ],
    'catalog' => [
        'quick_buy' => 'Купити зараз',
        'client_name' => 'Ім\'я',
        'client_phone' => 'Номер телефону',
        'get' => 'Замовити'
    ]

];