<?php

return [
    'heading' => 'Моя корзина',
    'th' => [
        'title' => 'Товар',
        'manufacturer' => 'Виробник',
        'price' => 'Ціна',
        'quantity' => 'Кіл-сть',
        'total' => 'Разом',
    ],
    'button' => 'Оформити замовлення',
    'total' => 'Разом :',
    'empty' => 'Ваша корзина пуста',
    'checkout' => [
        'shipment' => 'Доставка <span class="text-red">*</span>',
        'payment' => 'Оплата <span class="text-red">*</span>',
        'name' => 'П.І.Б. <span class="text-red">*</span>',
        'email' => 'Email <span class="text-red">*</span>',
        'phone' => 'Телефон <span class="text-red">*</span>',
        'address' => 'Адрес <span class="text-red">*</span>',
        'validation_notice' => 'Поля відмічені <span class="text-red">*</span> обов\'язкові для заповнення',
        'name_placeholder' => 'Иванов Иван Иванович'
    ],
    'errors' => [
        'quantity' => 'доступно :stock'
    ]
];