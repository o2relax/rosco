<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
    <title>{{cfg('main_title', 'langs')}}</title>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        table {
            border-spacing: 0;
        }
        table td {
            border-collapse: collapse;
        }
        .ExternalClass {
            width: 100%;
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        .ReadMsgBody {
            width: 100%;
            background-color: #ebebeb;
        }
        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        img {
            -ms-interpolation-mode: bicubic;
        }
        .yshortcuts a {
            border-bottom: none !important;
        }
        @media screen and (max-width: 599px) {
            table[class="force-row"],
            table[class="container"] {
                width: 100% !important;
                max-width: 100% !important;
            }
        }
        @media screen and (max-width: 400px) {
            td[class*="container-padding"] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }
        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }
    </style>
</head>
<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
    <tr>
        <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
            <br/>
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container"
                   style="width:600px;max-width:600px">
                <tr>
                    <td class="container-padding header" align="left"
                        style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#ffc91a;padding-left:24px;padding-right:24px">
                        {{cfg('main_title', 'langs')}}
                    </td>
                </tr>
                <tr>
                    <td class="container-padding content" align="left"
                        style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
                        <br/>
                        <div class="title"
                             style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">
                            @lang('mails.order.new.text', ['order_id' => $order->id, 'site_name' => cfg('main_title', 'langs')])</div>
                        <br/>
                        <div class="body-text"
                             style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                            <table cellpadding="0" cellspacing="0"
                                   style="width: 100%; border-right: 1px solid #F0F0F0; border-bottom: 1px solid #F0F0F0;">
                                <tr>
                                    <th style="padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">
                                        #
                                    </th>
                                    <th style="padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;"></th>
                                    <th style="padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">@lang('shop/cart.th.title')</th>
                                    <th style="text-align: center; padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">@lang('shop/cart.th.quantity')</th>
                                    <th style="text-align: center; padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">@lang('shop/cart.th.price')</th>
                                </tr>
                                @foreach($order->items as $item)
                                    <tr>
                                        <td style="text-align: center; padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">{{$item->product->id}}</td>
                                        <td style="text-align: center; padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">
                                            <img src="{{$item->product->image ? route('shop.image.get', ['products', $item->product->id, $item->product->image, cfg('image_list_width'), cfg('image_list_height')]) : route('shop.image.not', ['products', cfg('image_list_width'), cfg('image_list_height')]) }}">
                                        </td>
                                        <td style="padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">
                                            <a href="{{route('shop.product', [$item->product->slug])}}" target="_blank"
                                               style="color: #ffc91a;">{{$item->product->name}}</a></td>
                                        <td style="text-align: center; padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">{{$item->quantity}}</td>
                                        <td style="text-align: center; padding: 10px; border-top: 1px solid #F0F0F0; border-left: 1px solid #F0F0F0;">{{currency()->convert($item->product->price, $order->currency_id)}}</td>
                                    </tr>
                                @endforeach
                            </table>
                            <br/><br/>
                            @lang('mails.order.new.customer_data')
                            <br/><br/>
                            <table style="width: 100%;">
                                <tr>
                                    <th style="width: 30%;">@lang('shop/cart.checkout.name')</th>
                                    <td>{{$order->name}}</td>
                                </tr>
                                <tr>
                                    <th style="width: 30%;">@lang('shop/cart.checkout.phone')</th>
                                    <td>{{$order->phone}}</td>
                                </tr>
                                <tr>
                                    <th style="width: 30%;">@lang('shop/cart.checkout.email')</th>
                                    <td>{{$order->email}}</td>
                                </tr>
                                <tr>
                                    <th style="width: 30%;">@lang('shop/cart.checkout.address')</th>
                                    <td>{{$order->address}}</td>
                                </tr>
                            </table>
                            <br/><br/>
                            <p>@lang('mails.order.new.office', ['link' => route('shop.profile')])</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="container-padding footer-text" align="left"
                        style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                        <br/><br/>
                        © {{ date('Y') }} {{cfg('main_title', 'langs')}}.
                        <br/><br/>
                        <span class="ios-footer">
                            {{cfg('admin_email')}}<br>
                            {{cfg('phone_1')}}<br>
                            {{cfg('phone_2')}}
                        </span>
                        <a href="{{url('/')}}" style="color:#555">{{cfg('main_title', 'langs')}}</a><br/>
                        <br/><br/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>


