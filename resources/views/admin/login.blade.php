@extends('admin.layouts.guest')

@section('content')
    <form role="form" method="POST" action="{{ route('admin.login.post') }}">
        {{ csrf_field() }}
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                <h5 class="content-group-lg">@yield('title') <small class="display-block">Введите свои данные</small></h5>
            </div>

            <div class="form-group has-feedback has-feedback-left">
                <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Почта">
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
                @if($errors->has('email'))
                    <label id="email-error" class="validation-error-label" for="email">{{ $errors->first('email') }}</label>
                @endif
            </div>

            <div class="form-group has-feedback has-feedback-left">
                <input type="password" id="password" class="form-control" placeholder="Пароль" name="password" required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                @if($errors->has('password'))
                    <label id="password-error" class="validation-error-label" for="password">{{ $errors->first('password') }}</label>
                @endif
            </div>

            <div class="form-group login-options">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="remember" class="styled" checked="checked">
                            Запомнить меня
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn bg-blue btn-block">Войти <i class="icon-circle-right2 position-right"></i></button>
            </div>
        </div>
    </form>
@stop

