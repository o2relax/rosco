<fieldset class="content-group">
    <div class="form-group @if($errors->has('parent_id'))has-error has-feedback @endif">
        @input_maker_label('Родительская категория', ['class' => 'control-label col-lg-2'])
        <div class="col-md-6">
            <select class="form-control" name="parent_id">
                <option value="0" {{old_input('parent_id', $category) == 0 ? 'selected' : ''}}>Верхний уровень</option>
                @if(count($categories))
                    @foreach($categories as $cat)
                        <option value="{{$cat->id}}" {{$cat->id==old_input('parent_id', $category) ? 'selected' : ''}}>{{$cat->name}}</option>
                    @endforeach
                @endif
            </select>
            @if($errors->has('parent_id'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('parent_id')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('slug'))has-error has-feedback @endif">
        @input_maker_label('Урл', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('slug', ['type' => 'string'], $category)
            @if($errors->has('slug'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('slug')}}</span>
            @endif
            <code>сгенерируется автоматически, если оставиь пустым</code>
        </div>
    </div>
    <div class="form-group @if($errors->has('sort'))has-error has-feedback @endif">
        @input_maker_label('Сортировка', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('sort', ['type' => 'string'], $category)
            @if($errors->has('sort'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('sort')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        @input_maker_label('Отображается', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="is_active" 
                           class="switchery" {{old_input('is_active', $category) ? 'checked' : ''}}>
                    Вкл
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        @input_maker_label('Цвет', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <input type="text" class="form-control colorpicker-show-input" name="color" data-preferred-format="hex" value="{{old_input('color', $category)}}">
        </div>
        @if($errors->has('sort'))
            <div class="form-control-feedback">
                <i class="icon-cancel-circle2"></i>
            </div>
            <span class="help-block">{{$errors->first('sort')}}</span>
        @endif

    </div>
    <fieldset class="content-group">
        <legend class="text-bold">Основное изображение</legend>

        <div class="form-group col-xs-12">
            <input type="file" name="image" class="file-input-preview2" data-show-remove="true">
        </div>
    </fieldset>
</fieldset>