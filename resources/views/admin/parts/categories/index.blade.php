@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }

        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }
        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/categories/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Создать категорию</a>
            </div>
            <form id="" class="raw-margin-left-24" method="get" action="{{url('admin/categories/search')}}">
                {!! csrf_field() !!}
                <div class="col-md-6">
                    <input class="form-control" name="search[name]" placeholder="Поиск по названию"
                           value="{{request()->get('search')['name']}}">
                </div>
                <div class="col-md-3">
                    <input type="submit" value="найти" class="btn btn-primary btn-xs">

                    <a class="btn btn-default btn-xs" href="{{url('admin/categories')}}" data-popup="tooltip"
                       data-popup="tooltip" title="Очистить поиск"><i class="icon-eraser2"></i></a>
                </div>
            </form>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список категорий<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком категорий</div>
        <div class="table-responsive">
            <table class="table text-nowrap">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Избражение</th>
                    <th>Название</th>
                    <th class="text-center">Статус</th>
                    <th class="text-center">Дата</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($categories))
                    @foreach($categories as $category)
                        @include('admin.parts.products.includes.list_item')
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection