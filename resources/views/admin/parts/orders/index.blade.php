@extends('admin.layouts.dashboard')
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
    <script>
        function changeOrderStatus(id, status, label) {
            $.ajax({
                url: '/admin/orders/' + id + '/status',
                method: 'post',
                data: 'status_id=' + status,
                dataType: 'json',
                success: function (response) {
                    var cl = label.attr('class');
                    label.attr('class', cl.replace(/(bg+)/g, ''));
                    label.addClass('bg-' + response.code);
                    label.html(response.name + ' <span class="caret"></span>');
                }
            });
            return false;
        }

        $("#checkAll").change(function () {
            $(".form-check-order-input").prop('checked', $(this).prop("checked"));
        });

        //        function checkAllOrders() {
        //            if ($("#checkAll").data('myval') === "test") {
        //                $('.form-check-order-input').prop('checked',false);
        //                $("#checkAll").data('data-myval',"test2");
        //            } else {
        //                $('.form-check-order-input').prop('checked',true);
        //                $("#checkAll").data('data-myval',"test");
        //            }
        //        }
        function checkOrder() {
            var orderChecked = document.querySelectorAll('.form-check-order-input');
            var arr = [];
            for(var i=0;i<orderChecked.length;i++) {
                if (orderChecked[i].checked === true) {
                    arr.push(orderChecked[i].getAttribute('data-id'));
                }
            }
            var data = {data: arr};
            $.ajax({
                type: 'post',
                url: "/admin/orders/delete_all",
                data: data,
                success: function (result) {
                    location.reload();
                }
            });
        }
    </script>
@endsection
@section('content')

    @include('admin.modals')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Список заказов
            </h5>
        </div>
        <div class="panel-heading">
            <button class="btn btn-primary" type="button"  onclick="if(confirm('Вы уверены, что хотите удалить выбранные заказы?')) checkOrder();">Удалить</button>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="col-md-1"><label><input id="checkAll" data-myval="test" type="checkbox" value="2"  ></label ></th>
                    <th class="col-md-1"></th>
                    <th class="text-center">ID</th>
                    <th>Покупатель</th>
                    <th>Данные</th>
                    <th>Товары</th>
                    <th>Сумма</th>
                    <th class="text-center">Статус</th>
                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @if(count($orders))
                    @foreach($orders as $order)
                        @include('admin.parts.orders.item')
                    @endforeach
                </tbody>
            </table>
            @else
            </tbody>
            </table>
            <br>
            <div class="col-md-12 text-center">Заказов не найдено</div>
            @endif

        </div>
        @if(count($orders))
            <div class="col-xs-12 text-center">
                {!! $pagination !!}
            </div>
        @endif
    </div>

@endsection
