<tr>
    <td>
        <label><input  class="form-check-order-input" id="orderCheck{{$order->id}}" data-id="{{$order->id}}" type="checkbox" value=""></label>
    </td>
    <td>
        @if($order->viewed)
            <span class="label label-flat border-slate-300 text-slate-300 position-right">Просмотрен</span>
        @else
            <span class="label label-flat border-success text-success-600 position-right">Новый</span>
        @endif
    </td>
    <td class="text-center">
        <small class="display-block text-size-small no-margin">
            {{$order->id}}
        </small>
    </td>
    <td>
        <i class="icon-user"></i> {!! $order->name !!}<br>
    </td>
    <td>

        <i class="icon-mail5"></i> {!! $order->email !!}<br>
        <i class="icon-phone"></i> {!! $order->phone !!}<br>
        <i class="icon-map4"></i> {!! $order->address !!}
    </td>
    <td class="text-muted text-size-small">
        @foreach($order->items as $item)
            @if($item->product)
                - <a href="{{route('admin.products.edit', [$item->product_id])}}" target="_blank">{{$item->product->name}}</a> x{{$item->quantity}}<br>
            @else
                <span class="text-danger">- #{{$item->product_id}} x{{$item->quantity}}</span>
                <br>
            @endif
        @endforeach
    </td>
    <td>
        <h6 class="text-semibold">{{$order->total}}</h6>
    </td>
    <td class="text-center">
        <div class="btn-group">
            <a class="label bg-{{collect($statuses)->where('id', $order->status_id)->first()->code}} dropdown-toggle selector{{$order->id}}"
               data-toggle="dropdown">{{collect($statuses)->where('id', $order->status_id)->first()->name}}
                <span class="caret"></span></a>
            <ul class="dropdown-menu dropdown-menu-right menu{{$order->id}}">
                @foreach($statuses as $status)
                    <li>
                        <a onclick="changeOrderStatus({{$order->id}}, {{$status->id}}, $('.selector{{$order->id}}'));"><span
                                    class="status-mark position-left border-{{$status->code}}"></span> {{$status->name}}
                        </a></li>
                @endforeach
            </ul>
        </div>
    </td>
    <td class="text-center">
        <form method="post"
              action="{!! url('admin/orders/'.$order->id) !!}">
            {!! csrf_field() !!}
            {!! method_field('DELETE') !!}
            <button class="btn btn-danger btn-xs pull-right" type="submit"
                    onclick="return confirm('Вы уверены, что хотите удалить заказ?')"><i
                        class="icon-x" data-popup="tooltip" title="Удалить"></i>
            </button>
        </form>
        <a class="btn btn-xs bg-slate pull-right"
           href="{!! route('admin.orders.show', [$order->id]) !!}"><i
                    class="icon-eye" data-popup="tooltip" title="Просмотр"></i></a>
    </td>
</tr>