@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @include('admin.parts.orders.common')
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-1">
                    <form action="{!! url('admin.orders.delete', [$order->id]) !!}" method="post" id="delete">
                        {!! method_field('DELETE') !!}
                        {!! csrf_field() !!}
                    </form>
                    <div class="btn-group">
                        <a class="label bg-{{collect($statuses)->where('id', $order->status_id)->first()->code}} dropdown-toggle selector{{$order->id}}"
                           data-toggle="dropdown">{{collect($statuses)->where('id', $order->status_id)->first()->name}}
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right menu{{$order->id}}">
                            @foreach($statuses as $status)
                                <li>
                                    <a onclick="changeOrderStatus({{$order->id}}, {{$status->id}}, $('.selector{{$order->id}}'));"><span
                                                class="status-mark position-left border-{{$status->code}}"></span> {{$status->name}}
                                    </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-11">
                    <h3 class="panel-title">Заказ #{{$order->id}} на сумму {{$order->total}} ({{$order->date}})</h3>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @include('admin.parts.orders.tabs')
            </div>
        </div>
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ url('admin/' . request()->segment(2)) }}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-danger-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Удалить"
               onclick="if(confirm('Уверены, что хотите удалить доставку?')){$('#delete').submit();}">
                <i class="fab-icon-open icon-cross2"></i>
            </a>
        </li>
    </ul>
@endsection