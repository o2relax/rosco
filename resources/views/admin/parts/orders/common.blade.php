<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
<script>
    function changeOrderStatus(id, status, label) {
        $.ajax({
            url: '/admin/orders/' + id + '/status',
            method: 'post',
            data: 'status_id=' + status,
            dataType: 'json',
            success: function (response) {
                var cl = label.attr('class');
                label.attr('class', cl.replace(/(bg+)/g, ''));
                label.addClass('bg-' + response.code);
                label.html(response.name + ' <span class="caret"></span>');
            }
        });
        return false;
    }

    $(function () {

    });
</script>