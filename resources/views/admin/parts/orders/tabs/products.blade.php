<table class="table table-striped">
    <thead>
    <tr>
        <th class="text-center">ID</th>
        <th class="text-center">Картинка</th>
        <th>Артикул</th>
        <th>Название</th>
        <th>Категория</th>
        <th>Производитель</th>
        <th>Цена</th>
        <th class="text-center">Кол-во в заказе</th>
    </tr>
    </thead>
    <tbody>
    @if(count($order->items))
        @foreach($order->items as $product)
            @if($product->product)
            <tr>
                <td class="text-center">
                    <small class="display-block text-size-small no-margin">{{$product->id}}</small>
                </td>
                <td class="text-center">
                    <div class="thumbnail" style="max-height: 50px; max-width: 50px; overflow: hidden">
                        <div class="thumb">
                            <img src="{{$product->product->image}}" alt="{!! $product->product->name !!}"
                                 style="max-height: 100px; max-width: 100px">
                            <div class="caption-overflow">
										<span>
											<a href="{{$product->product->image}}" data-popup="lightbox"
                                               rel="gallery"
                                               class="btn border-white text-white btn-flat btn-icon btn-rounded"><i
                                                        class="icon-plus3"></i></a>
										</span>
                            </div>
                        </div>
                    </div>
                </td>
                <td><span class="label label-default">{!! $product->product->article !!}</span></td>
                <td>
                    <a href="{{route('admin.products.edit', [$product->product->id])}}">{!! $product->product->name !!}</a>
                </td>
                <td>
                    {{$product->product->category ? $product->product->category->name : ''}}
                </td>
                <td>
                    {{$product->product->manufacturer ? $product->product->manufacturer->name : ''}}
                </td>
                <td>
                    {{currency()->order($product->price, $order->currency)}}
                </td>
                <td class="text-center">
                    {{$product->quantity}}
                </td>
            </tr>
            @else
                <tr class="text-danger">
                    <td class="text-center">
                        <small class="display-block text-size-small no-margin">{{$product->id}}</small>
                    </td>
                    <td colspan="5">
                        Удален
                    </td>
                    <td>
                        {{currency()->order($product->price, $order->currency)}}
                    </td>
                    <td class="text-center">
                        {{$product->quantity}}
                    </td>
                </tr>
            @endif
        @endforeach
    @endif
    </tbody>
</table>