<div class="table-responsive">
    <table class="table table-bordered table-lg">
        <tbody>
        <tr>
            <td class="col-md-2 text-right"><strong>Имя</strong></td>
            <td>{{$order->name}}</td>
        </tr>
        <tr>
            <td class="col-md-2 text-right"><strong>Почта для связи</strong></td>
            <td>{{$order->email}}</td>
        </tr>
        <tr>
            <td class="col-md-2 text-right"><strong>Телефон для связи</strong></td>
            <td>{{$order->phone}}</td>
        </tr>
        <tr>
            <td class="col-md-2 text-right"><strong>Адрес доставки</strong></td>
            <td>{{$order->address}}</td>
        </tr>
        <tr class="border-double active">
            <th colspan="2">Доставка и оплата</th>
        </tr>
        <tr>
            <td class="col-md-2 text-right"><strong>Способ доставки</strong></td>
            <td>{{$order->shipment->name}}</td>
        </tr>
        <tr>
            <td class="col-md-2 text-right"><strong>Способ оплаты</strong></td>
            <td>{{isset($order->payment->name) ? $order->payment->name : ""}}</td>
        </tr>
        <tr>
            <td class="col-md-2 text-right"><strong>Валюта заказа</strong></td>
            <td>{{$order->currency->name}}</td>
        </tr>
        </tbody>
    </table>
</div>