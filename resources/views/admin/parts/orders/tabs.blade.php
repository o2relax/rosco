<ul class="nav nav-tabs nav-tabs-top nav-justified">
    <li class="active"><a href="#main" data-toggle="tab" class="main-tab">Основная информация</a></li>
    <li class=""><a href="#products" data-toggle="tab" class="main-tab">Товары</a></li>
</ul>
<div class="tab-content">
    <div class="main-pane tab-pane active" id="main" >
        @include('admin.parts.orders.tabs.main')
    </div>
    <div class="main-pane tab-pane" id="products">
        @include('admin.parts.orders.tabs.products')
    </div>
</div>