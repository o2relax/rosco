@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }

        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }
        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/reviews/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Добавить отзыв</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список отзывов<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком отзывов</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">#</th>
                    <th>Автор</th>
                    <th>Товар</th>
                    <th class="text-center">Статус</th>
                    <th class="text-center">Дата</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($reviews))
                    @foreach($reviews as $review)
                        <tr>
                            <td class="text-center">
                                @if($review->viewed)
                                    <span class="label label-flat border-slate-300 text-slate-300 position-right">Просмотрен</span>
                                @else
                                    <span class="label label-flat border-success text-success-600 position-right">Новый</span>
                                @endif
                            </td>
                            <td>
                                @if($review->user)
                                    <a
                                       href="{{route('admin.customers.show', [$review->user_id])}}"
                                       target="_blank">{{$review->user->name}}</a>
                                @else
                                    <span class="text-grey">{{$review->name}}</span>
                                @endif
                            </td>
                            <td>
                                @if($review->product)
                                    <a
                                       href="{{route('admin.products.edit', [$review->product_id])}}"
                                       target="_blank">{{$review->product->name}}</a>
                                @else
                                    <span class="text-danger">- #{{$review->product_id}}</span>
                                @endif
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery"
                                               @if($review->is_active) checked="checked"
                                               @endif data-url="{!! route('admin.reviews.active', [$review->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                {{$review->date}}
                            </td>
                            <td class="text-center">
                                <form method="post" action="{!! url('admin/reviews/'.$review->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить отзыв?')"><i
                                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-default btn-xs pull-right btn-xs"
                                   href="{{url('admin/reviews/'.$review->id.'/edit')}}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                                @if($review->product)
                                    <a class="btn bg-teal btn-xs pull-right btn-xs"
                                       href="{{route('shop.product', [$review->product->slug])}}"><i
                                                class="icon-eye" data-popup="tooltip" title="Просмотр"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection