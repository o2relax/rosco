@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @include('admin.parts.reviews.common')
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 text-right">
                    <form action="{!! url('admin/reviews/'.$review->id) !!}" method="post" id="delete">
                        {!! method_field('DELETE') !!}
                        {!! csrf_field() !!}
                    </form>
                </div>
            </div>
            <h3 class="panel-title">Отзыв от {{$review->name}} к
                товару {{$review->product ? $review->product->name : $review->product_id}}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                {!! Form::model($review, [
                'route' => ['admin.reviews.update', $review->id],
                'id' => 'form',
                'class' => 'form-horizontal',
                'method' => 'patch',
                'enctype' => 'multipart/form-data'
                ]) !!}
                <input type="hidden" name="id" value="{{$review->id}}">
                <fieldset class="content-group">
                    <div class="form-group @if($errors->has('name'))has-error has-feedback @endif">
                        @input_maker_label('Имя', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            @input_maker_create('name', ['type' => 'string'], $review)
                            @if($errors->has('name'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('text'))has-error has-feedback @endif">
                        @input_maker_label('Текст', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            @input_maker_create('text', ['type' => 'text', 'custom' => 'style = "height: 200px"'],
                            $review)
                            @if($errors->has('text'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('text')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('created_at'))has-error has-feedback @endif">
                        @input_maker_label('Дата публикации', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            @input_maker_create('created_at', ['type' => 'string', 'class' => 'daterange'], $review)
                            @if($errors->has('created_at'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('created_at')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        @input_maker_label('Отображается', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                                <label>
                                    Выкл
                                    <input type="checkbox" name="is_published" value="1"
                                           class="switchery" {{old_input('is_active', $review) ? 'checked' : ''}}>
                                    Вкл
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ url('admin/' . request()->segment(2)) }}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-danger-400 btn-float btn-rounded btn-icon" data-popup="tooltip"
               title="Удалить"
               onclick="if(confirm('Уверены, что хотите удалить отзыв?')){$('#delete').submit();}">
                <i class="fab-icon-open icon-cross2"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip"
               title="Сохранить"
               onclick="submitForm($(this));">
                <i class="fab-icon-open icon-floppy-disk"></i>
            </a>
        </li>
    </ul>
@endsection