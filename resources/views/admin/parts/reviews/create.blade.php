@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @include('admin.parts.reviews.common')
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">Создание оплаты</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                {{ Form::model($review,
                [
                'route' => ['admin.reviews.store'],
                'id' => 'form',
                'class' => 'form-horizontal',
                 'method' => 'post',
                 'enctype' => 'multipart/form-data'
                 ]) }}
                <fieldset class="content-group">
                    <div class="form-group @if($errors->has('product_id'))has-error has-feedback @endif">
                        @input_maker_label('Товар', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            <select name="product_id" class="form-control select" data-placeholder="Выберите товар...">
                                <option></option>
                                <optgroup label="Товары">
                                    @foreach($products as $product)
                                        <option value="{{$product->id}}">{{$product->name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                            @if($errors->has('product_id'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('product_id')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('user_id'))has-error has-feedback @endif">
                        @input_maker_label('Пользователь', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            <select name="user_id" class="form-control select">
                                <option value="0">Не зарегистрирован</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('user_id'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('user_id')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('name'))has-error has-feedback @endif">
                        @input_maker_label('Имя', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            @input_maker_create('name', ['type' => 'string'], $review)
                            @if($errors->has('name'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('text'))has-error has-feedback @endif">
                        @input_maker_label('Текст', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            @input_maker_create('text', ['type' => 'text', 'custom' => 'style = "height: 200px"'],
                            $review)
                            @if($errors->has('text'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('text')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('created_at'))has-error has-feedback @endif">
                        @input_maker_label('Дата публикации', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            <input id="Created_at" type="text" name="created_at"
                                   class="form-control daterange" value="{{old_input('created_at', $review) ? $review->created_at : date('Y-m-d H:i:s')}}">
                            @if($errors->has('created_at'))
                                <div class="form-control-feedback">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                <span class="help-block">{{$errors->first('created_at')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        @input_maker_label('Активность', ['class' => 'control-label col-lg-2'])
                        <div class="col-md-4">
                            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                                <label>
                                    Выкл
                                    <input type="checkbox" name="is_published" value="1"
                                           class="switchery" {{old_input('is_active', $review) ? 'checked' : ''}}>
                                    Вкл
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ url('admin/' . request()->segment(2)) }}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip"
               title="Сохранить"
               onclick="submitForm($(this));">
                <i class="fab-icon-open icon-floppy-disk"></i>
            </a>
        </li>
    </ul>
@endsection