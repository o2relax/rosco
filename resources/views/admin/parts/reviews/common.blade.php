<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/notifications/jgrowl.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/moment/moment.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/daterangepicker.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/anytime.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/picker.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/picker.date.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/picker.time.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/legacy.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/core/libraries/jquery_ui/interactions.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/selects/select2.min.js') !!}"></script>
<script>

    function submitForm(btn) {
        $('#form').submit();
    }

    $(function () {

        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');
            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }

        $('.daterange').AnyTime_picker({
            format: "%Y-%m-%d %H:%i:%s",
        });

        $('.select').select2({
            minimumResultsForSearch: Infinity
        });

    });

</script>