@extends('admin.layouts.dashboard')

@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a class="btn btn-success" href="{{ url('admin/users/invite') }}">Добавить сотрудника</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Список сотрудников<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h5>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Профиль</th>
                    <th class="text-right"></th>
                </tr>
                <form method="get" action="{{url('admin/users/search')}}">
                    {!! csrf_field() !!}
                    <tr>
                        <th><input class="form-control" name="search[id]" placeholder="Поиск по ID"
                                   value="{{request()->get('search')['id']}}"></th>
                        <th><input class="form-control" name="search[email]" placeholder="Поиск по email"
                                   value="{{request()->get('search')['email']}}"></th>
                        <th><input class="form-control" name="search[name]" placeholder="Поиск по имени"
                                   value="{{request()->get('search')['name']}}"></th>
                        <th><input class="form-control" name="search[surname]" placeholder="Поиск по фамилии"
                                   value="{{request()->get('search')['surname']}}"></th>
                        <th><input class="form-control" name="search[roles]" placeholder="Поиск по профилю"
                                   value="{{request()->get('search')['roles']}}"></th>
                        <th class="text-right"><input class="btn btn-primary" type="submit" name="submit" value="поиск">
                            <a class="btn btn-default" href="{{url('admin/users')}}" data-popup="tooltip" data-popup="tooltip" title="Очистить поиск"><i class="icon-eraser2"></i></a></th>
                    </tr>
                </form>
                </thead>
                <tbody>
                @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->surname }}</td>
                            <td><span class="label label-{{$user->roles->first()->style}}">{{ $user->roles->first()->name }}</span></td>
                            <td>
                                <form method="post" action="{!! url('admin/users/'.$user->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить сотрудника?')"><i class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-default btn-xs pull-right raw-margin-right-16"
                                   href="{{ url('admin/users/'.$user->id.'/edit') }}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
