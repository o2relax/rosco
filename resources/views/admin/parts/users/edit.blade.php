@extends('admin.layouts.dashboard')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Редактирование сотрудника<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a></h5>
        </div>
        <div class="panel-body">

            <form id="form" class="form-horizontal" method="POST" action="{{url('admin/users/' . $user->id)}}">
                <input name="_method" type="hidden" value="PATCH">
                {!! csrf_field() !!}
                <fieldset class="content-group">
                    <div class="form-group">
                        @input_maker_label('Email', ['class' => 'control-label col-lg-2'])
                        <div class="col-lg-10">
                            @input_maker_create('email', ['type' => 'string'], $user)
                        </div>
                    </div>
                    <div class="form-group">
                        @input_maker_label('Имя', ['class' => 'control-label col-lg-2'])
                        <div class="col-lg-10">
                            @input_maker_create('name', ['type' => 'string'], $user)
                        </div>
                    </div>
                    <div class="form-group">
                        @input_maker_label('Фамилия', ['class' => 'control-label col-lg-2'])
                        <div class="col-lg-10">
                            @input_maker_create('surname', ['type' => 'string'], $user)
                        </div>
                    </div>
                    <div class="form-group">
                        @input_maker_label('Телефон', ['class' => 'control-label col-lg-2'])
                        <div class="col-lg-10">
                            @input_maker_create('meta[phone]', ['type' => 'string'], $user)
                        </div>
                    </div>
                    <div class="form-group">
                        @input_maker_label('Профайл', ['class' => 'control-label col-lg-2'])
                        <div class="col-lg-10">
                            @input_maker_create('roles', ['type' => 'relationship', 'model' => 'App\Models\Role', 'label' => 'label', 'value' => 'name'], $user)
                        </div>
                    </div>
                </fieldset>
                <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                    <li style="display: inline">
                        <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
                           href="{{ url('admin/' . request()->segment(2)) }}">
                            <i class="fab-icon-open icon-arrow-left8"></i>
                        </a>
                    </li>
                    <li style="display: inline">
                        <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Сохранить"
                           onclick="$('#form').submit();">
                            <i class="fab-icon-open icon-floppy-disk"></i>
                        </a>
                    </li>
                </ul>
        </form>
    </div>

@stop