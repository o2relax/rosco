<fieldset class="content-group">
    <div class="form-group @if($errors->has('currency_id'))has-error has-feedback @endif">
        @input_maker_label('Валюта', ['class' => 'control-label col-lg-2'])
        <div class="col-md-6">
            <select class="form-control" name="currency_id">
                @if(count($currencies))
                    @foreach($currencies as $currency)
                        <option value="{{$currency->id}}" {{$currency->id==old_input('currency_id', $payment) ? 'selected' : ''}}>{{$currency->name}}</option>
                    @endforeach
                @endif
            </select>
            @if($errors->has('currency_id'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('currency_id')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('code'))has-error has-feedback @endif">
        @input_maker_label('Код', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('code', ['type' => 'string'], $payment)
            @if($errors->has('code'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('code')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        @input_maker_label('Отображается', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="is_active" value="1"
                           class="switchery" {{old_input('is_active', $payment) ? 'checked' : ''}}>
                    Вкл
                </label>
            </div>
        </div>
    </div>
    <fieldset class="content-group">
        <legend class="text-bold">Основное изображение</legend>

        <div class="form-group col-xs-12">
            <input type="file" name="image" class="file-input-preview2" data-show-remove="true">
        </div>
    </fieldset>
</fieldset>