@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }

        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }

            $('[data-popup="lightbox"]').fancybox({
                padding: 3
            });

        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/payments/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Добавить способ оплаты</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список способов оплаты<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком оплат</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">#</th>
                    <th class="text-center">Избражение</th>
                    <th>Название</th>
                    <th>Валюта</th>
                    <th>Код</th>
                    <th class="text-center">Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($payments))
                    @foreach($payments as $payment)
                        <tr>
                            <td class="text-center">
                                <h6 class="no-margin">
                                    <small class="display-block text-size-small no-margin">{{$payment->id}}</small>
                                </h6>
                            </td>
                            <td class="text-center">
                                <div class="thumbnail"
                                     style="max-height: 50px; max-width: 50px; overflow: hidden; margin: 0 auto;">
                                    <div class="thumb">
                                        <img src="{{$payment->image}}" alt="{!! $payment->name !!}"
                                             style="max-height: 50px; max-width: 50px">
                                        <div class="caption-overflow">
										<span>
											<a href="{!! $payment->image !!}" data-popup="lightbox" rel="gallery"
                                               class="btn border-white text-white btn-flat btn-icon btn-rounded"><i
                                                        class="icon-plus3"></i></a>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="{{url('admin/payments/'.$payment->id.'/edit')}}"
                                   class="display-inline-block text-default text-semibold letter-icon-title">{{$payment->name}}</a>
                            </td>
                            <td>
                                {{$payment->currency->name}} <code>{{$payment->currency->code}}</code>
                            </td>
                            <td>
                                <a class="btn btn-link"><code>{{$payment->code}}</code></a>
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery"
                                               @if($payment->is_active) checked="checked"
                                               @endif data-url="{!! route('admin.payments.active', [$payment->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                <form method="post" action="{!! url('admin/payments/'.$payment->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить производителя?')"><i
                                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-default btn-xs pull-right btn-xs"
                                   href="{{url('admin/payments/'.$payment->id.'/edit')}}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection