<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/notifications/jgrowl.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/moment/moment.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/daterangepicker.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/anytime.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/picker.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/picker.date.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/picker.time.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/pickers/pickadate/legacy.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/notifications/pnotify.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>
<script>

    function submitForm(btn) {
        $('#form').submit();
    }

    $(function () {

        $(".switch").bootstrapSwitch();

        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');
            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }

        $('.daterange-locale').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default',
            opens: "left",
            ranges: {
                'Сегодня': [moment(), moment()],
                '1 день': [moment(), moment().add(1, 'days')],
                'Неделя': [moment(), moment().add(7, 'days')],
                'Месяц': [moment(), moment().add(1, 'month')],
                'Этот месяц': [moment().startOf('month'), moment().endOf('month')]
            },
            locale: {
                applyLabel: 'Вперед',
                cancelLabel: 'Отмена',
                startLabel: 'Начальная дата',
                endLabel: 'Конечная дата',
                customRangeLabel: 'Выбрать дату',
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт','Сб'],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                firstDay: 1,
                format: 'YYYY-MM-DD'
            }
        });

        $('.multiselect').multiselect({
            onChange: function() {
                $.uniform.update();
            }
        });

        $(".styled, .multiselect-container input").uniform({ radioClass: 'choice'});

    });

</script>