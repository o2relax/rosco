@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }

        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }

        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/discounts/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Добавить скидку</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список скидок<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте скидок</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">#</th>
                    <th>Название</th>
                    <th class="text-center">Значение</th>
                    <th>Период</th>
                    <th class="text-center">Задействована</th>
                    <th class="text-center">Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($discounts))
                    @foreach($discounts as $discount)
                        <tr>
                            <td class="text-center">
                                <h6 class="no-margin">
                                    <small class="display-block text-size-small no-margin">{{$discount->id}}</small>
                                </h6>
                            </td>
                            <td>
                                <a href="{{route('admin.discounts.edit', [$discount->id])}}"
                                   class="display-inline-block text-default text-semibold letter-icon-title">{{$discount->name}}</a>
                            </td>
                            <td class="text-center">
                                <span class="label label-success">-{{$discount->value . $discount->type}}</span>
                            </td>
                            <td>
                                <strong>
                                    <i class="icon-calendar"></i>
                                    {{$discount->date_start ? $discount->date_start : 'не указан'}}
                                    <i class="icon-minus3"></i>
                                    {{$discount->date_end ? $discount->date_end : 'бесконечно'}}
                                </strong>
                            </td>
                            <td class="text-center">
                                {{$discount->items->count()}}
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery"
                                               @if($discount->is_active) checked="checked"
                                               @endif data-url="{!! route('admin.discounts.active', [$discount->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                <form method="post" action="{!! url('admin/discounts/'.$discount->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить скидку?')"><i
                                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-default btn-xs pull-right btn-xs"
                                   href="{{route('admin.discounts.edit', [$discount->id])}}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection