@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @include('admin.parts.discounts.common')
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 text-right">
                    <form action="{!! url('admin.discounts.delete', [$discount->id]) !!}" method="post" id="delete">
                        {!! method_field('DELETE') !!}
                        {!! csrf_field() !!}
                    </form>
                </div>
            </div>
            <h3 class="panel-title">{{$discount->name}}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                {!! Form::model($discount, [
                'route' => ['admin.discounts.update', $discount->id],
                'id' => 'form',
                'class' => 'form-horizontal',
                'method' => 'patch',
                'enctype' => 'multipart/form-data'
                ]) !!}
                <input type="hidden" name="id" value="{{$discount->id}}">
                @include('admin.parts.discounts.tabs')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ url('admin/' . request()->segment(2)) }}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-danger-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Удалить"
               onclick="if(confirm('Уверены, что хотите удалить доставку?')){$('#delete').submit();}">
                <i class="fab-icon-open icon-cross2"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Сохранить"
               onclick="submitForm($(this));">
                <i class="fab-icon-open icon-floppy-disk"></i>
            </a>
        </li>
    </ul>
@endsection