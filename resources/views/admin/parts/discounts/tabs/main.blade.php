<fieldset class="content-group">
    <div class="form-group @if($errors->has('name'))has-error has-feedback @endif">
        @input_maker_label('Название', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('name', ['type' => 'string'], $discount)
            @if($errors->has('name'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('name')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('value'))has-error has-feedback @endif">
        @input_maker_label('Значение', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('value', ['type' => 'number'], $discount)
            @if($errors->has('value'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('value')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('type'))has-error has-feedback @endif">
        @input_maker_label('Тип', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <input name="type" type="checkbox" data-on-color="success" data-off-color="default" data-on-text="Процент"
                   data-off-text="Значение" class="switch"
                   @if(isset($discount->type ))
                   {!! $discount->type == '%' ? 'checked="checked"' : '' !!}
                   @endif
                   value="%">
            @if($errors->has('type'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('type')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('value'))has-error has-feedback @endif">
        <div class="form-group @if($errors->has('currency_id'))has-error has-feedback @endif">
            @input_maker_label('Производители', ['class' => 'control-label col-lg-2'])
            <div class="input-group col-md-4">
                <span class="input-group-addon"><i class="icon-cabinet"></i></span>
                <div class="multi-select-full">
                    <select class="multiselect" multiple="multiple" name="manufacturers[]">
                        @if(isset($manufacturers))
                            @foreach($manufacturers as $manufacturer)
                                <option value="{{$manufacturer->id}}"
                                        @if(isset($chosen_manufacturers))
                                        {{in_array($manufacturer->id, $chosen_manufacturers) ? 'selected' : ''}}>
                                        @else
                                        {{$manufacturer->id}}>
                                        @endif
                                    {{$manufacturer->name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
                @if($errors->has('currency_id'))
                    <div class="form-control-feedback">
                        <i class="icon-cancel-circle2"></i>
                    </div>
                    <span class="help-block">{{$errors->first('currency_id')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group @if($errors->has('value'))has-error has-feedback @endif">
        <div class="form-group @if($errors->has('currency_id'))has-error has-feedback @endif">
            @input_maker_label('Товары', ['class' => 'control-label col-lg-2'])
            <div class="input-group col-md-4">
                <span class="input-group-addon"><i class="icon-database"></i></span>
                <div class="multi-select-full">
                    <select class="multiselect" multiple="multiple" name="products[]">
                        @if(count($products))
                            @foreach($products as $product)
                                <option value="{{$product->id}}"
                                        @if(isset($chosen_products))
                                        {{in_array($product->id, $chosen_products) ? 'selected' : ''}}>
                                    @else
                                        {{$product->id}}>
                                    @endif
                                    {{$product->name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
                @if($errors->has('currency_id'))
                    <div class="form-control-feedback">
                        <i class="icon-cancel-circle2"></i>
                    </div>
                    <span class="help-block">{{$errors->first('currency_id')}}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group @if($errors->has('value'))has-error has-feedback @endif">
        @input_maker_label('Период', ['class' => 'control-label col-lg-2'])
        <div class="input-group col-md-4">

            <input type="text" class="form-control daterange-locale" name="period"
                   @if(isset($discount->date_start)&&isset($discount->date_end))
                   value="{{$discount->date_start}} - {{$discount->date_end}}">
                   @else
                           value="0">
                    @endif
            <span class="input-group-addon"><i class="icon-calendar22"></i></span>

                @if($errors->has('value'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('value')}}</span>
            @endif
        </div>
    </div>

    <div class="form-group">
        @input_maker_label('Отображается', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="is_active" value="1"
                           class="switchery" {{old_input('is_active', $discount) ? 'checked' : ''}}>
                    Вкл
                </label>
            </div>
        </div>
    </div>
</fieldset>