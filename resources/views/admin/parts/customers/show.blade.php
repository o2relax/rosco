@extends('admin.layouts.dashboard')

@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
    <script>
        function changeOrderStatus(id, status, label) {
            $.ajax({
                url: '/admin/orders/' + id + '/status',
                method: 'post',
                data: 'status_id=' + status,
                dataType: 'json',
                success: function (response) {
                    var cl = label.attr('class');
                    label.attr('class', cl.replace(/(bg+)/g, ''));
                    label.addClass('bg-' + response.code);
                    label.html(response.name + ' <span class="caret"></span>');
                }
            });
            return false;
        }
    </script>
@endsection

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Профиль {{$customer->name}} {{$customer->surname}} ({{$customer->email}})<a
                        class="heading-elements-toggle"><i
                            class="icon-more"></i></a></h5>
        </div>
        <div class="panel-body">
            <table class="table">
                <tbody>
                <tr>
                    <td>Email</td>
                    <td>{{$customer->email}}</td>
                </tr>
                <tr>
                    <td>Имя</td>
                    <td>{{$customer->name}}</td>
                </tr>
                <tr>
                    <td>Фамилия</td>
                    <td>{{$customer->surname}}</td>
                </tr>
                <tr>
                    <td>Телефон</td>
                    <td>{{$customer->meta->phone}}</td>
                </tr>
                </tbody>
            </table>
            <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                <li style="display: inline">
                    <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip"
                       title="Назад"
                       href="{{ url('admin/' . request()->segment(2)) }}">
                        <i class="fab-icon-open icon-arrow-left8"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Список заказов
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="col-md-1"></th>
                    <th class="text-center">ID</th>
                    <th>Данные</th>
                    <th>Товары</th>
                    <th>Сумма</th>
                    <th class="text-center">Статус</th>
                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @if(count($orders))
                    @foreach($orders as $order)
                        @include('admin.parts.orders.item')
                    @endforeach
                </tbody>
            </table>
            @else
            </tbody>
            </table>
            <br>
            <div class="col-md-12 text-center">Заказов не найдено</div>
            @endif

        </div>
        @if(count($orders))
            <div class="col-xs-12 text-center">
                {!! $orders->links() !!}
            </div>
        @endif
    </div>

@stop