@extends('admin.layouts.dashboard')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Список покупателей<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h5>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th>Email</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th class="text-center">Кол-во заказов</th>
                    <th class="text-right"></th>
                </tr>
                <form method="get" action="{{url('admin/customers/search')}}">
                    {!! csrf_field() !!}
                    <tr>
                        <th></th>
                        <th><input class="form-control" name="search[email]" placeholder="Поиск по email"
                                   value="{{request()->get('search')['email']}}"></th>
                        <th><input class="form-control" name="search[name]" placeholder="Поиск по имени"
                                   value="{{request()->get('search')['name']}}"></th>
                        <th><input class="form-control" name="search[surname]" placeholder="Поиск по фамилии"
                                   value="{{request()->get('search')['surname']}}"></th>
                        <th></th>
                        <th class="text-right"><input class="btn btn-primary" type="submit" name="submit" value="поиск">
                            <a class="btn btn-default" href="{{url('admin/customers')}}" data-popup="tooltip" data-popup="tooltip" title="Очистить поиск"><i class="icon-eraser2"></i></a></th>
                    </tr>
                </form>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                        <tr>
                            <td class="text-center">
                                <div class="media-center media-middle">
                                    <a href="{{route('admin.customers.show', [$customer->id])}}">
                                        <img src="{{ Gravatar::src($customer->email) }}" class="img-circle img-xs" alt="{{ $customer->name }}">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <a href="{{route('admin.customers.show', [$customer->id])}}">{{ $customer->email }}</a>
                            </td>
                            <td>{{ $customer->name }}</td>
                            <td>{{ $customer->surname }}</td>
                            <td class="text-center">{{ $customer->orders->count() }}</td>
                            <td>
                                <form method="post" action="{!! url('admin/customers/'.$customer->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить покупателя?')"><i class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn bg-slate btn-xs pull-right raw-margin-right-16"
                                   href="{{route('admin.customers.show', [$customer->id])}}"><i
                                            class="icon-eye" data-popup="tooltip" title="Просмотр"></i></a>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
