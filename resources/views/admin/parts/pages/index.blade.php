@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }
        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }
        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/pages/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Добавить страницу</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список страниц<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком страниц</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">#</th>
                    <th>Название</th>
                    <th>Ссылка</th>
                    <th>Шаблон</th>
                    <th class="text-center">Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($pages))
                    @foreach($pages as $page)
                        <tr>
                            <td class="text-center">
                                <h6 class="no-margin">
                                    <small class="display-block text-size-small no-margin">{{$page->id}}</small>
                                </h6>
                            </td>
                            <td>
                                <a href="{{url('admin/pages/'.$page->id.'/edit')}}"
                                   class="display-inline-block text-default text-semibold letter-icon-title">{{$page->title}}</a>
                            </td>
                            <td>
                                <a class="btn btn-link" href="{{route('shop.page', [$page->slug])}}" target="_blank">{{$page->slug}}</a>
                            </td>
                            <td>
                                <a class="label label-primary">{{$page->template ? $page->template : 'default'}}</a>
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery"
                                               @if($page->is_active) checked="checked"
                                               @endif data-url="{!! route('admin.pages.active', [$page->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                <form method="post" action="{!! url('admin/pages/'.$page->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить страницу?')"><i
                                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-default btn-xs pull-right btn-xs"
                                   href="{{url('admin/pages/'.$page->id.'/edit')}}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                                <a class="btn bg-teal btn-xs pull-right btn-xs"
                                   href="{{route('shop.page', [$page->slug])}}"><i
                                            class="icon-eye" data-popup="tooltip" title="Просмотр"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection