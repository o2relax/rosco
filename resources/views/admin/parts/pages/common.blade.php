<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/editors/summernote/summernote.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>

<script>
    function uploadImage(image, input) {
        var data = new FormData();
        data.append("image", image);
        $.ajax({
            data: data,
            type: "POST",
            url: "{{route('admin.pages.upload', [isset($page_id) ? $page_id : $page->id])}}",
            cache: false,
            contentType: false,
            processData: false,
            success: function (url) {
                var image = $('<img>').attr('src', url);
                input.summernote("insertNode", image[0]);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function submitForm(btn) {
        $('#form').submit();
    }

    $(function () {
        $('.summernote').summernote({
            height: 600,
            callbacks: {
                onImageUpload: function (image) {
                    uploadImage(image[0], $(this));
                }
            }
        });
        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');
            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }
    });

</script>