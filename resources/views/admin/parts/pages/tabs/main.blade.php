<fieldset class="content-group">
    <div class="form-group @if($errors->has('slug'))has-error has-feedback @endif">
        @input_maker_label('Ссылка', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('slug', ['type' => 'string'], $page)
            @if($errors->has('slug'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('slug')}}</span>
            @endif
            <code>сгенерируется автоматически, если оставиь пустым</code>
        </div>
    </div>
    <div class="form-group @if($errors->has('template'))has-error has-feedback @endif">
        @input_maker_label('Шаблон', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('template', ['type' => 'string'], $page)
            @if($errors->has('template'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('template')}}</span>
            @endif
            <div class="label-block">
                <span class="label label-primary">resources/views/shop/pages/custom/{template}.blade.php</span>
            </div>
            <code>можно оставиь пустым</code>
        </div>
    </div>
    <div class="form-group">
        @input_maker_label('Активность', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="is_published" value="1"
                           class="switchery" {{old_input('is_active', $page) ? 'checked' : ''}}>
                    Вкл
                </label>
            </div>
        </div>
    </div>
</fieldset>