@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')

@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/attributes/groups/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Создать группу</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список групп<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком групп атрибутов</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Название</th>
                    <th class="text-center">Кол-во атрибутов</th>
                    <th class="text-center">Сортировка</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($groups as $group)
                    <tr>
                        <td class="text-center">
                            <h6 class="no-margin">
                                <small class="display-block text-size-small no-margin">{{$group->id}}</small>
                            </h6>
                        </td>
                        <td>
                            <a href="{{url('admin/attributes/groups/'.$group->id.'/edit')}}"
                               class="display-inline-block text-default text-semibold letter-icon-title">{{$group->name}}</a>
                        </td>
                        <td class="text-center">
                            <h6 class="no-margin">
                                <small class="display-block text-size-small no-margin">{{$group->attributes->count()}}</small>
                            </h6>
                        </td>
                        <td class="text-center">
                            {{$group->sort}}
                        </td>
                        <td class="text-center">
                            <form method="post" action="{!! url('admin/attributes/groups/'.$group->id) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button class="btn btn-danger btn-xs pull-right" type="submit"
                                        onclick="return confirm('Вы уверены, что хотите удалить группу со всеми атрибутами?')">
                                    <i
                                            class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                </button>
                            </form>
                            <a class="btn btn-default btn-xs pull-right btn-xs"
                               href="{{url('admin/attributes/groups/'.$group->id.'/edit')}}"><i
                                        class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection