@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')

@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/attributes/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Создать атрибут</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список атрибутов<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком атрибутов</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Группа</th>
                    <th>Название</th>
                    <th class="text-center">Сортировка</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($attributes as $attribute)
                    <tr>
                        <td class="text-center">
                            <h6 class="no-margin">
                                <small class="display-block text-size-small no-margin">{{$attribute->id}}</small>
                            </h6>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <small class="display-block text-size-small no-margin">{{$attribute->group->name}}</small>
                            </h6>
                        </td>
                        <td>
                            <a href="{{url('admin/attributes/'.$attribute->id.'/edit')}}"
                               class="display-inline-block text-default text-semibold letter-icon-title">{{$attribute->name}}</a>
                        </td>
                        <td class="text-center">
                            {{$attribute->sort}}
                        </td>
                        <td class="text-center">
                            <form method="post" action="{!! url('admin/attributes/'.$attribute->id) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button class="btn btn-danger btn-xs pull-right" type="submit"
                                        onclick="return confirm('Вы уверены, что хотите удалить группу со всеми атрибутами?')">
                                    <i
                                            class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                </button>
                            </form>
                            <a class="btn btn-default btn-xs pull-right btn-xs"
                               href="{{url('admin/attributes/'.$attribute->id.'/edit')}}"><i
                                        class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection