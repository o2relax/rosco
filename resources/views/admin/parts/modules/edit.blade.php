@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @parent
    <script>
        function submitForm(btn) {
            $('#form').submit();
        }
    </script>
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{$module->name}}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                {!! Form::model($module, [
                'route' => ['admin.modules.update', $module->code],
                'id' => 'form',
                'class' => 'form-horizontal',
                'method' => 'patch',
                'enctype' => 'multipart/form-data'
                ]) !!}
                @if(view()->exists('admin.parts.modules.custom.' . $module->code))
                    @include('admin.parts.modules.custom.' . $module->code)
                @else
                    @include('admin.parts.modules.common')
                @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ url('admin/' . request()->segment(2)) }}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip"
               title="Сохранить"
               onclick="submitForm($(this));">
                <i class="fab-icon-open icon-floppy-disk"></i>
            </a>
        </li>
    </ul>
@endsection