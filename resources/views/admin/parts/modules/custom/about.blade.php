<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
    @foreach(app('shared')->get('languages') as $language)
        <li class="{{$language->code == cfg('language', 'code') ? 'active' : ''}}">
            <a href="#language_{{$language->code}}" data-toggle="tab"><img src="{{$language->icon}}"
                                                                           alt=""> {{$language->name}}</a>
        </li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach(app('shared')->get('languages') as $language)
        <div class="tab-pane {{$language->code == cfg('language', 'code') ? 'active' : ''}}"
             id="language_{{$language->code}}">
            <fieldset class="content-group">
                <div class="form-group @if($errors->has($language->code . '.heading'))has-error has-feedback @endif">
                    @input_maker_label('Заголовок (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create('data[heading][' . $language->code . ']', ['type' => 'string'],
                        lang($module->data['heading'], $language->code))
                        @if($errors->has($language->code . '.heading'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.heading')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.text_left'))has-error has-feedback @endif">
                    @input_maker_label('Текст слева (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create('data[text_left][' . $language->code . ']', ['type' => 'text'],
                        isset($module->data['text_left']) ? lang($module->data['text_left'], $language->code) : null)
                        @if($errors->has($language->code . '.text_left'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.text_left')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.text_right'))has-error has-feedback @endif">
                    @input_maker_label('Текст справа (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create('data[text_right][' . $language->code . ']', ['type' => 'text'],
                        isset($module->data['text_right']) ? lang($module->data['text_left'], $language->code) : null)
                        @if($errors->has($language->code . '.text_right'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.text_right')}}</span>
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
    @endforeach
</div>