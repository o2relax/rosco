@section('javascript')
    @parent
    <script type="text/javascript" src="{{asset('back/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back/assets/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
    <script>
        var key = {{count($module->data)}};

        function addSlide() {
            $.get('/admin/modules/slider_bar/ajax/' + key, function (data) {
                $('.slider-container').append(data);
                key++;
            });
        }

        function deleteSlide() {

            if ($('.slider-container .content-group').length !== 1) {
                $('.slider-container .content-group:last-child').remove();
            }
        }

        var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
            '  <div class="modal-content">\n' +
            '    <div class="modal-header">\n' +
            '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
            '    </div>\n' +
            '    <div class="modal-body">\n' +
            '      <div class="floating-buttons btn-group"></div>\n' +
            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
            '    </div>\n' +
            '  </div>\n' +
            '</div>\n';

        // Buttons inside zoom modal
        var previewZoomButtonClasses = {
            toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
            fullscreen: 'btn btn-default btn-icon btn-xs',
            borderless: 'btn btn-default btn-icon btn-xs',
            close: 'btn btn-default btn-icon btn-xs'
        };

        // Icons inside zoom modal classes
        var previewZoomButtonIcons = {
            prev: '<i class="icon-arrow-left32"></i>',
            next: '<i class="icon-arrow-right32"></i>',
            toggleheader: '<i class="icon-menu-open"></i>',
            fullscreen: '<i class="icon-screen-full"></i>',
            borderless: '<i class="icon-alignment-unalign"></i>',
            close: '<i class="icon-cross3"></i>'
        };

        // File actions
        var fileActionSettings = {
            zoomClass: 'btn btn-link btn-xs btn-icon',
            zoomIcon: '<i class="icon-zoomin3"></i>',
            dragClass: 'btn btn-link btn-xs btn-icon',
            dragIcon: '<i class="icon-three-bars"></i>',
            removeClass: 'btn btn-link btn-icon btn-xs',
            removeIcon: '<i class="icon-trash"></i>',
            indicatorNew: '<i class="icon-file-plus text-slate"></i>',
            indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
            indicatorError: '<i class="icon-cross2 text-danger"></i>',
            indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
        };
    </script>
@stop

<div class="slider-container">
    @if(count($module->data))
        @foreach($module->data as $key => $data)
    @section('javascript')
        @parent
        <script>
            $(function () {
                $('.file-input-{{$key}}').fileinput({
                    showRemove: false,
                    browseLabel: 'Выбрать',
                    browseIcon: '<i class="icon-file-plus"></i>',
                    uploadIcon: '<i class="icon-file-upload2"></i>',
                    removeIcon: '<i class="icon-cross3"></i>',
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        modal: modalTemplate
                    },
                    initialPreview: [
                        @if(is_file(module_file($module->code, $data['file'], 'path')))
                            "{{module_file($module->code, $data['file'])}}",
                        @endif
                    ],
                    initialPreviewConfig: [
                        {
                            caption: "{{$data['file']}}",
                            url: '{{route('admin.modules.file.delete', [$module->code, $data['file']])}}',
                            showDrag: false
                        },
                    ],
                    initialPreviewAsData: true,
                    initialCaption: "No file selected",
                    previewZoomButtonClasses: previewZoomButtonClasses,
                    previewZoomButtonIcons: previewZoomButtonIcons,
                    fileActionSettings: fileActionSettings
                });
            });
        </script>
    @stop
    <fieldset class="content-group">
        <div class="form-group @if($errors->has('files'))has-error has-feedback @endif">
            @input_maker_label('Изображение ' . $key, ['class' => 'control-label col-lg-2'])
            <div class="col-md-10">
                @input_maker_create('data['.$key.'][file]', ['type' => 'hidden'],
                $module->data[$key]['file'])
                <input type="file" name="file[{{$key}}]" class="file-input-{{$key}}" data-show-caption="false"
                       data-show-upload="false">
                @if($errors->has('files'))
                    <div class="form-control-feedback">
                        <i class="icon-cancel-circle2"></i>
                    </div>
                    <span class="help-block">{{$errors->first('files')}}</span>
                @endif
            </div>
        </div>
    </fieldset>
    @endforeach
    @endif
</div>
<fieldset class="form-group">
    <div class="col-md-10">
        <a class="btn btn-success" onclick="addSlide()">Добавить слайд</a>
    </div>
</fieldset>
<fieldset class="form-group">
    <div class="col-md-10">
        <a class="btn btn-info" onclick="deleteSlide()">Удалить слайд</a>
    </div>
</fieldset>