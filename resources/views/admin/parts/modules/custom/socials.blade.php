<fieldset class="content-group">
    <div class="form-group @if($errors->has('data.twitter'))has-error has-feedback @endif">
        @input_maker_label('Twitter', ['class' => 'control-label col-lg-2'])
        <div class="col-md-6">
            @input_maker_create('data[twitter]', ['type' => 'string'],
            old_input_array('twitter', $module->data))
            @if($errors->has('data.twitter'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('data.twitter')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('data.facebook'))has-error has-feedback @endif">
        @input_maker_label('Facebook', ['class' => 'control-label col-lg-2'])
        <div class="col-md-6">
            @input_maker_create('data[facebook]', ['type' => 'string'],
            old_input_array('facebook', $module->data))
            @if($errors->has('data.facebook'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('data.facebook')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('data.vk'))has-error has-feedback @endif">
        @input_maker_label('VKontakte', ['class' => 'control-label col-lg-2'])
        <div class="col-md-6">
            @input_maker_create('data[vk]', ['type' => 'string'],
            old_input_array('vk', $module->data))
            @if($errors->has('data.vk'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('data.vk')}}</span>
            @endif
        </div>
    </div>
</fieldset>