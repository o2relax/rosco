@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }
        function changeCache(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'use_cache=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }
        function clearCache(url) {
            $.ajax({
                url: url,
                method: 'post',
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }
        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                    });
                });
            }



            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery-warning'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html, { color: '#FF7043' });
                    html.addEventListener('click', function () {
                        changeCache($(html).attr('data-url'), html.checked);
                    });
                });
            }
        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/pages/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Добавить страницу</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список страниц<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком страниц</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">#</th>
                    <th>Название</th>
                    <th>Код</th>
                    <th class="text-center">Кеширование</th>
                    <th class="text-center">Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($modules))
                    @foreach($modules as $module)
                        <tr>
                            <td class="text-center">
                                <i class="{{$module->icon}}"></i>
                            </td>
                            <td>
                                {{$module->name}}
                            </td>
                            <td>
                                <code>{{$module->code}}</code>
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery-warning"
                                               @if($module->use_cache) checked="checked"
                                               @endif data-url="{!! route('admin.modules.cache', [$module->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery"
                                               @if($module->is_active) checked="checked"
                                               @endif data-url="{!! route('admin.modules.active', [$module->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-left">
                                <a class="btn btn-default btn-xs btn-xs"
                                   href="{{route('admin.modules.config', [$module->code])}}"><i
                                            class="icon-gear" data-popup="tooltip" title="Настроить"></i></a>
                                <a class="btn bg-warning-300 btn-xs btn-xs"
                                   onclick="clearCache($(this).data('url'))" data-url="{{route('admin.modules.clear', [$module->code])}}"><i
                                            class="icon-eraser2" data-popup="tooltip" title="Очистить кеш"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 text-center pagination">
        {{$modules->links()}}
    </div>
@endsection