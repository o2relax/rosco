<tr>
    <td class="text-center">
        <h6 class="no-margin">
            <small class="display-block text-size-small no-margin">{{$category->id}}</small>
        </h6>
    </td>
    <td class="text-center">
        <div class="thumbnail"
             style="max-height: 50px; max-width: 50px; overflow: hidden; margin: 0;">
            <style>
                .thumb-{{$category->id}} {
                    fill: {{$category->color}};
                }
            </style>
            <div class="thumb thumb-{{$category->id}}">
                {{$category->image}}
            </div>
        </div>
    </td>
    <td>
        <a href="{{url('admin/categories/'.$category->id.'/edit')}}"
           class="display-inline-block text-default text-semibold letter-icon-title">{{$category->name}}</a>
    </td>
    <td class="text-center">
        <div class="checkbox checkbox-switchery switchery-sm">
            <label>
                <input type="checkbox" class="switchery"
                       @if($category->is_active) checked="checked"
                       @endif data-url="{!! route('admin.categories.status', [$category->id]) !!}">
            </label>
        </div>
    </td>
    <td class="text-center">
        {{$category->date}}
    </td>
    <td class="text-center">
        <form method="post" action="{!! url('admin/categories/'.$category->id) !!}">
            {!! csrf_field() !!}
            {!! method_field('DELETE') !!}
            <button class="btn btn-danger btn-xs pull-right" type="submit"
                    onclick="return confirm('Вы уверены, что хотите удалить категорию?')"><i
                        class="icon-x" data-popup="tooltip" title="Удалить"></i>
            </button>
        </form>
        <a class="btn btn-default btn-xs pull-right btn-xs"
           href="{{url('admin/categories/'.$category->id.'/edit')}}"><i
                    class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
        <a href="{{$category->link}}" target="_blank" class="btn bg-slate pull-right btn-xs"><i
                    class="icon icon-eye" data-popup="tooltip" title="Просмотр"></i></a>
    </td>
</tr>