<ul class="nav nav-tabs nav-tabs-top nav-justified">
    <li class="active"><a href="#main" data-toggle="tab" class="main-tab tab">Основная информация</a></li>
    <li class=""><a href="#description" data-toggle="tab" class="main-tab tab">Описание</a></li>
    {{--<li><a href="#store" data-toggle="tab" class="main-tabs tab">Склад</a></li>--}}
    <li class=""><a href="#images" data-toggle="tab" class="main-tab tab">Изображения</a></li>
    <li class=""><a href="#extend" data-toggle="tab" class="main-tab tab">Атрибуты</a></li>
</ul>
<div class="tab-content">
    <div class="main-pane tab-pane active" id="main" >
        @include('admin.parts.products.tabs.main')
        @include('admin.parts.products.tabs.store')
    </div>
    <div class="main-pane tab-pane" id="description">
        @include('admin.parts.products.tabs.description')
    </div>
    {{--<div class="tab-pane" id="store">--}}
        {{----}}
    {{--</div>--}}
    <div class="main-pane tab-pane" id="images">
        @include('admin.parts.products.tabs.images')
    </div>
    <div class="main-pane tab-pane" id="extend">
        @include('admin.parts.products.tabs.extend')
    </div>
</div>