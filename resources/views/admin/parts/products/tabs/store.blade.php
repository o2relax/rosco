<fieldset class="content-group">
    <div class="form-group">
        @input_maker_label('Статус', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <select class="form-control" name="status_id">
                @foreach($statuses as $status)
                    <option value="{{$status->id}}" {{$status->id==old('status_id', $product ? $product->status()->id : null) ? 'selected' : ''}}>{{$status->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group @if($errors->has('stock'))has-error has-feedback @endif">
        @input_maker_label('Остаток', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('stock', ['type' => 'string'], $product)
            @if($errors->has('stock'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('stock')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        @input_maker_label('Уменьшать кол-во', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="use_stock" value="1"
                           class="switchery" {{old_input('use_stock', $product) ? 'checked' : ''}}>
                    Вкл
                </label>
            </div>
        </div>
    </div>
</fieldset>