<fieldset class="content-group">
    @if($product)
        <div class="form-group">
            <div class="col-md-4">
                <select name="attribute" class="form-control">
                    <option value="">{{trans('options.admin.attributes')}}</option>
                    @foreach($attributes as $attr)
                        <option value="{{$attr->id}}">{{$attr->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <a class="btn bg-teal-400" onclick="addAttribute()"><i
                            class="icon-plus-circle2" data-popup="tooltip" title="{{trans('buttons.admin.product.add_attribute')}}"></i>
                </a>
            </div>
        </div>
        <div class="ajax-attributes">
            @include('admin.parts.products.ajax.attributes')
        </div>
    @else
        <div class="form-group text-center">
            <p>{{trans('messages.warnings.attributes_without_product')}}</p>

            <a class="btn btn-primary" onclick="submitForm($(this));">
                {{trans('buttons.admin.product.add_attribute')}}
            </a>
        </div>
    @endif
</fieldset>