<fieldset class="content-group">
    <div class="form-group @if($errors->has('parent_id'))has-error has-feedback @endif">
        @input_maker_label('Родительская категория', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="multi-select-full">
                <select class="form-control" name="category_id">
                    @foreach($categories as $cat)
                        <option value="{{$cat->id}}"
                                {{$cat->id == old('category_id', isset($product->category_id) ? $product->category_id : null) ? 'selected' : ''}}>
                            {{$cat->name}}
                        </option>
                    @endforeach
                </select>
            </div>
            @if($errors->has('parent_id'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('parent_id')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('parent_id'))has-error has-feedback @endif">
        @input_maker_label('Производитель', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="multi-select-full">
                <select class="form-control" name="manufacturer_id">
                    <option value="">Выберите</option>
                    @foreach($manufacturers as $manufacturer)
                        <option value="{{$manufacturer->id}}"
                                {{$manufacturer->id == old('manufacturer_id', isset($product->manufacturer_id) ? $product->manufacturer_id : null) ? 'selected' : ''}}>
                            {{$manufacturer->name}}
                        </option>
                    @endforeach
                </select>
            </div>
            @if($errors->has('parent_id'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('parent_id')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('article'))has-error has-feedback @endif">
        @input_maker_label('Артикул', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('article', ['type' => 'string'], $product)
            @if($errors->has('article'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('article')}}</span>
            @endif
        </div>
    </div>

    <div class="form-group @if($errors->has('price'))has-error has-feedback @endif">
        @input_maker_label('Цена', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('price', ['type' => 'string','class' => 'currency_input'], $product)
            @if($errors->has('price'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('price')}}</span>
            @endif
            <select class="form-control" style="width:28%;display:inline; " name="currency_id" required>
                <option value="">Выберите</option>
                @foreach($currencies as $currency)
                    <option value="{{$currency->id}}"
                            {{$currency->id == old('currency_id', isset($product->currency_id) ? $product->currency_id : null) ? 'selected' : ''}}>
                        {{$currency->name}}
                    </option>
                @endforeach
            </select>
        </div>
    </div>


    <div class="form-group @if($errors->has('substance'))has-error has-feedback @endif">
        @input_maker_label('Действующее вещество', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('substance', ['type' => 'string'], $product)
            @if($errors->has('substance'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('substance')}}</span>
            @endif
            <code>Слова вводите через запятую</code>
        </div>
    </div>


    <div class="form-group">
        @input_maker_label('Добавить этот товар в сопутствующие', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Снять
                    <input type="checkbox" name="is_related" value="1"
                           class="switchery" {{old_input('is_related', $product) ? 'checked' : ''}}>
                    Добавить
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        @input_maker_label('Добавить товар в новинки', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Снять
                    <input type="checkbox" name="is_latest" value="1"
                           class="switchery" {{old_input('is_latest', $product) ? 'checked' : ''}}>
                    Добавить
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        @input_maker_label('Добавить товар в популярные', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Снять
                    <input type="checkbox" name="is_popular" value="1"
                           class="switchery" {{old_input('is_popular', $product) ? 'checked' : ''}}>
                    Добавить
                </label>
            </div>
        </div>
    </div>


    <div class="form-group">
        @input_maker_label('Отображается', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="is_published" value="1"
                           {{--class="switchery" {{old_input('is_published', $product) || isset(old_input('is_published',$product)) ? 'checked' : ''}}>--}}
                           class="switchery" @if (old_input('is_published', $product) || !isset($product->id))
                        {{'checked'}}
                            @endif>
                    Вкл
                </label>
            </div>
        </div>
    </div>
    <div class="form-group @if($errors->has('slug'))has-error has-feedback @endif">
        @input_maker_label('Урл <code>(Сгенерируется автоматически, если оставить пустым)</code>', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('slug', ['type' => 'string'], $product)
            @if($errors->has('slug'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('slug')}}</span>
            @endif
            <code>сгенерируется автоматически, если оставиь пустым</code>
        </div>
    </div>
</fieldset>