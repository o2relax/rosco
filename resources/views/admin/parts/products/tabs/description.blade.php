<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
    @foreach(app('shared')->get('languages') as $language)
        <li class="tab {{$language->code == cfg('language', 'code') ? 'active' : ''}}">
            <a href="#language_{{$language->code}}" data-toggle="tab"><img src="{{$language->icon}}" alt=""> {{$language->name}}</a>
        </li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach(app('shared')->get('languages') as $language)
        <div class="tab-pane {{$language->code == cfg('language', 'code') ? 'active' : ''}}"
             id="language_{{$language->code}}">
            <fieldset class="content-group">
                <div class="form-group @if($errors->has($language->code . '.name'))has-error has-feedback @endif">
                    @input_maker_label('Название (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[name]', ['type' => 'string'],
                        trans_exist($product, 'name', $language->code))
                        @if($errors->has($language->code . '.name'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.short_description'))has-error has-feedback @endif">
                    @input_maker_label('Краткое описание (' . $language->code . ')', ['class' => 'control-label
                    col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[short_description]', ['type' => 'text', 'class' =>
                        'summernote'],
                        trans_exist($product, 'short_description', $language->code))
                        @if($errors->has($language->code . '.short_description'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.short_description')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.full_description'))has-error has-feedback @endif">
                    @input_maker_label('Полное описание (' . $language->code . ')', ['class' => 'control-label
                    col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[full_description]', ['type' => 'text', 'class' =>
                        'summernote', 'placeholder' => 'Можно оставить пустым'],
                        trans_exist($product, 'full_description', $language->code))
                        @if($errors->has($language->code . '.full_description'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.full_description')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.seo_title'))has-error has-feedback @endif">
                    @input_maker_label('Seo Title (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[seo_title]', ['type' => 'string'],
                        trans_exist($product, 'seo_title', $language->code))
                        @if($errors->has($language->code . '.seo_title'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.seo_title')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.seo_description'))has-error has-feedback @endif">
                    @input_maker_label('Seo Description (' . $language->code . ')', ['class' => 'control-label
                    col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[seo_description]', ['type' => 'text', 'custom' => 'style
                        =
                        "height: 100px"'],
                        trans_exist($product, 'seo_description', $language->code))
                        @if($errors->has($language->code . '.seo_description'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.seo_description')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.seo_keywords'))has-error has-feedback @endif">
                    @input_maker_label('Seo Keywords (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[seo_keywords]', ['type' => 'text', 'custom' => 'style =
                        "height:
                        100px"'],
                        trans_exist($product, 'seo_keywords', $language->code))
                        @if($errors->has($language->code . '.seo_keywords'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.seo_keywords')}}</span>
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
    @endforeach
</div>