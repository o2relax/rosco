<fieldset class="content-group">
    <legend class="text-bold">Основное изображение</legend>

    <div class="form-group col-xs-12">
        <input type="file" name="image" class="file-input-preview2" data-show-remove="false">
    </div>
</fieldset>
<fieldset class="content-group">
    <legend class="text-bold">Дополнительные изображения</legend>

    <div class="form-group col-xs-12">
        <input type="file" name="images[]" class="file-input-preview" data-show-remove="false" multiple="multiple">
    </div>
</fieldset>