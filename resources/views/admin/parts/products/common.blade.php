<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/editors/summernote/summernote.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/uploaders/fileinput/fileinput.min.js') !!}"></script>

<script>
    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };
    var fileActionSettings = {
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };

    function submitForm(btn) {
        btn.children('i').attr('class', 'fab-icon-open icon-reload-alt');
        $('#form').submit();
    }

    function updateAttributes(html) {
        $('.ajax-attributes').html(html);
    }

    function validation() {
        var click = false;
        $('#form').find('.has-error').each(function () {
            var id = $(this).parent().parent().attr('id');
            $('.tab[href="#' + id + '"]').addClass('has-error');
            if(!click) {
                $('.tab[href="#' + id + '"]').click();
                click = true;
            }
        });
    }

    @if($product)

    function addAttribute() {

        var attribute_id = $('select[name="attribute"]').val();

        if(!attribute_id) return false;

        $.ajax({
            url: '{{route('admin.products.attribute.add', [$product->id])}}',
            method: 'post',
            data: 'attribute_id=' + attribute_id,
            dataType: 'json',
            success: function (response) {
                var bg;
                if (response.status === 1) {
                    updateAttributes(response.html);
                    bg = 'success';
                } else {
                    bg = 'danger';
                }
                new PNotify({
                    text: response.message,
                    addclass: 'bg-' + bg
                });
            }
        });

        return false;
    }

    function deleteAttribute(attribute_id) {

        $.ajax({
            url: '{{route('admin.products.attribute.delete', [$product->id])}}',
            method: 'post',
            data: '_method=DELETE&attribute_id=' + attribute_id,
            dataType: 'json',
            success: function (response) {
                var bg;
                if (response.status === 1) {
                    updateAttributes(response.html);
                    bg = 'success';
                } else {
                    bg = 'danger';
                }
                new PNotify({
                    text: response.message,
                    addclass: 'bg-' + bg
                });
            }
        });

        return false;
    }

    @endif

    $(function () {

        var local_prefix;

        @if($product)
            local_prefix = '{{$product->id}}';
        @else
            local_prefix = '0';
        @endif

        var tab_name;

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href');
            localStorage.setItem(local_prefix + '_tab', tab);
        });

        if (localStorage.getItem(local_prefix + '_tab') === null) {
            localStorage.setItem(local_prefix + '_tab', '#main');
            tab_name = '#main';
        } else {
            tab_name = localStorage.getItem(local_prefix + '_tab');
        }

        $('.main-pane').removeClass('active');
        $(tab_name).addClass('active');
        $('body').find('.main-tab').each(function () {
            $(this).parent().removeClass('active');
        });
        $('body').find('.main-tab[href="' + tab_name + '"]').each(function () {
            $(this).parent().addClass('active');
        });

        $('.summernote').summernote({
            height: 300
        });
        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');
            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }

        $(".file-input-preview").fileinput({
            browseLabel: 'Выбрать',
            browseIcon: '<i class="icon-file-plus"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: modalTemplate
            },
            initialPreview: [
                @if($product)
                        @foreach($product->images as $image)
                    "{{product_image($product->id, $image->image)}}",
                @endforeach
                @endif
            ],
            initialPreviewConfig: [
                    @if($product)
                    @foreach($product->images as $image)
                {
                    caption: "{{$image->image}}",
                    url: '{{route('admin.product.image.delete', [$image->id])}}',
                    showDrag: true
                },
                @endforeach
                @endif
            ],
            initialPreviewAsData: true,
            overwriteInitial: false,
            maxFileCount: {{config('image.max_file_count')}},
            maxFileSize: {{config('image.max_size')}},
            previewZoomButtonClasses: previewZoomButtonClasses,
            previewZoomButtonIcons: previewZoomButtonIcons,
            fileActionSettings: fileActionSettings,
            showUpload: false,
            showRemove: false
        });
        $(".file-input-preview2").fileinput({
            browseLabel: 'Выбрать',
            browseIcon: '<i class="icon-file-plus"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: modalTemplate
            },
            initialPreview: [
                @if($product && $product->image)
                    "{{$product->image}}",
                @endif
            ],
            initialPreviewConfig: [
                    @if($product && $product->image)
                {
                    caption: "{{$product->image}}",
                    url: '{{route('admin.product.main_image.delete', [$product->id])}}',
                    showDrag: false
                },
                @endif
            ],
            initialPreviewAsData: true,
            overwriteInitial: true,
            maxFileCount: {{config('image.max_file_count')}},
            maxFileSize: {{config('image.max_size')}},
            previewZoomButtonClasses: previewZoomButtonClasses,
            previewZoomButtonIcons: previewZoomButtonIcons,
            fileActionSettings: fileActionSettings,
            showUpload: false,
            showRemove: false
        });
        $('.multiselect').multiselect({
            enableFiltering: true,
            templates: {
                filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text" placeholder="Введите название"></li>'
            },
            onChange: function () {
                $.uniform.update();
            }
        });
        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });
        validation();
    });

</script>