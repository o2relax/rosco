@extends('admin.layouts.dashboard')
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
    <script>
        function changeOrderStatus(id, status, label) {
            $.ajax({
                url: '/admin/products/' + id + '/status',
                method: 'post',
                data: 'status_id=' + status,
                dataType: 'json',
                success: function (response) {
                    var cl = label.attr('class');
                    label.attr('class', cl.replace(/(bg+)/g, ''));
                    label.addClass('bg-' + response.code);
                    label.html(response.name + ' <span class="caret"></span>');
                }
            });
            return false;
        }

        $(function () {
            $('[data-popup="lightbox"]').fancybox({
                padding: 3
            });
        });
    </script>
@endsection
@section('content')

    @include('admin.modals')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Список товаров
            </h5>
        </div>
        <div class="panel-heading">
            <div style="margin-bottom: 20px;">
                {!! Form::open(['url' => 'admin/products/delete_filters', 'method' => 'post']) !!}
                <select class="form-control" name="selected_category" style="display:inline; width:45%;">
                    <option value="">Категория для удаления</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">
                            {{$category->name}}
                        </option>
                    @endforeach
                </select>
                <select class="form-control" name="selected_manufacturer" style="display:inline;width:45%;">
                    <option value="">Производители для удаления</option>
                    @foreach($manufacturers as $manufacturer)
                        <option value="{{$manufacturer->id}}">
                            {{$manufacturer->name}}
                        </option>
                    @endforeach
                </select>
                <button class="btn btn-primary" type="submit"  onclick="return confirm('Вы уверены, что хотите удалить товары?')">Удалить</button>
                {!! Form::close() !!}
            </div>
            <a href="{{route('admin.products.all.delete')}}" onclick="return confirm('Вы уверены, что хотите удалить товары?')">
                <button class="btn btn-danger">Удалить все товары</button>
            </a>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Картинка</th>
                    <th>Артикул</th>
                    <th>Название</th>
                    <th>Категория</th>
                    <th>Производитель</th>
                    <th>Цена</th>
                    <th class="text-center">Остаток</th>
                    <th class="text-center">Статус</th>
                    <th class="text-right"></th>
                </tr>
                {!! Form::open(['url' => 'admin/products/search', 'method' => 'get']) !!}
                <tr>
                    <th><input class="form-control" name="search[id]" placeholder="Поиск по ID"
                               value="{{request()->get('search')['id']}}"></th>
                    <th></th>
                    <th><input class="form-control" name="search[article]" placeholder="Поиск по артикулу"
                               value="{{request()->get('search')['article']}}"></th>
                    <th><input class="form-control" name="search[name]" placeholder="Поиск по названию"
                               value="{{request()->get('search')['name']}}"></th>
                    <th>
                        <select class="form-control" name="search[category_id]">
                            <option value="">Поиск по категории</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"
                                        {{$category->id == request()->get('search')['category_id'] ? 'selected' : ''}}>
                                    {{$category->name}}
                                </option>
                            @endforeach
                        </select>
                    </th>
                    <th>
                        <select class="form-control" name="search[manufacturer_id]">
                            <option value="">Поиск по производителю</option>
                            @foreach($manufacturers as $manufacturer)
                                <option value="{{$manufacturer->id}}"
                                        {{$manufacturer->id == request()->get('search')['manufacturer_id'] ? 'selected' : ''}}>
                                    {{$manufacturer->name}}
                                </option>
                            @endforeach
                        </select>
                    </th>
                    <th><input class="form-control" name="search[price]" placeholder="Поиск по цене"
                               value="{{request()->get('search')['price']}}"></th>
                    <th></th>
                    <th> <!-- TODO: make it via Form or form maker (problem with method only on collection) -->
                        <select class="form-control" name="search[status]">
                            <option value="">статус</option>
                            @foreach($statuses as $status)
                                <option value="{{$status->id}}" {!! request()->get('search')['status'] == $status->id ? 'selected' : '' !!}>{{$status->name}}</option>
                            @endforeach
                        </select>
                    </th>
                    <th class="text-right" style="width: 150px">
                        <button class="btn btn-primary btn-xs" type="submit" name="submit" data-popup="tooltip"
                                data-popup="tooltip" title="Осуществить поиск"><i class="icon-search4"></i></button>
                        <a class="btn btn-default btn-xs" href="{{url('admin/products')}}" data-popup="tooltip"
                           data-popup="tooltip" title="Очистить поиск"><i class="icon-eraser2"></i></a>
                    </th>
                </tr>
                {!! Form::close() !!}
                </thead>
                <tbody>
                @if(count($products))
                    @foreach($products as $product)
                        <tr>
                            <td class="text-center">
                                <small class="display-block text-size-small no-margin">{{$product->id}}</small>
                            </td>
                            <td class="text-center">
                                <div class="thumbnail" style="max-height: 100px; max-width: 100px; overflow: hidden">
                                    <div class="thumb">
                                        <img src="{{$product->image}}" alt="{!! $product->name !!}"
                                             style="max-height: 100px; max-width: 100px">
                                        <div class="caption-overflow">
										<span>
											<a href="{!! $product->image !!}" data-popup="lightbox" rel="gallery"
                                               class="btn border-white text-white btn-flat btn-icon btn-rounded"><i
                                                        class="icon-plus3"></i></a>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td><span class="label label-default">{!! $product->article !!}</span></td>
                            <td>
                                <a href="{{route('admin.products.edit', [$product->id])}}">{!! $product->name !!}</a>
                            </td>
                            <td>
                                {{$product->category ? $product->category->name : ''}}
                            </td>
                            <td>
                                {{$product->manufacturer ? $product->manufacturer->name : ''}}
                            </td>
                            <td>
                                {{ $product->currency_id && $product->currency_id > 0 ? $product->price.$product->currency->code : $product->price }}
                            </td>
                            <td class="text-center">
                                <span class="label @if($product->stock == 0) label-danger @elseif($product->stock < 6) bg-orange @else label-success @endif">{!! $product->stock !!}</span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="label bg-{{collect($statuses)->where('id', $product->status_id)->first()->code}} dropdown-toggle selector{{$product->id}}"
                                       data-toggle="dropdown">{{collect($statuses)->where('id', $product->status_id)->first()->name}} <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right menu{{$product->id}}">
                                        @foreach($statuses as $status)
                                            <li>
                                                <a onclick="changeOrderStatus({{$product->id}}, {{$status->id}}, $('.selector{{$product->id}}'));"><span
                                                            class="status-mark position-left border-{{$status->code}}"></span> {{$status->name}}
                                                </a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </td>
                            <td class="text-center">
                                <form method="post"
                                      action="{!! url('admin/products/'.$product->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить товар?')"><i
                                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-xs btn-default pull-right"
                                   href="{!! route('admin.products.edit', [$product->id]) !!}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            </tbody>
            </table>
            <br>
            <div class="col-md-12 text-center">Товаров не найдено</div>
            @endif

        </div>
        @if(count($products))
            <div class="col-xs-12 text-center">
                {!! $pagination !!}
            </div>
        @endif
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-primary btn-float btn-rounded btn-icon" data-popup="tooltip"
               data-popup="tooltip" title="Добавить товар"
               href="{{ route('admin.products.create') }}">
                <i class="fab-icon-open icon-plus3"></i>
            </a>
        </li>
    </ul>
@endsection
