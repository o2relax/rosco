@if(isset($product->attributes))
    @foreach($product->attributes as $attribute)
        <div class="form-group border-bottom border-bottom-grey-300" style="padding-bottom: 20px !important;">
            @input_maker_label($attribute->attribute->name, ['class' => 'control-label col-md-2'])
            <div class="col-md-7">
                @foreach(app('shared')->get('languages') as $language)
                    <div class="form-group has-feedback has-feedback-left">
                        @input_maker_create('attributes[' . $attribute->attribute_id . '][' . $language->code .
                        '][text]', ['type' => 'string'],
                        $attribute->translate($language->code) ? $attribute->translate($language->code)->text : '')
                        <div class="form-control-feedback">
                            <img src="{{$language->icon}}" alt="">
                        </div>
                        @if($errors->has('attributes.' . $attribute->attribute_id . '.' . $language->code . '.text'))
                            <span class="help-block">{{$errors->first('attributes.' . $attribute->attribute_id . '.' . $language->code . '.text')}}</span>
                        @endif
                    </div>
                @endforeach
            </div>
            <div class="col-md-3 text-right">
                    <a class="btn btn-xs btn-danger pull-right" onclick="deleteAttribute({{$attribute->attribute_id}})"><i
                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                    </a>
            </div>
        </div>
    @endforeach
@endif