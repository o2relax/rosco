@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }

        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }

        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/currencies/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Добавить валюту</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список валют<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком валют. Чтобы указать валюты каталога и магазина, перейдите в раздел
            <a class="btn btn-link" href="{{route('admin.settings')}}"><i class="icon-link"></i> настроек</a></div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">#</th>
                    <th>Название</th>
                    <th>Код</th>
                    <th>Курс</th>
                    <th class="text-center">Округление</th>
                    <th>Символ</th>
                    <th class="text-center">Валюта каталога</th>
                    <th class="text-center">Валюта магазина</th>
                    <th class="text-center">Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($currencies))
                    @foreach($currencies as $currency)
                        <tr>
                            <td class="text-center">
                                <h6 class="no-margin">
                                    <small class="display-block text-size-small no-margin">{{$currency->id}}</small>
                                </h6>
                            </td>
                            <td>
                                <a href="{{url('admin/currencies/'.$currency->id.'/edit')}}"
                                   class="display-inline-block text-default text-semibold letter-icon-title">{{$currency->name}}</a>
                            </td>
                            <td>{{$currency->code}}</td>
                            <td><code>{{$currency->rate}}</code></td>
                            <td class="text-center">.{{$currency->round}}</td>
                            <td>
                                <strong class="text-size-large">{{$currency->prefix}}</strong>100<strong class="text-size-large">{{$currency->postfix}}</strong>
                            </td>
                            <td class="text-center">
                                <i class="icon-{{ $currency->id == cfg('currency_catalog') ? 'checkmark3 text-success' : 'cross2 text-danger' }}"></i>
                            </td>
                            <td class="text-center">
                                <i class="icon-{{ $currency->id == cfg('currency_shop') ? 'checkmark3 text-success' : 'cross2 text-danger' }}"></i>
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery"
                                               @if($currency->is_active) checked="checked"
                                               @endif data-url="{!! route('admin.currencies.status', [$currency->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                <form method="post" action="{!! url('admin/currencies/'.$currency->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить валюту? Учтите, что нельзя удалить валюту каталога или магазина')"><i
                                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-default btn-xs pull-right btn-xs"
                                   href="{{url('admin/currencies/'.$currency->id.'/edit')}}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection