<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>

<script>

    function submitForm(btn) {
        $('#form').submit();
    }

    $(function () {

        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');
            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }
        $(function () {

        });
    });

</script>