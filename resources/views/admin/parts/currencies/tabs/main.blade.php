<fieldset class="content-group">
    <div class="form-group @if($errors->has('name'))has-error has-feedback @endif">
        @input_maker_label('Название', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('name', ['type' => 'string'], $currency)
            @if($errors->has('name'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('name')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('code'))has-error has-feedback @endif">
        @input_maker_label('Код ISO', ['class' => 'control-label col-lg-2'])
        <div class="col-md-2">
            @input_maker_create('code', ['type' => 'string', 'custom' => 'maxlength="3"'], $currency)
            @if($errors->has('code'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('code')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('rate'))has-error has-feedback @endif">
        @input_maker_label('Курс', ['class' => 'control-label col-lg-2'])
        <div class="col-md-2">
            @input_maker_create('rate', ['type' => 'float'], $currency)
            @if($errors->has('rate'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('rate')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('round'))has-error has-feedback @endif">
        @input_maker_label('Округление', ['class' => 'control-label col-lg-2'])
        <div class="col-md-2">
            @input_maker_create('round', ['type' => 'integer'], $currency)
            @if($errors->has('round'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('round')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('prefix'))has-error has-feedback @endif">
        @input_maker_label('Символ слева', ['class' => 'control-label col-lg-2'])
        <div class="col-md-2">
            @input_maker_create('prefix', ['type' => 'string'], $currency)
            @if($errors->has('prefix'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('prefix')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('postfix'))has-error has-feedback @endif">
        @input_maker_label('Символ справа', ['class' => 'control-label col-lg-2'])
        <div class="col-md-2">
            @input_maker_create('postfix', ['type' => 'string'], $currency)
            @if($errors->has('postfix'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('postfix')}}</span>
            @endif
        </div>
    </div>
</fieldset>