@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script>
    function changeStatus(url, status) {
        status = status ? 1 : 0;
        $.ajax({
            url: url,
            method: 'post',
            data: 'is_active=' + status,
            dataType: 'json',
            success: function (response) {
                new PNotify({
                    text: response.message,
                    addclass: 'bg-teal-400'
                });
            }
        });
    }
    $(function () {
        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
                html.addEventListener('click', function () {
                    changeStatus($(html).attr('data-url'), html.checked);
                    console.log($(html).attr('data-url'));
                });
            });
        }

    });
</script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/menus/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Создать меню</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список меню<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком меню</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Название</th>
                    <th>Код</th>
                    <th class="text-center">Кол-во пунктов</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($menus as $menu)
                    <tr>
                        <td class="text-center">
                            <h6 class="no-margin">
                                <small class="display-block text-size-small no-margin">{{$menu->id}}</small>
                            </h6>
                        </td>
                        <td>
                            <a href="{{url('admin/menus/'.$menu->id.'/edit')}}"
                               class="display-inline-block text-default text-semibold letter-icon-title">{{$menu->name}}</a>
                        </td>
                        <td>
                            <code>{{$menu->code}}</code>
                        </td>
                        <td class="text-center">
                            {{$menu->links()->count()}}
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-switchery switchery-sm">
                                <label>
                                    <input type="checkbox" class="switchery"
                                           @if($menu->is_active) checked="checked"
                                           @endif data-url="{!! route('admin.menus.active', [$menu->id]) !!}">
                                </label>
                            </div>
                        </td>
                        <td class="text-center">
                            <form method="post" action="{!! url('admin/menus/'.$menu->id) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button class="btn btn-danger btn-xs pull-right" type="submit"
                                        onclick="return confirm('Вы уверены, что хотите удалить меню со всеми пунктами')">
                                    <i
                                            class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                </button>
                            </form>
                            <a class="btn btn-default btn-xs pull-right btn-xs"
                               href="{{url('admin/menus/'.$menu->id.'/edit')}}"><i
                                        class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection