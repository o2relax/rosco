<script type="text/javascript" src="{!! asset('back/assets/js/core/libraries/jquery_ui/core.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/core/libraries/jquery_ui/effects.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/core/libraries/jquery_ui/interactions.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/dragula.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/nestable/jquery.nestable.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/loaders/blockui.min.js') !!}"></script>

<script>
    $(function () {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html);
            html.onclick = function () {
                change_status($(html).data('id'), html.checked);
            };
        });
        @if($menu)

        var drake = dragula([document.getElementById('drag')], {
            mirrorContainer: document.querySelector('#drag'),
            moves: function (el, container, handle) {
                return handle.classList.contains('dragula-handle');
            }
        });
        drake.on('drop', function (el, target, source, sibling) {
            var ids = new Array;
            $(target).find('tr').each(function () {
                ids.push($(this).data('id'));
            });
            change_sort(ids);
        });
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target);
            if (window.JSON) {
                change_sort(window.JSON.stringify(list.nestable('serialize')));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        $('#nestable').nestable({
            group: 1
        }).on('change', updateOutput);

        updateOutput($('#nestable').data('output', $('#nestable-output')));

        @endif
    });
    @if($menu)
    function change_status(id, status) {
        var data = status ? 1 : 0;
        $.ajax({
            url: '{!! route('admin.menus.link.active', ['id' => $menu->id]) !!}',
            method: 'post',
            data: 'is_active=' + data + '&id=' + id,
            dataType: 'json',
            success: function(response) {
                new PNotify({
                    text: response.message,
                    addclass: 'bg-info'
                });
            }
        });
    }
    function change_sort(data) {
        $.ajax({
            url: '{!! route('admin.menus.link.sort', ['id' => $menu->id]) !!}',
            method: 'post',
            data: 'data=' + data,
            dataType: 'json',
            beforeSend: function() {
                $('.links').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    centerX: 0,
                    centerY: 0,
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        width: 16,
                        top: '-25px',
                        left: '',
                        right: '5px',
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $('.links').unblock();
                $('.links').block({
                    message: 'response.message',
                    timeout: 1000,
                    centerX: 0,
                    centerY: 0,
                    overlayCSS: {
                        backgroundColor: 'transparent',
                        opacity: 0.8
                    },
                    css: {
                        width: '100%',
                        top: '-25px',
                        left: '',
                        right: '5px',
                        border: 0,
                        padding: 0,
                        textAlign: 'right',
                        backgroundColor: 'transparent'
                    }
                });
            }
        });
    }
    @endif
    function submitForm(btn) {
        $('#form').submit();
    }
</script>