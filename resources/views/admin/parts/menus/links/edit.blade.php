@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @include('admin.parts.menus.links.common')
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 text-right">
                    <form action="{!! route('admin.menus.link.delete', [$link->menu->id, $link->id]) !!}" method="post" id="delete">
                        {!! method_field('DELETE') !!}
                        {!! csrf_field() !!}
                    </form>
                </div>
            </div>
            <h3 class="panel-title">{{$link->menu->name}} -> {{$link->title}}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                {!! Form::model($link, ['url' => route('admin.menus.link.update', [$link->menu->id, $link->id]), 'id' => 'form', 'class' => 'form-horizontal', 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}
                @include('admin.parts.menus.links.tabs.description')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ route('admin.menus.edit', [$link->menu->id]) }}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-danger-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Удалить"
               onclick="if(confirm('Уверены, что хотите удалить пункт?')){$('#delete').submit();}">
                <i class="fab-icon-open icon-cross2"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Сохранить"
               onclick="submitForm($(this));">
                <i class="fab-icon-open icon-floppy-disk"></i>
            </a>
        </li>
    </ul>
@endsection