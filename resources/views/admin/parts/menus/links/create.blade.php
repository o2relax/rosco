@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @include('admin.parts.menus.links.common')
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">Создание группы</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                {!! Form::model($link, ['url' => route('admin.menus.link.create', [$menu_id]), 'id' => 'form', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                @include('admin.parts.menus.links.tabs.description')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ route('admin.menus.edit', [$menu_id]) }}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Сохранить"
               onclick="submitForm($(this));">
                <i class="fab-icon-open icon-floppy-disk"></i>
            </a>
        </li>
    </ul>
@endsection