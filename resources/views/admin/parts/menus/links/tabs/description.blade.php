<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
    @foreach(app('shared')->get('languages') as $language)
        <li class="tab {{$language->code == cfg('language', 'code') ? 'active' : ''}}">
            <a href="#language_{{$language->code}}" data-toggle="tab"><img src="{{$language->icon}}" alt=""> {{$language->name}}</a>
        </li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach(app('shared')->get('languages') as $language)
        <div class="tab-pane {{$language->code == cfg('language', 'code') ? 'active' : ''}}"
             id="language_{{$language->code}}">
            <fieldset class="content-group">
                <div class="form-group @if($errors->has($language->code . '.title'))has-error has-feedback @endif">
                    @input_maker_label('Заголовок (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-4">
                        @input_maker_create($language->code . '[title]', ['type' => 'string'],
                        trans_exist($link, 'title', $language->code))
                        @if($errors->has($language->code . '.title'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.title')}}</span>
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
    @endforeach
</div>
<fieldset class="content-group">
    <div class="form-group @if($errors->has('url'))has-error has-feedback @endif">
        @input_maker_label('Ссылка (' . cfg('domain') . ')', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('url', ['type' => 'string'], $link)
            @if($errors->has('url'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('url')}}</span>
            @endif
        </div>
    </div>
</fieldset>