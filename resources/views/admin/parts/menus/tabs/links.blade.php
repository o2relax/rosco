<p class="text-size-small text-uppercase text-muted">Пункты <code>(максимальный уровень вложенности 2)</code></p>
<div class="dd links" id="nestable">
    @if(count($menu->links))
        <ol class="dd-list">
            @foreach($menu->parents() as $item)
                <li class="dd-item" data-id="{{$item->id}}">
                    <div class="dd-handle">ID:{{$item->id}} <span class="title">{!! $item->title !!}</span> <small>({{$item->url}})</small>
                    </div>
                    <div class="dd-nav">
                        <form method="post" action="{!! route('admin.menus.link.delete', [$menu->id, $item->id]) !!}" class="pull-right">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <button class="btn btn-link pull-right text-danger" type="submit"
                                    onclick="return confirm('Вы уверены, что хотите удалить пункт')">
                                <i
                                        class="icon-x" data-popup="tooltip" title="Удалить"></i>
                            </button>
                        </form>
                        <a href="{!! route('admin.menus.link.edit', ['id' => $menu->id, 'link_id' => $item->id]) !!}" class="btn btn-link pull-right text-grey">
                            <i class="icon-pen" data-popup="tooltip" title="Изменить"></i>
                        </a>
                        <a href="{{$item->url}}" target="_blank" class="btn pull-right"><i class="icon-earth"></i></a>
                        <span class="checkbox checkbox-switchery switchery-xs pull-right">
                                <label>
                                    <input type="checkbox" class="switchery" data-id="{{$item->id}}" @if($item->is_active)checked="checked"@endif>
                                </label>
                            </span>
                    </div>
                    @if($item->child())
                        <ol class="dd-list">
                            @foreach($item->child() as $lv2)
                                <li class="dd-item" data-id="{{$lv2->id}}">
                                    <div class="dd-handle">ID:{{$lv2->id}} <span class="title">{!! $lv2->title !!}</span> <small>({{$lv2->url}})</small></div>
                                    <div class="dd-nav">
                                        <a href="{!! route('admin.menus.link.edit', ['id' => $menu->id, 'link_id' => $lv2->id]) !!}" class="btn pull-right"><i class="fa fa-edit"></i></a>
                                        <a href="{{$lv2->url}}" target="_blank" class="btn pull-right"><i class="fa fa-globe"></i></a>
                                        <span class="checkbox checkbox-switchery switchery-xs pull-right">
                                            <label>
                                                <input type="checkbox" class="switchery" data-id="{{$lv2->id}}" @if($lv2->is_active)checked="checked"@endif>
                                            </label>
                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ol>
                    @endif
                </li>
            @endforeach
        </ol>
    @endif
</div>
{{--<textarea id="nestable-output" class="form-control"></textarea>--}}