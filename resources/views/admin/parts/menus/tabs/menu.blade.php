<p class="text-size-small text-uppercase text-muted">Основные данные</p>
<div class="panel panel-body border-top-primary">
    <div class="form-group @if($errors->has('name'))has-error has-feedback @endif">
        @input_maker_label('Название', ['class' => 'control-label col-md-2'])
        <div class="col-md-10">
            @input_maker_create('name', ['type' => 'string'], $menu)
            @if($errors->has('name'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('name')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('code'))has-error has-feedback @endif">
        @input_maker_label('Код', ['class' => 'control-label col-md-2'])
        <div class="col-md-10">
            @input_maker_create('code', ['type' => 'string'], $menu)
            @if($errors->has('code'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('code')}}</span>
            @endif
        </div>
    </div>
    @if($menu)В шаблоне для вывода меню исползуется код <code>{{'@'}}menu('{{$menu->code}}')</code>@endif
</div>