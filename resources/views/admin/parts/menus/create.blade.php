@extends('admin.layouts.dashboard')

@section('stylesheets')
    @parent

@stop

@section('javascript')
    @include('admin.parts.menus.common')
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">Создание меню</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            {!! Form::model($menu, ['url' => url('admin/menus'), 'id' => 'form', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
            @include('admin.parts.menus.tabs.menu')
            {!! Form::close() !!}
        </div>
        <div class="col-md-7">
            <p class="text-size-small text-uppercase text-muted">Пункты <code>(максимальный уровень вложенности 2)</code></p>
            <div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
                <span class="text-semibold">Для добавления пунктов, сохраните меню</span>
            </div>
        </div>
    </div>

    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
        <li style="display: inline">
            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
               href="{{ url('admin/' . request()->segment(2) )}}">
                <i class="fab-icon-open icon-arrow-left8"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-danger-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Удалить"
               onclick="if(confirm('Уверены, что хотите удалить меню?')){$('#delete').submit();}">
                <i class="fab-icon-open icon-cross2"></i>
            </a>
        </li>
        <li style="display: inline">
            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Сохранить"
               onclick="submitForm($(this));">
                <i class="fab-icon-open icon-floppy-disk"></i>
            </a>
        </li>
    </ul>
@endsection