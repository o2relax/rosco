@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }

        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }
        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/languages/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Создать язык</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список языков<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком языков</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Иконка</th>
                    <th>Имя</th>
                    <th class="text-center">Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($langs as $language)
                    <tr @if($language->code == cfg('language', 'code')) class="border-left border-left-blue-300" @endif>
                        <td class="text-center">
                            <h6 class="no-margin">
                                <small class="display-block text-size-small no-margin">{{$language->id}}</small>
                            </h6>
                        </td>
                        <td class="text-center">
                            <img src="{{$language->icon}}" alt="{!! $language->name !!}"
                                 style="max-height: 20px; width: auto">
                        </td>
                        <td>
                            @if($language->code == cfg('language', 'code'))
                                <span class="label label-primary">Main</span>
                            @endif
                            <a href="{{url('admin/languages/'.$language->id.'/edit')}}"
                               class="display-inline-block text-default text-semibold letter-icon-title">{{$language->name}}</a>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-switchery switchery-sm">
                                <label>
                                    <input type="checkbox" class="switchery"
                                           @if($language->is_active) checked="checked"
                                           @endif data-url="{!! route('admin.languages.active', [$language->id]) !!}">
                                </label>
                            </div>
                        </td>
                        <td class="text-center">
                            <form method="post" action="{!! url('admin/languages/'.$language->id) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button class="btn btn-danger btn-xs pull-right" type="submit"
                                        onclick="return confirm('Вы уверены, что хотите удалить язык?')">
                                    <i
                                            class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                </button>
                            </form>
                            <a class="btn btn-default btn-xs pull-right btn-xs"
                               href="{{url('admin/languages/'.$language->id.'/edit')}}"><i
                                        class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection