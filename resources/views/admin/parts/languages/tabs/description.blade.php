<fieldset class="content-group">
    <div class="form-group @if($errors->has('attribute_group_id'))has-error has-feedback @endif">
        @input_maker_label('Группа', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <select class="form-control" name="attribute_group_id">
                @foreach($groups as $group)
                    <option value="{{$group->id}}" {{old('attribute_group_id', $attribute) ? 'selected' : ''}}>{{$group->name}}</option>
                @endforeach
            </select>
            @if($errors->has('attribute_group_id'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('attribute_group_id')}}</span>
            @endif
        </div>
    </div>
</fieldset>
<fieldset class="content-group">
    <div class="form-group @if($errors->has('sort'))has-error has-feedback @endif">
        @input_maker_label('Сортировка', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('sort', ['type' => 'string'], $attribute)
            @if($errors->has('sort'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('sort')}}</span>
            @endif
        </div>
    </div>
</fieldset>
<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
    @foreach(app('shared')->get('languages') as $language)
        <li class="{{$language->code == cfg('language', 'code') ? 'active' : ''}}">
            <a href="#language_{{$language->code}}" data-toggle="tab"><img src="{{$language->icon}}" alt=""> {{$language->name}}</a>
        </li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach(app('shared')->get('languages') as $language)
        <div class="tab-pane {{$language->code == cfg('language', 'code') ? 'active' : ''}}"
             id="language_{{$language->code}}">
            <fieldset class="content-group">
                <div class="form-group @if($errors->has($language->code . '.name'))has-error has-feedback @endif">
                    @input_maker_label('Название (' . $language->code . ')', ['class' => 'control-label col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[name]', ['type' => 'string'],
                        trans_exist($attribute, 'name', $language->code))
                        @if($errors->has($language->code . '.name'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has($language->code . '.description'))has-error has-feedback @endif">
                    @input_maker_label('Краткое описание (' . $language->code . ')', ['class' => 'control-label
                    col-lg-2'])
                    <div class="col-md-10">
                        @input_maker_create($language->code . '[description]', ['type' => 'text', 'class' =>
                        'summernote'],
                        trans_exist($attribute, 'description', $language->code))
                        @if($errors->has($language->code . '.description'))
                            <div class="form-control-feedback">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            <span class="help-block">{{$errors->first($language->code . '.description')}}</span>
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
    @endforeach
</div>