<fieldset class="content-group">
    <div class="form-group @if($errors->has('name'))has-error has-feedback @endif">
        @input_maker_label('Название', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('name', ['type' => 'string'], $language)
            @if($errors->has('name'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('name')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group @if($errors->has('code'))has-error has-feedback @endif">
        @input_maker_label('Код (2 символа)', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('code', ['type' => 'string'], $language)
            @if($errors->has('code'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('code')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        @input_maker_label('Активность', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="is_active" value="1"
                           class="switchery" {{old_input('is_active', $language) ? 'checked' : ''}}>
                    Вкл
                </label>
            </div>
        </div>
    </div>
    <fieldset class="content-group">
        <legend class="text-bold">Основное изображение</legend>

        <div class="form-group col-xs-12">
            <input type="file" name="icon" class="file-input-preview2" data-show-remove="true">
        </div>
    </fieldset>
</fieldset>
