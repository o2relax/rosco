@extends('admin.layouts.dashboard')

@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a class="btn btn-success" href="{{ url('admin/roles/create') }}">Создать профиль</a>
            </div>
            <form id="" class="raw-margin-left-24" method="get" action="{{url('admin/roles/search')}}">
                {!! csrf_field() !!}
                <div class="col-md-6">
                    <input class="form-control" name="search[label]" placeholder="Поиск по имени"
                           value="{{request()->get('search')['label']}}">
                </div>
                <div class="col-md-3">
                    <input type="submit" value="найти" class="btn btn-primary btn-xs">

                    <a class="btn btn-default btn-xs" href="{{url('admin/roles')}}" data-popup="tooltip"
                       data-popup="tooltip" title="Очистить поиск"><i class="icon-eraser2"></i></a>
                </div>
            </form>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Список профилей<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">

                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Код</th>
                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->label }}</td>
                        <td><span class="label label-{{ $role->style }}">{{ $role->name }}</span></td>
                        <td>
                            <form method="post" action="{!! url('admin/roles/'.$role->id) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button class="btn btn-danger btn-xs pull-right" type="submit"
                                        onclick="return confirm('Вы уверены, что хотите удалить профиль?')"><i
                                            class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                </button>
                            </form>
                            <a class="btn btn-default btn-xs pull-right"
                               href="{{ url('admin/roles/'.$role->id.'/edit') }}"><i
                                        class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>
        </div>
    </div>

@stop
