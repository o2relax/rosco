@extends('admin.layouts.dashboard')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Редактирование профиля<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a></h5>
        </div>
        <div class="panel-body">

            <form id="form" class="form-horizontal" method="post" action="{{url('admin/roles')}}">
                {!! csrf_field() !!}
                <fieldset class="content-group">
                    <div class="form-group">
                        @input_maker_label('Код', ['class' => 'control-label col-lg-2'])
                        <div class="col-lg-10">
                            @input_maker_create('name', ['type' => 'string', 'placeholder' => 'код'])
                        </div>
                    </div>

                    <div class="form-group">
                        @input_maker_label('Название', ['class' => 'control-label col-lg-2'])
                        <div class="col-lg-10">
                            @input_maker_create('label', ['type' => 'string', 'placeholder' => 'название'])
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <legend class="text-bold">Цвет</legend>
                        <div class="checkbox">
                            <label for="danger">
                                <input type="radio" class="styled" name="style" id="danger" value="danger"
                                        {{old('style', 'danger') == 'danger' ? 'checked' : ''}}>
                                <span class="label label-danger">красный</span>
                            </label>
                            <label for="primary">
                                <input type="radio" class="styled" name="style" id="primary" value="primary"
                                        {{old('style') == 'primary' ? 'checked' : ''}}>
                                <span class="label label-primary">синий</span>
                            </label>
                            <label for="warning">
                                <input type="radio" class="styled" name="style" id="warning" value="warning"
                                        {{old('style') == 'warning' ? 'checked' : ''}}>
                                <span class="label label-warning">оранжевый</span>
                            </label>
                            <label for="success">
                                <input type="radio" class="styled" name="style" id="success" value="success"
                                        {{old('style') == 'success' ? 'checked' : ''}}>
                                <span class="label label-success">зеленый</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <legend class="text-bold">Права</legend>
                        @foreach(config()->get('permissions', []) as $permission => $name) <!-- // TODO: make permissions in db -->
                            <div class="checkbox">
                                <label for="{{ $name }}">
                                    <input type="checkbox" class="styled" name="permissions[{{ $permission }}]" id="{{ $name }}" value="{{ $permission }}">
                                    {{ $name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
                <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                    <li style="display: inline">
                        <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" data-popup="tooltip" title="Назад"
                           href="{{ url('admin/' . request()->segment(2)) }}">
                            <i class="fab-icon-open icon-arrow-left8"></i>
                        </a>
                    </li>
                    <li style="display: inline">
                        <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip" title="Сохранить"
                           onclick="$('#form').submit();">
                            <i class="fab-icon-open icon-floppy-disk"></i>
                        </a>
                    </li>
                </ul>
            </form>
        </div>

@stop