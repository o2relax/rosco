<ul class="nav nav-tabs nav-tabs-top nav-justified">
    <li class="active"><a href="#main" data-toggle="tab" class="main-tab">Основная информация</a></li>
    <li class=""><a href="#description" data-toggle="tab" class="main-tab">Описание</a></li>
</ul>
<div class="tab-content">
    <div class="main-pane tab-pane active" id="main" >
        @include('admin.parts.manufacturers.tabs.main')
    </div>
    <div class="main-pane tab-pane" id="description">
        @include('admin.parts.manufacturers.tabs.description')
    </div>
</div>