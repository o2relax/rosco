@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
    <script>
        function changeStatus(url, status) {
            status = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'post',
                data: 'is_active=' + status,
                dataType: 'json',
                success: function (response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }

        $(function () {
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                    html.addEventListener('click', function () {
                        changeStatus($(html).attr('data-url'), html.checked);
                        console.log($(html).attr('data-url'));
                    });
                });
            }

            $('[data-popup="lightbox"]').fancybox({
                padding: 3
            });

        });
    </script>
@endsection
@section('content')
    <h6 class="content-group text-semibold">
        <div class="row">
            <div class="col-md-2">
                <a href="{!! url('admin/manufacturers/create') !!}" class="btn btn-success"><i
                            class="icon icon-plus3"></i> Добавить производителя</a>
            </div>
        </div>
    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список производителей<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>
        <div class="panel-body">Управляйте списком производителей</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">#</th>
                    <th class="text-center">Избражение</th>
                    <th>Название</th>
                    <th>URL</th>
                    <th class="text-center">Статус</th>
                    <th></th>
                </tr>
                {!! Form::open(['url' => url('admin/manufacturers/search'), 'method' => 'get']) !!}
                <tr>
                    <th><input class="form-control" name="search[id]" placeholder="Поиск по ID"
                               value="{{request()->get('search')['id']}}"></th>
                    <th></th>
                    <th><input class="form-control" name="search[name]" placeholder="Поиск по названию"
                               value="{{request()->get('search')['name']}}"></th>
                    <th><input class="form-control" name="search[slug]" placeholder="Поиск по урлу"
                               value="{{request()->get('search')['slug']}}"></th>
                    <th>
                        <select class="form-control" name="search[active]">
                            <option value="">статус</option>
                            <option value="1">активен</option>
                            <option value="0">не активен</option>
                        </select>
                    </th>
                    <th class="text-right" style="width: 150px">
                        <button class="btn btn-primary btn-xs" type="submit" name="submit" data-popup="tooltip"
                                data-popup="tooltip" title="Осуществить поиск"><i class="icon-search4"></i></button>
                        <a class="btn btn-default btn-xs" href="{{url('admin/manufacturers')}}" data-popup="tooltip"
                           data-popup="tooltip" title="Очистить поиск"><i class="icon-eraser2"></i></a>
                    </th>
                </tr>
                {!! Form::close() !!}
                </thead>
                <tbody>
                @if(count($manufacturers))
                    @foreach($manufacturers as $manufacturer)
                        <tr>
                            <td class="text-center">
                                <h6 class="no-margin">
                                    <small class="display-block text-size-small no-margin">{{$manufacturer->id}}</small>
                                </h6>
                            </td>
                            <td class="text-center">
                                <div class="thumbnail" style="max-height: 50px; max-width: 50px; overflow: hidden; margin: 0 auto;">
                                    <div class="thumb">
                                        <img src="{{$manufacturer->image}}" alt="{!! $manufacturer->name !!}"
                                             style="max-height: 50px; max-width: 50px">
                                        <div class="caption-overflow">
										<span>
											<a href="{!! $manufacturer->image !!}" data-popup="lightbox" rel="gallery"
                                               class="btn border-white text-white btn-flat btn-icon btn-rounded"><i
                                                        class="icon-plus3"></i></a>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="{{url('admin/manufacturers/'.$manufacturer->id.'/edit')}}"
                                   class="display-inline-block text-default text-semibold letter-icon-title">{{$manufacturer->name}}</a>
                            </td>
                            <td>
                                <a class="btn btn-link">{{$manufacturer->slug}}</a>
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-sm">
                                    <label>
                                        <input type="checkbox" class="switchery"
                                               @if($manufacturer->is_active) checked="checked"
                                               @endif data-url="{!! route('admin.manufacturers.active', [$manufacturer->id]) !!}">
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                <form method="post" action="{!! url('admin/manufacturers/'.$manufacturer->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit"
                                            onclick="return confirm('Вы уверены, что хотите удалить производителя?')"><i
                                                class="icon-x" data-popup="tooltip" title="Удалить"></i>
                                    </button>
                                </form>
                                <a class="btn btn-default btn-xs pull-right btn-xs"
                                   href="{{url('admin/manufacturers/'.$manufacturer->id.'/edit')}}"><i
                                            class="icon-pen" data-popup="tooltip" title="Изменить"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection