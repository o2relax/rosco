<fieldset class="content-group">
    <div class="form-group @if($errors->has('slug'))has-error has-feedback @endif">
        @input_maker_label('Урл', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('slug', ['type' => 'string'], $manufacturer)
            @if($errors->has('slug'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('slug')}}</span>
            @endif
            <code>сгенерируется автоматически, если оставиь пустым</code>
        </div>
    </div>
    <fieldset class="content-group">
        <legend class="text-bold">Основное изображение</legend>

        <div class="form-group col-xs-12">
            <input type="file" name="image" class="file-input-preview2" data-show-remove="true">
        </div>
    </fieldset>
</fieldset>