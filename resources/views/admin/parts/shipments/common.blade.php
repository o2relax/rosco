<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/media/fancybox.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/editors/summernote/summernote.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') !!}"></script>
<script type="text/javascript"
        src="{!! asset('back/assets/js/plugins/uploaders/fileinput/fileinput.min.js') !!}"></script>

<script>
    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };
    var fileActionSettings = {
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };

    function submitForm(btn) {
        $('#form').submit();
    }

    $(function () {
        $('.summernote').summernote({
            height: 300
        });
        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');
            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }

        $(".file-input-preview2").fileinput({
            browseLabel: 'Выбрать',
            browseIcon: '<i class="icon-file-plus"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: modalTemplate
            },
            initialPreview: [
                @if($shipment && $shipment->def_image)
                    "{{$shipment->def_image}}",
                @endif
            ],
            initialPreviewConfig: [
                    @if($shipment && $shipment->def_image)
                {
                    caption: "{{$shipment->def_image}}",
                    url: '{{route('admin.shipments.main_image.delete', [$shipment->id])}}',
                    showDrag: false
                },
                @endif
            ],
            initialPreviewAsData: true,
            overwriteInitial: true,
            maxFileCount: {{config('image.max_file_count')}},
            maxFileSize: {{config('image.max_size')}},
            previewZoomButtonClasses: previewZoomButtonClasses,
            previewZoomButtonIcons: previewZoomButtonIcons,
            fileActionSettings: fileActionSettings,
            showUpload: false,
            showRemove: false
        });

    });

</script>