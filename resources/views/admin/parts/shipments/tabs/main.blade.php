<fieldset class="content-group">

    <div class="form-group @if($errors->has('code'))has-error has-feedback @endif">
        @input_maker_label('Код', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            @input_maker_create('code', ['type' => 'string'], $shipment)
            @if($errors->has('code'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('code')}}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        @input_maker_label('Отображается', ['class' => 'control-label col-lg-2'])
        <div class="col-md-4">
            <div class="checkbox checkbox-switchery switchery-xs switchery-double">
                <label>
                    Выкл
                    <input type="checkbox" name="is_active" value="1"
                           class="switchery" {{old_input('is_active', $shipment) ? 'checked' : ''}}>
                    Вкл
                </label>
            </div>
        </div>
    </div>
    <div class="form-group @if($errors->has('payments'))has-error has-feedback @endif">
        @input_maker_label('Способы оплат', ['class' => 'control-label col-lg-2'])
        <div class="col-md-6">
            @foreach($payments as $payment)
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="styled" name="payments[]" value="{{$payment->id}}"
                               @if(in_array($payment->id, old('payments', isset($chosen_payments) ? $chosen_payments : []))) checked="checked" @endif>
                        {{$payment->name}}
                    </label>
                </div>
            @endforeach
            @if($errors->has('payments'))
                <div class="form-control-feedback">
                    <i class="icon-cancel-circle2"></i>
                </div>
                <span class="help-block">{{$errors->first('payments')}}</span>
            @endif
        </div>
    </div>
    <fieldset class="content-group">
        <legend class="text-bold">Основное изображение</legend>

        <div class="form-group col-xs-12">
            <input type="file" name="image" class="file-input-preview2" data-show-remove="true">
        </div>
    </fieldset>
</fieldset>