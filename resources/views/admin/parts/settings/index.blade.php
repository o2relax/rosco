@extends('admin.layouts.dashboard')

@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') !!}"></script>
    <script>
        $(function () {
            $(".touchspin-no-mousewheel").TouchSpin({
                mousewheel: false
            });
            $(".file-styled-primary").uniform({
                fileButtonClass: 'action btn bg-blue'
            });
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
                    $('.fab-menu-bottom-left, .fab-menu-bottom-right').addClass('reached-bottom');
                }
                else {
                    $('.fab-menu-bottom-left, .fab-menu-bottom-right').removeClass('reached-bottom');
                }
            });
        });
    </script>
@stop
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">Найстройки сайта</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                <form class="form" id="form" action="{!! route('admin.settings.save') !!}" method="post"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                        @foreach($parts as $part)
                            <li class="{{$part->id == 1 ? 'active' : ''}}">
                                <a href="#setting_{{$part->id}}" data-toggle="tab">{{$part->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($parts as $part)
                            <div class="tab-pane {{$part->id == 1 ? 'active' : ''}}"
                                 id="setting_{{$part->id}}">
                                <fieldset class="content-group">
                                    <legend class="text-bold">{{$part->name}}</legend>
                                    @foreach($part->settings as $setting)
                                        <div class="form-group row @if($errors->has($setting->code))has-error has-feedback @endif">
                                            <label class="control-label col-lg-2 text-semibold">
                                                {{$setting->name}}
                                                <code>{{$setting->description}}</code>
                                            </label>
                                            <div class="col-lg-7">
                                                @includeIf('admin.parts.settings.types.' . ($setting->type ? $setting->type : 'default'))
                                                @if($errors->has($setting->code))
                                                    <div class="form-control-feedback">
                                                        <i class="icon-cancel-circle2"></i>
                                                    </div>
                                                    <span class="help-block">{{$errors->first($setting->code)}}</span>
                                                @endif
                                            </div>
                                            <div class="col-lg-3">
                                                <code>{{$setting->code}}</code>
                                            </div>
                                        </div>
                                    @endforeach
                                </fieldset>
                            </div>
                        @endforeach

                    </div>
                </form>
            </div>
        </div>
        <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
            <li style="display: inline">
                <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" data-popup="tooltip"
                   title="Сохранить" onclick="$('#form').submit();">
                    <i class="fab-icon-open icon-floppy-disk"></i>
                </a>
            </li>
        </ul>
    </div>

@stop