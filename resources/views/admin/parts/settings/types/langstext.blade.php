<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
    @foreach(app('shared')->get('languages') as $language)
        <li class="{{$language->code == cfg('language', 'code') ? 'active' : ''}}">
            <a href="#{{$setting->code}}_{{$language->code}}" data-toggle="tab"><img src="{{$language->icon}}"
                                                                           alt=""> {{$language->name}}</a>
        </li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach(app('shared')->get('languages') as $language)
        <div class="tab-pane {{$language->code == cfg('language', 'code') ? 'active' : ''}}"
             id="{{$setting->code}}_{{$language->code}}">
            <fieldset class="content-group">
                <input type="text" class="form-control" name="{{$setting->code}}[{{$language->code}}]" placeholder="" value="{!! old($setting->code) ? old($setting->code) : $setting->value[$language->code] !!}">
            </fieldset>
        </div>
    @endforeach
</div>
