@foreach(json_decode($setting->options, true) as $key => $option)
    <div class="radio-inline">
        <label>
            <input type="radio" name="{{$setting->code}}" class="styled" {!! $setting->value == $key ? 'checked="checked"' : '' !!} value="{{$key}}">
            {{$option}}
        </label>
    </div>
@endforeach