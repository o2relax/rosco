<select name="{{$setting->code}}" class="form-control">
    @if($setting->options)
        @foreach(json_decode($setting->options) as $key => $option)
            <option value="{{$key}}" {{$setting->value == $key ? 'selected' : ''}}>{{$option}}</option>
        @endforeach
    @elseif($setting->relation)
        @foreach($setting->relation::all() as $option)
            <option value="{{$option->id}}" {{$setting->value == $option->id ? 'selected' : ''}}>{{$option->name}}</option>
        @endforeach
    @endif
</select>