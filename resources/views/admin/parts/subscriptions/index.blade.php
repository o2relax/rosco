@extends('admin.layouts.dashboard')

@section('stylesheets')

@endsection
@section('javascript')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>

@endsection
@section('content')
    <h6 class="content-group text-semibold">

    </h6>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Список подписок<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a>
            </h6>
        </div>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center col-md-1">ID</th>
                    <th>Email</th>
                    <th>Создана</th>
                </tr>
                </thead>
                <tbody>
                @if (count($subscriptions))
                 @foreach($subscriptions as $subscription)
                     <tr>
                         <td class="text-center col-md-1">{{$subscription->id}}</td>
                         <td>{{$subscription->email}}</td>
                         <td>{{$subscription->created_at}}</td>
                     </tr>
                @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection