<div class="sidebar-user">
    <div class="category-content">
        <div class="media">
            <a href="#" class="media-left"><img src="{{ asset('back/assets/images/placeholder.jpg') }}"
                                                class="img-circle img-sm" alt=""></a>
            <div class="media-body">
                <span class="media-heading text-semibold">{{auth()->user()->name}}</span>
                <div class="text-size-mini text-muted">
                    <i class="icon-pin text-size-small"></i> &nbsp;{{auth()->user()->email}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            <li class="navigation-header"><span>Администрация</span></li>

            <li class="{{is_active_route('admin/dashboard') ? 'active' : ''}}">
                <a href="{!! url('admin/dashboard') !!}"><i
                            class="icon-home4"></i> <span>Главная</span></a></li>
            <li class="{{is_active_route('admin/users') ? 'active' : ''}}">
                <a href="{!! url('admin/users') !!}"><i
                            class="icon-users2"></i> <span>Сотрудники</span></a></li>
            <li class="{{is_active_route('admin/roles') ? 'active' : ''}}">
                <a href="{!! url('admin/roles') !!}"><i
                            class="icon-lock4"></i> <span>Профили</span></a></li>

            <li class="navigation-header"><span>Магазин</span></li>

            <li class="{{is_active_route('admin/categories') ? 'active' : ''}}">
                <a href="{!! url('admin/categories') !!}"><i
                            class="icon-list"></i> <span>Категории</span></a></li>

            <li class="{{is_active_route('admin/products') ? 'active' : ''}}">
                <a href="{!! url('admin/products') !!}"><i
                            class="icon-database"></i> <span>Товары</span></a></li>

            <li class="{{is_active_route('admin/attributes') ? 'active' : ''}}">
                <a href="#" class="has-ul"><i
                            class="icon-more"></i> <span>Атрибуты</span></a>
                <ul>
                    <li class="{{is_active_route('admin/attributes/groups') ? 'active' : ''}}">
                        <a href="{!! url('admin/attributes/groups') !!}"> Группы атрибутов</a></li>
                    <li class="{{is_active_route('admin/attributes') && !is_active_route('admin/attributes/groups') ? 'active' : ''}}">
                        <a href="{!! url('admin/attributes') !!}"> Атрибуты</a></li>
                </ul>
            </li>
            <li class="{{is_active_route('admin/manufacturers') ? 'active' : ''}}">
                <a href="{!! url('admin/manufacturers') !!}"><i
                            class="icon-cabinet"></i> <span>Производители</span></a></li>
            <li class="{{is_active_route('admin/discounts') ? 'active' : ''}}">
                <a href="{!! url('admin/discounts') !!}"><i
                            class="icon-percent"></i> <span>Скидки</span></a></li>
            <li class="{{is_active_route('admin/shipments') ? 'active' : ''}}">
                <a href="{!! url('admin/shipments') !!}"><i
                            class="icon-truck"></i> <span>Способы доставки</span></a></li>
            <li class="{{is_active_route('admin/payments') ? 'active' : ''}}">
                <a href="{!! url('admin/payments') !!}"><i
                            class="icon-credit-card2"></i> <span>Способы оплаты</span></a></li>

            <li class="navigation-header"><span>Коммерция</span></li>
            <li class="{{is_active_route('admin/orders') ? 'active' : ''}}">
                <a href="{!! url('admin/orders') !!}"><i
                            class="icon-clipboard6"></i> <span>Заказы</span></a></li>
            <li class="{{is_active_route('admin/customers') ? 'active' : ''}}">
                <a href="{!! url('admin/customers') !!}"><i
                            class="icon-users4"></i> <span>Покупатели</span></a></li>
            <li class="{{is_active_route('admin/reviews') ? 'active' : ''}}">
                <a href="{!! url('admin/reviews') !!}"><i
                            class="icon-bubble9"></i> <span>Отзывы</span></a></li>
            <li class="{{is_active_route('admin/subscriptions') ? 'active' : ''}}">
                <a href="{!! url('admin/subscriptions') !!}"><i
                            class="icon-mention"></i> <span>Подписки</span></a></li>

            <li class="navigation-header"><span>Сайт</span></li>
            <li class="{{is_active_route('admin/pages') ? 'active' : ''}}">
                <a href="{!! url('admin/pages') !!}"><i
                            class="icon-stack"></i> <span>Страницы</span></a></li>
            <li class="{{is_active_route('admin/blog') ? 'active' : ''}}">
                <a href="{!! url('admin/blog') !!}"><i
                            class="icon-pencil3"></i> <span>Блог</span></a></li>

            <li class="navigation-header"><span>Система</span></li>
            <li class="{{is_active_route('admin/modules') ? 'active' : ''}}">
                <a href="{!! url('admin/modules') !!}"><i
                            class="icon-grid6"></i> <span>Модули</span></a></li>
            <li class="{{is_active_route('admin/menus') ? 'active' : ''}}">
                <a href="{!! url('admin/menus') !!}"><i
                            class="icon-stairs"></i> <span>Меню</span></a></li>
            <li class="{{is_active_route('admin/currencies') ? 'active' : ''}}">
                <a href="{!! url('admin/currencies') !!}"><i
                            class="icon-coin-dollar"></i> <span>Валюты</span></a></li>
            <li class="{{is_active_route('admin/languages') ? 'active' : ''}}">
                <a href="{!! url('admin/languages') !!}"><i
                            class="icon-earth"></i> <span>Языки</span></a></li>
            <li class="{{is_active_route('admin/settings') ? 'active' : ''}}">
                <a href="{!! url('admin/settings') !!}"><i
                            class="icon-cogs"></i> <span>Настройки</span></a></li>
        </ul>
    </div>
</div>
