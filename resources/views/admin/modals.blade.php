<div class="modal fade" id="deleteModal" tabindex="-3" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="deleteModalLabel">Удаление товара</h4>
            </div>
            <div class="modal-body">
                <p>Вы уверены, что хотите удалить данный товар?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <a id="deleteBtn" type="button" class="btn btn-danger" href="#">Удалить</a>
            </div>
        </div>
    </div>
</div>