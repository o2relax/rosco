@extends('admin.layouts.navigation')

@section('page-content')

    <div class="sidebar sidebar-main">
        <div class="sidebar-content">
            @include('admin.menu')
        </div>
    </div>

    <div class="content-wrapper">
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                                class="text-semibold">@lang('admin.titles.' . request()->segment(2)) @lang('admin.titles.' . request()->segment(4))</span></h4>
                </div>
            </div>
            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="{!! route('admin.dashboard') !!}"><i class="icon-home2 position-left"></i> Главная</a></li>
                    @yield('breadcrumbs')
                    <li class="active">@lang('admin.titles.' . request()->segment(2))</li>
                </ul>
            </div>
        </div>

        <div class="content">
            @yield('content')

            <div class="footer text-muted">
                Designed by <a href="https://www.startlightsoft.com/" target="_blank">StartLightSoft.com</a> &copy; {{date('Y')}}
            </div>

        </div>
    </div>
@stop
