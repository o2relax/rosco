@extends('admin.layouts.master')

@section('navigation')
    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{route('admin.dashboard')}}">
                <img src="{{ asset('back/assets/images/logo_light.png') }}" alt=""></a>
            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
                </li>
            </ul>
            <p class="navbar-text">
                <a onclick="clearAllCache();" class="label btn-danger">Очистить весь кеш</a>
            </p>
            <p class="navbar-text">
                <a href="{{route('shop.main')}}" class="label btn-info">Вернуться на сайт</a>
            </p>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('back/assets/images/placeholder.jpg') }}" alt="">
                        <span>{{auth()->user()->name}}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="divider"></li>
                        <li><a href="{!! route('logout') !!}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="icon-switch2"></i> Выход</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <form id="logout-form" action="{!! route('admin.logout') !!}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
    <!-- /main navbar -->
@stop