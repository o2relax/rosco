<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', 'Админ-панель')</title>
    
    <link href="//fonts.googleapis.com/css-family=Roboto-400,300,100,500,700,900.css') !!}"  rel="stylesheet" type="text/css">
    <link href="{!! asset('back/assets/css/icons/icomoon/styles.css') !!}"  rel="stylesheet" type="text/css">
    <link href="{!! asset('back/assets/css/bootstrap.css') !!}"  rel="stylesheet" type="text/css">
    <link href="{!! asset('back/assets/css/core.css') !!}"  rel="stylesheet" type="text/css">
    <link href="{!! asset('back/assets/css/components.css') !!}"  rel="stylesheet" type="text/css">
    <link href="{!! asset('back/assets/css/colors.css') !!}"  rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/loaders/pace.min.js') !!}" ></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/core/libraries/jquery.min.js') !!}" ></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/core/libraries/bootstrap.min.js') !!}" ></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/loaders/blockui.min.js') !!}" ></script>

    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}" ></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/core/app.js') !!}" ></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/pages/login.js') !!}" ></script>

</head>
<body class="login-container bg-slate-800">

<div class="page-container">
    <div class="page-content">
        <div class="content-wrapper">
            <div class="content">
                @yield('content')
            </div>
        </div>
    </div>
</div>

</body>
</html>
