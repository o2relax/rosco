<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>AdminPanel: @lang('admin.titles.' . request()->segment(2))</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('back/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/custom.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

@yield('stylesheets')

<!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/notifications/pnotify.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>

    <script type="text/javascript" src="{{ asset('back/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/common.js') }}"></script>

    <!-- /theme JS files -->

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            @if (session('success'))
            new PNotify({
                text: '{{ session('success') }}',
                addclass: 'bg-success'
            });
            @endif
            @if (session('error'))
            new PNotify({
                text: '{{ session('error') }}',
                addclass: 'bg-danger'
            });
            @endif
            @if (session('errors'))
            @foreach ($errors->all() as $error)
            new PNotify({
                text: '{{ $error }}',
                addclass: 'bg-danger'
            });
            @endforeach
            @endif
            @if (session('notification'))
            new PNotify({
                text: '{{ session('notification') }}',
                addclass: 'bg-warning {{ session('notificationType') }}'
            });
            @endif
            @if (session('message'))
            new PNotify({
                text: '{{ session('message') }}',
                addclass: 'bg-teal-400'
            });
            @endif
            $(".styled").uniform({
                radioClass: 'choice'
            });
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
                    $('.fab-menu-bottom-left, .fab-menu-bottom-right').addClass('reached-bottom');
                }
                else {
                    $('.fab-menu-bottom-left, .fab-menu-bottom-right').removeClass('reached-bottom');
                }
            });
        });
        function clearAllCache() {
            $.ajax({
                url: '{{route('admin.cache.clear')}}',
                method: 'post',
                success: function (response) {
                    new PNotify({
                        text: '@lang('messages.action.success')',
                        addclass: 'bg-teal-400'
                    });
                }
            });
        }
    </script>

</head>
<body>

@include('admin.layouts.loading-overlay')

@yield("navigation")

<div class="page-container">
    <div class="page-content">
        @yield("page-content")
    </div>
</div>
@yield("javascript")
</body>
</html>