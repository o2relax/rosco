@if ($paginator->hasPages())
    <div class="item-list">
        <ul class="pager">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="pager-current">&laquo;</li>
        @else
            <li class="pager-prev"><a href="{{ $paginator->previousPageUrl() }}" rel="prev"> @lang('pagination.previous') </a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="pager-current first">{{ $element }}</li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pager-current">{{ $page }}</li>
                    @else
                        <li class="pager-item"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pager-next"><a href="{{ $paginator->nextPageUrl() }}" rel="next"> @lang('pagination.next') </a></li>
        @else
            <li class="pager-current"> @lang('pagination.next') </li>
        @endif
    </ul>
    </div>
@endif
