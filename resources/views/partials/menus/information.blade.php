<div class="info_title">
    <h5>@lang('shop.menus.' . $menu->code)</h5>
</div>
<div class="info_links">
    @foreach($menu->links as $item)
        <a href="{{$item->url}}">{!! $item->title !!}</a>
    @endforeach
</div>
