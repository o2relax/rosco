@foreach($menu->links as $item)
    <li class="@if(url()->current() == url($item->url)) active @endif">
        <a href="{{$item->url}}">{!! $item->title !!}</a>
    </li>
@endforeach