<div class="news">
    <div class="container">
        <div class="title">
            <h5>{{lang($config['heading'])}}</h5>
        </div>
        <div class="news_columns">
            @foreach($data as $item)
            <div class="news_column">
                <div class="news_img">
                    <img src="{{$item->image}}" alt="{{$item->title}}">
                </div>
                <div class="news_title">
                    <h4>{{$item->title}}</h4>
                </div>
                <div class="news_text">
                    <p>{{$item->short_description}}</p>
                </div>
                <div class="news_link">
                    <a href="{{$item->link}}">@lang('shop.blog.read_more')</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>