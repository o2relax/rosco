<div class="slider_bar">
        <div class="slider_bar-slider" >
                @if(count($config))
                        @foreach($config as $slide)
                                <div class="item-slider">
                                                <div class="slider_bar-photo">
                                                        <img data-lazy="{{module_file($code, $slide['file'])}}" alt="">
                                                </div>
                                </div>
                        @endforeach
                @endif
        </div>
</div>