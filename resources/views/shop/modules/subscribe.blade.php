@section('javascripts')
    @parent
    <script>
        function subscribe() {

            $.ajax({
                url: '{{route('shop.subscribe')}}',
                method: 'post',
                data: 'email=' + $('input[name="subscribe"]').val(),
                dataType: 'json',
                success: function(response) {
                    $('.subscription_form > form > div').text(response.message);
                }
            });

            return false;
        }
    </script>
@show
<div class="subscription_form">
    <div class="title">
        <h5>{{lang($config['heading'])}}</h5>
    </div>
    <form>
        <div>
            <input name="subscribe" type="text" placeholder="@lang('shop/auth.placeholders.email')">
            <a onclick="subscribe();"><img src="{{asset('front/assets/img/forms-arrow.png')}}" alt="arrow">
            </a>
        </div>
    </form>
</div>