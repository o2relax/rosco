<div class="about-shop">
    <div class="container">
        <div class="about-shop-title">
            <h5>{{lang($config['heading'])}}</h5>
        </div>
        <div class="about-shop-columns">
            <div class="column">
                <p>{{lang($config['text_left'])}}</p>
            </div>
            <div class="column">
                <p>{{lang($config['text_right'])}}</p>
            </div>
        </div>
    </div>
</div>