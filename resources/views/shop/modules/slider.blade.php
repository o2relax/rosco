<div class="slider">
    @foreach($config as $slide)
    <div class="item-slid" style="background: url('{{module_file($code, $slide['file'])}}') no-repeat center; background-size: cover;">
        <div class="slids-content">
            <div class="content">
                <h3>{{lang($slide['title'])}}
                </h3>
                <a href="{{$slide['link']}}">@lang('shop.modules.slider.btn')</a>
            </div>
        </div>
    </div>
    @endforeach
</div>



