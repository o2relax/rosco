<div class="delivery-column">
            <span>
                @icon('svg/money')
            </span>
    <div>
        <h5>{{lang($config['heading'])}}</h5>
        <p>{{lang($config['text'])}}</p>
    </div>
</div>