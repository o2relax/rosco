<div class="shares">
    <div class="container">
        <div class="title-lines">
            <div class="line"></div>
            <h4>{{lang($config['heading'])}}</h4>
            <div class="line"></div>
        </div>
        <div class="product-slider">
            @if(count($data))
                @foreach($data as $product)
                    <div class="item-slider">
                        <div class="product-card">
                            <div class="product-price">
                                <span>{{$product->price}}</span>
                            </div>
                            <div class="product-photo">
                                <img data-lazy="{{$product->image}}" alt="{{$product->name}}">
                            </div>
                            <div class="product-info">
                                <div class="product-name">
                                    <a href="{{$product->link}}">{{$product->name}}</a>
                                </div>
                                <div class="product-characteristics">
                                    <p>{{$product->short_description}}</p>
                                </div>
                                <div class="button">
                                    {!! cart()->addToCartBtn($product->id, 'btn') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>