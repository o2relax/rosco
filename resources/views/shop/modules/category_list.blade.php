<div class="catalog">
    <div class="title">
        <h3>{{lang($config['heading'])}}</h3>
    </div>

    <form action="{{url()->current()}}">

        <input id="category"
               type="hidden"
               name="filter[category_id]"
               value="{{request()->input('filter.category_id')}}">

        <div class="catalog-filter">
            @foreach($data as $item)
                @if($item->parent_id)
                    <div class="form-check">
                        <input type="checkbox"
                               class="form-check-category-input"
                               data-id="{{$item->id}}"
                               onchange="checkCategory()"
                               id="categoryCheck{{$item->id}}">
                        <label class="form-check-label" for="categoryCheck{{$item->id}}">{{$item->name}}</label>
                    </div>
                @else
                    <h4>{{$item->name}}</h4>
                @endif
                {{--<a class="{{url()->full() == $item->link ? 'active' : ''}}"--}}
                {{--href="{{$item->link}}">{{$item->name}}</a>--}}
            @endforeach
        </div>
        <div class="detailed-filters">
            <button class="btn">применить</button>
        </div>
    </form>

</div>
@section('javascripts')
    @parent
    <script>
        $(function () {
            $('.catalog-filter').find('a').each(function () {
                var url = $(this).attr('href'),
                    current = window.location.href;
                if (url === current) {
                    $(this).addClass('active');
                }
            });
        });

        function checkCategory() {
            var categoryInput = document.querySelector('#category');
            var categoryFilter = document.querySelector('.catalog-filter');
            var checked = categoryFilter.querySelectorAll('.form-check-category-input');
            var i = 0;
            var a = 0;
            var result = checked.length;
            var arr = [];
            for (; i < result; i++) {
                if (checked[i].checked === true) {
                    arr[a] = checked[i].getAttribute('data-id');
                    a++;
                }
            }
            categoryInput.value = arr.join(',');
        }
    </script>
@stop