<div class="catalog-categories">
    @foreach($data as $item)
        <style>
            .catalog-categories .card-{{$item->id}}:hover {
                background: {{$item->color}};
                box-shadow: 0 10px 25px -10px {{$item->color}};
            }
            .catalog-categories .card-{{$item->id}} a span svg {
                fill: {{$item->color}};
            }
        </style>
        <div class="catalog-categories-cards card-{{$item->id}}">
            <a href="{{$item->link}}">
                <span>
                    {{$item->image}}
                </span>
                <h5>{{$item->name}}</h5>
            </a>
        </div>
    @endforeach
</div>