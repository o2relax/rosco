<div class="categories">
    <div class="categories-title">
        <h4>{{lang($config['heading'])}}</h4>
    </div>
    <div class="categories-cards">
        @foreach($data as $item)
        <div class="card">
                <span>
                   {{$item->image}}
                </span>
            <a href="{{$item->link}}">{{$item->name}}</a>
        </div>
        @endforeach
    </div>
</div>