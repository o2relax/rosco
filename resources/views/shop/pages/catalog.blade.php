@extends('shop.layouts.catalog')

@section('title') {{cfg('catalog_title', 'langs')}} @endsection
@section('description') {{cfg('catalog_description', 'langs')}} @endsection
@section('keywords') {{cfg('catalog_keywords', 'langs')}} @endsection
@section('h1') {{cfg('catalog_h1', 'langs')}} @endsection

@section('content_top')

    @module('slider_bar')

    @module('category_tiles')

@endsection

@section('content')

    @include('shop.partials.products')

@endsection