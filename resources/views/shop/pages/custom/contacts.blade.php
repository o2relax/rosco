@extends('shop.layouts.product')

@section('title') {{$page->seo_title}} @endsection
@section('description') {{$page->seo_description}} @endsection
@section('keywords') {{$page->seo_keywords}} @endsection
@section('h1') {{$page->title}} @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')
    <div class="page-container">
        <div class="left">
            {!! $page->description !!}
        </div>
        <div class="right">
            @module('feedback')
        </div>
    </div>

    {{--<script type="text/javascript" charset="utf-8" async--}}
    {{--src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Addf8dc70f6a7896bf4fc25cd58d58c80d1e01f12f5b4ca18d61c3c9cb5597199&amp;width=100%25&amp;height=328&amp;lang=ru_UA&amp;scroll=true"></script>--}}
    <div id="google-map" style="height:350px; width:100%;"></div>
    <script>
        function initMap() {

            var map = new google.maps.Map(document.getElementById('google-map'), {
                zoom: 15,
                center: {lat:46.46542247,lng: 30.75769211}
            });
            var marker = new google.maps.Marker({
                position: {lat: 50.41051279, lng: 30.54537772},
                map: map,
                title: "Agrolan"
            });
            var marker2 = new google.maps.Marker({
                position: {lat:46.41937708,lng: 30.28794633},
                map:map,
                title: "Agrolan"
            });
            var marker3 = new google.maps.Marker({
                position: {lat:46.46542247,lng: 30.75769211},
                map:map,
                title: "ROSCO"
            });

            var contentString = '<div id="content">'+
                '<p>Agrolan</p>'+
                '</div>';
            var contentString2 = '<div id="content">'+
                '<p>ROSCO</p>'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            var infowindow2 = new google.maps.InfoWindow({
                content: contentString
            });
            var infowindow3 = new google.maps.InfoWindow({
                content: contentString2
            });


            infowindow3.open(map,marker3);

        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXETpc6nsmEIc8ZS5JY48MuUubsdYetO8&callback=initMap">
    </script>
@endsection