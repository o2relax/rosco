@extends('shop.layouts.product')

@section('title') @lang('shop.cart.heading') @endsection
@section('description')  @endsection
@section('keywords')  @endsection
@section('h1') @lang('shop.cart.heading') @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')
    {!! Form::model(old(), [
            'route' => ['shop.checkout'],
            'id' => 'form',
            'method' => 'post'
            ]) !!}
    <div class="cart-container">
        <div class="title-container">
            <div class="title-lines">
                <div class="line"></div>
                <h1> @yield('h1') </h1>
                <div class="line"></div>
            </div>
        </div>
        @include('shop.includes.cart')
    </div>
    @include('shop.partials.checkout_form')
    {!! Form::close() !!}
@endsection