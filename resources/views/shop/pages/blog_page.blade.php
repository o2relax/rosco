@extends('shop.layouts.product')

@section('title') {{$post->seo_title}} @endsection
@section('description') {{$post->seo_description}} @endsection
@section('keywords') {{$post->seo_keywords}} @endsection
@section('h1') {{$post->title}} @endsection

@section('breadcrumbs')
    @parent
    <a href="{{route('shop.blog')}}">@lang('shop.blog.heading') <span>&raquo;</span></a>
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')

    <div class="page-container">
        <div class="title-container">
            <div class="title-lines">
                <div class="line"></div>
                <h1> @yield('h1') </h1>
                <div class="line"></div>
            </div>
        </div>
    </div>
    <div class="page-container">
        <div class="page-content">
            {!! $post->description !!}
        </div>
    </div>
    <div id="disqus_thread"></div>
    <script>

        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://pluacomua.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection