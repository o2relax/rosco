@extends('shop.layouts.product')

@section('title') @lang('shop/checkout.heading') @endsection
@section('description')  @endsection
@section('keywords')  @endsection
@section('h1') @lang('shop/checkout.heading') @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')

        <div class="checkout-success-container">
            <div class="title-container">
                <div class="title-lines">
                    <div class="line"></div>
                    <h1> {{$heading}} </h1>
                    <div class="line"></div>
                </div>
            </div>
            <p>{!! $message !!}</p>
            <a class="btn" href="{{route('shop.profile.orders')}}">@lang('shop/checkout.btn')</a>
        </div>

@endsection