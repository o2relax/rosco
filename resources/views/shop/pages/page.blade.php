@extends('shop.layouts.product')

@section('title') {{$page->seo_title}} @endsection
@section('description') {{$page->seo_description}} @endsection
@section('keywords') {{$page->seo_keywords}} @endsection
@section('h1') {{$page->title}} @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')
    <div class="page-container">
       {!! $page->description !!}
    </div>
@endsection