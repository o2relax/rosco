@extends('shop.layouts.product')

@section('title') {{$product->seo_title}} @endsection
@section('description') {{$product->seo_description}} @endsection
@section('keywords') {{$product->seo_keywords}} @endsection
@section('h1') {{$product->name}} @endsection

@section('breadcrumbs')
    @parent
    <a href="{{route('shop.catalog')}}">@lang('shop/common.catalog_page') <span>&raquo;</span></a>
    <a href="{{route('shop.category', [$product->category->slug])}}">{{$product->category->name}}
        <span>&raquo;</span></a>
    <a href="{{$product->link}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')

    <div class="general-description-product">
        <div class="product-photos">
            <div class="big-photos">
                @foreach($product->big_images as $big)
                    <div class="container-photo">
                        <div class="photo">
                            <div>
                                <img data-lazy="{{$big}}" alt="{{$product->name}} @lang('shop/product.image.main_alt')">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="small-photos">
                @foreach($product->small_images as $small)
                    <div class="container-photo">
                        <div class="photo">
                            <div>
                            <img data-lazy="{{$small}}" alt="{{$product->name}} @lang('shop/product.image.ext_alt')">
                                </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="product-description">
            <div class="product-name">
                <h2>@yield('h1')</h2>
            </div>
            <div class="presence">
                <span>{{$product->status()->name}}</span>
            </div>
            <div class="about-product">
                <p>{!!$product->short_description!!}</p>
            </div>
            @if(!empty($product->substance))
            <div class="active-substance">
                <h5>@lang('shop/product.substance') <span>{{$product->substance}}</span></h5>
            </div>
            @endif
            <div class="vendor-code">
                <h5>@lang('shop/product.article') <span>{{$product->article}}</span></h5>
            </div>
            <div class="quantity">
                <label>@lang('shop/product.quantity')</label>
                @if($product->status_id == 3 )
                    <input type="number" value="0" class="quantity-input" disabled>
                @else
                    <input type="number" min="1" value="1" class="quantity-input">
                @endif
            </div>
            <div class="price">
                <div class="actual-price">
                    <span>{{$product->price}}</span>
                </div>
                @if($product->old_price)
                    <div class="not-actual-price">
                        <span>{{$product->old_price}}</span>
                    </div>
                @endif
            </div>
            <div class="button">
                {!! cart()->addToCartBtn($product->id, 'btn') !!}
            </div>
        </div>
    </div>
    <div class="detailed-description-product" id="reviews">
        <div class="tabs">
            <div class="description-tab {{!$tab ? 'active' : ''}}">@lang('shop/product.description')</div>
            @if(count($attributes))
                <div class="characteristics-tab">@lang('shop/product.attributes')</div>
            @endif
            <div class="reviews-tab {{$tab == 'review' ? 'active' : ''}}">@lang('shop/product.reviews')</div>
        </div>
        <div class="containers">
            <div class="description-container {{!$tab ? 'active' : ''}} animated fadeIn tab-cont">
                {!! $product->full_description !!}
            </div>
            <div class="reviews-container {{$tab == 'review' ? 'active' : ''}} animated fadeIn tab-cont">
                @include('shop.partials.reviews')
            </div>
            @if(count($attributes))
                <div class="characteristics-container animated fadeIn tab-cont">
                    <table>
                        <thead>
                        <tr>
                            <th>@lang('shop/product.attribute.title')</th>
                            <th>@lang('shop/product.attribute.value')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attributes as $attribute)
                            <tr>
                                <td>
                                    {{$attribute->attribute->name}}
                                </td>
                                <td>
                                    {{$attribute->text}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
@endsection