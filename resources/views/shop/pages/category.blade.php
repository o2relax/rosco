@extends('shop.layouts.catalog')

@section('title') {{$category->seo_title}} @endsection
@section('description') {{$category->seo_description}} @endsection
@section('keywords') {{$category->seo_keywords}} @endsection
@section('h1') {{$category->name}} @endsection

@section('content')

    @include('shop.partials.products')

@endsection