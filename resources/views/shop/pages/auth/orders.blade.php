@extends('shop.layouts.profile')

@section('title') @lang('shop/profile.headings.main') @endsection
@section('description')  @endsection
@section('keywords')  @endsection
@section('h1') @lang('shop/profile.headings.main') @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')
    @if(count($orders))
    <table>
        <thead>
        <tr>
            <th></th>
            <th>@lang('shop/profile.heads.status')</th>
            <th>@lang('shop/profile.heads.products')</th>
            <th>@lang('shop/profile.heads.total')</th>
            <th>@lang('shop/profile.heads.date')</th>
        </tr>
        </thead>
        <tbody>

        @foreach($orders as $order)
            <tr>
                <td>#{{$order->id}}</td>
                <td>
                    <span class="status_{{$order->status_id}}">
                        {{collect($statuses)->where('id', $order->status_id)->first()->name}}
                    </span>
                </td>
                <td>
                    @foreach($order->items as $item)
                        <div>{!! $item->product ? $item->product->name : trans('shop/profile.product_deleted') !!} <span>x{{$item->quantity}}</span></div>
                    @endforeach
                </td>
                <td>{{$order->total}}</td>
                <td>{{$order->date}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
        <div class="pagination-orders">
            {{$orders->links()}}
        </div>

    @else

    @endif
@stop

