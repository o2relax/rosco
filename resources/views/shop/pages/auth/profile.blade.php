@extends('shop.layouts.profile')

@section('title') @lang('shop/profile.headings.main') @endsection
@section('description')  @endsection
@section('keywords')  @endsection
@section('h1') @lang('shop/profile.headings.main') @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')
    @if(session('message'))
        <p>{{session('message')}}</p>
    @else
    <form role="form" method="POST" action="{{ route('shop.profile.password.update') }}">
        {{ csrf_field() }}
        <div class="panel panel-body login-form">
            <div class="form-group has-feedback has-feedback-left">
                <input type="password" id="old_password" class="form-control"
                       placeholder="@lang('shop/profile.placeholders.old_password')" name="old_password"
                       required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                @if($errors->has('old_password'))
                    <label id="password-error" class="validation-error-label"
                           for="password">{{ $errors->first('old_password') }}</label>
                @endif
            </div>
            <div class="form-group has-feedback has-feedback-left">
                <input type="password" id="password" class="form-control"
                       placeholder="@lang('shop/profile.placeholders.password')" name="password"
                       required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                @if($errors->has('password'))
                    <label id="password-error" class="validation-error-label"
                           for="password">{{ $errors->first('password') }}</label>
                @endif
            </div>
            <div class="form-group has-feedback has-feedback-left">
                <input type="password" id="password_confirmation" class="form-control"
                       placeholder="@lang('shop/profile.placeholders.password_confirmation')"
                       name="password_confirmation"
                       required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                @if($errors->has('password_confirmation'))
                    <label id="password-error" class="validation-error-label"
                           for="password">{{ $errors->first('password_confirmation') }}</label>
                @endif
            </div>
            <div class="form-group">
                <button type="submit"
                        class="btn bg-blue btn-block">@lang('shop/profile.buttons.change_password') <i
                            class="icon-circle-right2 position-right"></i></button>
            </div>
        </div>
        <div id="np-tracking" class="np-w-br-0" style="min-height: 322px; left: 30%; width: 40%; margin-top : 20px;"> <div id="np-first-state"> <div id="np-tracking-logo"></div> <div id="np-title"> <div class="np-h1"> <span class="np-h1-span1">ОТСЛЕЖИВАНИЕ</span> <br> <span class="np-h1-span2">ПОСЫЛОК</span> </div> </div> <div id="np-input-container"> <div id="np-clear-input"></div> <input id="np-user-input" type="text" name="number" placeholder="Номер посылки"> </div> <div id="np-warning-message"></div> <button id="np-submit-tracking" type="button" disabled="" style="background-color: rgb(209, 213, 218); border: 1px solid rgb(196, 196, 196);"> <span id="np-text-button-tracking">ОТСЛЕДИТЬ</span> <div id="np-load-image-tracking"></div> </button> <div id="np-error-status"></div> </div> <div id="np-second-state"> <div id="np-status-icon"></div> <div id="np-status-message"></div> <div class="np-track-mini-logo"> <div class="np-line-right"></div> <div class="np-line-left"></div> </div> <a href="#" id="np-more">Подробнее на сайте</a> <div id="np-return-button"> <span id="np-return-button-span">Другая посылка</span> </div> </div> </div>
    </form>
    @endif
@stop

