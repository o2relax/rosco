@extends('shop.layouts.page')

@section('title') @lang('shop/auth.headings.forgot') @endsection
@section('description')  @endsection
@section('keywords')  @endsection
@section('h1') @lang('shop/auth.headings.forgot') @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')
    <div class="checkout-success-container">
        <div class="title-container">
            <div class="title-lines">
                <div class="line"></div>
                <h1> @yield('h1') </h1>
                <div class="line"></div>
            </div>
        </div>
    </div>
    <div class="login-container">
        <div class="center">
            @if(session('message'))
                <p>{{session('message')}}</p>
            @else
            <form role="form" method="POST" action="{{ route('forgot.password.post') }}">
                {{ csrf_field() }}
                <div class="panel panel-body login-form">
                    <div class="form-group has-feedback has-feedback-left">
                        <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}"
                               required
                               autofocus placeholder="@lang('shop/auth.placeholders.email')">
                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                        @if($errors->has('email'))
                            <label id="email-error" class="validation-error-label"
                                   for="email">{{ $errors->first('email') }}</label>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn bg-blue btn-block">@lang('shop/auth.buttons.forgot_post') <i
                                    class="icon-circle-right2 position-right"></i></button>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
@stop

