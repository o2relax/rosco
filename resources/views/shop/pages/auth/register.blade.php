@extends('shop.layouts.page')

@section('title') @lang('shop/auth.headings.register') @endsection
@section('description')  @endsection
@section('keywords')  @endsection
@section('h1') @lang('shop/auth.headings.register') @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')
    <div class="checkout-success-container">
        <div class="title-container">
            <div class="title-lines">
                <div class="line"></div>
                <h1> @yield('h1') </h1>
                <div class="line"></div>
            </div>
        </div>
    </div>
    <div class="login-container">
        <div class="center">
            <form method="POST" action="{{route('register.post')}}">
                {!! csrf_field() !!}

                <div class="form-group has-feedback has-feedback-left">
                    <input type="text" id="name" class="form-control" name="name" value="{{ old('name') }}"
                           required
                           autofocus placeholder="@lang('shop/auth.placeholders.name')">
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                    @if($errors->has('name'))
                        <label id="email-error" class="validation-error-label"
                               for="email">{{ $errors->first('name') }}</label>
                    @endif
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}"
                           required
                           autofocus placeholder="@lang('shop/auth.placeholders.email')">
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                    @if($errors->has('email'))
                        <label id="email-error" class="validation-error-label"
                               for="email">{{ $errors->first('email') }}</label>
                    @endif
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="password" id="password" class="form-control" name="password" value=""
                           required
                           autofocus placeholder="@lang('shop/auth.placeholders.password')">
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                    @if($errors->has('password'))
                        <label id="email-error" class="validation-error-label"
                               for="email">{{ $errors->first('password') }}</label>
                    @endif
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" value=""
                           required
                           autofocus placeholder="@lang('shop/auth.placeholders.password_confirmation')">
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                    @if($errors->has('password_confirmation'))
                        <label id="email-error" class="validation-error-label"
                               for="email">{{ $errors->first('password_confirmation') }}</label>
                    @endif
                </div>

                <div class="form-group">
                    <button class="btn" type="submit">@lang('shop/auth.buttons.register')</button>
                </div>
                <div class="form-group">
                    <a href="{{route('login')}}">@lang('shop/auth.buttons.enter')</a>
                </div>
            </form>
        </div>
    </div>
@stop