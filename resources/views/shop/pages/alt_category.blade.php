@extends('shop.layouts.alt_catalog')

@section('title') {{cfg('catalog_title_' . $page, 'langs')}} @endsection
@section('description') {{cfg('catalog_description_' . $page, 'langs')}} @endsection
@section('keywords') {{cfg('catalog_keywords_' . $page, 'langs')}} @endsection
@section('h1') {{cfg('catalog_h1_' . $page, 'langs')}} @endsection

@section('content')

    @include('shop.partials.products')

@endsection