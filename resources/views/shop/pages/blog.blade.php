@extends('shop.layouts.product')

@section('title') {{cfg('blog_title', 'langs')}} @endsection
@section('description') {{cfg('blog_description', 'langs')}} @endsection
@section('keywords') {{cfg('blog_keywords', 'langs')}} @endsection
@section('h1') {{cfg('blog_h1', 'langs')}} @endsection

@section('breadcrumbs')
    @parent
    <a href="{{url()->current()}}">@yield('h1') <span>&raquo;</span></a>
@stop

@section('content')

    <div class="page-container">
        <div class="title-container">
            <div class="title-lines">
                <div class="line"></div>
                <h1> @yield('h1') </h1>
                <div class="line"></div>
            </div>
        </div>
    </div>
    <div class="page-container">
        <div class="page-content">
            <div class="news">
                <div class="container">
                    <div class="news_columns">
                        @foreach($posts as $item)
                            <div class="news_column">
                                <div class="news_img">
                                    <img src="{{$item->image}}" alt="{{$item->title}}">
                                </div>
                                <div class="news_title">
                                    <h4>{{$item->title}}</h4>
                                </div>
                                <div class="news_text">
                                    <p>{{$item->short_description}}</p>
                                </div>
                                <div class="news_link">
                                    <a href="{{$item->link}}">@lang('shop.blog.read_more')</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection