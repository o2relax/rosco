<footer class="footer">
    <div class="container">

        <div class="top_footer">
            {{--<div class="info">--}}
                {{--@menu('information')--}}
            {{--</div>--}}
            <div class="account">
                @menu('account')
            </div>
            <div class="contacts">
                <div class="contacts_title">
                    <h5>@lang('shop.footer.contacts')</h5>
                </div>
                <div class="contacts_text">
                    <div class="address">
                        <img src="{{asset('front/assets/img/contact-icon-1.png')}}" alt="icon">
                        <p>{{cfg('address')}}</p>
                    </div>
                    <div class="phone">
                        <img src="{{asset('front/assets/img/contact-icon-2.png')}}" alt="icon">
                        <p>{{cfg('phone_1')}}<br>
                            {{cfg('phone_2')}}
                        </p>
                    </div>
                    <div class="mail">
                        <img src="{{asset('front/assets/img/contact-icon-3.png')}}" alt="icon">
                        <p><a href="mailto:{{cfg('email')}}">{{cfg('email')}}</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="bottom_footer">
            @module('subscribe')
            {{--<div class="logo">--}}
                {{--<a href="#">--}}
                    {{--<img src="{{asset('front/assets/img/logo.png')}}" alt="Agrolan-logo">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="apps">--}}
                {{--<a href="#">--}}
                    {{--<img src="{{asset('front/assets/img/google-play.png')}}" alt="google-play">--}}
                {{--</a>--}}
                {{--<a href="#">--}}
                    {{--<img src="{{asset('front/assets/img/app-store.png')}}" alt="app-store">--}}
                {{--</a>--}}
            {{--</div>--}}
        </div>
    </div>
</footer>