<div class="cabinet">
    <div class="container">
        <div class="currency-and-language">
            @include('shop.partials.currency')
            @include('shop.partials.language')
        </div>
        <div class="entry">
            @include('shop.partials.auth')
        </div>
    </div>
</div>