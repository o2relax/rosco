<div class="popular-products">
    <div class="container">
        <div class="title-lines">
            <div class="line"></div>
            <h4>@lang('shop/product.related_products')</h4>
            <div class="line"></div>
        </div>
        <div class="product-slider">
            @if(count($data))
                @foreach($data as $product)
                    <div class="item-slider">
                        <div class="product-card">
                            <div class="product-price">
                                <span>
                                    {{currency()->price($product->price)}}
                                </span>
                            </div>
                            <div class="product-photo">
                                <a href="{{$product->link}}" style="text-decoration: none; ">
                                    @if(!empty($product->image))
                                        <img src="{{url('images/products/'.$product->id.'/'.$product->image.'|168x188')}}" alt="{{$product->name}}">
                                    @else
                                        <img src="{{url('images/products/no-photo|168x188')}}" alt="{{$product->name}}">
                                    @endif
                                </a>
                            </div>
                            <div class="product-info">
                                <div class="product-name">
                                    <a href="{{$product->link}}">{{$product->name}}</a>
                                </div>
                                <div class="product-characteristics">
                                    <p>{!! $product->short_description !!}</p>
                                </div>
                                <div class="button">
                                    {!! cart()->addToCartBtn($product->id, 'btn') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>