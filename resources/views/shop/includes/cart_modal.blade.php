<a href="#" rel="modal:close" class="modal-close" onclick="$('#cart_modal').hide();">x</a>
<div class="container">
    <div class="column-left">
        <div class="title">Товар добавлен в корзину</div>
        <div class="image">
            <img src="{{$item->image}}" alt="">
        </div>
        <div class="product_name">{{$item->name}}</div>
        <div>Колличество: {{isset($cart[$item->id]['quantity']) ? $cart[$item->id]['quantity'] : 1}}</div>
        <div>Итого к
            оплате: {{currency()->price($item->price, isset($cart[$item->id]['quantity']) ? $cart[$item->id]['quantity'] : 1)}}</div>
    </div>
    <div class="column-right">
        <div class="title">Товаров в корзине {{cart()->count()}}</div>
        <div>Итого: {{currency()->price(cart()->total())}}</div>
        <div class="buttons">
            <a class="btn btn-close" onclick="$('#cart_modal').hide();">Продолжить покупки</a>
            <a href="{{url('cart')}}" class="btn">Оформить заказ</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="title">
        <h4>@lang('shop/product.related_products')</h4>
    </div>

    <div class="product-slider">
        @if(count($data))
            @foreach($data as $product)
                <div class="product-card">
                    <div class="product-price">
                                <span>
                                    {{currency()->price($product->price)}}
                                </span>
                    </div>
                    <div class="product-photo">
                        <a href="{{$product->link}}" style="text-decoration: none; ">
                            @if(!empty($product->image))
                                <img src="{{url('images/products/'.$product->id.'/'.$product->image.'|168x188')}}"
                                     alt="{{$product->name}}">
                            @else
                                <img src="{{url('images/products/no-photo|168x188')}}" alt="{{$product->name}}">
                            @endif
                        </a>
                    </div>
                    <div class="product-info">
                        <div class="product-name">
                            <a href="{{$product->link}}">{{$product->name}}</a>
                        </div>
                        <div class="product-characteristics">
                            <p>{{strip_tags($product->short_description)}}</p>
                        </div>
                        <div class="button">
                            {!! cart()->addToCartBtn($product->id, 'btn') !!}
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>