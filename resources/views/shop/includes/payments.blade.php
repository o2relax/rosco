<div class="form-group @if($errors->has('customer.payment_id'))has-error has-feedback @endif">
    <label for="payment">@lang('shop/cart.checkout.payment')</label>
    <div class="form-block">
        @if(count($payments))
            @foreach($payments as $payment)
                <div class="shipment-block">
                    <label for="Customer[payment_id][{{$payment->id}}]">
                        <img src="{{$payment->image}}">
                        <input id="Customer[payment_id][{{$payment->id}}]"
                               type="radio" name="customer[payment_id]"
                               {{old('customer.payment_id', session()->get('payment_id')) == $payment->id ? 'checked' : ''}}
                               value="{{$payment->id}}" required>
                        {{$payment->name}}
                    </label>
                </div>
            @endforeach
        @endif
        @if($errors->has('customer.payment_id'))
            <span class="help-block">{{$errors->first('customer.payment_id')}}</span>
        @endif
    </div>
</div>