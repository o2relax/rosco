@if(count($items))
    <table>
        <thead>
        <tr>
            <th>@lang('shop/cart.th.title')</th>
            <th></th>
            <th>@lang('shop/cart.th.manufacturer')</th>
            <th>@lang('shop/cart.th.price')</th>
            <th>@lang('shop/cart.th.quantity')</th>
            <th>@lang('shop/cart.th.total')</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <td>
                    <img src="{{$item->image}}" alt="">
                </td>
                <td>
                    <a href="{{route('shop.product', [$item->slug])}}">{{$item->name}}</a>
                    <small>
                        {{$item->short_description}}
                    </small>
                </td>
                <td>
                    {{$item->manufacturer->name}}
                </td>
                <td>
                    @if($item->old_price)
                        <span class="old-price">{{$item->old_price}}</span>
                    @endif
                    {{$item->formatted_price}}
                </td>
                <td>
                    <input  type="number"
                           name="items[{{$item->id}}]"
                           value="{{isset($cart[$item->id]['quantity']) ? $cart[$item->id]['quantity'] : 1}}"
                           onchange="cart.quantity({{$item->id}}, $(this).val())"
                            min="1">
                    @if($cart[$item->id]['quantity'] > $item->stock)
                        <span>@lang('shop/cart.errors.quantity', ['stock' => $item->stock])</span>
                    @endif
                </td>
                <td>
                    {{currency()->price($item->price, isset($cart[$item->id]['quantity']) ? $cart[$item->id]['quantity'] : 1)}}
                </td>
                <td>
                    {!! cart()->deleteFromCartBtn($item->id, 'btn') !!}
                </td>
            </tr>
        @endforeach
        <tr class="total">
            <td colspan="5" class="right top-line">
                @lang('shop/cart.total')
            </td>
            <td colspan="2" class="top-line">
                {{currency()->price(cart()->total())}}
            </td>
        </tr>
        </tbody>
    </table>
@else
    <p>@lang('shop/cart.empty')</p>
@endif