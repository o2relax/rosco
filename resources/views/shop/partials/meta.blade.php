<title>@yield('title', cfg('main_title', 'langs'))</title>
<meta name="description" content="@yield('description', cfg('main_description', 'langs'))">
<meta name="keywords" content="@yield('keywords', cfg('main_keywords', 'langs'))">
<meta name="author" content="{{cfg('domain')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">