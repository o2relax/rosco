<div class="filters">

    {{--@module('category_list')--}}

    <div class="filter-title">
        <h6>@lang('shop/filter.heading')</h6>
    </div>

    <form id="filter-all" action="{{url()->current()}}">


        <input id="category"
               type="hidden"
               name="filter[category_id]"
               value="{{request()->input('filter.category_id')}}">

        <div class="catalog-filter">
            @php  $checkboxes = explode(",",app('request')->input('filter.category_id')); @endphp
            @foreach($categories as $item)
                @if($item->parent_id)
                    <div class="form-check">
                        <input type="checkbox"
                               class="form-check-category-input"
                               data-id="{{$item->id}}"
                               onchange="checkCategory()"
                               id="categoryCheck{{$item->id}}"
                                @php if(in_array($item->id,$checkboxes)) echo 'checked'; @endphp>
                        <label class="form-check-label" for="categoryCheck{{$item->id}}">{{$item->name}}</label>
                    </div>
                @else
                    <h6> - {{$item->name}}</h6>
                @endif
                {{--<a class="{{url()->full() == $item->link ? 'active' : ''}}"--}}
                {{--href="{{$item->link}}">{{$item->name}}</a>--}}
            @endforeach
        </div>

        <input id="manufacturer"
               type="hidden"
               name="filter[manufacturer_id]"
               value="{{request()->input('filter.manufacturer_id')}}">

        <div class="substance-filter">
            <h6>@lang('shop/filter.substance.title')</h6>
            <div class="substance-filter__input" >
                <input oninput="search(this)"  name="filter[substance_from]" type="text"
                       placeholder="@lang('shop/filter.substance.placeholders')"
                       value="{{request()->input('filter.substance_from')}}" autocomplete="off">
            </div>
            <div class="substance-filter__result">
                <ul id="substance__result" ></ul>
            </div>
        </div>


        <div class="manufacturer-filter">
            <h6>@lang('shop/filter.titles.manufacturer')</h6>
            <div class="manufacturer-filter__check">
                @php
                    $a = 1;
                    $checkboxes = explode(",",app('request')->input('filter.manufacturer_id'));
                @endphp
                @foreach($manufacturers as $manufacturer)

                    <div class="form-check {{$a > 10 ? 'hidden' : ''}}">
                        <input type="checkbox"
                               class="form-check-input"
                               data-id="{{$manufacturer->id}}"
                               onchange="check()"
                               id="exampleCheck{{$manufacturer->id}}"
                                @php if(in_array($manufacturer->id,$checkboxes)) echo 'checked'; @endphp>
                        <label class="form-check-label"
                               for="exampleCheck{{$manufacturer->id}}">{{$manufacturer->name}}</label>
                    </div>
                    @php $a++; @endphp
                @endforeach
                <a class="filter-down">
                    <i class="fa fa-angle-down"></i> Еще {{ count($manufacturers) - 10 }}
                </a>
                <a class="filter-up hidden">
                    <i class="fa fa-angle-up"></i> Свернуть
                </a>
            </div>
        </div>


        {{--<div class="manufacturer-filter">--}}
        {{--<h6>@lang('shop/filter.titles.manufacturer')</h6>--}}
        {{--<select name="filter[manufacturer_id]">--}}
        {{--<option value="0">@lang('shop/filter.any')</option>--}}
        {{--@foreach($manufacturers as $manufacturer)--}}
        {{--<option value="{{$manufacturer->id}}"--}}
        {{--{{request()->input('filter.manufacturer_id') == $manufacturer->id ? 'selected' : ''}}>--}}
        {{--{{$manufacturer->name}}--}}
        {{--</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}
        {{--</div>--}}


        <div class="price-filter">
            <h6>@lang('shop/filter.titles.price', ['currency' => currency()->get('prefix').currency()->get('postfix')])</h6>
            <div class="price-inputs">
                <input name="filter[price_from]" type="number" min="1" placeholder="@lang('shop/filter.placeholders.from')"
                       value="{{request()->input('filter.price_from')}}">
                <input name="filter[price_to]" type="number" min="1" placeholder="@lang('shop/filter.placeholders.to')"
                       value="{{request()->input('filter.price_to')}}">
            </div>
        </div>
        {{--<div class="weight-filter">--}}
        {{--<div class="slider-weight">--}}
        {{--<p>--}}
        {{--<label for="weightMinMax">Вес, кг: </label>--}}
        {{--<input type="text" id="weightMinMax" readonly>--}}
        {{--</p>--}}
        {{--<div id="weight"></div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="amount-filter">--}}
        {{--<div class="slider-amount">--}}
        {{--<p>--}}
        {{--<label for="amountMinMax">Объем, л: </label>--}}
        {{--<input type="text" id="amountMinMax" readonly>--}}
        {{--</p>--}}
        {{--<div id="amount"></div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="detailed-filters">
            <button class="btn">применить</button>
        </div>
    </form>
</div>


@section('javascripts')
    @parent
    <script>

        function check() {
            var manufacturerInput = document.querySelector('#manufacturer');
            var manufacturerFilter = document.querySelector('.manufacturer-filter');
            var checked = manufacturerFilter.querySelectorAll('.form-check-input');
            var i = 0;
            var a = 0;
            var result = checked.length;
            var arr = [];
            for (; i < result; i++) {
                if (checked[i].checked === true) {
                    arr[a] = checked[i].getAttribute('data-id');
                    a++;
                }
            }
            manufacturerInput.value = arr.join(',');
        }


        function checkCategory() {
            var categoryInput = document.querySelector('#category');
            var categoryFilter = document.querySelector('.catalog-filter');
            var checked = categoryFilter.querySelectorAll('.form-check-category-input');
            var i = 0;
            var a = 0;
            var result = checked.length;
            var arr = [];
            for (; i < result; i++) {
                if (checked[i].checked === true) {
                    arr[a] = checked[i].getAttribute('data-id');
                    a++;
                }
            }
            categoryInput.value = arr.join(',');
        }

        $(document).mouseup(function(e) {
            var div = $("#substance__result");

            if (!div.is(e.target)  && div.has(e.target).length === 0) {
                div.hide();
                div.innerHTML = "";
            }
        });

        function search(obg) {
            event.stopPropagation();
            if (obg.value.length === 1 && obg.value.slice(0, 1) === ' ') {
            } else if(obg.value.length === 0) {
                var substanceResult = document.querySelector('.substance-filter__result ul');
                if (substanceResult) substanceResult.style.display = 'none';
            } else {
                var data = {data: obg.value};
                $.ajax({
                    type: 'post',
                    url: "/catalog/substance",
                    data: data,
                    success: function (result) {
                        if (result.length !== 0) {
                            var date = $.parseJSON(result);
                            var substanceResult = document.querySelector('.substance-filter__result ul');
                            substanceResult.innerHTML = "";
                            date.forEach(function (item, i) {
                                var li = document.createElement('li');
                                li.innerHTML = item;
                                substanceResult.appendChild(li);
                                substanceResult.style.display = 'block'
                            });
                            $(substanceResult).hover(function () {
                                $(obg).blur();
                            });
                            $(substanceResult).on('click', 'li', function (e) {
                                var item = $(this).text();
                                $(obg).val(item);
                                $(substanceResult).fadeOut();
                                substanceResult.innerHTML = "";
                            });

                        }
                    }
                });
            }

        }

        //        function closeSubstance() {
        //            var substance = document.querySelector('.substance-filter__result ul');
        //            substance.innerHTML ="";
        //
        //        }

        window.addEventListener('DOMContentLoaded', function (e) {
            var manufacturerFilter = document.querySelector('.manufacturer-filter');
            if (manufacturerFilter) {
                var hidden = manufacturerFilter.querySelectorAll('.form-check.hidden');

                var down = document.querySelector('.filter-down');
                var up = document.querySelector('.filter-up');

                down.addEventListener('click', function (e) {
                    var i = 0;
                    var result = hidden.length;
                    for (; i < result; i++) {
                        hidden[i].classList.remove('hidden');
                        hidden[i].classList.add('visible');
                    }
                    down.classList.add('hidden');
                    up.classList.remove('hidden')
                });

                up.addEventListener('click', function (e) {
                    var visible = manufacturerFilter.querySelectorAll('.form-check.visible');
                    var i = 0;
                    var result = visible.length;
                    for (; i < result; i++) {
                        visible[i].classList.remove('visible');
                        visible[i].classList.add('hidden');
                    }
                    up.classList.add('hidden');
                    down.classList.remove('hidden')
                });
            }
        });


    </script>
@stop
