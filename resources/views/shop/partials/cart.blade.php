<a class="basket" href="{{route('shop.cart')}}">
    <div class="basket-img">
        <img src="{{ asset('front/assets/img/basket-icon.png') }}" alt="basket">
        <div class="basket-count">{{cart()->count()}}</div>
    </div>
    <span class="basket-title">@lang('shop.cart.heading')</span>
</a>