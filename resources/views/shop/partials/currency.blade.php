<div class="currency">
    <select name="currency">
        @foreach(app('shared')->get('currencies') as $currency)
            <option value="{{$currency->code}}" {{request()->cookie('currency', cfg('currency_shop', 'code')) == $currency->code ? 'selected' : ''}}>{{$currency->name}}</option>
        @endforeach
    </select>
</div>