<div class="language">
    <select name="language" onchange="language.set($(this).val())">
        @foreach(app('shared')->get('languages') as $language)
            <option value="{{$language->code}}" {{app()->isLocale($language->code) ? 'selected' : ''}}>{{$language->name}}</option>
        @endforeach
    </select>
</div>