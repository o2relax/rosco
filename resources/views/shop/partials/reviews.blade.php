<div class="left">
    @guest
        <p>@lang('shop/common.reviews.rules', ['login' => route('login'), 'register' => route('register')])</p>
    @endauth
    @auth
        @if(session('message'))
            <p>{{session('message')}}</p>
        @else
            <form method="post" id="check-review" action="{{route('shop.review.post', [$product->slug])}}">
                {{csrf_field()}}
                @input_maker_create('text', ['type' => 'text', 'placeholder' => 'Текст'], old('text'))
                <div id="text-area-error" hidden><strong>Введите допустимое количество символов : от 5 до 200</strong></div>
                @if($errors->has('text'))
                    <div class="help-block">{{$errors->first('text')}}</div>
                @endif
                {!! Form::captcha() !!}
                @if($errors->has('g-recaptcha-response'))
                    <div class="help-block">{{$errors->first('g-recaptcha-response')}}</div>
                @endif
                <button class="btn" id="check-reviews" >@lang('shop/common.reviews.btn')</button>
            </form>
        @endif
    @endauth
</div>
<div class="right">
    @if(count($reviews))
        @foreach($reviews as $review)
            <div class="review">
                <div class="review-name">
                    {{$review->name}} <small>({{$review->date}})</small>
                </div>
                <div class="review-text">
                    {{$review->text}}
                </div>
            </div>
        @endforeach
    @else
        <p>@lang('shop/product.empty_reviews')</p>
    @endif
</div>