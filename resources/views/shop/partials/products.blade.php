<div class="filtered-products">
    @if(count($products))
        @foreach($products as $product)
            <div class="filtered-product-card product-{{$product->id}}" style="position:relative">
                @if ($product->old_price)
                    <div class="discount-img"></div>
                @endif
                <div class="product-photo">
                    <a href="{{$product->link}}">
                        <img src="{{$product->image}}" alt="{{$product->name}}" style="height: 100%; width: 90%;">
                    </a>
                </div>
                <div class="product-info">
                    <div class="product-name">
                        <a href="{{$product->link}}">{{$product->name}}</a>
                    </div>
                    <div class="product-characteristics">
                        <p>{{$product->short_description}}</p>
                    </div>
                    <div class="button">

                        @if($product->old_price)
                            <div class="not-actual-price">
                                <span>{{$product->old_price}}</span>
                            </div>
                        @endif
                        <div class="price">
                            <span>{{$product->price}}</span>
                        </div>
                        {!! cart()->addToCartBtn($product->id, 'btn') !!}
                        <input type="hidden" value="{{$product->price}}" id="price{{$product->id}}"/>
                        <div class="buy-link">
                            <a href="#dialog" name="modal" type="button" class="btn one-click-buy"
                               id="{{$product->id}}">Купить сейчас</a>
                        </div>
                    </div>
                </div>

            </div>
        @endforeach
    @endif
    <div class="pagination-products">
        @if(count($products))
            {{$products->appends(request()->except(['_token', '_method']))->links()}}
        @endif
    </div>
    <div id="boxes">
        <div id="dialog" class="window">
            <form onsubmit="checkSubstanceForm(event)">
                <label>@lang('shop.catalog.client_name') *</label>
                <input type="text" id="lname" placeholder="Enter your name">
                <label>@lang('shop.catalog.client_phone') *</label>
                <input type="text" id="phone-number" placeholder="Example : 077-777-7777"/>
                <input type="hidden" value="0" id="product-id"/>
                <input type="submit" id="one-click"  value="ЗАКАЗАТЬ"/>
            </form>
            <a href="#" class="close"/>X</a>
        </div>
        <!-- Маска, которая затемняет весь экран -->
        <div id="mask"></div>
    </div>
</div>
@section('javascripts')
    @parent
    <script>

        function checkSubstanceForm(event) {
            event.preventDefault();
            var phoneValue = $('#phone-number');
            var param = ($('#product-id').val()).split(',');
            var clientName = $('#lname');
            param[2] = clientName.val();
            var phoneRegexp = /^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$/im;
            if (phoneValue.val().match(phoneRegexp) && param[2] !== "") {
                event.preventDefault();
                var data = {data: phoneValue.val(), param: param};
                $.ajax({
                    type: 'POST',
                    url: "/catalog/oneclick",
                    data: data,
                    success: function () {
                        location.reload();
                    }
                });

                //location.reload();
            }
            else {
                event.preventDefault();
                if (!clientName.val()) {
                    clientName.prop('placeholder','Введите имя');
                    clientName.css({"border-color": "red",
                        "border-width":"2px",
                        "border-style":"solid"})
                }
                if (!phoneValue.val().match(phoneRegexp)) {
                    phoneValue.val('');
                    phoneValue.prop('placeholder','Введите корректный телефон');
                    phoneValue.css({"border-color": "red",
                        "border-width":"2px",
                        "border-style":"solid"})
                }
            }
            return false;
        }

        $(document).ready(function () {
            $('a[name=modal]').click(function (e) {
                e.preventDefault();
                var id = $(this).attr('href');

                var maskHeight = $(document).height();
                var maskWidth = $(window).width();

                $('#mask').css({'width': maskWidth, 'height': maskHeight});

                $('#mask').fadeIn(1000);
                $('#mask').fadeTo("slow", 0.8);

                var winH = $(window).height();
                var winW = $(window).width();

                $(id).css('top', winH / 1.5 - $(id).height() / 2);
                $(id).css('left', winW / 2 - $(id).width() / 2);

                $(id).fadeIn(1000);

            });

            $('.one-click-buy').click(function () {

                var id = $(this).attr('id');
                var price = parseInt(($('#price' + id).val().replace(" ", "")));
                $('#product-id').val(id + "," + price);
            });

            // $('#one-click').submit(function (event) {
            //     var phoneValue = $('#phone-number').val();
            //     var param = ($('#product-id').val()).split(',');
            //     param[2] = $('#lname').val();
            //     var phoneRegexp = /^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$/im;
            //     if (phoneValue.match(phoneRegexp)) {
            //         var data = {data: phoneValue, param: param};
            //         $.ajax({
            //             type: 'post',
            //             url: "/catalog/oneclick",
            //             data: data,
            //             success: function (data) {
            //                 console.log(data);
            //             }
            //         });
            //
            //     }
            //
            // });

            $('.window .close').click(function (e) {
                e.preventDefault();
                $('#mask, .window').hide();
            });

            $('#mask').click(function () {
                $(this).hide();
                $('.window').hide();
            });

        });
    </script>
@stop
