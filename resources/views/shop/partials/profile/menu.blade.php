<ul class="profile-navigation">
    <li>
        <a href="{{route('shop.profile')}}"
           class="{{url()->current() == route('shop.profile') ? 'active' : ''}}">
            @lang('shop/profile.headings.password')
        </a>
    </li>
    <li>
        <a href="{{route('shop.profile.orders')}}"
           class="{{url()->current() == route('shop.profile.orders') ? 'active' : ''}}">
            @lang('shop/profile.headings.orders')
        </a>
    </li>
    <li>
        <a onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            @lang('shop/profile.headings.exit')
        </a>
    </li>
</ul>
<form id="logout-form" action="{!! route('logout') !!}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
