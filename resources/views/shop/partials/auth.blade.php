@if(auth()->check())
    <a href="{{route('shop.profile')}}">{{auth()->user()->name}} <img src="{{asset('front/assets/img/entry-icon.png')}}" alt="icon"></a>
@else
    <a href="{{route('login')}}">@lang('shop.common.enter') <img src="{{asset('front/assets/img/entry-icon.png')}}" alt="icon"></a>
@endif