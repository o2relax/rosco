<div class="catalog-container">
    <div class="sortable">
        @lang('shop/filter.sorts.title')
        <select name="sort">
            <option value="date_add_desc" {{request()->get('sort') == 'date_add_desc' ? 'selected' : ''}}>@lang('shop/filter.sorts.date_add_desc')</option>
            <option value="date_add_asc" {{request()->get('sort') == 'date_add_asc' ? 'selected' : ''}}>@lang('shop/filter.sorts.date_add_asc')</option>
            <option value="price_asc" {{request()->get('sort') == 'price_asc' ? 'selected' : ''}}>@lang('shop/filter.sorts.price_asc')</option>
            <option value="price_desc" {{request()->get('sort') == 'price_desc' ? 'selected' : ''}}>@lang('shop/filter.sorts.price_desc')</option>
            <option value="name_asc" {{request()->get('sort') == 'name_asc' ? 'selected' : ''}}>@lang('shop/filter.sorts.name_asc')</option>
            <option value="name_desc" {{request()->get('sort') == 'name_desc' ? 'selected' : ''}}>@lang('shop/filter.sorts.name_desc')</option>
            <option value="stock_desc" {{request()->get('sort') == 'stock_desc' ? 'selected' : ''}}>@lang('shop/filter.sorts.stock_desc')</option>
            <option value="stock_asc" {{request()->get('sort') == 'stock_asc' ? 'selected' : ''}}>@lang('shop/filter.sorts.stock_asc')</option>
        </select>
        @lang('shop/filter.per_page')
        <select name="per_page">
            <option value="{{cfg('catalog_pagination')}}" {{request()->get('per_page', session()->get('per_page')) == cfg('catalog_pagination') ? 'selected' : ''}}>{{cfg('catalog_pagination')}}</option>
            <option value="16" {{request()->get('per_page', session()->get('per_page')) == 16 ? 'selected' : ''}}>16</option>
        </select>
    </div>
</div>