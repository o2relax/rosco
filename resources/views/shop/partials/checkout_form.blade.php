@if(cart()->count())
    <div class="checkout-container">
        {{csrf_field()}}
        <div class="left">
            <div class="form-group @if($errors->has('customer.name'))has-error has-feedback @endif">
                <label for="name">@lang('shop/cart.checkout.name')</label>
                <div class="form-block">
                    @input_maker_create('customer[name]', ['type' => 'string', 'placeholder' => 'Иванов Иван Иванович'],
                    old('customer.name', session()->get('name', auth()->check() ? auth()->user()->name : null)))
                    @if($errors->has('customer.name'))
                        <span class="help-block">{{$errors->first('customer.name')}}</span>
                    @endif
                </div>
            </div>
            <div class="form-group @if($errors->has('customer.email'))has-error has-feedback @endif">
                <label for="email">@lang('shop/cart.checkout.email')</label>
                <div class="form-block">
                    @input_maker_create('customer[email]', ['type' => 'string', 'placeholder' => 'example@ukr.net'],
                    old('customer.email', session()->get('email', auth()->check() ? auth()->user()->email : null)))
                    @if($errors->has('customer.email'))
                        <span class="help-block">{{$errors->first('customer.email')}}</span>
                    @endif
                </div>
            </div>
            <div class="form-group @if($errors->has('customer.phone'))has-error has-feedback @endif">
                <label for="phone">@lang('shop/cart.checkout.phone')</label>
                <div class="form-block">
                    @input_maker_create('customer[phone]', ['type' => 'string','class' => 'client_phone_mask',  'placeholder' => ' +38(098) 000 00 00'],
                    old('customer.phone', session()->get('phone')))
                    @if($errors->has('customer.phone'))
                        <span class="help-block">{{$errors->first('customer.phone')}}</span>
                    @endif
                </div>
            </div>
            <div class="form-group @if($errors->has('customer.address'))has-error has-feedback @endif">
                <label for="address">@lang('shop/cart.checkout.address')</label>
                <div class="form-block">
                    @input_maker_create('customer[address]', ['type' => 'text', 'placeholder' => 'г.Киев, ул.Ленина 5'],
                    old('customer.address', session()->get('address')))
                    @if($errors->has('customer.address'))
                        <span class="help-block">{{$errors->first('customer.address')}}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="right">
            <button type="submit" class="btn">@lang('shop/cart.button')</button>
        </div>
    </div>
    <div class="checkout-container">
        <div class="left">
            @if(count($shipments))
                <div class="form-group @if($errors->has('customer.shipment_id'))has-error has-feedback @endif">
                    <label for="shipment">@lang('shop/cart.checkout.shipment')</label>
                    <div class="form-block">
                        @foreach($shipments as $shipment)
                            <div class="shipment-block">
                                <label for="Customer[shipment_id][{{$shipment->id}}]">
                                    <img src="{{$shipment->image}}">
                                    <input id="Customer[shipment_id][{{$shipment->id}}]"
                                           type="radio" name="customer[shipment_id]"
                                           data-payments="{{implode(',', $shipment->payments->keyBy('id')->keys()->toArray())}}"
                                           onchange="cart.payments($(this).val(), $(this).attr('data-payments'))"
                                           {{old('customer.shipment_id', session()->get('shipment_id')) == $shipment->id ? 'checked' : ''}}
                                           value="{{$shipment->id}}">
                                    {{$shipment->name}}
                                </label>
                            </div>
                        @endforeach
                        @if($errors->has('customer.shipment_id'))
                            <span class="help-block">{{$errors->first('customer.shipment_id')}}</span>
                        @endif
                    </div>
                </div>
            @endif
        </div>
        <div class="right payments-container">
            @if(count($payments))
                @include('shop.includes.payments')
            @endif
        </div>

    </div>
@endif