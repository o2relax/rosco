<header class="header">
    @include('shop.topbar')
    <div class="header-logo">
        <div class="container">
            @module('socials')
            <div class="logo">
                <a href="{{url('/')}}"><img src="{{ asset('front/assets/img/28056253_153921961937610_8835592045230312313_n.png') }}" alt="logo"></a>
            </div>
            @include('shop.partials.cart')
        </div>
    </div>
    <div class="menu">
        <ul>
            @menu('main')
        </ul>
    </div>
</header>