<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('shop.partials.meta')
    @section('styles')
        <link rel="stylesheet" href="{{asset('front/assets/css/main.css')}}">
    @show
</head>
<body>

@include('shop.header')

@yield('content_top')

<div class="products-catalog">
    <div class="title-container">
        <div class="title-lines">
            <div class="line"></div>
            <h1>@yield('h1')</h1>
            <div class="line"></div>
        </div>
        <span id="filterToggle"><i class="fa fa-filter" aria-hidden="true"></i></span>
    </div>
    @include('shop.partials.sortable')
    <div class="catalog-container">
        @include('shop.partials.filter')
        @yield('content')
    </div>
</div>

@module('products_popular')

<div class="delivery">

    @module('delivery')
    @module('payment')

</div>

@include('shop.footer')


@section('javascripts')
    <script src="{{asset('front/assets/libs/jquery-3.0.0.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/jquery-migrate-3.0.0.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/jquery-ui/jquery-ui-1.12.1/jquery-ui.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/slick/slick.min.js')}}"></script>
    <script src="https://use.fontawesome.com/2c8aecedb1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    @include('shop.partials.scripts')
    <script src="{{asset('front/assets/js/main.js')}}"></script>
@show
@section('modals')
    @include('shop.includes.modals')
@show</body>
</html>

<link rel="stylesheet" href="{{asset('front/assets/libs/slick/slick-theme.css')}}">
<link rel="stylesheet" href="{{asset('front/assets/libs/jquery-ui/jquery-ui-1.12.1/jquery-ui.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />