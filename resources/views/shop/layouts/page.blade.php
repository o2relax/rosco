<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('shop.partials.meta')
    @section('styles')

        <link rel="stylesheet" href="{{asset('front/assets/libs/slick/slick-theme.css')}}">
        <link rel="stylesheet" href="{{asset('front/assets/libs/jquery-ui/jquery-ui-1.12.1/jquery-ui.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/assets/css/main.css')}}">

    @show
</head>
<body>

@include('shop.header')

<div class="product-page">
    <div class="container">
        <div class="bread-crumbs">
            @section('breadcrumbs')
                @include('shop.partials.breadcrumbs')
            @show
        </div>
        @yield('content')
    </div>
</div>

<div class="delivery">
    @module('delivery')
    @module('payment')
</div>

@include('shop.footer')

@section('javascripts')
    <script src="{{asset('front/assets/libs/jquery-3.0.0.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/jquery-migrate-3.0.0.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/jquery-ui/jquery-ui-1.12.1/jquery-ui.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/slick/slick.min.js')}}"></script>
    <script src="https://use.fontawesome.com/2c8aecedb1.js"></script>
    @include('shop.partials.scripts')
    <script src="{{asset('front/assets/js/main.js')}}"></script>
@show
@section('modals')
    @include('shop.includes.modals')
@show
</body>
</html>