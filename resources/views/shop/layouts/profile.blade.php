<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('shop.partials.meta')
    @section('styles')
        <link rel="stylesheet" href="{{asset('front/assets/libs/slick/slick-theme.css')}}">
        <link rel="stylesheet" href="{{asset('front/assets/libs/jquery-ui/jquery-ui-1.12.1/jquery-ui.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/assets/css/main.css')}}">
        <link rel='stylesheet' href='https://apimgmtstorelinmtekiynqw.blob.core.windows.net/content/MediaLibrary/Widget/Tracking/styles/tracking.css' /></head>
<body>

@include('shop.header')

<div class="product-page">
    <div class="container">
        <div class="bread-crumbs">
            @section('breadcrumbs')
                @include('shop.partials.breadcrumbs')
            @show
        </div>
        <div class="checkout-success-container">
            <div class="title-container">
                <div class="title-lines">
                    <div class="line"></div>
                    <h1> @yield('h1') </h1>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="login-container">
            <div class="left">
                @include('shop.partials.profile.menu')
            </div>
            <div class="right">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<div class="delivery">
    @module('delivery')
    @module('payment')
</div>

@include('shop.footer')

@section('javascripts')
    <script src="{{asset('front/assets/libs/jquery-3.0.0.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/jquery-migrate-3.0.0.min.js')}}"></script>
    <script src="{{asset('front/assets/libs/jquery-ui/jquery-ui-1.12.1/jquery-ui.min.js')}}"></script>
    <script type='text/javascript' id='track' charset='utf-8' data-lang='ru' apiKey='bcc0127c0405a518bd132d3c6a59f361' data-town='city-not-default' data-town-name='undefined' data-town-id='undefined' src='https://apimgmtstorelinmtekiynqw.blob.core.windows.net/content/MediaLibrary/Widget/Tracking/dist/track.min.js'></script>    <script src="https://use.fontawesome.com/2c8aecedb1.js"></script>
    @include('shop.partials.scripts')
    <script src="{{asset('front/assets/js/main.js')}}"></script>
@show
@section('modals')
    @include('shop.includes.modals')
@show
</body>
</html>