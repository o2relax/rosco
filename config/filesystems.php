<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'products' => [
            'driver' => 'local',
            'root' => storage_path('app/public/products'),
            'url' => env('APP_URL').'/storage/products',
            'visibility' => 'public',
        ],

        'categories' => [
            'driver' => 'local',
            'root' => storage_path('app/public/categories'),
            'url' => env('APP_URL').'/storage/categories',
            'visibility' => 'public',
        ],

        'languages' => [
            'driver' => 'local',
            'root' => storage_path('app/public/languages'),
            'url' => env('APP_URL').'/storage/languages',
            'visibility' => 'public',
        ],

        'manufacturers' => [
            'driver' => 'local',
            'root' => storage_path('app/public/manufacturers'),
            'url' => env('APP_URL').'/storage/manufacturers',
            'visibility' => 'public',
        ],

        'modules' => [
            'driver' => 'local',
            'root' => storage_path('app/public/modules'),
            'url' => env('APP_URL').'/storage/modules',
            'visibility' => 'public',
        ],

        'shipments' => [
            'driver' => 'local',
            'root' => storage_path('app/public/shipments'),
            'url' => env('APP_URL').'/storage/shipments',
            'visibility' => 'public',
        ],

        'payments' => [
            'driver' => 'local',
            'root' => storage_path('app/public/payments'),
            'url' => env('APP_URL').'/storage/payments',
            'visibility' => 'public',
        ],

        'pages' => [
            'driver' => 'local',
            'root' => storage_path('app/public/pages'),
            'url' => env('APP_URL').'/storage/pages',
            'visibility' => 'public',
        ],

        'blog' => [
            'driver' => 'local',
            'root' => storage_path('app/public/blog'),
            'url' => env('APP_URL').'/storage/blog',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

    ],

];
