<?php

return [
    'backend-route-prefix' => 'admin',
    'paginate' => 25,
    'invite' => [
        'send_mail' => true
    ]
];