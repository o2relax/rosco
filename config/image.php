<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    'max_file_count' => 10,
    'max_size' => 5000,
    'no-photo' => 'no-photo.png',
    'shipment' => [
        'width' => 100,
        'height' => 100
    ]

];
