<?php

use Illuminate\Database\Seeder;

class PaymentTranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_translations')->delete();
        
        \DB::table('payment_translations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'payment_id' => 1,
                'name' => 'Безналичный расчет',
                'description' => NULL,
                'lang_code' => 'ru',
            ),
            1 => 
            array (
                'id' => 2,
                'payment_id' => 1,
                'name' => 'Безготівковий розрахунок',
                'description' => NULL,
                'lang_code' => 'uk',
            ),
            2 => 
            array (
                'id' => 5,
                'payment_id' => 4,
                'name' => 'Наложенный платеж',
                'description' => NULL,
                'lang_code' => 'ru',
            ),
            3 => 
            array (
                'id' => 6,
                'payment_id' => 4,
                'name' => 'Післяплата',
                'description' => NULL,
                'lang_code' => 'uk',
            ),
        ));
        
        
    }
}