<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payments')->delete();
        
        \DB::table('payments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'beznalichnyy-raschet',
                'image' => 'beznalichnyy-raschet.png',
                'currency_id' => 2,
                'is_active' => 1,
                'created_at' => '2018-01-16 03:25:23',
                'updated_at' => '2018-01-16 03:28:38',
            ),
            1 => 
            array (
                'id' => 4,
                'code' => 'nalozhennyy-platezh',
                'image' => 'nalozhennyy-platezh.gif',
                'currency_id' => 2,
                'is_active' => 1,
                'created_at' => '2018-01-16 03:35:06',
                'updated_at' => '2018-01-16 03:35:19',
            ),
        ));
        
        
    }
}