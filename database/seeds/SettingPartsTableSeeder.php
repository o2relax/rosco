<?php

use Illuminate\Database\Seeder;

class SettingPartsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('setting_parts')->delete();
        
        \DB::table('setting_parts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Основные',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Магазин',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'СЕО',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Оптимизция',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Уведомления',
            ),
        ));
        
        
    }
}