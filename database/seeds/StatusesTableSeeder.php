<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('statuses')->delete();
        
        \DB::table('statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'success',
                'type' => 'Product',
                'created_at' => '2018-01-03 17:15:22',
                'updated_at' => '2018-01-03 17:15:23',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'warning',
                'type' => 'Product',
                'created_at' => '2018-01-03 17:15:34',
                'updated_at' => '2018-01-03 17:15:35',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'danger',
                'type' => 'Product',
                'created_at' => '2018-01-03 17:15:44',
                'updated_at' => '2018-01-03 17:15:44',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'success',
                'type' => 'Order',
                'created_at' => '2018-01-18 18:19:12',
                'updated_at' => '2018-01-18 18:19:13',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'orange-300',
                'type' => 'Order',
                'created_at' => '2018-01-18 18:19:12',
                'updated_at' => '2018-01-18 18:19:13',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'danger',
                'type' => 'Order',
                'created_at' => '2018-01-18 18:20:02',
                'updated_at' => '2018-01-18 18:20:02',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'primary',
                'type' => 'Order',
                'created_at' => '2018-01-18 18:20:02',
                'updated_at' => '2018-01-18 18:20:02',
            ),
        ));
        
        
    }
}