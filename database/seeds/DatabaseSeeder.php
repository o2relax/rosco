<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ProductsTableSeeder::class);

        Model::reguard();
        $this->call(SettingsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SettingPartsTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(StatusTranslationsTableSeeder::class);
        $this->call(ShipmentsTableSeeder::class);
        $this->call(ShipmentTranslationsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(PaymentTranslationsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleToUserTableSeeder::class);
        $this->call(PaymentToShipmentsTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
    }
}
