<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'label' => 'Admin',
                'style' => 'danger',
                'permissions' => 'admin',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'manager',
                'label' => 'Manager',
                'style' => 'primary',
                'permissions' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'user',
                'label' => 'User',
                'style' => 'success',
                'permissions' => NULL,
            ),
        ));
        
        
    }
}