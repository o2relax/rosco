<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'part_id' => 2,
                'name' => 'Валюта магазина',
                'description' => 'Валюта, в которой по умолчанию показываются товары пользователям магазина',
                'code' => 'currency_shop',
                'value' => '2',
                'options' => NULL,
                'relation' => '\\App\\Models\\Currency',
                'type' => 'select',
                'sort' => 0,
                'created_at' => '2017-12-28 19:52:47',
                'updated_at' => '2018-01-05 14:57:10',
            ),
            1 => 
            array (
                'id' => 2,
                'part_id' => 2,
                'name' => 'Валюта каталога',
                'description' => 'Эталонная валюта, от которой будет считаться любая другая по курсу',
                'code' => 'currency_catalog',
                'value' => '1',
                'options' => NULL,
                'relation' => '\\App\\Models\\Currency',
                'type' => 'select',
                'sort' => 0,
                'created_at' => '2017-12-28 19:53:31',
                'updated_at' => '2018-01-05 14:57:10',
            ),
            2 => 
            array (
                'id' => 3,
                'part_id' => 2,
                'name' => 'Язык магазина',
                'description' => 'Язык, который будет по умолчанию для пользователей',
                'code' => 'language',
                'value' => '1',
                'options' => NULL,
                'relation' => '\\App\\Models\\Language',
                'type' => 'select',
                'sort' => 0,
                'created_at' => '2017-12-29 16:23:16',
                'updated_at' => '2018-01-05 14:57:10',
            ),
            3 => 
            array (
                'id' => 4,
                'part_id' => 3,
                'name' => 'Заголовок',
                'description' => 'Заголовок главной страницы',
                'code' => 'main_title',
                'value' => '{"ru":"Rosco Shop","uk":"Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstext',
                'sort' => 1,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-07 04:24:59',
            ),
            4 => 
            array (
                'id' => 7,
                'part_id' => 1,
                'name' => 'Адрес сайта',
                'description' => 'Используется в рассылках и т.д.',
                'code' => 'domain',
                'value' => 'http://rosco.local',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-09 06:43:28',
                'updated_at' => '2018-01-06 10:12:29',
            ),
            5 => 
            array (
                'id' => 8,
                'part_id' => 3,
                'name' => 'Описание',
                'description' => 'Описание главной страницы',
                'code' => 'main_description',
                'value' => '{"ru":"Rosco Shop","uk":"Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstextarea',
                'sort' => 2,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-07 03:58:14',
            ),
            6 => 
            array (
                'id' => 9,
                'part_id' => 3,
                'name' => 'Ключевые слова',
                'description' => 'Ключевые слова главной страницы',
                'code' => 'main_keywords',
                'value' => '{"ru":"Rosco, Shop","uk":"Rosco, Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstextarea',
                'sort' => 3,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-07 03:58:14',
            ),
            7 => 
            array (
                'id' => 10,
                'part_id' => 3,
                'name' => 'Ключевые слова каталога',
                'description' => 'Ключевые слова  главной страницы каталога',
                'code' => 'catalog_keywords',
                'value' => '{"ru":"Rosco, Shop, \\u043a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433","uk":"Rosco, Shop, \\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstextarea',
                'sort' => 6,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-07 04:22:24',
            ),
            8 => 
            array (
                'id' => 11,
                'part_id' => 3,
                'name' => 'Описание каталога',
                'description' => 'Описание главной страницы каталога',
                'code' => 'catalog_description',
                'value' => '{"ru":"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433 Rosco Shop","uk":"\\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstextarea',
                'sort' => 5,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-07 04:24:09',
            ),
            9 => 
            array (
                'id' => 12,
                'part_id' => 3,
                'name' => 'Заголовок каталога',
                'description' => 'Заголовок  главной страницы каталога',
                'code' => 'catalog_title',
                'value' => '{"ru":"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433 Rosco Shop","uk":"\\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstext',
                'sort' => 4,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-07 04:24:59',
            ),
            10 => 
            array (
                'id' => 13,
                'part_id' => 3,
                'name' => 'Заголовок H1 каталога',
                'description' => 'Заголовок H1  главной страницы каталога',
                'code' => 'catalog_h1',
                'value' => '{"ru":"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433 Rosco Shop","uk":"\\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstext',
                'sort' => 7,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-07 04:24:59',
            ),
            11 => 
            array (
                'id' => 14,
                'part_id' => 4,
                'name' => 'Ширина картинки в списке',
            'description' => 'Ширина картинки в списке товаров в каталоге  (в px)',
                'code' => 'image_list_width',
                'value' => '168',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-09 06:43:24',
                'updated_at' => '2018-01-07 01:38:41',
            ),
            12 => 
            array (
                'id' => 15,
                'part_id' => 4,
                'name' => 'Высота картинки в списке',
            'description' => 'Высота картинки в списке товаров в каталоге  (в px)',
                'code' => 'image_list_height',
                'value' => '188',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-09 06:43:24',
                'updated_at' => '2018-01-18 03:38:04',
            ),
            13 => 
            array (
                'id' => 16,
                'part_id' => 2,
                'name' => 'Товаров на странице',
                'description' => 'Кол-во товаров на одной странице каталога',
                'code' => 'catalog_pagination',
                'value' => '16',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-11 16:41:09',
                'updated_at' => '2018-01-06 10:19:09',
            ),
            14 => 
            array (
                'id' => 17,
                'part_id' => 2,
                'name' => 'Сортировка по умолчанию',
                'description' => NULL,
                'code' => 'catalog_order',
                'value' => 'id',
                'options' => '{"id":"ID","created_at":"\\u0414\\u0430\\u0442\\u0430 \\u0441\\u043e\\u0437\\u0434\\u0430\\u043d\\u0438\\u044f","price":"\\u0426\\u0435\\u043d\\u0430"}',
                'relation' => NULL,
                'type' => 'select',
                'sort' => 0,
                'created_at' => '2018-01-11 16:41:09',
                'updated_at' => '2018-01-06 10:19:09',
            ),
            15 => 
            array (
                'id' => 18,
                'part_id' => 2,
                'name' => 'Направление по умолчанию',
                'description' => NULL,
                'code' => 'catalog_sort',
                'value' => 'desc',
                'options' => '{"desc":"\\u041f\\u043e \\u0443\\u0431\\u044b\\u0432\\u0430\\u043d\\u0438\\u044e","asc":"\\u041f\\u043e \\u0432\\u043e\\u0437\\u0440\\u0430\\u0441\\u0442\\u0430\\u043d\\u0438\\u044e"}',
                'relation' => NULL,
                'type' => 'select',
                'sort' => 0,
                'created_at' => '2018-01-11 16:41:09',
                'updated_at' => '2018-01-06 14:05:12',
            ),
            16 => 
            array (
                'id' => 19,
                'part_id' => 2,
                'name' => 'Длина краткого описания',
                'description' => 'Длина краткого описания на странице товара',
                'code' => 'product_description_length',
                'value' => '400',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-12 16:01:42',
                'updated_at' => '2018-01-12 16:01:47',
            ),
            17 => 
            array (
                'id' => 20,
                'part_id' => 2,
            'name' => 'Длина краткого описания (список)',
                'description' => 'Длина краткого описания в списке товаров',
                'code' => 'product_list_description_length',
                'value' => '100',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-12 16:01:43',
                'updated_at' => '2018-01-12 16:01:46',
            ),
            18 => 
            array (
                'id' => 21,
                'part_id' => 4,
                'name' => 'Ширина главной картинки продукта',
            'description' => 'Ширина главной картинки на странице товара (в px)',
                'code' => 'product_main_width',
                'value' => '366',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-12 16:01:45',
                'updated_at' => '2018-01-12 16:01:46',
            ),
            19 => 
            array (
                'id' => 22,
                'part_id' => 4,
                'name' => 'Высота главной картинки продукта',
            'description' => 'Высота главной картинки на странице товара  (в px)',
                'code' => 'product_main_height',
                'value' => '366',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-12 16:01:44',
                'updated_at' => '2018-01-12 16:01:48',
            ),
            20 => 
            array (
                'id' => 23,
                'part_id' => 4,
                'name' => 'Ширина дополинтельной картинки продукта',
            'description' => 'Ширина дополинтельной картинки на странице товара (в px)',
                'code' => 'product_ext_width',
                'value' => '98',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-12 16:01:45',
                'updated_at' => '2018-01-12 16:01:46',
            ),
            21 => 
            array (
                'id' => 24,
                'part_id' => 4,
                'name' => 'Высота дополинтельной картинки продукта',
            'description' => 'Высота дополинтельной картинки на странице товара  (в px)',
                'code' => 'product_ext_height',
                'value' => '98',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-12 16:01:44',
                'updated_at' => '2018-01-12 16:01:48',
            ),
            22 => 
            array (
                'id' => 25,
                'part_id' => 5,
                'name' => 'Письмо на почту пользователя при заказе',
                'description' => NULL,
                'code' => 'notice_email_user_order',
                'value' => '1',
                'options' => '["\\u0412\\u044b\\u043a\\u043b","\\u0412\\u043a\\u043b"]',
                'relation' => NULL,
                'type' => 'radio',
                'sort' => 0,
                'created_at' => '2018-01-15 16:45:58',
                'updated_at' => '2018-01-15 16:45:59',
            ),
            23 => 
            array (
                'id' => 26,
                'part_id' => 1,
                'name' => 'Почта администратора',
                'description' => 'Для уведомлений',
                'code' => 'admin_email',
                'value' => 'webmaster865@yandex.ru',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-25 19:25:48',
                'updated_at' => '2018-01-25 19:25:49',
            ),
            24 => 
            array (
                'id' => 27,
                'part_id' => 5,
                'name' => 'Письмо на почту администратора при заказе',
                'description' => NULL,
                'code' => 'notice_email_admin_order',
                'value' => '1',
                'options' => '["\\u0412\\u044b\\u043a\\u043b","\\u0412\\u043a\\u043b"]',
                'relation' => NULL,
                'type' => 'radio',
                'sort' => 0,
                'created_at' => '2018-01-15 16:45:58',
                'updated_at' => '2018-01-15 16:45:59',
            ),
            25 => 
            array (
                'id' => 28,
                'part_id' => 1,
                'name' => 'Телефон',
                'description' => NULL,
                'code' => 'phone_1',
            'value' => '+38 (044) 390-00-77',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-25 19:25:47',
                'updated_at' => '2018-01-25 19:25:50',
            ),
            26 => 
            array (
                'id' => 29,
                'part_id' => 1,
                'name' => 'Телефон',
                'description' => NULL,
                'code' => 'phone_2',
            'value' => '+38 (098) 390-00-77',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-25 19:25:47',
                'updated_at' => '2018-01-25 19:25:52',
            ),
            27 => 
            array (
                'id' => 30,
                'part_id' => 2,
                'name' => 'Новостей на странице',
                'description' => 'Кол-во новостей на одной странице блога',
                'code' => 'blog_pagination',
                'value' => '9',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-11 16:41:09',
                'updated_at' => '2018-01-06 10:19:09',
            ),
            28 => 
            array (
                'id' => 31,
                'part_id' => 3,
                'name' => 'Заголовок H1 блога',
                'description' => 'Заголовок H1  главной страницы блога',
                'code' => 'blog_h1',
                'value' => '{"ru":"\\u0411\\u043b\\u043e\\u0433  Rosco Shop","uk":"\\u0411\\u043b\\u043e\\u0433 Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstext',
                'sort' => 8,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-17 16:44:14',
            ),
            29 => 
            array (
                'id' => 32,
                'part_id' => 3,
                'name' => 'Заголовок блога',
                'description' => 'Заголовок  главной страницы блога',
                'code' => 'blog_title',
                'value' => '{"ru":"\\u0411\\u043b\\u043e\\u0433  Rosco Shop","uk":"\\u0411\\u043b\\u043e\\u0433 Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstext',
                'sort' => 9,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-17 16:44:14',
            ),
            30 => 
            array (
                'id' => 33,
                'part_id' => 3,
                'name' => 'Описание блога',
                'description' => 'Описание главной страницы блога',
                'code' => 'blog_description',
                'value' => '{"ru":"\\u0411\\u043b\\u043e\\u0433  Rosco Shop","uk":"\\u0411\\u043b\\u043e\\u0433 Rosco Shop"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstextarea',
                'sort' => 10,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-17 16:44:14',
            ),
            31 => 
            array (
                'id' => 34,
                'part_id' => 3,
                'name' => 'Ключевые слова блога',
                'description' => 'Ключевые слова  главной страницы блога',
                'code' => 'blog_keywords',
                'value' => '{"ru":"Rosco, Shop, \\u0411\\u043b\\u043e\\u0433","uk":"Rosco, Shop, \\u0411\\u043b\\u043e\\u0433"}',
                'options' => NULL,
                'relation' => NULL,
                'type' => 'langstextarea',
                'sort' => 11,
                'created_at' => '2018-01-03 18:28:14',
                'updated_at' => '2018-01-17 16:44:14',
            ),
            32 => 
            array (
                'id' => 35,
                'part_id' => 1,
                'name' => 'Адрес компании',
                'description' => NULL,
                'code' => 'address',
                'value' => '65012, Украина, Одесская область, Одесса, ул. Вице-Адмирала Азарова, 13',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-25 19:25:53',
                'updated_at' => '2018-01-25 19:25:51',
            ),
            33 => 
            array (
                'id' => 36,
                'part_id' => 1,
                'name' => 'Почта компании',
                'description' => NULL,
                'code' => 'email',
                'value' => 'sale@rosco.ua',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-25 19:25:54',
                'updated_at' => '2018-01-25 19:25:55',
            ),
            34 => 
            array (
                'id' => 37,
                'part_id' => 1,
                'name' => 'Телефон',
                'description' => NULL,
                'code' => 'phone_3',
            'value' => '+38 (095) 390-00-77',
                'options' => NULL,
                'relation' => NULL,
                'type' => NULL,
                'sort' => 0,
                'created_at' => '2018-01-25 19:25:47',
                'updated_at' => '2018-01-25 19:25:52',
            ),
        ));
        
        
    }
}