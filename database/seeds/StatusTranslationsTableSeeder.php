<?php

use Illuminate\Database\Seeder;

class StatusTranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('status_translations')->delete();
        
        \DB::table('status_translations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'status_id' => 1,
                'name' => 'В наличии',
                'description' => 'Товар в наличии и его можно купить',
                'lang_code' => 'ru',
            ),
            1 => 
            array (
                'id' => 2,
                'status_id' => 2,
                'name' => 'Под заказ',
                'description' => 'Срок появления товара 2-3 дня',
                'lang_code' => 'ru',
            ),
            2 => 
            array (
                'id' => 3,
                'status_id' => 3,
                'name' => 'Отсутствует',
                'description' => 'Товара нет в наличии',
                'lang_code' => 'ru',
            ),
            3 => 
            array (
                'id' => 4,
                'status_id' => 1,
                'name' => 'В наявності',
                'description' => 'Товар в наявності і його можна купити',
                'lang_code' => 'uk',
            ),
            4 => 
            array (
                'id' => 5,
                'status_id' => 2,
                'name' => 'Під замовлення',
                'description' => 'Термін появи товару 2-3 дня',
                'lang_code' => 'uk',
            ),
            5 => 
            array (
                'id' => 6,
                'status_id' => 3,
                'name' => 'Відсутній',
                'description' => 'Товару немає в наявності',
                'lang_code' => 'uk',
            ),
            6 => 
            array (
                'id' => 7,
                'status_id' => 4,
                'name' => 'Новый',
                'description' => 'Заказ получен и ожидает обработки менеджером',
                'lang_code' => 'ru',
            ),
            7 => 
            array (
                'id' => 8,
                'status_id' => 5,
                'name' => 'В работе',
                'description' => 'Заказ обработан менеджером',
                'lang_code' => 'ru',
            ),
            8 => 
            array (
                'id' => 9,
                'status_id' => 6,
                'name' => 'Отменен',
                'description' => 'Заказ отменен',
                'lang_code' => 'ru',
            ),
            9 => 
            array (
                'id' => 10,
                'status_id' => 7,
                'name' => 'Выполнен',
                'description' => 'Заказ доставлен',
                'lang_code' => 'ru',
            ),
            10 => 
            array (
                'id' => 12,
                'status_id' => 4,
                'name' => 'Новий',
                'description' => 'Замовлення отримано і чекає обробки менеджером',
                'lang_code' => 'uk',
            ),
            11 => 
            array (
                'id' => 13,
                'status_id' => 5,
                'name' => 'В роботі',
                'description' => 'Замовлення оброблений менеджером',
                'lang_code' => 'uk',
            ),
            12 => 
            array (
                'id' => 14,
                'status_id' => 6,
                'name' => 'Скасовано',
                'description' => 'Замовлення скасовано',
                'lang_code' => 'uk',
            ),
            13 => 
            array (
                'id' => 15,
                'status_id' => 7,
                'name' => 'Виконаний',
                'description' => 'Замовлення доставлено',
                'lang_code' => 'uk',
            ),
        ));
        
        
    }
}