<?php

use Illuminate\Database\Seeder;

class PaymentToShipmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_to_shipments')->delete();
        
        \DB::table('payment_to_shipments')->insert(array (
            0 => 
            array (
                'id' => 3,
                'shipment_id' => 3,
                'payment_id' => 4,
            ),
            1 => 
            array (
                'id' => 6,
                'shipment_id' => 1,
                'payment_id' => 4,
            ),
            2 => 
            array (
                'id' => 7,
                'shipment_id' => 1,
                'payment_id' => 1,
            ),
        ));
        
        
    }
}