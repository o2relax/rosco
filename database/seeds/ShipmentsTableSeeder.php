<?php

use Illuminate\Database\Seeder;

class ShipmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shipments')->delete();
        
        \DB::table('shipments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'novaya-pochta',
                'image' => 'novaya-pochta.jpg',
                'price' => NULL,
                'free_from' => NULL,
                'is_active' => 1,
                'created_at' => '2018-01-16 02:15:10',
                'updated_at' => '2018-01-16 05:19:10',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'deliveri',
                'image' => 'deliveri.jpg',
                'price' => NULL,
                'free_from' => NULL,
                'is_active' => 1,
                'created_at' => '2018-01-16 02:39:05',
                'updated_at' => '2018-01-16 02:39:05',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'samovyvoz',
                'image' => 'samovyvoz.png',
                'price' => NULL,
                'free_from' => NULL,
                'is_active' => 1,
                'created_at' => '2018-01-16 02:44:46',
                'updated_at' => '2018-01-16 02:44:46',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'dostavka-transportom-kompanii',
                'image' => 'dostavka-transportom-kompanii.png',
                'price' => NULL,
                'free_from' => NULL,
                'is_active' => 1,
                'created_at' => '2018-01-16 02:45:49',
                'updated_at' => '2018-01-16 02:45:49',
            ),
        ));
        
        
    }
}