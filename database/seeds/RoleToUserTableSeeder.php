<?php

use Illuminate\Database\Seeder;

class RoleToUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_to_user')->delete();
        
        \DB::table('role_to_user')->insert(array (
            0 => 
            array (
                'user_id' => 1,
                'role_id' => 1,
            ),
            1 => 
            array (
                'user_id' => 2,
                'role_id' => 2,
            ),
            2 => 
            array (
                'user_id' => 33,
                'role_id' => 3,
            ),
            3 => 
            array (
                'user_id' => 35,
                'role_id' => 3,
            ),
        ));
        
        
    }
}