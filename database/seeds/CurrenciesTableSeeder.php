<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('currencies')->delete();
        
        \DB::table('currencies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Доллар',
                'code' => 'USD',
                'prefix' => '$',
                'postfix' => NULL,
                'round' => '2',
                'rate' => 1.0,
                'is_active' => 1,
                'created_at' => '2017-12-29 02:14:25',
                'updated_at' => '2017-12-28 09:16:02',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Гривна',
                'code' => 'UAH',
                'prefix' => NULL,
                'postfix' => '&nbsp;грн.',
                'round' => '0',
                'rate' => 27.95,
                'is_active' => 1,
                'created_at' => '2017-12-29 02:15:08',
                'updated_at' => '2018-01-16 17:31:46',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Евро',
                'code' => 'EUR',
                'prefix' => '€',
                'postfix' => NULL,
                'round' => '2',
                'rate' => 0.8342,
                'is_active' => 1,
                'created_at' => '2017-12-29 17:02:01',
                'updated_at' => '2017-12-29 17:02:01',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Золото',
                'code' => 'GLD',
                'prefix' => 'G',
                'postfix' => NULL,
                'round' => '5',
                'rate' => 0.01,
                'is_active' => 1,
                'created_at' => '2017-12-28 09:37:21',
                'updated_at' => '2017-12-28 09:37:21',
            ),
        ));
        
        
    }
}