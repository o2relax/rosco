<?php

use Illuminate\Database\Seeder;

class ShipmentTranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shipment_translations')->delete();
        
        \DB::table('shipment_translations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'shipment_id' => 1,
                'name' => 'Новая почта',
                'description' => NULL,
                'lang_code' => 'ru',
            ),
            1 => 
            array (
                'id' => 2,
                'shipment_id' => 1,
                'name' => 'Нова пошта',
                'description' => NULL,
                'lang_code' => 'uk',
            ),
            2 => 
            array (
                'id' => 3,
                'shipment_id' => 2,
                'name' => 'Деливери',
                'description' => NULL,
                'lang_code' => 'ru',
            ),
            3 => 
            array (
                'id' => 4,
                'shipment_id' => 2,
                'name' => 'Делівері',
                'description' => NULL,
                'lang_code' => 'uk',
            ),
            4 => 
            array (
                'id' => 5,
                'shipment_id' => 3,
                'name' => 'Самовывоз',
                'description' => NULL,
                'lang_code' => 'ru',
            ),
            5 => 
            array (
                'id' => 6,
                'shipment_id' => 3,
                'name' => 'Самовивіз',
                'description' => NULL,
                'lang_code' => 'uk',
            ),
            6 => 
            array (
                'id' => 7,
                'shipment_id' => 4,
                'name' => 'Доставка транспортом компании',
                'description' => NULL,
                'lang_code' => 'ru',
            ),
            7 => 
            array (
                'id' => 8,
                'shipment_id' => 4,
                'name' => 'Доставка транспортом компанії',
                'description' => NULL,
                'lang_code' => 'uk',
            ),
        ));
        
        
    }
}