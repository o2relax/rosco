<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'surname' => NULL,
                'email' => 'admin@admin.loc',
                'password' => '$2y$10$Hr2KeG1nixnP6Jrm2qGXV.wkc7lD7qtJztJgbyWaSUl4p679hEa3K',
                'remember_token' => 'M8qPOjVK1sRz3XmwX0eIAngIeZokYMGtFxTmnudXRixwI1bBvsFiK2u5FPpv',
                'created_at' => '2017-11-28 13:38:51',
                'updated_at' => '2017-11-28 13:38:51',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Manager 1',
                'surname' => NULL,
                'email' => 'manager@admin.loc',
                'password' => '$2y$10$Hr2KeG1nixnP6Jrm2qGXV.wkc7lD7qtJztJgbyWaSUl4p679hEa3K',
                'remember_token' => 'quxZ12xzt5Bl4E50VP8MEBtzebaUSBb4utHYjzoXIy4GiP8xyZhBMZ277TLJ',
                'created_at' => '2017-11-28 13:38:51',
                'updated_at' => '2017-11-28 13:38:51',
            ),
            2 => 
            array (
                'id' => 33,
                'name' => 'Роман',
                'surname' => NULL,
                'email' => 'webmaster865@yandex.ru',
                'password' => '$2y$10$6wUpNhz13jAETTuGTYt4Ius.QTESEhx9nWpB837QKeUBtB34NCPEa',
                'remember_token' => 'sM5IFCF9rgdZWT2g0GixsTwbqyqLnp94BPL1gsOYI1FsUO0vpaPJPwrdHKJd',
                'created_at' => '2017-12-11 14:11:35',
                'updated_at' => '2018-01-17 05:45:47',
            ),
            3 => 
            array (
                'id' => 35,
                'name' => 'Тест',
                'surname' => NULL,
                'email' => 'test@front.end',
                'password' => '$2y$10$wJqLZ8ngZBLUjAMqXb.hMO1Os2M6Gyug.v89JBZfSa2srbcmjYoZW',
                'remember_token' => NULL,
                'created_at' => '2018-01-16 22:46:55',
                'updated_at' => '2018-01-16 23:58:30',
            ),
        ));
        
        
    }
}