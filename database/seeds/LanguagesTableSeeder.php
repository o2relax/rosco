<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('languages')->delete();
        
        \DB::table('languages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Русский',
                'code' => 'ru',
                'icon' => 'ru.png',
                'is_active' => 1,
                'created_at' => '2017-12-01 17:52:24',
                'updated_at' => '2017-12-25 22:29:38',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Українська',
                'code' => 'uk',
                'icon' => 'uk.png',
                'is_active' => 1,
                'created_at' => '2017-12-01 17:52:35',
                'updated_at' => '2018-01-06 04:41:12',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'English',
                'code' => 'en',
                'icon' => 'en.png',
                'is_active' => 0,
                'created_at' => '2017-12-25 22:58:46',
                'updated_at' => '2018-01-05 11:34:21',
            ),
        ));
        
        
    }
}