<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueKeysToSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('slug')->unsigned()->change();
            $table->unique(['slug']);
        });
        Schema::table('products', function (Blueprint $table) {
            $table->string('slug')->unsigned()->change();
            $table->unique(['slug']);
        });
        Schema::table('manufacturers', function (Blueprint $table) {
            $table->string('slug')->unsigned()->change();
            $table->unique(['slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
