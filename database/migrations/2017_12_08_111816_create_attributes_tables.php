<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attribute_group_id');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });
        Schema::create('attribute_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['attribute_id', 'lang_code']);
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
        });
        Schema::create('attribute_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });
        Schema::create('attribute_group_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_group_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['attribute_group_id', 'lang_code']);
            $table->foreign('attribute_group_id')->references('id')->on('attribute_groups')->onDelete('cascade');
        });
        Schema::create('product_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('attribute_id')->unsigned();
            $table->text('text')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['product_id', 'attribute_id', 'lang_code']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attribute_translations', function(Blueprint $table)
        {
            $table->dropForeign([
                'attribute_id',
            ]);
        });
        Schema::table('attribute_group_translations', function(Blueprint $table)
        {
            $table->dropForeign([
                'attribute_group_id',
            ]);
        });
        Schema::table('product_attributes', function(Blueprint $table)
        {
            $table->dropForeign([
                'product_id',
            ]);
        });
        Schema::dropIfExists('attributes');
        Schema::dropIfExists('attribute_translations');
        Schema::dropIfExists('attribute_groups');
        Schema::dropIfExists('attribute_group_translations');
        Schema::dropIfExists('product_attributes');
    }
}
