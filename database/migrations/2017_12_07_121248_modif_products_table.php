<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('weight');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('depth');
            $table->dropColumn('discount');
            $table->dropColumn('notification');
            $table->dropColumn('discount_type');
            $table->dropColumn('discount_start_date');
            $table->dropColumn('discount_end_date');
            $table->dropColumn('file');
            $table->dropColumn('is_download');
            $table->dropColumn('is_featured');
            $table->string('image')->nullable()->after('slug');
            $table->integer('status_id')->after('slug')->change();
            $table->timestamp('showed_at')->nullable()->after('is_published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('weight')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('depth')->nullable();
            $table->integer('discount')->default(0);
            $table->string('notification')->nullable();
            $table->string('discount_type')->nullable();
            $table->date('discount_start_date')->nullable();
            $table->date('discount_end_date')->nullable();
            $table->integer('is_download')->default(0);
            $table->integer('is_featured')->default(0);
            $table->string('file')->nullable();
        });
    }
}
