<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStatusTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_status_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_status_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['product_status_id','lang_code']);
            $table->foreign('product_status_id')->references('id')->on('product_statuses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_status_translations');
    }
}
