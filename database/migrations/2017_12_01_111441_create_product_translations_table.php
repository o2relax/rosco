<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_keywords');
            $table->dropColumn('details');
            $table->dropColumn('hero_image');
            $table->renameColumn('code', 'article');
            $table->renameColumn('url', 'slug');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->string('slug')->nullable()->change();
        });
        Schema::create('product_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('name');
            $table->text('short_description')->nullable();
            $table->text('full_description')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['product_id','lang_code']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');

        Schema::table('products', function (Blueprint $table) {
            $table->string('name');
            $table->string('seo_description')->nullable();
            $table->string('seo_keywords')->nullable();
            $table->text('details')->nullable();
            $table->string('hero_image')->nullable();
            $table->renameColumn('article', 'code');
            $table->renameColumn('slug', 'url');
            $table->string('url')->default('')->change();
        });

    }
}
