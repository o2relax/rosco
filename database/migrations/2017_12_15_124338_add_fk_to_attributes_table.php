<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attributes', function (Blueprint $table) {
            $table->dropColumn('attribute_group_id');
        });
        Schema::table('attributes', function (Blueprint $table) {
            $table->integer('attribute_group_id')->unsigned()->after('id');
            $table->foreign('attribute_group_id')->references('id')->on('attribute_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attributes', function(Blueprint $table)
        {
            $table->dropForeign([
                'attribute_group_id',
            ]);
        });
        Schema::table('attributes', function (Blueprint $table) {
            $table->dropColumn('attribute_group_id');
        });
        Schema::table('attributes', function (Blueprint $table) {
            $table->integer('attribute_group_id')->unsigned();
            $table->foreign('attribute_group_id')->references('id')->on('attribute_groups')->onDelete('cascade');
        });
    }
}
