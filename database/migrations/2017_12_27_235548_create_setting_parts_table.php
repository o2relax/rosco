<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->foreign('part_id')->references('id')->on('setting_parts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function(Blueprint $table)
        {
            $table->dropForeign(['part_id']);
        });
        Schema::dropIfExists('setting_parts');
    }
}
