<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });

        Schema::create('menu_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('parent_id')->default(0)->unsigned();
            $table->string('url')->nullable();
            $table->integer('sort')->default(0);
            $table->integer('is_active')->default(1);
            $table->timestamps();

            $table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('menu_links')->onDelete('cascade');
        });

        Schema::create('menu_link_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_link_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['menu_link_id', 'lang_code']);
            $table->foreign('menu_link_id')->references('id')->on('menu_links')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_link_translations', function (Blueprint $table) {
            $table->dropForeign(['menu_link_id']);
        });
        Schema::table('menu_links', function (Blueprint $table) {
            $table->dropForeign(['menu_id', 'parent_id']);
        });
        Schema::dropIfExists('menus');
        Schema::dropIfExists('menu_links');
        Schema::dropIfExists('menu_link_translations');
    }
}
