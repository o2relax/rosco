<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });
        Schema::create('manufacturer_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manufacturer_id')->unsigned();
            $table->string('name')->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['manufacturer_id', 'lang_code']);
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacturer_translations', function(Blueprint $table)
        {
            $table->dropForeign(['manufacturer_id']);
        });
        Schema::dropIfExists('manufacturers');
        Schema::dropIfExists('manufacturer_translations');
    }
}
