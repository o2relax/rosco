<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteNotUsedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('blogs');
        Schema::dropIfExists('cart');
        Schema::dropIfExists('coupons');
        Schema::dropIfExists('events');
        Schema::dropIfExists('faqs');
        Schema::dropIfExists('favorites');
        Schema::dropIfExists('files');
        Schema::dropIfExists('images');
        Schema::dropIfExists('links');
        Schema::dropIfExists('menus');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('plans');
        Schema::dropIfExists('refunds');
        Schema::dropIfExists('subscriptions');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('widgets');
        Schema::dropIfExists('team_user');
        Schema::dropIfExists('teams');
        Schema::dropIfExists('product_variants');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
