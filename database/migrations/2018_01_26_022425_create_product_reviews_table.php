<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('user_id')->default(0);
            $table->string('name')->nullable();
            $table->text('text')->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('viewed')->default(0);
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_reviews', function(Blueprint $table) {
            $table->dropForeign([
                'product_id'
            ]);
        });
        Schema::dropIfExists('product_reviews');
    }
}
