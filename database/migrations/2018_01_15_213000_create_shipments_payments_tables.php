<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsPaymentsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->decimal('price', 8, 2)->nullable();
            $table->decimal('free_from', 8, 2)->nullable();
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });
        Schema::create('shipment_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipment_id')->unsigned();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['shipment_id', 'lang_code']);
            $table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
        });
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->integer('currency_id');
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });
        Schema::create('payment_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')->unsigned();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('lang_code')->index();

            $table->unique(['payment_id', 'lang_code']);
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
        Schema::table('shipment_translations', function (Blueprint $table) {
            $table->dropForeign(['shipment_id']);
        });
        Schema::dropIfExists('shipment_translations');

        Schema::dropIfExists('payments');
        Schema::table('payment_translations', function (Blueprint $table) {
            $table->dropForeign(['payment_id']);
        });
        Schema::dropIfExists('payment_translations');
    }
}
