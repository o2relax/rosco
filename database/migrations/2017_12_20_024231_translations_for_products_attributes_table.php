<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TranslationsForProductsAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_attributes', function(Blueprint $table)
        {
            $table->dropColumn('lang_code');
            $table->dropColumn('text');
        });
        Schema::create('product_attribute_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_attribute_id')->unsigned();
            $table->string('text')->nullabe();
            $table->string('lang_code')->index();

            $table->unique(['product_attribute_id', 'lang_code'], 'prod_attr_lang_code_unique');
            $table->foreign('product_attribute_id')->references('id')->on('product_attributes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_attribute_translations', function(Blueprint $table)
        {
            $table->dropForeign([
                'product_attribute_id'
            ]);
        });
        Schema::dropIfExists('product_attribute_translations');
        Schema::table('product_attributes', function(Blueprint $table) {
            $table->string('lang_code')->index();
            $table->text('text')->nullable();
        });
    }
}
