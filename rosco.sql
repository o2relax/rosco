-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: rosco
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attribute_group_translations`
--

DROP TABLE IF EXISTS `attribute_group_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_group_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_group_translations_attribute_group_id_lang_code_unique` (`attribute_group_id`,`lang_code`),
  KEY `attribute_group_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `attribute_group_translations_attribute_group_id_foreign` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_group_translations`
--

LOCK TABLES `attribute_group_translations` WRITE;
/*!40000 ALTER TABLE `attribute_group_translations` DISABLE KEYS */;
INSERT INTO `attribute_group_translations` VALUES (3,2,'Основные характеристики',NULL,'ru'),(4,2,'Основні характеристики',NULL,'uk');
/*!40000 ALTER TABLE `attribute_group_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_groups`
--

DROP TABLE IF EXISTS `attribute_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_groups`
--

LOCK TABLES `attribute_groups` WRITE;
/*!40000 ALTER TABLE `attribute_groups` DISABLE KEYS */;
INSERT INTO `attribute_groups` VALUES (2,0,'2018-01-06 15:02:49','2018-01-06 15:02:49');
/*!40000 ALTER TABLE `attribute_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_translations`
--

DROP TABLE IF EXISTS `attribute_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_translations_attribute_id_lang_code_unique` (`attribute_id`,`lang_code`),
  KEY `attribute_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `attribute_translations_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_translations`
--

LOCK TABLES `attribute_translations` WRITE;
/*!40000 ALTER TABLE `attribute_translations` DISABLE KEYS */;
INSERT INTO `attribute_translations` VALUES (7,4,'Объем, л',NULL,'ru'),(8,4,'Обсяг, л',NULL,'uk'),(9,5,'Норма затрат препарата, л/га',NULL,'ru'),(10,5,'Норма витрат препарату, л/га',NULL,'uk');
/*!40000 ALTER TABLE `attribute_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(10) unsigned NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attributes_attribute_group_id_foreign` (`attribute_group_id`),
  CONSTRAINT `attributes_attribute_group_id_foreign` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
INSERT INTO `attributes` VALUES (4,2,0,'2018-01-06 15:13:01','2018-01-06 15:13:01'),(5,2,0,'2018-01-06 15:14:13','2018-01-06 15:14:13');
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_translations`
--

DROP TABLE IF EXISTS `blog_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_h1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blog_translations_blog_id_lang_code_unique` (`blog_id`,`lang_code`),
  KEY `blog_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `blog_translations_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_translations`
--

LOCK TABLES `blog_translations` WRITE;
/*!40000 ALTER TABLE `blog_translations` DISABLE KEYS */;
INSERT INTO `blog_translations` VALUES (1,1,'Стеблевой кукурузный мотылек (Ostrinia nubilalis Hb.) : возможен ли контроль?',NULL,'<p><img src=\"/storage/blog/1/74cd46f6817bd93a578fd005e62ca2c5.jpg\" style=\"float: left;\"> <span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" text-align:=\"\" justify;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</span></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>','Стеблевой кукурузный мотылек (Ostrinia nubilalis Hb.) : возможен ли контроль?',NULL,NULL,NULL,'ru'),(2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'uk'),(3,2,'Химический способ борьбы с амброзией полыннолистной',NULL,'<p><img src=\"/storage/blog/2/3f8270b3ebe2dc1a58c8584ec5ea8506.jpg\" style=\"float: left;\"><span style=\"color: rgb(72, 72, 72); font-family: Roboto, sans-serif;\">Реформирование сельскохозяйственного производства и его переход на рыночные условия привели к глубоким изменениям</span><br></p>','Химический способ борьбы с амброзией полыннолистной',NULL,NULL,NULL,'ru'),(4,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'uk'),(5,3,'Химический способ борьбы с амброзией полыннолистной',NULL,'<p><img src=\"/storage/blog/3/af331c6479fbfbd4c0ac7765b5fa8c0c.jpg\" style=\"float: left;\"><span style=\"color: rgb(72, 72, 72); font-family: Roboto, sans-serif;\">Реформирование сельскохозяйственного производства и его переход на рыночные условия привели к глубоким изменениям</span><br></p>',NULL,NULL,NULL,NULL,'ru'),(6,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'uk');
/*!40000 ALTER TABLE `blog_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` VALUES (1,'steblevoy-kukuruznyy-motylek-ostrinia-nubilalis-hb-vozmozhen-li-kontrol','vnimanie-pes-s-galstukom.jpg',1,'2018-01-17 11:44:46','2018-01-17 13:08:56'),(2,'khimicheskiy-sposob-borby-s-ambroziey-polynnolistnoy','khimicheskiy-sposob-borby-s-ambroziey-polynnolistnoy.jpg',1,'2018-01-17 13:18:14','2018-01-17 13:20:18'),(3,'khimicheskiy-sposob-borby-s-ambroziey-polynnolistnoy','khimicheskiy-sposob-borby-s-ambroziey-polynnolistnoy.jpg',1,'2018-01-17 13:20:56','2018-01-17 13:20:56');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_user_id_foreign` (`user_id`),
  CONSTRAINT `cart_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (12,36,'gerbitsidy','gerbicidu.svg','#8bac53',1,'2018-01-05 18:31:08','2018-02-21 12:11:37',0),(15,36,'insektitsidy','insecticidi.svg','#c65e5e',1,'2018-01-06 09:36:58','2018-02-21 12:12:55',0),(16,36,'protraviteli','potraviteli.svg','#79a4f8',1,'2018-01-06 10:40:17','2018-02-21 12:12:21',0),(21,0,'tekhnika','tractor.svg',NULL,0,'2018-01-17 12:29:00','2018-02-21 06:19:41',0),(22,0,'udobreniya','sprout.svg','#8bac53',1,'2018-01-17 12:52:50','2018-03-23 11:22:21',0),(23,0,'posevnoy-material','wheat.svg','#8bac53',1,'2018-01-25 07:35:31','2018-03-23 11:26:22',0),(24,0,'biopreparaty','bug.svg','#8bac53',1,'2018-01-25 07:39:46','2018-03-07 11:01:27',1),(25,22,'mikroudobreniya','microudobrenie.svg','#8b71db',1,'2018-01-25 07:50:49','2018-01-25 07:57:19',0),(28,36,'vspomogatelnye-veshchestva','canistra.svg','#5b4520',1,'2018-01-25 08:00:12','2018-02-21 12:14:46',0),(30,36,'fumiganty','fumigant.svg','#3172f5',1,'2018-01-25 10:24:02','2018-02-21 12:13:50',0),(31,36,'desikanty','sun.svg','#fdcd24',1,'2018-01-25 10:25:42','2018-02-21 12:13:13',0),(33,36,'akaritsidy','pauk.svg','#c76161',1,'2018-01-25 10:31:54','2018-02-21 12:14:20',0),(34,36,'rodentitsidy','rodenticidu.svg','#c65e5e',1,'2018-01-25 10:35:23','2018-02-21 12:14:05',0),(35,36,'fungitsidy','fungicidu.svg','#a593da',1,'2018-01-25 10:36:53','2018-02-21 12:12:38',0),(36,0,'sredstva-zashchity-rasteniy-krupnaya-fasovka',NULL,'#8bac53',1,'2018-02-21 12:04:21','2018-03-23 11:42:42',0),(40,36,'regulyatory-rosta-rasteniy',NULL,'#8bac53',1,'2018-02-21 12:13:34','2018-03-23 11:09:33',0),(100,36,'prilepateli',NULL,'#8bac53',1,'2018-03-23 11:08:33','2018-03-23 11:32:36',0),(101,36,'biologicheskie-preparaty',NULL,'#8bac53',1,'2018-03-23 11:11:14','2018-03-23 11:32:27',0),(102,36,'bytovye-sredstva-zashchity',NULL,'#8bac53',1,'2018-03-23 11:12:19','2018-03-23 11:32:16',0),(103,22,'mineralnye-udobreniya',NULL,NULL,1,'2018-03-23 11:15:51','2018-03-23 11:15:51',0),(104,23,'ovoshchnye-semena',NULL,'#8bac53',1,'2018-03-23 11:29:04','2018-03-23 11:29:27',0),(105,23,'semena-podsolnukha',NULL,NULL,1,'2018-03-23 11:30:02','2018-03-23 11:30:02',0),(106,23,'semena-kukuruzy',NULL,NULL,1,'2018-03-23 11:30:31','2018-03-23 11:30:31',0),(107,23,'semena-pshenitsy',NULL,NULL,1,'2018-03-23 11:31:02','2018-03-23 11:31:02',0),(108,23,'semena-rapsa',NULL,NULL,1,'2018-03-23 11:31:26','2018-03-23 11:31:26',0),(109,0,'soputstvuyushchie-tovary',NULL,NULL,1,'2018-03-23 11:35:30','2018-03-23 11:35:30',0),(110,109,'agrovolokno',NULL,NULL,1,'2018-03-23 11:36:12','2018-03-23 11:36:12',0),(111,109,'plenka-tiplichnaya',NULL,NULL,1,'2018-03-23 11:36:30','2018-03-23 11:36:30',0),(112,109,'kapelnoe-oroshenie',NULL,NULL,1,'2018-03-23 11:36:57','2018-03-23 11:36:57',0),(113,109,'kassety-dlya-rassady',NULL,NULL,1,'2018-03-23 11:37:25','2018-03-23 11:37:25',0),(115,109,'setka',NULL,NULL,1,'2018-03-23 11:38:33','2018-03-23 11:38:33',0),(116,109,'sadovyy-inventar',NULL,NULL,1,'2018-03-23 11:38:54','2018-03-23 11:38:54',0),(117,109,'torfyanye-substraty',NULL,NULL,1,'2018-03-23 11:39:45','2018-03-23 11:39:45',0),(118,109,'sredstva-individualnoy-zashchity',NULL,'#8bac53',1,'2018-03-23 11:40:14','2018-03-23 12:11:19',0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_translations`
--

DROP TABLE IF EXISTS `category_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_translations_category_id_lang_code_unique` (`category_id`,`lang_code`),
  KEY `category_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_translations`
--

LOCK TABLES `category_translations` WRITE;
/*!40000 ALTER TABLE `category_translations` DISABLE KEYS */;
INSERT INTO `category_translations` VALUES (25,12,'Гербициды',NULL,NULL,NULL,NULL,'ru'),(26,12,'Гербіциди',NULL,NULL,NULL,NULL,'uk'),(31,15,'Инсектициды',NULL,NULL,NULL,NULL,'ru'),(32,15,'Інсектициди',NULL,NULL,NULL,NULL,'uk'),(33,16,'Протравители',NULL,NULL,NULL,NULL,'ru'),(34,16,'Протруйники',NULL,NULL,NULL,NULL,'uk'),(43,21,'Техника',NULL,NULL,NULL,NULL,'ru'),(44,21,'Техніка',NULL,NULL,NULL,NULL,'uk'),(45,22,'Удобрения',NULL,NULL,NULL,NULL,'ru'),(46,22,'Добрива',NULL,NULL,NULL,NULL,'uk'),(47,23,'Посевной материал',NULL,NULL,NULL,NULL,'ru'),(48,23,'Насіння',NULL,NULL,NULL,NULL,'uk'),(49,24,'Биопрепараты',NULL,NULL,NULL,NULL,'ru'),(50,24,'Бioпрепарати',NULL,NULL,NULL,NULL,'uk'),(51,25,'Микроудобрения',NULL,NULL,NULL,NULL,'ru'),(52,25,'Мікродобрива',NULL,NULL,NULL,NULL,'uk'),(55,28,'Вспомогательные вещества',NULL,NULL,NULL,NULL,'ru'),(56,28,'Допоміжні речовини',NULL,NULL,NULL,NULL,'uk'),(59,30,'Фумиганты',NULL,NULL,NULL,NULL,'ru'),(60,30,'Фуміганти',NULL,NULL,NULL,NULL,'uk'),(61,31,'Десиканты',NULL,NULL,NULL,NULL,'ru'),(62,31,'Десиканти',NULL,NULL,NULL,NULL,'uk'),(65,33,'Акарициды',NULL,NULL,NULL,NULL,'ru'),(66,33,'Акарициди',NULL,NULL,NULL,NULL,'uk'),(67,34,'Родентициды',NULL,NULL,NULL,NULL,'ru'),(68,34,'Родентициди',NULL,NULL,NULL,NULL,'uk'),(69,35,'Фунгициды',NULL,NULL,NULL,NULL,'ru'),(70,35,'Фунгіциди',NULL,NULL,NULL,NULL,'uk'),(71,36,'CЗР (крупная фасовка)',NULL,NULL,NULL,NULL,'ru'),(72,36,'СЗР (крупная фасовка)',NULL,NULL,NULL,NULL,'uk'),(73,40,'Регуляторы и стимуляторы роста',NULL,NULL,NULL,NULL,'ru'),(74,40,'регуляторы роста растений',NULL,NULL,NULL,NULL,'uk'),(173,100,'Прилепатели',NULL,NULL,NULL,NULL,'ru'),(174,100,NULL,NULL,NULL,NULL,NULL,'uk'),(175,101,'Биологические препараты',NULL,NULL,NULL,NULL,'ru'),(176,101,NULL,NULL,NULL,NULL,NULL,'uk'),(177,102,'Бытовые средства защиты',NULL,NULL,NULL,NULL,'ru'),(178,102,NULL,NULL,NULL,NULL,NULL,'uk'),(179,103,'Минеральные удобрения',NULL,NULL,NULL,NULL,'ru'),(180,103,NULL,NULL,NULL,NULL,NULL,'uk'),(181,104,'Овощные семена',NULL,NULL,NULL,NULL,'ru'),(182,104,NULL,NULL,NULL,NULL,NULL,'uk'),(183,105,'Семена подсолнуха',NULL,NULL,NULL,NULL,'ru'),(184,105,NULL,NULL,NULL,NULL,NULL,'uk'),(185,106,'Семена кукурузы',NULL,NULL,NULL,NULL,'ru'),(186,106,NULL,NULL,NULL,NULL,NULL,'uk'),(187,107,'Семена пшеницы',NULL,NULL,NULL,NULL,'ru'),(188,107,NULL,NULL,NULL,NULL,NULL,'uk'),(189,108,'Семена рапса',NULL,NULL,NULL,NULL,'ru'),(190,108,NULL,NULL,NULL,NULL,NULL,'uk'),(191,109,'СОПУТСТВУЮЩИЕ ТОВАРЫ',NULL,NULL,NULL,NULL,'ru'),(192,109,NULL,NULL,NULL,NULL,NULL,'uk'),(193,110,'Агроволокно',NULL,NULL,NULL,NULL,'ru'),(194,110,NULL,NULL,NULL,NULL,NULL,'uk'),(195,111,'Пленка типличная',NULL,NULL,NULL,NULL,'ru'),(196,111,NULL,NULL,NULL,NULL,NULL,'uk'),(197,112,'Капельное орошение',NULL,NULL,NULL,NULL,'ru'),(198,112,NULL,NULL,NULL,NULL,NULL,'uk'),(199,113,'Кассеты для рассады',NULL,NULL,NULL,NULL,'ru'),(200,113,NULL,NULL,NULL,NULL,NULL,'uk'),(203,115,'Сетка',NULL,NULL,NULL,NULL,'ru'),(204,115,NULL,NULL,NULL,NULL,NULL,'uk'),(205,116,'Садовый инвентарь',NULL,NULL,NULL,NULL,'ru'),(206,116,NULL,NULL,NULL,NULL,NULL,'uk'),(207,117,'Торфяные субстраты',NULL,NULL,NULL,NULL,'ru'),(208,117,NULL,NULL,NULL,NULL,NULL,'uk'),(209,118,'Средства индивидуальной защиты',NULL,NULL,NULL,NULL,'ru'),(210,118,NULL,NULL,NULL,NULL,NULL,'uk');
/*!40000 ALTER TABLE `category_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postfix` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `round` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `rate` double(15,8) NOT NULL DEFAULT '1.00000000',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (1,'Доллар','USD',NULL,'$','2',1.00000000,1,'2017-12-28 23:14:25','2018-03-19 11:42:16'),(2,'Гривна','UAH',NULL,'&nbsp;грн.','0',27.95000000,1,'2017-12-28 23:15:08','2018-01-16 14:31:46'),(3,'Евро','EUR','€',NULL,'2',0.83420000,1,'2017-12-29 14:02:01','2017-12-29 14:02:01'),(4,'Золото','GLD','G',NULL,'5',0.01000000,1,'2017-12-28 06:37:21','2017-12-28 06:37:21');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount_items`
--

DROP TABLE IF EXISTS `discount_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `discount_id` int(10) unsigned NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `discount_items_discount_id_foreign` (`discount_id`),
  CONSTRAINT `discount_items_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount_items`
--

LOCK TABLES `discount_items` WRITE;
/*!40000 ALTER TABLE `discount_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discounts`
--

DROP TABLE IF EXISTS `discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` decimal(8,2) NOT NULL DEFAULT '0.00',
  `type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discounts`
--

LOCK TABLES `discounts` WRITE;
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `languages_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'Русский','ru','ru.png',1,'2017-12-01 14:52:24','2017-12-25 19:29:38'),(2,'Українська','uk','uk.png',1,'2017-12-01 14:52:35','2018-01-06 01:41:12'),(4,'English','en','en.png',0,'2017-12-25 19:58:46','2018-02-16 12:38:12');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer_translations`
--

DROP TABLE IF EXISTS `manufacturer_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `manufacturer_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manufacturer_translations_manufacturer_id_lang_code_unique` (`manufacturer_id`,`lang_code`),
  KEY `manufacturer_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `manufacturer_translations_manufacturer_id_foreign` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer_translations`
--

LOCK TABLES `manufacturer_translations` WRITE;
/*!40000 ALTER TABLE `manufacturer_translations` DISABLE KEYS */;
INSERT INTO `manufacturer_translations` VALUES (1,1,'Ukravit','UkravitUkravitUkravitUkravitUkravit',NULL,NULL,NULL,NULL,'ru'),(2,1,NULL,NULL,NULL,NULL,NULL,NULL,'uk'),(5,3,'Щелково Агрохим',NULL,NULL,NULL,NULL,NULL,'ru'),(6,3,NULL,NULL,NULL,NULL,NULL,NULL,'uk'),(7,4,'Syngenta','Syngenta (Сингента)',NULL,NULL,NULL,NULL,'ru'),(8,4,'Syngenta','Syngenta (Сингента)',NULL,NULL,NULL,NULL,'uk'),(9,5,'Alfa smart agro','Alfa smart agro (Альфа смарт агро)',NULL,NULL,NULL,NULL,'ru'),(10,5,'Alfa smart agro',NULL,NULL,NULL,NULL,NULL,'uk'),(11,6,'Avgust','Avgust (Август)',NULL,NULL,NULL,NULL,'ru'),(12,6,'Avgust (Август)',NULL,NULL,NULL,NULL,NULL,'uk'),(13,7,'Basf','Basf (Басф)',NULL,NULL,NULL,NULL,'ru'),(14,7,'Basf (Басф)',NULL,NULL,NULL,NULL,NULL,'uk'),(15,8,'Нертус','Нертус',NULL,NULL,NULL,NULL,'ru'),(16,8,'Нертус','Нертус',NULL,NULL,NULL,NULL,'uk'),(17,9,'Bayer','Bayer (Байер)',NULL,NULL,NULL,NULL,'ru'),(18,9,'Bayer',NULL,NULL,NULL,NULL,NULL,'uk'),(19,10,'Du Pont','Du Pont (Дюпон)',NULL,NULL,NULL,NULL,'ru'),(20,10,'Du Pont (Дюпон)',NULL,NULL,NULL,NULL,NULL,'uk'),(21,11,'Астракем','Астракем',NULL,NULL,NULL,NULL,'ru'),(22,11,'Астракем',NULL,NULL,NULL,NULL,NULL,'uk'),(23,12,'Agri Sciences','Agri Sciences',NULL,NULL,NULL,NULL,'ru'),(24,12,'Agri Sciences',NULL,NULL,NULL,NULL,NULL,'uk'),(25,13,'Долина','Долина',NULL,NULL,NULL,NULL,'ru'),(26,13,'Долина',NULL,NULL,NULL,NULL,NULL,'uk'),(27,14,'Экоорганик','Экоорганик',NULL,NULL,NULL,NULL,'ru'),(28,14,'Экоорганик',NULL,NULL,NULL,NULL,NULL,'uk'),(29,15,'Valagro','Valagro',NULL,NULL,NULL,NULL,'ru'),(30,15,'Valagro',NULL,NULL,NULL,NULL,NULL,'uk'),(31,16,'Terra Tarsa','Terra Tarsa',NULL,NULL,NULL,NULL,'ru'),(32,16,'Terra Tarsa',NULL,NULL,NULL,NULL,NULL,'uk'),(33,17,'Haifa','Haifa',NULL,NULL,NULL,NULL,'ru'),(34,17,'Haifa',NULL,NULL,NULL,NULL,NULL,'uk'),(35,18,'Yara','Yara (Яра)',NULL,NULL,NULL,NULL,'ru'),(36,18,'Yara (Яра)',NULL,NULL,NULL,NULL,NULL,'uk'),(37,19,'Ирригатор Украина','Ирригатор Украина',NULL,NULL,NULL,NULL,'ru'),(38,19,'Ирригатор Украина',NULL,NULL,NULL,NULL,NULL,'uk'),(39,20,'Унипак Киев','Унипак Киев',NULL,NULL,NULL,NULL,'ru'),(40,20,'Унипак Киев',NULL,NULL,NULL,NULL,NULL,'uk');
/*!40000 ALTER TABLE `manufacturer_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manufacturers_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturers`
--

LOCK TABLES `manufacturers` WRITE;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` VALUES (1,'ukravit','ukravit.jpg',1,'2017-12-27 15:21:21','2018-01-16 07:48:00'),(3,'betarenua','shchelkovo-agrokhim.jpg',1,'2017-12-26 02:37:02','2017-12-26 02:37:02'),(4,'singenta',NULL,1,'2018-02-21 11:39:52','2018-02-21 11:39:52'),(5,'alfa-smart-agro',NULL,1,'2018-02-21 11:41:10','2018-02-21 11:41:10'),(6,'avgust',NULL,1,'2018-02-21 11:41:30','2018-02-21 11:41:30'),(7,'basf',NULL,1,'2018-02-21 11:41:50','2018-02-21 11:41:50'),(8,'nertus',NULL,1,'2018-02-21 11:42:06','2018-02-21 11:42:06'),(9,'bayer',NULL,1,'2018-02-21 11:42:25','2018-02-21 11:42:25'),(10,'du-pont',NULL,1,'2018-02-21 11:42:39','2018-02-21 11:42:39'),(11,'astrakem',NULL,1,'2018-02-21 11:43:31','2018-02-21 11:43:31'),(12,'agri-sciences',NULL,1,'2018-02-21 11:43:44','2018-02-21 11:43:44'),(13,'dolina',NULL,1,'2018-02-21 11:44:10','2018-02-21 11:44:10'),(14,'ekoorganik',NULL,1,'2018-02-21 11:44:20','2018-02-21 11:44:20'),(15,'valagro',NULL,1,'2018-02-21 11:45:20','2018-02-21 11:45:20'),(16,'terra-tarsa',NULL,1,'2018-02-21 11:45:32','2018-02-21 11:45:32'),(17,'haifa',NULL,1,'2018-02-21 11:45:53','2018-02-21 11:45:53'),(18,'yara',NULL,1,'2018-02-21 11:46:11','2018-02-21 11:46:11'),(19,'irrigator-ukraina',NULL,1,'2018-02-21 11:46:44','2018-02-21 11:46:44'),(20,'unipak-kiev',NULL,1,'2018-02-21 11:46:59','2018-02-21 11:46:59');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_link_translations`
--

DROP TABLE IF EXISTS `menu_link_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_link_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_link_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_link_translations_menu_link_id_lang_code_unique` (`menu_link_id`,`lang_code`),
  KEY `menu_link_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `menu_link_translations_menu_link_id_foreign` FOREIGN KEY (`menu_link_id`) REFERENCES `menu_links` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_link_translations`
--

LOCK TABLES `menu_link_translations` WRITE;
/*!40000 ALTER TABLE `menu_link_translations` DISABLE KEYS */;
INSERT INTO `menu_link_translations` VALUES (1,1,'Агролан','ru'),(2,2,'Каталог','ru'),(3,3,'Лаборатория','ru'),(5,4,'Партнерам','ru'),(6,5,'Контакты','ru'),(7,6,'Блог','ru'),(8,7,'<img src=\"/front/assets/img/search-icon.png\" alt=\"search\"></a>','ru'),(11,1,'Головна','uk'),(12,2,'Довідник','uk'),(13,10,'Скидки','ru'),(14,10,NULL,'uk'),(15,11,'Новые товары','ru'),(16,11,NULL,'uk'),(17,12,'Популярные товары','ru'),(18,12,NULL,'uk'),(19,13,'Свяжитесь с нами','ru'),(20,13,NULL,'uk'),(21,14,'Доставка','ru'),(22,14,NULL,'uk'),(23,15,'О магазине','ru'),(24,15,'Про магазин','uk'),(25,16,'Мои заказы','ru'),(26,16,NULL,'uk'),(27,17,'Мои возвраты','ru'),(28,17,NULL,'uk'),(29,18,'Мои адреса','ru'),(30,18,NULL,'uk'),(31,19,'Личная информация','ru'),(32,19,NULL,'uk'),(33,20,'Моя лаборатрия','ru'),(34,20,NULL,'uk'),(35,21,'Вход','ru'),(36,21,NULL,'uk'),(37,5,'Контакти','uk'),(38,6,NULL,'uk');
/*!40000 ALTER TABLE `menu_link_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_links`
--

DROP TABLE IF EXISTS `menu_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_links_menu_id_foreign` (`menu_id`),
  KEY `menu_links_parent_id_foreign` (`parent_id`),
  CONSTRAINT `menu_links_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
  CONSTRAINT `menu_links_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menu_links` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_links`
--

LOCK TABLES `menu_links` WRITE;
/*!40000 ALTER TABLE `menu_links` DISABLE KEYS */;
INSERT INTO `menu_links` VALUES (1,1,0,'www.agrolan.ua',0,1,'2018-01-03 20:56:00','2018-03-23 08:35:07'),(2,1,0,'catalog',1,1,'2018-01-05 10:01:48','2018-01-05 11:09:18'),(3,1,0,NULL,3,1,'2018-01-05 10:40:22','2018-01-05 06:36:26'),(4,1,0,NULL,2,1,'2018-01-05 10:40:20','2018-01-05 06:36:26'),(5,1,0,'/page/kontakty',4,1,'2018-01-05 10:40:20','2018-01-16 13:16:51'),(6,1,0,'blog',5,1,'2018-01-05 10:40:19','2018-01-17 13:30:18'),(7,1,0,NULL,6,1,'2018-01-05 10:40:17','2018-01-05 06:55:57'),(10,5,0,NULL,0,1,'2018-01-05 18:59:27','2018-01-05 18:59:29'),(11,5,0,NULL,1,1,'2018-01-05 18:59:36','2018-01-05 18:59:38'),(12,5,0,NULL,2,1,'2018-01-05 19:00:01','2018-01-05 19:00:03'),(13,5,0,NULL,3,1,'2018-01-05 19:00:21','2018-01-05 19:00:23'),(14,5,0,NULL,4,1,'2018-01-05 19:00:31','2018-01-05 19:00:33'),(15,5,0,'/page/o-kompanii',5,1,'2018-01-05 19:00:38','2018-01-16 13:08:31'),(16,6,0,NULL,0,1,'2018-01-05 19:01:39','2018-01-05 19:01:41'),(17,6,0,NULL,1,1,'2018-01-05 19:01:47','2018-01-05 19:01:49'),(18,6,0,NULL,2,1,'2018-01-05 19:01:53','2018-01-05 19:01:56'),(19,6,0,NULL,3,1,'2018-01-05 19:02:03','2018-01-05 19:02:05'),(20,6,0,NULL,4,1,'2018-01-05 19:02:10','2018-01-05 19:02:13'),(21,6,0,NULL,5,1,'2018-01-05 19:02:18','2018-01-05 19:02:20');
/*!40000 ALTER TABLE `menu_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'main','Главное',1,'2018-01-03 20:54:33','2018-01-05 09:30:23'),(5,'information','Информация',1,'2018-01-05 18:59:12','2018-01-05 19:03:12'),(6,'account','Моя учетная запись',1,'2018-01-05 19:01:27','2018-01-05 19:01:27');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2015_07_03_133637_create_files_table',1),(4,'2015_09_29_193916_create_images_table',1),(5,'2015_09_29_211249_create_blogs_table',1),(6,'2015_10_03_021221_create_pages_table',1),(7,'2015_10_03_032000_create_widgets_table',1),(8,'2015_10_03_032005_create_archives_table',1),(9,'2015_10_12_195952_create_faqs_table',1),(10,'2015_10_12_196052_create_events_table',1),(11,'2015_10_28_174809_create_menus_table',1),(12,'2015_10_28_175046_create_links_table',1),(13,'2015_11_30_191713_create_user_meta_table',1),(14,'2015_11_30_215038_create_roles_table',1),(15,'2015_11_30_215040_create_role_user_table',1),(16,'2015_12_04_155900_create_teams_table',1),(17,'2015_12_04_155900_create_teams_users_table',1),(18,'2016_02_03_175046_add_tags_to_images',1),(19,'2016_02_03_185046_add_templates_to_pages',1),(20,'2016_03_20_185046_add_templates_to_blogs',1),(21,'2016_03_20_186046_add_templates_to_events',1),(22,'2016_06_01_002825_convert_to_published_at',1),(23,'2016_10_27_224359_create_translations_table',1),(24,'2016_12_11_034855_add_lang_to_translations_table',1),(25,'2017_01_11_132526_create_analytics_table',1),(26,'2017_06_10_234231_add_order_to_menus',1),(27,'2017_07_05_002825_add_blocks',1),(28,'2017_07_08_223935_add_entity_to_images',1),(29,'2015_10_03_021904_create_products_table',2),(30,'2015_10_03_022009_create_product_variants_table',2),(31,'2015_10_03_025411_create_cart_table',2),(32,'2015_10_03_031453_create_transactions_table',2),(33,'2015_10_25_212216_create_orders_table',2),(34,'2015_10_25_212229_create_subscriptions_table',2),(35,'2016_01_29_061206_create_customer_profiles_table',2),(36,'2016_03_31_153304_create_admin_plans_table',2),(37,'2017_06_28_163746_create_favorites_table',2),(38,'2017_06_29_113145_create_coupons_table',2),(39,'2017_09_07_212216_create_order_items_table',2),(40,'2017_09_10_175754_create_refunds_table',2),(41,'2017_11_30_120136_add_columns_to_users_tale',3),(42,'2017_11_30_124955_add_columns_to_roles_tale',4),(44,'2017_12_01_111441_create_product_translations_table',5),(45,'2017_12_01_120424_create_product_images_table',6),(46,'2017_12_01_125124_create_product_statuses_table',7),(47,'2017_12_01_125832_create_product_status_translations_table',7),(48,'2017_12_01_134625_create_languages_table',8),(50,'2017_12_07_121248_modif_products_table',9),(51,'2017_12_08_111816_create_attributes_tables',10),(53,'2017_12_12_111936_create_categories_table',11),(54,'2017_12_13_121757_add_column_to_categories_tabel',12),(55,'2017_12_14_112828_create_product_to_category_table',13),(56,'2017_12_14_132507_rename_role_user_table',14),(57,'2017_12_15_124338_add_fk_to_attributes_table',15),(59,'2017_12_20_024231_translations_for_products_attributes_table',17),(61,'2017_12_20_015429_add_fk_to_products_attributes_table',18),(62,'2017_12_25_115910_change_translations_tables',19),(63,'2017_12_25_143744_add_column_to_languages_table',20),(65,'2017_12_26_010316_create_manufacturers_table',21),(66,'2017_12_27_211714_add_manufacturer_id_to_products_table',22),(70,'2017_12_27_211923_delete_not_used_tables',23),(73,'2017_12_27_235201_create_settings_table',24),(74,'2017_12_27_235548_create_setting_parts_table',24),(75,'2017_12_28_015056_create_currencies_table',25),(76,'2017_12_28_020746_add_description_to_settings_table',26),(77,'2017_12_28_055543_rename_active_columns',27),(79,'2017_12_28_111441_create_column_statuses_table',28),(80,'2018_01_03_032011_delete_product_statuses_table',29),(82,'2018_01_03_080951_create_menus_table',30),(84,'2018_01_05_085746_add_language_to_user_meta',31),(85,'2018_01_05_160645_create_modules_table',32),(86,'2018_01_05_173611_modif_active_column_in_categories_table',33),(87,'2018_01_05_190138_add_category_id_column_to_products_table',34),(88,'2018_01_05_224323_create_cart_table',35),(89,'2018_01_05_233510_remove_is_available_from_products_table',35),(92,'2018_01_06_075506_add_product_stats_table',36),(93,'2018_01_06_094740_add_column_to_categories_table',36),(94,'2018_01_06_143553_add_unique_keys_to_slug',37),(95,'2018_01_06_200137_create_orders_table',37),(96,'2018_01_06_214752_add_column_to_orders_table',38),(100,'2018_01_06_231640_delete_foraign_from_orders_table',39),(101,'2018_01_15_213000_create_shipments_payments_tables',40),(102,'2018_01_15_213544_change_price_column_in_products_table',40),(103,'2018_01_16_001614_add_image_to_shipments_payments_tables',41),(104,'2018_01_16_001947_create_payment_to_shipments',42),(105,'2018_01_16_005316_add_unique_on_code_column_shipment_payment_tables',43),(106,'2018_01_16_042358_change_price_column_in_orders',44),(107,'2018_01_16_064200_add_use_stock_to_products_table',45),(108,'2018_01_16_082810_create_pages_table',46),(109,'2018_01_16_093245_add_column_to_modules_table',47),(110,'2018_01_16_165651_add_column_to_pages_table',48),(111,'2018_01_16_190824_add_status_to_orders_table',49),(112,'2018_01_17_050650_add_viewed_to_orders_table',50),(113,'2018_01_17_064308_create_blogs_table',51),(117,'2018_01_17_151053_create_product_sales_table',52),(118,'2018_01_17_211337_create_discounts_table',52),(119,'2018_01_24_150126_add_icon_to_modules_table',53),(120,'2018_01_25_172709_create_subscribes_table',54),(121,'2018_01_26_022425_create_product_reviews_table',55);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci,
  `use_cache` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'icon-list','Список категорий','category_list','{\"heading\":{\"ru\":\"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433\",\"uk\":\"\\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a\"}}',1,1,'2018-01-09 14:15:26','2018-01-27 12:30:05'),(2,'icon-grid','Плитки категорий','category_tiles','{\"heading\":{\"ru\":null,\"uk\":null}}',1,1,'2018-01-09 14:15:26','2018-01-27 12:29:58'),(3,'icon-new','Последние товары','products_latest','{\"heading\":{\"ru\":\"\\u041d\\u043e\\u0432\\u0438\\u043d\\u043a\\u0438\",\"uk\":\"\\u041d\\u043e\\u0432\\u0438\\u043d\\u0438\"},\"count\":\"6\"}',1,1,'2018-01-09 14:15:26','2018-01-27 12:30:01'),(4,'icon-thumbs-up3','Популярные товары','products_popular','{\"heading\":{\"ru\":\"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u044b\\u0435 \\u0442\\u043e\\u0432\\u0430\\u0440\\u044b\",\"uk\":\"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u0456 \\u0442\\u043e\\u0432\\u0430\\u0440\\u0438\"},\"count\":\"6\"}',1,1,'2018-01-09 14:15:26','2018-01-27 12:29:59'),(5,'icon-grid6','Блоки категорий','category_blocks','{\"heading\":{\"ru\":\"\\u041a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u0438\",\"uk\":\"\\u041a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0456\\u0457\"}}',1,1,'2018-01-09 14:15:26','2018-01-27 12:29:55'),(6,'icon-truck','Доставка','delivery','{\"heading\":{\"ru\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430\",\"uk\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430\"},\"text\":{\"ru\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u0438\\u043c \\u0432\\u0430\\u0448\\u0438 \\u0437\\u0430\\u043a\\u0430\\u0437\\u044b, \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438 \\u0432 \\u043b\\u044e\\u0431\\u043e\\u0439 \\u0433\\u043e\\u0440\\u043e\\u0434 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u044b \\u0437\\u0430 1-2 \\u0440\\u0430\\u0431\\u043e\\u0447\\u0438\\u0445 \\u0434\\u043d\\u044f\",\"uk\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u0438\\u043c\\u043e \\u0432\\u0430\\u0448\\u0456 \\u0437\\u0430\\u043c\\u043e\\u0432\\u043b\\u0435\\u043d\\u043d\\u044f, \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u0447\\u043d\\u043e \\u0432 \\u0431\\u0443\\u0434\\u044c-\\u044f\\u043a\\u0435 \\u043c\\u0456\\u0441\\u0442\\u043e \\u0423\\u043a\\u0440\\u0430\\u0457\\u043d\\u0438 \\u0437\\u0430 1-2 \\u0440\\u043e\\u0431\\u043e\\u0447\\u0438\\u0445 \\u0434\\u043d\\u0456\"}}',1,1,'2018-01-09 14:15:26','2018-01-27 12:29:56'),(7,'icon-credit-card2','Оплата','payment','{\"heading\":{\"ru\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430\",\"uk\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430\"},\"text\":{\"ru\":\"\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u043e\\u0441\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u0438\\u0442\\u044c \\u043e\\u043f\\u043b\\u0430\\u0442\\u0443 \\u043d\\u0430\\u043b\\u0438\\u0447\\u043d\\u044b\\u043c\\u0438 \\u043f\\u0440\\u0438 \\u043f\\u043e\\u043b\\u0443\\u0447\\u0435\\u043d\\u0438\\u0438 \\u043b\\u0438\\u0431\\u043e \\u043a\\u0440\\u0435\\u0434\\u0438\\u0442\\u043d\\u043e\\u0439 \\u043a\\u0430\\u0440\\u0442\\u043e\\u0439\",\"uk\":\"\\u0412\\u0438 \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0434\\u0456\\u0439\\u0441\\u043d\\u0438\\u0442\\u0438 \\u043e\\u043f\\u043b\\u0430\\u0442\\u0443 \\u0433\\u043e\\u0442\\u0456\\u0432\\u043a\\u043e\\u044e \\u043f\\u0440\\u0438 \\u043e\\u0442\\u0440\\u0438\\u043c\\u0430\\u043d\\u043d\\u0456 \\u0430\\u0431\\u043e \\u043a\\u0440\\u0435\\u0434\\u0438\\u0442\\u043d\\u043e\\u044e \\u043a\\u0430\\u0440\\u0442\\u043a\\u043e\\u044e\"}}',1,1,'2018-01-09 14:15:26','2018-01-27 12:29:57'),(8,'icon-percent','Товары со скидкой','products_discount','{\"heading\":{\"ru\":\"\\u0410\\u043a\\u0446\\u0438\\u044f\",\"uk\":\"\\u0410\\u043a\\u0446\\u0456\\u044f\"},\"count\":\"6\"}',1,1,'2018-01-09 14:15:26','2018-02-24 12:56:28'),(9,'icon-image3','Слайдер','slider','[{\"file\":\"0.jpg\",\"link\":\"\\/catalog\",\"title\":{\"ru\":\"\\u041a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u0430\\u044f \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u044f \\u043f\\u043e \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u044b\\u043c \\u0446\\u0435\\u043d\\u0430\\u043c\",\"uk\":\"\\u042f\\u043a\\u0456\\u0441\\u043d\\u0430 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0456\\u044f \\u0437\\u0430 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u0438\\u043c\\u0438 \\u0446\\u0456\\u043d\\u0430\\u043c\\u0438\"}},{\"file\":\"1.jpg\",\"link\":\"\\/catalog\",\"title\":{\"ru\":\"\\u041a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u0430\\u044f \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u044f \\u043f\\u043e \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u044b\\u043c \\u0446\\u0435\\u043d\\u0430\\u043c\",\"uk\":\"\\u042f\\u043a\\u0456\\u0441\\u043d\\u0430 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0456\\u044f \\u0437\\u0430 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u0438\\u043c\\u0438 \\u0446\\u0456\\u043d\\u0430\\u043c\\u0438\"}}]',1,1,'2018-01-09 14:15:26','2018-02-22 10:34:23'),(10,'icon-facebook2','Соц. сети','socials','{\"twitter\":\"https:\\/\\/twitter.com\",\"facebook\":\"https:\\/\\/www.facebook.com\\/rosco.ua\\/\",\"vk\":\"https:\\/\\/vk.com\"}',1,1,'2018-01-09 14:15:26','2018-03-23 08:25:24'),(11,'icon-mail5','Подписка','subscribe','{\"heading\":{\"ru\":\"\\u041f\\u043e\\u0434\\u043f\\u0438\\u0448\\u0438\\u0441\\u044c \\u043d\\u0430 \\u043d\\u0430\\u0448\\u0438 \\u043d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0438:\",\"uk\":\"\\u041f\\u0456\\u0434\\u043f\\u0438\\u0448\\u0438\\u0441\\u044c \\u043d\\u0430 \\u043d\\u0430\\u0448\\u0456 \\u043d\\u043e\\u0432\\u0438\\u043d\\u0438:\"}}',1,1,'2018-01-09 14:15:26','2018-01-27 12:29:59'),(12,'icon-newspaper','Последние новости','news','{\"heading\":{\"ru\":\"\\u041d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0438\",\"uk\":\"\\u041d\\u043e\\u0432\\u0438\\u043d\\u0438\"},\"count\":\"3\"}',1,1,'2018-01-09 14:15:26','2018-01-27 12:30:00'),(13,'icon-file-text','О магазине','about','{\"heading\":{\"ru\":\"\\u0418\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442-\\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u00abAgrolan\\u00bb\",\"uk\":\"\\u0406\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442-\\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u00abAgrolan\\u00bb\"},\"text_left\":{\"ru\":\"\\u0413\\u0440\\u0443\\u043f\\u043f\\u0430 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0439 \\u00ab\\u0420\\u041e\\u0421\\u041a\\u041e\\u00bb \\u0441 2008 \\u0433\\u043e\\u0434\\u0430 \\u0430\\u043a\\u0442\\u0438\\u0432\\u043d\\u043e \\u0440\\u0430\\u0437\\u0432\\u0438\\u0432\\u0430\\u0435\\u0442\\u0441\\u044f \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u044b \\u043a\\u0430\\u043a \\u0434\\u0438\\u0441\\u0442\\u0440\\u0438\\u0431\\u044c\\u044e\\u0442\\u043e\\u0440 \\u0438 \\u0438\\u043c\\u043f\\u043e\\u0440\\u0442\\u0435\\u0440 \\u0441\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432 \\u0437\\u0430\\u0449\\u0438\\u0442\\u044b \\u0440\\u0430\\u0441\\u0442\\u0435\\u043d\\u0438\\u0439, \\u043f\\u043e\\u0441\\u0435\\u0432\\u043d\\u043e\\u0433\\u043e \\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u0430 \\u0438 \\u0443\\u0434\\u043e\\u0431\\u0440\\u0435\\u043d\\u0438\\u0439. \\u0417\\u0430 \\u044d\\u0442\\u043e \\u0432\\u0440\\u0435\\u043c\\u044f \\u043c\\u044b \\u0437\\u0430\\u0440\\u0435\\u043a\\u043e\\u043c\\u0435\\u043d\\u0434\\u043e\\u0432\\u0430\\u043b\\u0438 \\u0441\\u0435\\u0431\\u044f \\u043a\\u0430\\u043a \\u043b\\u043e\\u044f\\u043b\\u044c\\u043d\\u0430\\u044f, \\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u0447\\u043d\\u043e \\u0440\\u0430\\u0437\\u0432\\u0438\\u0432\\u0430\\u044e\\u0449\\u0430\\u044f\\u0441\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f, \\u043e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u0430\\u044f \\u0432 \\u043f\\u0435\\u0440\\u0432\\u0443\\u044e \\u043e\\u0447\\u0435\\u0440\\u0435\\u0434\\u044c \\u043d\\u0430 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u044c \\u0430\\u0433\\u0440\\u0430\\u0440\\u0438\\u0435\\u0432 \\u0432 \\u0438\\u043d\\u043d\\u043e\\u0432\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0445 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u044f\\u0445 \\u0434\\u043b\\u044f \\u0443\\u0432\\u0435\\u043b\\u0438\\u0447\\u0435\\u043d\\u0438\\u044f \\u0443\\u0440\\u043e\\u0436\\u0430\\u0439\\u043d\\u043e\\u0441\\u0442\\u0438 \\u0438 \\u043f\\u0440\\u0438 \\u044d\\u0442\\u043e\\u043c \\u0432 \\u043e\\u043f\\u0442\\u0438\\u043c\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0437\\u0430\\u0442\\u0440\\u0430\\u0442 \\u043d\\u0430 \\u0432\\u044b\\u0440\\u0430\\u0449\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435.\",\"uk\":\"\\u0413\\u0440\\u0443\\u043f\\u0430 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0456\\u0439 \\u00ab\\u0420\\u041e\\u0421\\u041a\\u041e\\u00bb \\u0437 2008 \\u0440\\u043e\\u043a\\u0443 \\u0430\\u043a\\u0442\\u0438\\u0432\\u043d\\u043e \\u0440\\u043e\\u0437\\u0432\\u0438\\u0432\\u0430\\u0454\\u0442\\u044c\\u0441\\u044f \\u043d\\u0430 \\u0440\\u0438\\u043d\\u043a\\u0443 \\u0423\\u043a\\u0440\\u0430\\u0457\\u043d\\u0438 \\u044f\\u043a \\u0434\\u0438\\u0441\\u0442\\u0440\\u0438\\u0431\'\\u044e\\u0442\\u043e\\u0440 \\u0442\\u0430 \\u0456\\u043c\\u043f\\u043e\\u0440\\u0442\\u0435\\u0440 \\u0437\\u0430\\u0441\\u043e\\u0431\\u0456\\u0432 \\u0437\\u0430\\u0445\\u0438\\u0441\\u0442\\u0443 \\u0440\\u043e\\u0441\\u043b\\u0438\\u043d, \\u043f\\u043e\\u0441\\u0456\\u0432\\u043d\\u043e\\u0433\\u043e \\u043c\\u0430\\u0442\\u0435\\u0440\\u0456\\u0430\\u043b\\u0443 \\u0456 \\u0434\\u043e\\u0431\\u0440\\u0438\\u0432. \\u0417\\u0430 \\u0446\\u0435\\u0439 \\u0447\\u0430\\u0441 \\u043c\\u0438 \\u0437\\u0430\\u0440\\u0435\\u043a\\u043e\\u043c\\u0435\\u043d\\u0434\\u0443\\u0432\\u0430\\u043b\\u0438 \\u0441\\u0435\\u0431\\u0435 \\u044f\\u043a \\u043b\\u043e\\u044f\\u043b\\u044c\\u043d\\u0430, \\u0449\\u043e \\u0434\\u0438\\u043d\\u0430\\u043c\\u0456\\u0447\\u043d\\u043e \\u0440\\u043e\\u0437\\u0432\\u0438\\u0432\\u0430\\u0454\\u0442\\u044c\\u0441\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0456\\u044f, \\u043e\\u0440\\u0456\\u0454\\u043d\\u0442\\u043e\\u0432\\u0430\\u043d\\u0430 \\u0432 \\u043f\\u0435\\u0440\\u0448\\u0443 \\u0447\\u0435\\u0440\\u0433\\u0443 \\u043d\\u0430 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u0443 \\u0430\\u0433\\u0440\\u0430\\u0440\\u0456\\u0457\\u0432 \\u0432 \\u0456\\u043d\\u043d\\u043e\\u0432\\u0430\\u0446\\u0456\\u0439\\u043d\\u0438\\u0445 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0456\\u044f\\u0445 \\u0434\\u043b\\u044f \\u0437\\u0431\\u0456\\u043b\\u044c\\u0448\\u0435\\u043d\\u043d\\u044f \\u0432\\u0440\\u043e\\u0436\\u0430\\u0439\\u043d\\u043e\\u0441\\u0442\\u0456 \\u0456 \\u043f\\u0440\\u0438 \\u0446\\u044c\\u043e\\u043c\\u0443 \\u0432 \\u043e\\u043f\\u0442\\u0438\\u043c\\u0456\\u0437\\u0430\\u0446\\u0456\\u0457 \\u0432\\u0438\\u0442\\u0440\\u0430\\u0442 \\u043d\\u0430 \\u0432\\u0438\\u0440\\u043e\\u0449\\u0443\\u0432\\u0430\\u043d\\u043d\\u044f.\"},\"text_right\":{\"ru\":\"\\u041c\\u044b \\u0441\\u043e\\u0437\\u0434\\u0430\\u043b\\u0438 \\u0430\\u0433\\u0440\\u043e\\u043d\\u043e\\u043c\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0446\\u0435\\u043d\\u0442\\u0440 \\u0434\\u043b\\u044f \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u0438 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u0438\\u044f \\u043e\\u043f\\u044b\\u0442\\u043e\\u0432 \\u0438 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0437\\u0430 \\u043f\\u043e\\u043b\\u0443\\u0447\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u0432 \\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438 \\u043e\\u0442 \\u043f\\u043e\\u0433\\u043e\\u0434\\u043d\\u044b\\u0445 \\u0443\\u0441\\u043b\\u043e\\u0432\\u0438\\u0439 \\u0438 \\u043f\\u0440\\u0438\\u043c\\u0435\\u043d\\u044f\\u0435\\u043c\\u044b\\u0445 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0439, \\u0430 \\u0442\\u0430\\u043a\\u0436\\u0435 \\u0441\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432 \\u0437\\u0430\\u0449\\u0438\\u0442\\u044b \\u0440\\u0430\\u0441\\u0442\\u0435\\u043d\\u0438\\u0439 \\u0438 \\u043f\\u043e\\u0441\\u0435\\u0432\\u043d\\u043e\\u0433\\u043e \\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u0430. \\u041d\\u0430\\u0448\\u0430 \\u043b\\u0430\\u0431\\u043e\\u0440\\u0430\\u0442\\u043e\\u0440\\u0438\\u044f \\u0434\\u0435\\u043b\\u0430\\u0435\\u0442 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0437 \\u0433\\u0440\\u0443\\u043d\\u0442\\u0430 \\u0438 \\u043b\\u0438\\u0441\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043f\\u043e\\u0432\\u0435\\u0440\\u0445\\u043d\\u043e\\u0441\\u0442\\u0438. \\u041d\\u0430\\u0448\\u0438 \\u0441\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u0438\\u0441\\u0442\\u044b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0432\\u0443\\u044e\\u0442 \\u0432 \\u043c\\u0435\\u0436\\u0434\\u0443\\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u044b\\u0445 \\u0441\\u0435\\u043c\\u0438\\u043d\\u0430\\u0440\\u0430\\u0445 \\u0438 \\u0435\\u0436\\u0435\\u0433\\u043e\\u0434\\u043d\\u043e \\u043f\\u0440\\u043e\\u0445\\u043e\\u0434\\u044f\\u0442 \\u043f\\u043e\\u0432\\u044b\\u0448\\u0435\\u043d\\u0438\\u0435 \\u043a\\u0432\\u0430\\u043b\\u0438\\u0444\\u0438\\u043a\\u0430\\u0446\\u0438\\u0438. \\u041c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435!\",\"uk\":\"\\u041c\\u0438 \\u0441\\u0442\\u0432\\u043e\\u0440\\u0438\\u043b\\u0438 \\u0430\\u0433\\u0440\\u043e\\u043d\\u043e\\u043c\\u0456\\u0447\\u043d\\u0438\\u0439 \\u0446\\u0435\\u043d\\u0442\\u0440 \\u0434\\u043b\\u044f \\u043c\\u043e\\u0436\\u043b\\u0438\\u0432\\u043e\\u0441\\u0442\\u0456 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u043d\\u044f \\u0434\\u043e\\u0441\\u043b\\u0456\\u0434\\u0456\\u0432 \\u0442\\u0430 \\u0430\\u043d\\u0430\\u043b\\u0456\\u0437\\u0443 \\u043e\\u0442\\u0440\\u0438\\u043c\\u0430\\u043d\\u0438\\u0445 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u0456\\u0432 \\u0432 \\u0437\\u0430\\u043b\\u0435\\u0436\\u043d\\u043e\\u0441\\u0442\\u0456 \\u0432\\u0456\\u0434 \\u043f\\u043e\\u0433\\u043e\\u0434\\u043d\\u0438\\u0445 \\u0443\\u043c\\u043e\\u0432 \\u0456 \\u0437\\u0430\\u0441\\u0442\\u043e\\u0441\\u043e\\u0432\\u0443\\u0432\\u0430\\u043d\\u0438\\u0445 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0456\\u0439, \\u0430 \\u0442\\u0430\\u043a\\u043e\\u0436 \\u0437\\u0430\\u0441\\u043e\\u0431\\u0456\\u0432 \\u0437\\u0430\\u0445\\u0438\\u0441\\u0442\\u0443 \\u0440\\u043e\\u0441\\u043b\\u0438\\u043d \\u0442\\u0430 \\u043f\\u043e\\u0441\\u0456\\u0432\\u043d\\u043e\\u0433\\u043e \\u043c\\u0430\\u0442\\u0435\\u0440\\u0456\\u0430\\u043b\\u0443. \\u041d\\u0430\\u0448\\u0430 \\u043b\\u0430\\u0431\\u043e\\u0440\\u0430\\u0442\\u043e\\u0440\\u0456\\u044f \\u0440\\u043e\\u0431\\u0438\\u0442\\u044c \\u0430\\u043d\\u0430\\u043b\\u0456\\u0437 \\u0433\\u0440\\u0443\\u043d\\u0442\\u0443 \\u0456 \\u043b\\u0438\\u0441\\u0442\\u043a\\u043e\\u0432\\u043e\\u0457 \\u043f\\u043e\\u0432\\u0435\\u0440\\u0445\\u043d\\u0456. \\u041d\\u0430\\u0448\\u0456 \\u0444\\u0430\\u0445\\u0456\\u0432\\u0446\\u0456 \\u0431\\u0435\\u0440\\u0443\\u0442\\u044c \\u0443\\u0447\\u0430\\u0441\\u0442\\u044c \\u0432 \\u043c\\u0456\\u0436\\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u0438\\u0445 \\u0441\\u0435\\u043c\\u0456\\u043d\\u0430\\u0440\\u0430\\u0445 \\u0456 \\u0449\\u043e\\u0440\\u0456\\u0447\\u043d\\u043e \\u043f\\u0440\\u043e\\u0445\\u043e\\u0434\\u044f\\u0442\\u044c \\u043f\\u0456\\u0434\\u0432\\u0438\\u0449\\u0435\\u043d\\u043d\\u044f \\u043a\\u0432\\u0430\\u043b\\u0456\\u0444\\u0456\\u043a\\u0430\\u0446\\u0456\\u0457. \\u041c\\u0438 \\u043f\\u0440\\u043e\\u043f\\u043e\\u043d\\u0443\\u0454\\u043c\\u043e \\u0442\\u0456\\u043b\\u044c\\u043a\\u0438 \\u043d\\u0430\\u0439\\u043a\\u0440\\u0430\\u0449\\u0435 \\u0440\\u0456\\u0448\\u0435\\u043d\\u043d\\u044f!\"}}',1,1,'2018-01-09 14:15:26','2018-01-27 12:29:56'),(14,'icon-image4','Слайдер в каталоге','slider_bar','[{\"file\":\"0.png\"},{\"file\":\"1.png\"}]',1,1,'2018-02-21 23:35:38','2018-02-22 10:42:34');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_order_id_foreign` (`order_id`),
  CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES (1,2,'99',500.00,'1','2018-01-06 22:26:59','2018-01-06 22:26:59'),(2,2,'102',500.00,'2','2018-01-06 22:26:59','2018-01-06 22:26:59'),(3,2,'105',500.00,'2','2018-01-06 22:26:59','2018-01-06 22:26:59'),(4,2,'106',15.00,'2','2018-01-06 22:26:59','2018-01-06 22:26:59'),(5,3,'106',15.00,'1','2018-01-06 22:33:46','2018-01-06 22:33:46'),(6,4,'106',15.00,'1','2018-01-06 22:35:21','2018-01-06 22:35:21'),(7,5,'106',15.00,'1','2018-01-06 22:37:55','2018-01-06 22:37:55'),(8,6,'106',15.00,'2','2018-01-06 22:38:46','2018-01-06 22:38:46'),(9,7,'106',15.00,'1','2018-01-06 22:43:42','2018-01-06 22:43:42'),(10,9,'106',15.00,'1','2018-01-07 02:06:46','2018-01-07 02:06:46'),(11,10,'106',15.00,'1','2018-01-07 02:10:20','2018-01-07 02:10:20'),(12,11,'106',15.00,'1','2018-01-07 02:14:57','2018-01-07 02:14:57'),(13,12,'106',15.00,'1','2018-01-07 02:18:25','2018-01-07 02:18:25'),(14,13,'106',15.00,'2','2018-01-07 02:20:32','2018-01-07 02:20:32'),(15,14,'106',15.00,'1','2018-01-07 02:22:05','2018-01-07 02:22:05'),(16,15,'106',15.00,'2','2018-01-07 02:22:48','2018-01-07 02:22:48'),(17,16,'106',15.00,'1','2018-01-07 03:09:24','2018-01-07 03:09:24'),(18,18,'106',15.00,'1','2018-01-16 03:20:06','2018-01-16 03:20:06'),(19,19,'106',419.25,'1','2018-01-16 03:29:57','2018-01-16 03:29:57'),(27,28,'106',419.25,'1','2018-01-16 05:32:01','2018-01-16 05:32:01'),(28,29,'106',419.25,'1','2018-01-16 05:46:16','2018-01-16 05:46:16'),(29,30,'103',13975.00,'1','2018-01-16 05:50:36','2018-01-16 05:50:36'),(30,31,'103',13975.00,'1','2018-01-16 05:57:01','2018-01-16 05:57:01'),(32,33,'106',419.25,'1','2018-01-16 05:58:48','2018-01-16 05:58:48'),(33,34,'106',419.25,'1','2018-01-16 06:06:17','2018-01-16 06:06:17'),(34,35,'106',419.25,'1','2018-01-16 06:31:30','2018-01-16 06:31:30'),(35,37,'106',419.25,'1','2018-01-16 06:57:34','2018-01-16 06:57:34'),(36,39,'106',419.00,'1','2018-01-16 17:56:41','2018-01-16 17:56:41'),(37,40,'106',419.00,'1','2018-01-16 17:57:41','2018-01-16 17:57:41'),(38,41,'106',419.00,'1','2018-01-16 17:57:55','2018-01-16 17:57:55'),(39,39,'103',419.00,'1','2018-01-16 17:56:41','2018-01-16 17:56:41'),(40,43,'106',419.00,'1','2018-01-24 10:06:56','2018-01-24 10:06:56'),(41,44,'106',377.00,'1','2018-01-24 10:10:28','2018-01-24 10:10:28'),(42,45,'106',377.00,'1','2018-01-25 21:43:48','2018-01-25 21:43:48'),(43,46,'106',377.00,'1','2018-01-30 11:36:45','2018-01-30 11:36:45'),(47,50,'107',13947.00,'1','2018-03-13 09:53:10','2018-03-13 09:53:10'),(61,64,'106',503.00,'1','2018-03-13 11:25:33','2018-03-13 11:25:33'),(62,65,'106',503.00,'1','2018-03-13 11:41:40','2018-03-13 11:41:40'),(63,66,'107',13947.00,'1','2018-03-13 12:33:22','2018-03-13 12:33:22'),(64,67,'107',13947.00,'1','2018-03-13 12:34:14','2018-03-13 12:34:14'),(65,68,'106',503.00,'1','2018-03-13 13:25:09','2018-03-13 13:25:09');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipment_id` int(10) unsigned NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  `currency_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (2,1,'Admin','admin@admin.loc','Кривой Рог','0980747324',1,1,2,5,1,'2018-01-06 22:26:59','2018-01-17 04:58:58'),(3,1,'Admin','admin@admin.loc','Кривой Рог','0980747324',1,1,1,4,0,'2018-01-06 22:33:46','2018-01-06 22:33:46'),(4,1,'Admin','admin@admin.loc','арварв','456456',1,1,1,4,0,'2018-01-06 22:35:21','2018-01-06 22:35:21'),(5,1,'Admin','admin@admin.loc','вапвапвапв','4564565',1,1,1,4,0,'2018-01-06 22:37:55','2018-01-06 22:37:55'),(6,1,'Admin','admin@admin.loc','купаыпаы','345345',1,1,1,4,0,'2018-01-06 22:38:46','2018-01-06 22:38:46'),(7,1,'Admin','admin@admin.loc','аорпаоп','567567',1,1,1,4,0,'2018-01-06 22:43:42','2018-01-06 22:43:42'),(9,1,'Admin','admin@admin.loc','gjgjgjgyuj','67567565',1,1,1,4,1,'2018-01-07 02:06:46','2018-01-17 04:37:40'),(10,1,'Admin','admin@admin.loc','ghgfjgjghjghjf','56456',1,1,1,4,0,'2018-01-07 02:10:20','2018-01-07 02:10:20'),(11,1,'Admin','admin@admin.loc','ygjgjgjy','65875678567',1,1,1,4,0,'2018-01-07 02:14:57','2018-01-07 02:14:57'),(12,1,'Admin','admin@admin.loc','gfjhgjhtyjt','867567',1,1,1,4,0,'2018-01-07 02:18:25','2018-01-07 02:18:25'),(13,1,'Admin','webmaster865@yandex.ru','fgdfgdbdfbdf','45634',1,1,1,4,0,'2018-01-07 02:20:32','2018-01-07 02:20:32'),(14,1,'Admin','webmaster865@yandex.ru','5464564564','546456546',1,1,1,4,0,'2018-01-07 02:22:05','2018-01-07 02:22:05'),(15,1,'Admin','webmaster865@yandex.com','sdfgsrggsgg','453453453453',1,1,1,4,0,'2018-01-07 02:22:48','2018-01-07 02:22:48'),(16,1,'Admin','admin@admin.loc','ерпваивариваяи','5546456',1,1,1,4,0,'2018-01-07 03:09:23','2018-01-07 03:09:23'),(18,1,'Admin','webmaster865@yandex.ru','rjdfjyjyfj','4567',1,1,1,4,0,'2018-01-16 03:20:06','2018-01-16 03:20:06'),(19,1,'Admin','webmaster865@yandex.ru','fhfhfdhfdh','65365',1,1,2,4,0,'2018-01-16 03:29:56','2018-01-16 03:29:56'),(28,1,'Admin','webmaster865@yandex.ru','fghfghfghfghfg','546456',1,1,2,4,0,'2018-01-16 05:32:01','2018-01-16 05:32:01'),(29,1,'Admin','webmaster865@yandex.ru','dfgdfgdfgdfgd','345345',1,1,2,4,0,'2018-01-16 05:46:15','2018-01-16 05:46:15'),(30,1,'Admin','webmaster865@yandex.ru','dfgdfgdfgdfg','35653',1,1,2,4,0,'2018-01-16 05:50:36','2018-01-16 05:50:36'),(31,1,'Admin','webmaster865@yandex.ru','dfgdfgdfgdfg','35653',1,1,2,4,0,'2018-01-16 05:57:01','2018-01-16 05:57:01'),(33,1,'Admin','webmaster865@yandex.ru','dfgdfgdfgdfg','35653',1,1,2,4,0,'2018-01-16 05:58:48','2018-01-16 05:58:48'),(34,1,'Admin','webmaster865@yandex.ru','dfgdfgdfgdfg','35653',1,1,2,4,1,'2018-01-16 06:06:16','2018-02-16 12:38:52'),(35,1,'Admin','webmaster865@yandex.ru','fhfghfghfghfg','546456456',1,1,2,4,0,'2018-01-16 06:31:30','2018-01-16 06:31:30'),(37,1,'Admin','webmaster865@yandex.ru','dgddghhf','45345345345',1,1,2,4,1,'2018-01-16 06:57:34','2018-01-17 04:59:09'),(39,33,'Роман','webmaster865@yandex.ru','dfgfdgfhgfgh','546456456',1,1,2,4,0,'2018-01-16 17:56:41','2018-01-16 17:56:41'),(40,33,'Роман','webmaster865@yandex.ru','dfgfdgfhgfgh','546456456',1,1,2,5,1,'2018-01-16 17:57:41','2018-02-08 07:21:33'),(41,33,'Роман','webmaster865@yandex.ru','fthrhfghfghfg','5464564',1,1,2,4,1,'2018-01-16 17:57:55','2018-01-25 22:37:43'),(43,1,'Admin','webmaster865@yandex.ru','ghjhjkgjghj','657568567',1,1,2,4,0,'2018-01-24 10:06:56','2018-01-24 10:06:56'),(44,0,'Ахмед','webmaster865@yandex.com','ghjhjkgjghj','657568567',1,1,2,4,0,'2018-01-24 10:10:28','2018-01-24 10:10:28'),(45,1,'Роман','admin@admin.loc','ыкпвпвапвап','0980747324',1,1,2,4,1,'2018-01-25 21:43:48','2018-03-13 06:27:32'),(46,NULL,'Алексей Алексей Алексей','bombela1988@gmail.com','город-герой Одесса','0634262680',1,1,2,4,1,'2018-01-30 11:36:45','2018-02-08 07:21:02'),(50,1,'Quick Buy','quickbuy@gmail.com','quickbuy','050-328-6614',1,4,2,4,0,'2018-03-13 09:53:10','2018-03-13 09:53:10'),(64,1,'Quick Buy','quickbuy@gmail.com','quickbuy','050-328-1234',1,4,2,4,0,'2018-03-13 11:25:33','2018-03-13 11:25:33'),(65,1,'Quick Buy','quickbuy@gmail.com','quickbuy','050-328-6614',1,4,2,4,0,'2018-03-13 11:41:40','2018-03-13 11:41:40'),(66,1,',Vasya','quickbuy@gmail.com','quickbuy','050-328-3333',1,4,2,4,0,'2018-03-13 12:33:22','2018-03-13 12:33:22'),(67,1,'SADSAD','quickbuy@gmail.com','quickbuy','050-328-1111',1,4,2,7,1,'2018-03-13 12:34:14','2018-03-23 07:11:21'),(68,1,'Василий','quickbuy@gmail.com','quickbuy','050-111-1111',1,4,2,7,1,'2018-03-13 13:25:09','2018-03-21 08:08:07');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_translations`
--

DROP TABLE IF EXISTS `page_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_h1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_translations_page_id_lang_code_unique` (`page_id`,`lang_code`),
  KEY `page_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_translations`
--

LOCK TABLES `page_translations` WRITE;
/*!40000 ALTER TABLE `page_translations` DISABLE KEYS */;
INSERT INTO `page_translations` VALUES (5,3,'О компании','<p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\"><span style=\"margin: 0px; padding: 0px;\">Группа компаний «РОСКО»</span>&nbsp;&nbsp;с 2008 года активно развивается на рынке Украины как дистрибьютор и импортер средств защиты растений, посевного материала и удобрений. За это время мы зарекомендовали себя как лояльная, динамично развивающаяся компания, ориентированная в первую очередь на потребность аграриев в инновационных технологиях для увеличения урожайности и при этом в оптимизации затрат на выращивание.</span></p><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Мы создали<span style=\"margin: 0px; padding: 0px;\">&nbsp;агрономический центр</span>&nbsp;для возможности проведения опытов и анализа полученных результатов в зависимости от погодных условий и применяемых технологий, а также средств защиты растений и посевного материала.</span></p><ul style=\"margin-top: 10px; margin-right: 0px; margin-left: 30px; padding: 0px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px; list-style-type: circle;\"><li style=\"margin: 5px 0px; padding: 0px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Наша лаборатория делает анализ грунта и листовой поверхности.</span></li><li style=\"margin: 5px 0px; padding: 0px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Наши специалисты участвуют в международных семинарах и ежегодно проходят повышение квалификации.</span></li></ul><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\"><span style=\"margin: 0px; padding: 0px;\">Мы предлагаем только лучшее решение! &nbsp;</span></span></p><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Нами создан логистический центр в Одесской области, площадью&nbsp; более 4 тысяч кв метров, предназначенный для хранения пестицидов, удобрений и посевного материала.</span></p><ul style=\"margin-top: 10px; margin-right: 0px; margin-left: 30px; padding: 0px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px; list-style-type: circle;\"><li style=\"margin: 5px 0px; padding: 0px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Мы оказываем услуги по хранению, а также производим доставку и экспедирование.</span></li><li style=\"margin: 5px 0px; padding: 0px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Нашими услугами пользуются многие международные химические компании.</span></li><li style=\"margin: 5px 0px; padding: 0px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Нами налажена складская логистика в регионах так, что мы доставляем товар в любую точку Украины в течении суток.</span></li></ul><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Мы предлагаем&nbsp;<span style=\"margin: 0px; padding: 0px;\">комплекс услуг</span>&nbsp;по внесению средств защиты растений современными самоходными опрыскивателями с высоким клиренсом, т.к они являются лучшим универсальным средством для обработки различных культур жидкими агрохимикатами и обеспечивают высокую скорость внесения.</span></p><div class=\"content\" style=\"margin: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(79, 77, 74); font-family: Roboto; font-size: 16px;\"><div class=\"content_box\" style=\"margin: 0px; padding: 0px;\"><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">Специально для аграриев нами разработана программа:&nbsp;<span style=\"margin: 0px; padding: 0px;\">ФИНАНСОВАЯ ПОДДЕРЖКА и ФИНАНСОВАЯ ЛОЯЛЬНОСТЬ</span>, помогающая строить крепкие и надежные отношения на долгие годы.</span></p><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">С января 2016 года наша компания начинает бессрочную благотворительную акцию для детей сирот из разных регионов Украины. Для этого мы организовали благотворительный фонд «<span style=\"margin: 0px; padding: 0px;\">ДИВО</span>» и команду, готовую прививать в нашей стране европейские ценности.&nbsp;</span></p><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; text-align: justify;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\">&nbsp;</span></p><p style=\"margin-bottom: 0px; padding: 0px; line-height: 25px; text-align: center;\"><span style=\"margin: 0px; padding: 0px; font-size: 10pt;\"><span style=\"margin: 0px; padding: 0px;\">ROSCO - best solution!</span></span></p></div></div>',NULL,NULL,NULL,NULL,'ru'),(6,3,'Про компанію','<p>Група компаній «РОСКО» з 2008 року активно розвивається на ринку України як дистриб\'ютор та імпортер засобів захисту рослин, посівного матеріалу і добрив. За цей час ми зарекомендували себе як лояльна, що динамічно розвивається компанія, орієнтована в першу чергу на потребу аграріїв в інноваційних технологіях для збільшення врожайності і при цьому в оптимізації витрат на вирощування.</p><p>Ми створили агрономічний центр для можливості проведення дослідів та аналізу отриманих результатів в залежності від погодних умов і застосовуваних технологій, а також засобів захисту рослин та посівного матеріалу.</p><p>Наша лабораторія робить аналіз грунту і листкової поверхні.</p><p>Наші фахівці беруть участь в міжнародних семінарах і щорічно проходять підвищення кваліфікації.</p><p>Ми пропонуємо тільки найкраще рішення!&nbsp;</p><p>Нами створений логістичний центр в Одеській області, площею понад 4 тисяч кв метрів, призначений для зберігання пестицидів, добрив і посівного матеріалу.</p><p>Ми надаємо послуги по зберіганню, а також виконуємо доставку і експедирування.</p><p>Нашими послугами користуються багато міжнародні хімічні компанії.</p><p>Нами налагоджена складська логістика в регіонах так, що ми доставляємо товар в будь-яку точку України протягом доби.</p><p>Ми пропонуємо комплекс послуг щодо внесення засобів захисту рослин сучасними самохідними обприскувачами з високим кліренсом, т. до вони є кращим універсальним засобом для обробки різних культур рідкими агрохімікатами і забезпечують високу швидкість внесення.</p><p>Спеціально для аграріїв нами розроблена програма: ФІНАНСОВА ПІДТРИМКА і ФІНАНСОВА ЛОЯЛЬНІСТЬ, допомагає будувати міцні і надійні відносини на довгі роки.</p><p>З січня 2016 року наша компанія розпочинає безстрокову благодійну акцію для дітей сиріт з різних регіонів України. Для цього ми організували благодійний фонд «ДИВО» і команду, готову прищеплювати в нашій країні європейські цінності.&nbsp;</p><p><br></p><p>ROSCO - best solution!</p>',NULL,NULL,NULL,NULL,'uk'),(7,4,'Контакты','<p><span style=\"margin: 0px; padding: 0px; font-size: 20px; color: rgb(0, 0, 0); font-family: Roboto; text-align: center;\">Мы находимся по адресу</span><span style=\"color: rgb(0, 0, 0); font-family: Roboto; font-size: 14px; text-align: center;\"></span></p><p style=\"margin-top: 30px; margin-bottom: 30px; padding: 0px; font-size: 16px; color: rgb(79, 77, 74); font-family: Roboto; text-align: center;\">65101, г. Одесса, ул. 25 Чапаевской Дивизии, 6/1</p><p><span style=\"margin: 0px; padding: 0px; color: rgb(145, 145, 145); font-family: Roboto; text-align: center;\">звоните нам</span><span style=\"color: rgb(0, 0, 0); font-family: Roboto; font-size: 14px; text-align: center;\"></span></p><div class=\"phone\" style=\"margin: 20px 0px 5px; padding: 0px; color: rgb(50, 202, 119); font-size: 22px; font-family: Roboto; text-align: center;\">(044) 390 - 00 - 77</div><div class=\"phone\" style=\"margin: 20px 0px 0px; padding: 0px; color: rgb(50, 202, 119); font-size: 22px; font-family: Roboto; text-align: center;\">(067) 482 - 82 - 00</div>','Контакты',NULL,NULL,NULL,'ru'),(8,4,'Контакти','<p><span style=\"margin: 0px; padding: 0px; font-size: 20px; color: rgb(0, 0, 0); font-family: Roboto; text-align: center;\">Мы находимся по адресу</span><span style=\"color: rgb(0, 0, 0); font-family: Roboto; font-size: 14px; text-align: center;\"></span></p><p style=\"margin-top: 30px; margin-bottom: 30px; padding: 0px; font-size: 16px; color: rgb(79, 77, 74); font-family: Roboto; text-align: center;\">65101, г. Одесса, ул. 25 Чапаевской Дивизии, 6/1</p><p><span style=\"margin: 0px; padding: 0px; color: rgb(145, 145, 145); font-family: Roboto; text-align: center;\">звоните нам</span><span style=\"color: rgb(0, 0, 0); font-family: Roboto; font-size: 14px; text-align: center;\"></span></p><div class=\"phone\" style=\"margin: 20px 0px 5px; padding: 0px; color: rgb(50, 202, 119); font-size: 22px; font-family: Roboto; text-align: center;\">(044) 390 - 00 - 77</div><div class=\"phone\" style=\"margin: 20px 0px 0px; padding: 0px; color: rgb(50, 202, 119); font-size: 22px; font-family: Roboto; text-align: center;\">(067) 482 - 82 - 00</div>',NULL,NULL,NULL,NULL,'uk');
/*!40000 ALTER TABLE `page_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (3,'o-kompanii',NULL,1,'2018-01-16 13:05:49','2018-01-16 13:05:49'),(4,'kontakty','contacts',1,'2018-01-16 13:15:53','2018-01-16 13:15:53');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_to_shipments`
--

DROP TABLE IF EXISTS `payment_to_shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_to_shipments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int(10) unsigned NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_to_shipments_shipment_id_foreign` (`shipment_id`),
  KEY `payment_to_shipments_payment_id_foreign` (`payment_id`),
  CONSTRAINT `payment_to_shipments_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `payment_to_shipments_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_to_shipments`
--

LOCK TABLES `payment_to_shipments` WRITE;
/*!40000 ALTER TABLE `payment_to_shipments` DISABLE KEYS */;
INSERT INTO `payment_to_shipments` VALUES (3,3,4),(6,1,4),(7,1,1);
/*!40000 ALTER TABLE `payment_to_shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_translations`
--

DROP TABLE IF EXISTS `payment_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payment_translations_payment_id_lang_code_unique` (`payment_id`,`lang_code`),
  KEY `payment_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `payment_translations_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_translations`
--

LOCK TABLES `payment_translations` WRITE;
/*!40000 ALTER TABLE `payment_translations` DISABLE KEYS */;
INSERT INTO `payment_translations` VALUES (1,1,'Безналичный расчет',NULL,'ru'),(2,1,'Безготівковий розрахунок',NULL,'uk'),(5,4,'Наложенный платеж',NULL,'ru'),(6,4,'Післяплата',NULL,'uk');
/*!40000 ALTER TABLE `payment_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payments_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,'beznalichnyy-raschet','beznalichnyy-raschet.png',2,1,'2018-01-16 00:25:23','2018-01-16 00:28:38'),(4,'nalozhennyy-platezh','nalozhennyy-platezh.gif',2,1,'2018-01-16 00:35:06','2018-01-16 00:35:19');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attribute_translations`
--

DROP TABLE IF EXISTS `product_attribute_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attribute_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_attribute_id` int(10) unsigned NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `prod_attr_lang_code_unique` (`product_attribute_id`,`lang_code`),
  KEY `product_attribute_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `product_attribute_translations_product_attribute_id_foreign` FOREIGN KEY (`product_attribute_id`) REFERENCES `product_attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attribute_translations`
--

LOCK TABLES `product_attribute_translations` WRITE;
/*!40000 ALTER TABLE `product_attribute_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attribute_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_attributes_product_id_attribute_id_lang_code_unique` (`product_id`,`attribute_id`),
  KEY `product_attributes_attribute_id_foreign` (`attribute_id`),
  CONSTRAINT `product_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attributes`
--

LOCK TABLES `product_attributes` WRITE;
/*!40000 ALTER TABLE `product_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_images_product_id_foreign` (`product_id`),
  CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_reviews`
--

DROP TABLE IF EXISTS `product_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_reviews_product_id_foreign` (`product_id`),
  CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_reviews`
--

LOCK TABLES `product_reviews` WRITE;
/*!40000 ALTER TABLE `product_reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_stats`
--

DROP TABLE IF EXISTS `product_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `carts` int(11) NOT NULL DEFAULT '0',
  `orders` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_stats_product_id_foreign` (`product_id`),
  CONSTRAINT `product_stats_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_stats`
--

LOCK TABLES `product_stats` WRITE;
/*!40000 ALTER TABLE `product_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_to_category`
--

DROP TABLE IF EXISTS `product_to_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_to_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_to_category_product_id_foreign` (`product_id`),
  KEY `product_to_category_category_id_foreign` (`category_id`),
  CONSTRAINT `product_to_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_to_category_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_to_category`
--

LOCK TABLES `product_to_category` WRITE;
/*!40000 ALTER TABLE `product_to_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_to_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_translations`
--

DROP TABLE IF EXISTS `product_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `full_description` text COLLATE utf8mb4_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_translations_product_id_lang_code_unique` (`product_id`,`lang_code`),
  KEY `product_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_translations`
--

LOCK TABLES `product_translations` WRITE;
/*!40000 ALTER TABLE `product_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `substance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `stock` int(11) NOT NULL DEFAULT '0',
  `use_stock` tinyint(4) NOT NULL DEFAULT '0',
  `is_published` int(11) NOT NULL DEFAULT '0',
  `is_related` int(11) NOT NULL DEFAULT '0',
  `showed_at` timestamp NULL DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_to_user`
--

DROP TABLE IF EXISTS `role_to_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_to_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  KEY `role_user_user_id_index` (`user_id`),
  KEY `role_user_role_id_index` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_to_user`
--

LOCK TABLES `role_to_user` WRITE;
/*!40000 ALTER TABLE `role_to_user` DISABLE KEYS */;
INSERT INTO `role_to_user` VALUES (1,1),(2,2),(33,3),(35,3),(38,3),(39,3),(42,3),(43,3),(44,3),(45,3),(46,3),(48,3),(51,1);
/*!40000 ALTER TABLE `role_to_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Admin','danger','admin'),(2,'manager','Manager','primary',NULL),(3,'user','User','success',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting_parts`
--

DROP TABLE IF EXISTS `setting_parts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting_parts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting_parts`
--

LOCK TABLES `setting_parts` WRITE;
/*!40000 ALTER TABLE `setting_parts` DISABLE KEYS */;
INSERT INTO `setting_parts` VALUES (1,'Основные'),(2,'Магазин'),(3,'СЕО'),(4,'Оптимизция'),(5,'Уведомления');
/*!40000 ALTER TABLE `setting_parts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `part_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `relation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `settings_part_id_foreign` (`part_id`),
  CONSTRAINT `settings_part_id_foreign` FOREIGN KEY (`part_id`) REFERENCES `setting_parts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,2,'Валюта магазина','Валюта, в которой по умолчанию показываются товары пользователям магазина','currency_shop','2',NULL,'\\App\\Models\\Currency','select',0,'2017-12-28 16:52:47','2018-03-19 13:08:09'),(2,2,'Валюта каталога','Эталонная валюта, от которой будет считаться любая другая по курсу','currency_catalog','1',NULL,'\\App\\Models\\Currency','select',0,'2017-12-28 16:53:31','2018-03-20 11:37:22'),(3,2,'Язык магазина','Язык, который будет по умолчанию для пользователей','language','1',NULL,'\\App\\Models\\Language','select',0,'2017-12-29 13:23:16','2018-03-14 11:21:58'),(4,3,'Заголовок','Заголовок главной страницы','main_title','{\"ru\":\"Rosco Shop\",\"uk\":\"Rosco Shop\"}',NULL,NULL,'langstext',1,'2018-01-03 15:28:14','2018-01-07 01:24:59'),(7,1,'Адрес сайта','Используется в рассылках и т.д.','domain','http://rosco.local',NULL,NULL,NULL,0,'2018-01-09 03:43:28','2018-01-06 07:12:29'),(8,3,'Описание','Описание главной страницы','main_description','{\"ru\":\"Rosco Shop\",\"uk\":\"Rosco Shop\"}',NULL,NULL,'langstextarea',2,'2018-01-03 15:28:14','2018-01-07 00:58:14'),(9,3,'Ключевые слова','Ключевые слова главной страницы','main_keywords','{\"ru\":\"Rosco, Shop\",\"uk\":\"Rosco, Shop\"}',NULL,NULL,'langstextarea',3,'2018-01-03 15:28:14','2018-01-07 00:58:14'),(10,3,'Ключевые слова каталога','Ключевые слова  главной страницы каталога','catalog_keywords','{\"ru\":\"Rosco, Shop, \\u043a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433\",\"uk\":\"Rosco, Shop, \\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a\"}',NULL,NULL,'langstextarea',6,'2018-01-03 15:28:14','2018-01-07 01:22:24'),(11,3,'Описание каталога','Описание главной страницы каталога','catalog_description','{\"ru\":\"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433 Rosco Shop\",\"uk\":\"\\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a Rosco Shop\"}',NULL,NULL,'langstextarea',5,'2018-01-03 15:28:14','2018-01-07 01:24:09'),(12,3,'Заголовок каталога','Заголовок  главной страницы каталога','catalog_title','{\"ru\":\"\\u041a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433 Rosco Shop\",\"uk\":\"\\u0414\\u043e\\u0432\\u0456\\u0434\\u043d\\u0438\\u043a Rosco Shop\"}',NULL,NULL,'langstext',4,'2018-01-03 15:28:14','2018-01-07 01:24:59'),(13,3,'Заголовок H1 каталога','Заголовок H1  главной страницы каталога','catalog_h1','{\"ru\":\"\\u0418\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d Rosco Shop\",\"uk\":\"\\u0406\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d Rosco Shop\"}',NULL,NULL,'langstext',7,'2018-01-03 15:28:14','2018-02-21 06:58:19'),(14,4,'Ширина картинки в списке','Ширина картинки в списке товаров в каталоге  (в px)','image_list_width','168',NULL,NULL,NULL,0,'2018-01-09 03:43:24','2018-01-06 22:38:41'),(15,4,'Высота картинки в списке','Высота картинки в списке товаров в каталоге  (в px)','image_list_height','188',NULL,NULL,NULL,0,'2018-01-09 03:43:24','2018-01-18 00:38:04'),(16,2,'Товаров на странице','Кол-во товаров на одной странице каталога','catalog_pagination','16',NULL,NULL,NULL,0,'2018-01-11 13:41:09','2018-01-06 07:19:09'),(17,2,'Сортировка по умолчанию',NULL,'catalog_order','created_at','{\"id\":\"ID\",\"created_at\":\"\\u0414\\u0430\\u0442\\u0430 \\u0441\\u043e\\u0437\\u0434\\u0430\\u043d\\u0438\\u044f\",\"price\":\"\\u0426\\u0435\\u043d\\u0430\"}',NULL,'select',0,'2018-01-11 13:41:09','2018-03-14 11:22:48'),(18,2,'Направление по умолчанию',NULL,'catalog_sort','desc','{\"desc\":\"\\u041f\\u043e \\u0443\\u0431\\u044b\\u0432\\u0430\\u043d\\u0438\\u044e\",\"asc\":\"\\u041f\\u043e \\u0432\\u043e\\u0437\\u0440\\u0430\\u0441\\u0442\\u0430\\u043d\\u0438\\u044e\"}',NULL,'select',0,'2018-01-11 13:41:09','2018-01-06 11:05:12'),(19,2,'Длина краткого описания','Длина краткого описания на странице товара','product_description_length','400',NULL,NULL,NULL,0,'2018-01-12 13:01:42','2018-01-12 13:01:47'),(20,2,'Длина краткого описания (список)','Длина краткого описания в списке товаров','product_list_description_length','100',NULL,NULL,NULL,0,'2018-01-12 13:01:43','2018-01-12 13:01:46'),(21,4,'Ширина главной картинки продукта','Ширина главной картинки на странице товара (в px)','product_main_width','366',NULL,NULL,NULL,0,'2018-01-12 13:01:45','2018-01-12 13:01:46'),(22,4,'Высота главной картинки продукта','Высота главной картинки на странице товара  (в px)','product_main_height','366',NULL,NULL,NULL,0,'2018-01-12 13:01:44','2018-01-12 13:01:48'),(23,4,'Ширина дополинтельной картинки продукта','Ширина дополинтельной картинки на странице товара (в px)','product_ext_width','98',NULL,NULL,NULL,0,'2018-01-12 13:01:45','2018-01-12 13:01:46'),(24,4,'Высота дополинтельной картинки продукта','Высота дополинтельной картинки на странице товара  (в px)','product_ext_height','98',NULL,NULL,NULL,0,'2018-01-12 13:01:44','2018-01-12 13:01:48'),(25,5,'Письмо на почту пользователя при заказе',NULL,'notice_email_user_order','1','[\"\\u0412\\u044b\\u043a\\u043b\",\"\\u0412\\u043a\\u043b\"]',NULL,'radio',0,'2018-01-15 13:45:58','2018-01-15 13:45:59'),(26,1,'Почта администратора','Для уведомлений','admin_email','webmaster865@yandex.ru',NULL,NULL,NULL,0,'2018-01-25 16:25:48','2018-01-25 16:25:49'),(27,5,'Письмо на почту администратора при заказе',NULL,'notice_email_admin_order','1','[\"\\u0412\\u044b\\u043a\\u043b\",\"\\u0412\\u043a\\u043b\"]',NULL,'radio',0,'2018-01-15 13:45:58','2018-01-15 13:45:59'),(28,1,'Телефон',NULL,'phone_1','+38 (044) 390-00-77',NULL,NULL,NULL,0,'2018-01-25 16:25:47','2018-01-30 09:12:05'),(29,1,'Телефон',NULL,'phone_2','+38 (098) 390-00-77',NULL,NULL,NULL,0,'2018-01-25 16:25:47','2018-01-30 09:12:05'),(30,2,'Новостей на странице','Кол-во новостей на одной странице блога','blog_pagination','9',NULL,NULL,NULL,0,'2018-01-11 13:41:09','2018-01-06 07:19:09'),(31,3,'Заголовок H1 блога','Заголовок H1  главной страницы блога','blog_h1','{\"ru\":\"\\u0411\\u043b\\u043e\\u0433  Rosco Shop\",\"uk\":\"\\u0411\\u043b\\u043e\\u0433 Rosco Shop\"}',NULL,NULL,'langstext',8,'2018-01-03 15:28:14','2018-01-17 13:44:14'),(32,3,'Заголовок блога','Заголовок  главной страницы блога','blog_title','{\"ru\":\"\\u0411\\u043b\\u043e\\u0433  Rosco Shop\",\"uk\":\"\\u0411\\u043b\\u043e\\u0433 Rosco Shop\"}',NULL,NULL,'langstext',9,'2018-01-03 15:28:14','2018-01-17 13:44:14'),(33,3,'Описание блога','Описание главной страницы блога','blog_description','{\"ru\":\"\\u0411\\u043b\\u043e\\u0433  Rosco Shop\",\"uk\":\"\\u0411\\u043b\\u043e\\u0433 Rosco Shop\"}',NULL,NULL,'langstextarea',10,'2018-01-03 15:28:14','2018-01-17 13:44:14'),(34,3,'Ключевые слова блога','Ключевые слова  главной страницы блога','blog_keywords','{\"ru\":\"Rosco, Shop, \\u0411\\u043b\\u043e\\u0433\",\"uk\":\"Rosco, Shop, \\u0411\\u043b\\u043e\\u0433\"}',NULL,NULL,'langstextarea',11,'2018-01-03 15:28:14','2018-01-17 13:44:14'),(35,1,'Адрес компании',NULL,'address','65012, Украина, Одесская область, Одесса, ул. Вице-Адмирала Азарова, 13',NULL,NULL,NULL,0,'2018-01-25 16:25:53','2018-01-30 09:11:44'),(36,1,'Почта компании',NULL,'email','sale@rosco.ua',NULL,NULL,NULL,0,'2018-01-25 16:25:54','2018-01-25 16:25:55');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment_translations`
--

DROP TABLE IF EXISTS `shipment_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shipment_translations_shipment_id_lang_code_unique` (`shipment_id`,`lang_code`),
  KEY `shipment_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `shipment_translations_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_translations`
--

LOCK TABLES `shipment_translations` WRITE;
/*!40000 ALTER TABLE `shipment_translations` DISABLE KEYS */;
INSERT INTO `shipment_translations` VALUES (1,1,'Новая почта',NULL,'ru'),(2,1,'Нова пошта',NULL,'uk'),(3,2,'Деливери',NULL,'ru'),(4,2,'Делівері',NULL,'uk'),(5,3,'Самовывоз',NULL,'ru'),(6,3,'Самовивіз',NULL,'uk'),(7,4,'Доставка транспортом компании',NULL,'ru'),(8,4,'Доставка транспортом компанії',NULL,'uk');
/*!40000 ALTER TABLE `shipment_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipments`
--

DROP TABLE IF EXISTS `shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `free_from` decimal(8,2) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shipments_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipments`
--

LOCK TABLES `shipments` WRITE;
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` VALUES (1,'novaya-pochta','novaya-pochta.jpg',NULL,NULL,1,'2018-01-15 23:15:10','2018-01-16 02:19:10'),(2,'deliveri','deliveri.jpg',NULL,NULL,1,'2018-01-15 23:39:05','2018-01-15 23:39:05'),(3,'samovyvoz','samovyvoz.png',NULL,NULL,1,'2018-01-15 23:44:46','2018-01-15 23:44:46'),(4,'dostavka-transportom-kompanii','dostavka-transportom-kompanii.png',NULL,NULL,1,'2018-01-15 23:45:49','2018-01-15 23:45:49');
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_translations`
--

DROP TABLE IF EXISTS `status_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `lang_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `status_translations_status_id_lang_code_unique` (`status_id`,`lang_code`),
  KEY `status_translations_lang_code_index` (`lang_code`),
  CONSTRAINT `status_translations_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_translations`
--

LOCK TABLES `status_translations` WRITE;
/*!40000 ALTER TABLE `status_translations` DISABLE KEYS */;
INSERT INTO `status_translations` VALUES (1,1,'В наличии','Товар в наличии и его можно купить','ru'),(2,2,'Под заказ','Срок появления товара 2-3 дня','ru'),(3,3,'Отсутствует','Товара нет в наличии','ru'),(4,1,'В наявності','Товар в наявності і його можна купити','uk'),(5,2,'Під замовлення','Термін появи товару 2-3 дня','uk'),(6,3,'Відсутній','Товару немає в наявності','uk'),(7,4,'Новый','Заказ получен и ожидает обработки менеджером','ru'),(8,5,'В работе','Заказ обработан менеджером','ru'),(9,6,'Отменен','Заказ отменен','ru'),(10,7,'Выполнен','Заказ доставлен','ru'),(12,4,'Новий','Замовлення отримано і чекає обробки менеджером','uk'),(13,5,'В роботі','Замовлення оброблений менеджером','uk'),(14,6,'Скасовано','Замовлення скасовано','uk'),(15,7,'Виконаний','Замовлення доставлено','uk');
/*!40000 ALTER TABLE `status_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES (1,'success','Product','2018-01-03 14:15:22','2018-01-03 14:15:23'),(2,'warning','Product','2018-01-03 14:15:34','2018-01-03 14:15:35'),(3,'danger','Product','2018-01-03 14:15:44','2018-01-03 14:15:44'),(4,'success','Order','2018-01-18 15:19:12','2018-01-18 15:19:13'),(5,'orange-300','Order','2018-01-18 15:19:12','2018-01-18 15:19:13'),(6,'danger','Order','2018-01-18 15:20:02','2018-01-18 15:20:02'),(7,'primary','Order','2018-01-18 15:20:02','2018-01-18 15:20:02');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` VALUES (1,'webmaster865@yandex.ru','2018-01-25 14:50:46','2018-01-25 14:50:46');
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_meta`
--

DROP TABLE IF EXISTS `user_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `language` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `activation_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marketing` tinyint(1) NOT NULL DEFAULT '0',
  `terms_and_cond` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` text COLLATE utf8mb4_unicode_ci,
  `billing_address` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_meta_user_id_foreign` (`user_id`),
  CONSTRAINT `user_meta_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_meta`
--

LOCK TABLES `user_meta` WRITE;
/*!40000 ALTER TABLE `user_meta` DISABLE KEYS */;
INSERT INTO `user_meta` VALUES (1,1,'ru','UAH','0980747324',0,NULL,0,1,'2018-01-08 22:03:04','2018-03-22 20:06:14',NULL,NULL,NULL,NULL,NULL),(2,2,NULL,NULL,NULL,0,NULL,0,1,'2018-01-08 22:03:05','2018-01-08 22:03:06',NULL,NULL,NULL,NULL,NULL),(23,33,NULL,NULL,'0980747324',0,'5a3b60a88b35022d615566c29a62750e',0,1,'2017-12-11 11:11:35','2017-12-11 11:11:35',NULL,NULL,NULL,NULL,NULL),(24,35,NULL,NULL,NULL,0,'d3f2c4f730e023743acadd34db0d84aa',0,1,'2018-01-16 19:46:55','2018-01-16 19:46:55',NULL,NULL,NULL,NULL,NULL),(25,36,NULL,NULL,NULL,0,'c615e98d87fd6a40b35dacc1e56c5fa8',0,1,'2018-03-13 12:36:31','2018-03-13 12:36:31',NULL,NULL,NULL,NULL,NULL),(26,37,NULL,NULL,NULL,0,'c64060e6a32413034d9cc597d920f150',0,1,'2018-03-13 12:37:32','2018-03-13 12:37:32',NULL,NULL,NULL,NULL,NULL),(27,38,NULL,NULL,NULL,0,'e990e4978901146ec4fd2cb919b30435',0,1,'2018-03-13 12:43:56','2018-03-13 12:43:56',NULL,NULL,NULL,NULL,NULL),(28,39,NULL,NULL,NULL,0,'14eba7faa043560e25264799560c4e70',0,1,'2018-03-13 12:45:07','2018-03-13 12:45:07',NULL,NULL,NULL,NULL,NULL),(29,40,NULL,NULL,NULL,0,'ce875aae33e5b79680e2fff15a42c7d8',0,1,'2018-03-13 12:54:29','2018-03-13 12:54:29',NULL,NULL,NULL,NULL,NULL),(30,41,NULL,NULL,NULL,0,'38531a28bb45a976e0db7485b0579be6',0,1,'2018-03-13 13:04:01','2018-03-13 13:04:01',NULL,NULL,NULL,NULL,NULL),(31,42,NULL,NULL,NULL,0,'4a358ea9ae6fe39d13e8b4f107fe3358',0,1,'2018-03-13 13:05:01','2018-03-13 13:05:01',NULL,NULL,NULL,NULL,NULL),(32,43,NULL,NULL,NULL,0,'7a1cad2393b6edc6e17cfe992f2413a5',0,1,'2018-03-13 13:09:08','2018-03-13 13:09:08',NULL,NULL,NULL,NULL,NULL),(33,44,NULL,NULL,NULL,0,'8f25efa43e9bfc3f0a32dbb6bf1a5aa8',0,1,'2018-03-13 13:10:41','2018-03-13 13:10:41',NULL,NULL,NULL,NULL,NULL),(34,45,NULL,NULL,NULL,0,'0afd972460b59ef3e25b34241e349778',0,1,'2018-03-13 13:17:21','2018-03-13 13:17:21',NULL,NULL,NULL,NULL,NULL),(35,46,NULL,NULL,NULL,0,'ac1258b1da65b13c1d44171cf7b720d2',0,1,'2018-03-13 13:18:01','2018-03-13 13:18:01',NULL,NULL,NULL,NULL,NULL),(37,48,NULL,NULL,NULL,0,'a80d7dd046249de627f3198c44c69e3a',0,1,'2018-03-15 08:19:55','2018-03-15 08:19:55',NULL,NULL,NULL,NULL,NULL),(39,51,NULL,NULL,NULL,0,'939603a70a91907370da0dc55cff5056',0,1,'2018-03-23 06:55:34','2018-03-23 06:55:34',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin',NULL,'admin@admin.loc','$2y$10$Hr2KeG1nixnP6Jrm2qGXV.wkc7lD7qtJztJgbyWaSUl4p679hEa3K','DGjYsPnhIEH4VPijAVgBCpclLdXD1UGwSSBKssSrvolBRbbxXGeEYkPB0IC4','2017-11-28 10:38:51','2017-11-28 10:38:51'),(2,'Manager 1',NULL,'manager@admin.loc','$2y$10$Hr2KeG1nixnP6Jrm2qGXV.wkc7lD7qtJztJgbyWaSUl4p679hEa3K','quxZ12xzt5Bl4E50VP8MEBtzebaUSBb4utHYjzoXIy4GiP8xyZhBMZ277TLJ','2017-11-28 10:38:51','2017-11-28 10:38:51'),(33,'Роман',NULL,'webmaster865@yandex.ru','$2y$10$6wUpNhz13jAETTuGTYt4Ius.QTESEhx9nWpB837QKeUBtB34NCPEa','sM5IFCF9rgdZWT2g0GixsTwbqyqLnp94BPL1gsOYI1FsUO0vpaPJPwrdHKJd','2017-12-11 11:11:35','2018-01-17 02:45:47'),(35,'Тест',NULL,'test@front.end','$2y$10$wJqLZ8ngZBLUjAMqXb.hMO1Os2M6Gyug.v89JBZfSa2srbcmjYoZW',NULL,'2018-01-16 19:46:55','2018-01-16 20:58:30'),(36,'test',NULL,'test@gmail.com','$2y$10$5jMc41lr.lQdpd3jzUtGyONu2RQDIp9m8SI1ifA1wUo2lP/sfoNPG',NULL,'2018-03-13 12:36:31','2018-03-13 12:36:31'),(37,'ostap',NULL,'ostapchernetsky@gmail.com','$2y$10$t3groK9gfa0EY210XJLrB.Acm.s..LS4TAbwzs3XvwI2I8tVy1oHG','BPtGB50tt5PDL4V43Cy8ZFn7GqKJH3aoko5jzNKeafbYQoDR82Sa2fDALXag','2018-03-13 12:37:32','2018-03-13 12:37:32'),(38,'test',NULL,'test@test.com','$2y$10$z62ITZtCaiAVmNnjsQT24OJPg45R.MDaWxQtYHHYt6J7kwUxN9Ny6','w4hgHKsfs2lcHbbUWhCqJlUL3PlS8nj46IOvvMCtnoSOPnt1Ehkjoqi5na9V','2018-03-13 12:43:56','2018-03-13 12:43:56'),(39,'test',NULL,'test@test.com2','$2y$10$ajCOWA/7oKKBX5SK6pPGZOcauZXmACUsFDeOOfm.omI5tU4jwTjHu',NULL,'2018-03-13 12:45:07','2018-03-13 12:45:07'),(40,'profiletest',NULL,'profile@test.ru','$2y$10$YI845sfxxXO3RI6xbFDYR..P7assE1P28AvFwHIHMN3W0YgR8fNtm',NULL,'2018-03-13 12:54:29','2018-03-13 12:54:29'),(41,'profiletest',NULL,'test@test.test','$2y$10$YFOtRBj8kVc7KMTQdMU3Au7OyRD/MtRExnd5jdXqsxPBp4VPbFhAS',NULL,'2018-03-13 13:04:01','2018-03-13 13:04:01'),(42,'test',NULL,'test@test.te','$2y$10$BFP9/Akm6l338gBsV7r3P.PJY4ooxH6DxR1QNsgcTw0FKzfvRaUYi',NULL,'2018-03-13 13:05:01','2018-03-13 13:05:01'),(43,'test',NULL,'test@test.asd','$2y$10$9CawUqayOdnoNyxQ.x9UgOPaz7/ImoEc.yaNKDcQeeR3X8zJaclVC',NULL,'2018-03-13 13:09:08','2018-03-13 13:09:08'),(44,'test',NULL,'te@teee.tes','$2y$10$CphQfppdD.6k/dGHaIxoq.w3/KYAK85ZG6waYWZa8QL7vnXnXfT.6',NULL,'2018-03-13 13:10:41','2018-03-13 13:10:41'),(45,'test',NULL,'test@app.com','$2y$10$aMVhiB4pNK8xSClPQ3e3uOmpUQtlZRAtFc36z5KFhgdH8dSf9EQqu','9PAGi3F27dvlCHuIDqA3d9DfKthc7ffTBITNr5TMBjDZwiLWcgR8NtbZFkOD','2018-03-13 13:17:21','2018-03-13 13:17:21'),(46,'qwerty',NULL,'qwerty@gmail.com','$2y$10$kVf.v5quKb9fT9ss8kRiaONirYXHBoAoao4V75u1uGi91H4NPahiu','NL1qbwoiUWbtSKXCG4jrDUH1dtt9YcWEHcnyr4zmdXtBNeEAhduAEYw5Ry9O','2018-03-13 13:18:01','2018-03-13 13:18:01'),(48,'Rosco',NULL,'info@rosco.ua','$2y$10$hjF5Ab8g4eYO1.zQmbs8muH4Rk3.SyrX300hnXagrYHPi27sEU0fu','7syXOh6HFp9NLhqaGz8ttWrJpnUojcGZFhdjcBQ2d5KkjiEXn5dt1BQRiffc','2018-03-15 08:19:55','2018-03-15 08:19:55'),(51,'Junior PHP developer',NULL,'test@test.h','$2y$10$oBS5LXgWpNB4hq1vI0a4reis0u.PkYtEMUzRc1g.wiaLltJ4jXbkq',NULL,'2018-03-23 06:55:34','2018-03-23 06:55:34');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-03 13:00:27
