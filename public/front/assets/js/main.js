$(document).ready(function () {

    $(function () {

        var langs = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];

        $("#search_products").autocomplete({
            source: '/product_search',
            minLength: 3,
            select: function (event, ui) {
                window.location = '/product/' + encodeURIComponent(ui.item['link']);
            }
        });
        $(".client_phone_mask").mask("+99(999) 999 99 99");
    });


    $("#filterToggle").click(function () {
        $(".filters").toggleClass("show");
    });

    $(".tabs").find("div").click(function () {
        $(".tabs").find("div").removeClass("active");
        $(this).addClass("active");
    });

    $(".description-tab").click(function () {
        $(".containers").find("div").removeClass("active");
        $(".containers").find("div").removeClass("active_reviews");
        $(".description-container").addClass("active");
    });

    $(".characteristics-tab").click(function () {
        $(".containers").find("div").removeClass("active");
        $(".containers").find("div").removeClass("active_reviews");
        $(".characteristics-container").addClass("active");
    });

    $(".reviews-tab").click(function () {
        $(".containers").find("div").removeClass("active");
        $(".reviews-container").addClass("active_reviews");
    });

    $("#check-reviews").click(function (e) {
        var textReviewValue = $("#Text").val();
        if (textReviewValue.lengtlength > 200 || textReviewValue.length < 5) {
            $("#text-area-error").show();
            e.preventDefault();
        }
    });

    $('.big-photos').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.small-photos'
    });
    $('.small-photos').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.big-photos',
        focusOnSelect: true
    });

    $(".slider_bar-slider").slick({
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        focusOnSelect: true
    });

    $(".product-slider").slick({
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 940,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 650,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 370,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            }
        ]
    });

    $(".slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 630,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            }
        ]
    });

    $("#weight").slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function (event, ui) {
            $("#weightMinMax").val(ui.values[0] + "кг" + " - " + ui.values[1] + "кг");
        }
    });
    $("#weightMinMax").val($("#weight").slider("values", 0) + "кг" +
        " - " + $("#weight").slider("values", 1) + "кг");

    $("#amount").slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function (event, ui) {
            $("#amountMinMax").val(ui.values[0] + "л" + " - " + ui.values[1] + "л");
        }
    });
    $("#amountMinMax").val($("#amount").slider("values", 0) + "л" +
        " - " + $("#amount").slider("values", 1) + "л");

    $('select[name="language"]').on('change', function () {
        return language.set($(this).val());
    });

    $('select[name="currency"]').on('change', function () {
        return currency.set($(this).val());
    });

    $('select[name="currency"]').on('change', function () {
        return currency.set($(this).val());
    });

    $('select', '.sortable').on('change', function () {
        return sort.do($(this));
    });

    $('#filter-all').on('submit', function () {

        var query = parseQuery(window.location.search);

        $(this).find('[name]').each(function () {
            query[$(this).attr('name')] = $(this).val();
        });

        window.location.search = $.param(query);

        return false;
    });

    $('button[name=modal]').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('href');

        var winH = $(window).height();
        var winW = $(window).width();

        var modH = $(id).height();

        var offsetTop = 0;

        if(winH - $(id).height() > 0) {
            offsetTop = (winH - modH) / 4
        }

        $(id).css('top', offsetTop);
        $(id).css('left', winW / 2 - $(id).width() / 2);

        $(id).fadeIn(1000);

    });

});

var cart = {
    'add': function (product_id, quantity, btn) {
        if (quantity === undefined) quantity = 1;

        $.ajax({
            url: '/cart/add',
            method: 'post',
            dataType: 'json',
            data: 'product_id=' + product_id + '&quantity=' + quantity,
            success: function (response) {
                $('.basket-count').text(response.count);
                $('#cart_modal').html(response.html);
            },
            complete: function () {
                btn.text(btn.attr('data-pushed'));
            }
        });

    },
    'delete': function (product_id) {
        $.ajax({
            url: '/cart/delete',
            method: 'post',
            dataType: 'json',
            data: 'product_id=' + product_id,
            success: function (response) {
                $('.basket-count').text(response.count);
                $('.cart-container').html(response.html);
            }
        });
        return false;
    },
    'quantity': function (product_id, quantity) {
        if (quantity === undefined || quantity < 1) quantity = 1;

        $.ajax({
            url: '/cart/quantity',
            method: 'post',
            dataType: 'json',
            data: 'product_id=' + product_id + '&quantity=' + quantity,
            success: function (response) {
                $('.basket-count').text(response.count);
                $('.cart-container').html(response.html);
            }
        });
        return false;
    },
    'payments': function (shipment_id, ids) {
        var test = $('.payments-container');
        $.ajax({
            url: '/cart/payments',
            method: 'post',
            data: 'shipment_id=' + shipment_id + '&ids=' + ids,
            success: function (response) {

                // $('.payments-container').html(response);

                test.html(response);

            },
            error: function (response) {
                test.html("");
            }
        });
        return false;
    }
};

var language = {
    'set': function (code) {
        document.location.href = '/language/' + code;
    }
};

var currency = {
    'set': function (code) {
        document.location.href = '/currency/' + code;
    }
};

var sort = {
    'do': function (obj) {
        var query = parseQuery(window.location.search);
        query[obj.attr('name')] = obj.val();

        window.location.search = $.param(query);
    }
}

function parseQuery(queryString) {
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}