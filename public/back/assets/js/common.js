function tab_function(local_prefix) {

    var tab_name;

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var tab = $(e.target).attr('href');
        localStorage.setItem(local_prefix, tab);
    });

    if(localStorage.getItem(local_prefix) === null) {
        localStorage.setItem(local_prefix, '#main');
    }

    tab_name = localStorage.getItem(local_prefix);

    return tab_name;
}