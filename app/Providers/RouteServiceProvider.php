<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     * @param Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes($router);

        //
    }

    /**
     * @param $router
     */
    protected function mapWebRoutes($router)
    {
        if(env('MULTI_DOMAINS')) {
            Route::middleware('web')
                ->domain('shop.' . env('APP_DOMAIN'))
                ->namespace($this->namespace)
                ->group(function () use ($router) {
                    require base_path('routes/web/admin.php');
                    require base_path('routes/web/auth.php');
                    require base_path('routes/web/web.php');

                });
        } else {
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(function () use ($router) {
                    require base_path('routes/web/admin.php');
                    require base_path('routes/web/auth.php');
                    require base_path('routes/web/web.php');

                });
        }

    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
