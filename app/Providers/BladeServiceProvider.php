<?php

namespace App\Providers;

use App;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Blade::directive('menu', function($code) {
            return "<?php echo Menu::show($code); ?>";
        });
        Blade::directive('module', function($code) {
            return "<?php echo Module::show($code); ?>";
        });
    }

    public function register()
    {
        App::bind('menu', function()
        {
            return new App\Helpers\MenuHelper;
        });
        App::bind('module', function()
        {
            return new App\Helpers\ModuleHelper;
        });
    }
}
