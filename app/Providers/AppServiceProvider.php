<?php

namespace App\Providers;

use App\Contracts\Svg\SvgFactory;
use Illuminate\Support\Collection;
use App\Helpers\LocaleHelper;
use App\Services\CartDatabaseService;
use App\Services\CartSessionService;
use App\Services\SharingService;
use Illuminate\Support\ServiceProvider;
use App\Helpers\CurrencyHelper;
use App\Helpers\SettingHelper;


class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->singleton('shared', function () {

            $sharingService = new SharingService;

            $sharingService->share('languages', locale()->list());
            $sharingService->share('currencies', currency()->list());

            return $sharingService;

        });

        app(SvgFactory::class)->registerBladeTag();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Helpers\CurrencyHelper', function () {
            return new CurrencyHelper();
        });

        $this->app->singleton('App\Helpers\SettingHelper', function () {
            return new SettingHelper();
        });

        $this->app->singleton('App\Helpers\LocaleHelper', function () {
            return new LocaleHelper();
        });
        $this->app->singleton('App\Services\CartDatabaseService', function () {
            return new CartDatabaseService();
        });
        $this->app->singleton('App\Services\CartSessionService', function () {
            return new CartSessionService();
        });
        $this->app->singleton(SvgFactory::class, function () {

            $config = Collection::make(config('blade-svg', []))->merge([
                'spritesheet_path' => base_path(config('blade-svg.spritesheet_path')),
                'svg_path' => base_path(config('blade-svg.svg_path', config('blade-svg.icon_path'))),
            ])->all();

            return new SvgFactory($config);
        });
    }
}
