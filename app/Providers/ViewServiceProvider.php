<?php

namespace App\Providers;

use App\Repositories\CategoryRepository;
use App\Gateways\Actions\Categories\TreeCategoryAction;
use App\Gateways\Actions\Categories\GetCategoryListAction;
use App\Repositories\ManufacturerRepository;
use App\Traits\CallableTrait;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    use CallableTrait;

    public function boot()
    {
        view()->composer('shop.partials.filter', function ($view) {

            $manufacturers = app(ManufacturerRepository::class)->getList();

            $categoriy = app(CategoryRepository::class)->getList();
            $categories = $this->call(TreeCategoryAction::class,[$categoriy]);
            $view->with('manufacturers', $manufacturers);
            $view->with('categories', $categories);

        });
    }

    public function register()
    {
        //
    }
}
