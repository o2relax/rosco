<?php

namespace App\Providers;

use App\Models\Blog;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Language;
use App\Models\Menu;
use App\Models\MenuLink;
use App\Models\Module;
use App\Models\Order;
use App\Models\Page;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Shipment;
use App\Models\UserMeta;
use App\Observers\BlogObserver;
use App\Observers\CategoryObserver;
use App\Observers\CurrencyObserver;
use App\Observers\LanguageObserver;
use App\Observers\MenuLinkObserver;
use App\Observers\MenuObserver;
use App\Observers\ModuleObserver;
use App\Observers\OrderObserver;
use App\Observers\PageObserver;
use App\Observers\PaymentObserver;
use App\Observers\ProductObserver;
use App\Observers\SettingObserver;
use App\Observers\ShipmentObserver;
use App\Observers\UserMetaObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        UserMeta::observe(UserMetaObserver::class);
        Menu::observe(MenuObserver::class);
        MenuLink::observe(MenuLinkObserver::class);
        Setting::observe(SettingObserver::class);
        Language::observe(LanguageObserver::class);
        Currency::observe(CurrencyObserver::class);
        Category::observe(CategoryObserver::class);
        Module::observe(ModuleObserver::class);
        Product::observe(ProductObserver::class);
        Order::observe(OrderObserver::class);
        Payment::observe(PaymentObserver::class);
        Shipment::observe(ShipmentObserver::class);
        Page::observe(PageObserver::class);
        Blog::observe(BlogObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
