<?php

namespace App\Listeners;

use App\Events\ProductViewedEvent;
use App\Gateways\Actions\Products\Custom\UpdateProductStatsAction;
use App\Traits\CallableTrait;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductViewedListener
{
    use CallableTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductViewedEvent  $event
     * @return void
     */
    public function handle(ProductViewedEvent $event)
    {

        $this->call(UpdateProductStatsAction::class, [$event->product, $event->data]);

    }
}
