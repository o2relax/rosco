<?php

namespace App\Observers;

use App\Models\Payment;
use App\Traits\CallableTrait;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class PaymentObserver
{

    use CallableTrait;

    /**
     * @param Payment $model
     * @throws \Exception
     */
    public function created(Payment $model)
    {
        cache()->tags('payments')->flush();
    }

    /**
     * @param Payment $model
     * @throws \Exception
     */
    public function saved(Payment $model)
    {
        cache()->tags('payments')->flush();
    }

    /**
     * @param Payment $model
     * @throws \Exception
     */
    public function deleted(Payment $model)
    {
        cache()->tags('payments')->flush();
    }
}