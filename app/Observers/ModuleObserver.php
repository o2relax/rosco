<?php

namespace App\Observers;


use App\Models\Module;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class ModuleObserver
{
    
    /**
     * @param Module $model
     * @throws \Exception
     */
    public function created(Module $model)
    {
        module()->clearCache($model->code);
    }

    /**
     * @param Module $model
     * @throws \Exception
     */
    public function saved(Module $model)
    {
        module()->clearCache($model->code);
    }

    /**
     * @param Module $model
     * @throws \Exception
     */
    public function deleted(Module $model)
    {
        module()->clearCache($model->code);
    }
}