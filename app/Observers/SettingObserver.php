<?php

namespace App\Observers;

use App\Models\Setting;

/**
 * Class SettingObserver
 * @package App\Observers
 */
class SettingObserver
{
    /**
     * @param Setting $model
     * @throws \Exception
     */
    public function created(Setting $model)
    {

    }

    /**
     * @param Setting $model
     * @throws \Exception
     */
    public function saved(Setting $model)
    {
        cache()->forget('settings');
        cache()->tags('settings')->flush();
    }

    /**
     * @param Setting $model
     * @throws \Exception
     */
    public function deleted(Setting $model)
    {

    }
}