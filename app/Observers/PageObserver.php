<?php

namespace App\Observers;

use App\Models\Page;
use App\Traits\CallableTrait;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class PageObserver
{

    use CallableTrait;

    /**
     * @param Page $model
     * @throws \Exception
     */
    public function created(Page $model)
    {

    }

    /**
     * @param Page $model
     * @throws \Exception
     */
    public function saved(Page $model)
    {
        cache()->tags('page_' . $model->slug)->flush();
    }

    /**
     * @param Page $model
     * @throws \Exception
     */
    public function deleted(Page $model)
    {
        cache()->tags('page_' . $model->slug)->flush();
    }
}