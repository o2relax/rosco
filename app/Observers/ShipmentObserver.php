<?php

namespace App\Observers;

use App\Models\Shipment;
use App\Traits\CallableTrait;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class ShipmentObserver
{

    use CallableTrait;

    /**
     * @param Shipment $model
     * @throws \Exception
     */
    public function created(Shipment $model)
    {
        cache()->tags('shipments')->flush();
    }

    /**
     * @param Shipment $model
     * @throws \Exception
     */
    public function saved(Shipment $model)
    {
        cache()->tags('shipments')->flush();
    }

    /**
     * @param Shipment $model
     * @throws \Exception
     */
    public function deleted(Shipment $model)
    {
        cache()->tags('shipments')->flush();
    }
}