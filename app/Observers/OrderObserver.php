<?php

namespace App\Observers;

use App\Mail\OrderCreated;
use App\Models\Order;
use App\Traits\CallableTrait;
use Illuminate\Support\Facades\Mail;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class OrderObserver
{

    use CallableTrait;

    /**
     * @param Order $model
     * @throws \Exception
     */
    public function created(Order $model)
    {

    }

    /**
     * @param Order $model
     * @throws \Exception
     */
    public function saved(Order $model)
    {

    }

    /**
     * @param Order $model
     * @throws \Exception
     */
    public function deleted(Order $model)
    {

    }
}