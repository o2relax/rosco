<?php

namespace App\Observers;

use App\Models\Language;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class LanguageObserver
{
    /**
     * @param Language $model
     * @throws \Exception
     */
    public function created(Language $model)
    {
        cache()->forget('languages');
    }

    /**
     * @param Language $model
     * @throws \Exception
     */
    public function saved(Language $model)
    {
        cache()->forget('languages');
    }

    /**
     * @param Language $model
     * @throws \Exception
     */
    public function deleted(Language $model)
    {
        cache()->forget('languages');
    }
}