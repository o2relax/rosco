<?php

namespace App\Observers;

use App\Models\User;

/**
 * Class UserObserver
 * @package App\Observers
 */
class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User $model
     * @return void
     */
    public function created(User $model)
    {

    }

    /**
     * Listen to the User saved\updated event.
     *
     * @param  User $model
     * @return void
     */
    public function saved(User $model)
    {

    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User $model
     * @return void
     */
    public function deleted(User $model)
    {

    }
}