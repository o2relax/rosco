<?php

namespace App\Observers;

use App\Models\MenuLink;
use App\Traits\CacheableTrait;

/**
 * Class MenuLinkObserver
 * @package App\Observers
 */
class MenuLinkObserver
{

    use CacheableTrait;

    /**
     * @param MenuLink $model
     * @throws \Exception
     */
    public function created(MenuLink $model)
    {
        $this->forgetCache('menu_' . $model->menu->code);
    }

    /**
     * @param MenuLink $model
     * @throws \Exception
     */
    public function saved(MenuLink $model)
    {
        $this->forgetCache('menu_' . $model->menu->code);
    }

    /**
     * @param MenuLink $model
     * @throws \Exception
     */
    public function deleted(MenuLink $model)
    {
        $this->forgetCache('menu_' . $model->menu->code);
    }
}