<?php

namespace App\Observers;

use App\Gateways\Tasks\Users\GetUserByIdTask;
use App\Models\UserMeta;
//use App\Notifications\ActivateUserEmail;

/**
 * Class UserObserver
 * @package App\Observers
 */
class UserMetaObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  UserMeta $model
     * @return void
     */
    public function created(UserMeta $model)
    {
        $token = md5(str_random(40));

        $user = app(GetUserByIdTask::class)->run($model->user_id);

        $model->update([
            'activation_token' => $token
        ]);

        //$user->notify(new ActivateUserEmail($token));
    }

    /**
     * Listen to the User saved\updated event.
     *
     * @param  UserMeta $model
     * @return void
     */
    public function saved(UserMeta $model)
    {

    }

    /**
     * Listen to the User deleting event.
     *
     * @param  UserMeta $model
     * @return void
     */
    public function deleted(UserMeta $model)
    {

    }
}