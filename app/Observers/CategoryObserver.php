<?php

namespace App\Observers;

use App\Gateways\Actions\Categories\DropParentCategoryAction;
use App\Gateways\Actions\Products\DropProductsCategoryAction;
use App\Models\Category;
use App\Traits\CallableTrait;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class CategoryObserver
{

    use CallableTrait;

    /**
     * @param Category $model
     * @throws \Exception
     */
    public function created(Category $model)
    {
        cache()->tags('categories')->flush();
    }

    /**
     * @param Category $model
     * @throws \Exception
     */
    public function saved(Category $model)
    {
        cache()->tags(['categories', 'category_' . $model->slug])->flush();
    }

    /**
     * @param Category $model
     * @throws \Exception
     */
    public function deleted(Category $model)
    {
        $this->call(DropProductsCategoryAction::class, [$model->id]);
        $this->call(DropParentCategoryAction::class, [$model->id]);
        cache()->tags(['categories', 'category_' . $model->slug])->flush();
    }
}