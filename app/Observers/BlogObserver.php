<?php

namespace App\Observers;

use App\Models\Blog;
use App\Traits\CallableTrait;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class BlogObserver
{

    use CallableTrait;

    /**
     * @param Blog $model
     * @throws \Exception
     */
    public function created(Blog $model)
    {
        cache()->tags('blog')->flush();
    }

    /**
     * @param Blog $model
     * @throws \Exception
     */
    public function saved(Blog $model)
    {
        cache()->tags('blog')->flush();
    }

    /**
     * @param Blog $model
     * @throws \Exception
     */
    public function deleted(Blog $model)
    {
        cache()->tags('blog')->flush();
    }
}