<?php

namespace App\Observers;

use App\Models\Menu;
use App\Traits\CacheableTrait;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class MenuObserver
{

    use CacheableTrait;
    /**
     * @param Menu $model
     * @throws \Exception
     */
    public function created(Menu $model)
    {

    }

    /**
     * @param Menu $model
     * @throws \Exception
     */
    public function saved(Menu $model)
    {
        $this->forgetCache('menu_' . $model->code);
    }

    /**
     * @param Menu $model
     * @throws \Exception
     */
    public function deleted(Menu $model)
    {
        $this->forgetCache('menu_' . $model->code);
    }
}