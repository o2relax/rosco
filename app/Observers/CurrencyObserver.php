<?php

namespace App\Observers;

use App\Models\Currency;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class CurrencyObserver
{
    /**
     * @param Currency $model
     * @throws \Exception
     */
    public function created(Currency $model)
    {
        cache()->forget('currencies');
    }

    /**
     * @param Currency $model
     * @throws \Exception
     */
    public function saved(Currency $model)
    {
        cache()->forget('currencies');
    }

    /**
     * @param Currency $model
     * @throws \Exception
     */
    public function deleted(Currency $model)
    {
        cache()->forget('currencies');
    }
}