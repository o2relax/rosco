<?php

namespace App\Observers;


use App\Models\Product;

/**
 * Class MenuObserver
 * @package App\Observers
 */
class ProductObserver
{
    
    /**
     * @param Product $model
     * @throws \Exception
     */
    public function created(Product $model)
    {
        cache()->tags('products')->flush();
    }

    /**
     * @param Product $model
     * @throws \Exception
     */
    public function saved(Product $model)
    {
        cache()->tags('products')->flush();
    }

    /**
     * @param Product $model
     * @throws \Exception
     */
    public function deleted(Product $model)
    {
        cache()->tags('products')->flush();
    }
}