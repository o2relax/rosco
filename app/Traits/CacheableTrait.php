<?php

namespace App\Traits;

/**
 * Class CacheableTrait
 * @package App\Traits
 */
trait CacheableTrait
{
    /**
     * @var string 
     */
    protected $driver = 'redis';

    /**
     * CacheableTrait constructor.
     */
    public function __construct()
    {
        
    }

    public function locale() {
        return app()->getLocale();
    }
    
    /**
     * @param $key
     * @param null $local
     * @return null|string
     */
    public function getKey($key, $local = null) {
        return ($local ? $local : $this->locale()) . '_' . $key;
    }

    /**
     * @param $key
     * @return bool
     * @throws \Exception
     */
    public function hasCache($key)
    {
        return cache()->has($this->getKey($key));
    }

    /**
     * @param $key
     * @param null $default
     * @return \Illuminate\Contracts\Cache\Repository
     * @throws \Exception
     */
    public function getCache($key, $default = null)
    {
        return cache()->get($this->getKey($key), $default);
    }

    /**
     * @param $key
     * @param $value
     * @param $expire
     * @param array|null $tags
     * @throws \Exception
     */
    public function putCache($key, $value, $expire, array $tags = null)
    {
        if ($tags) {
            return cache()->put($this->getKey($key), $value, $expire);
        } else {
            return cache()->put($this->getKey($key), $value, $expire);
        }

    }

    /**
     * @param $key
     * @return bool
     * @throws \Exception
     */
    public function forgetCache($key) {
        foreach(locale()->list() as $lang) {
            cache()->forget($this->getKey($key, $lang->code));
        };
        return true;
    }

}
