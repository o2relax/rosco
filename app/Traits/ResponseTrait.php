<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ResponseTrait
 * @package App\Traits
 */
trait ResponseTrait
{

    /**     * @param            $data
     * @param null $transformerName
     * @return Collection|Model
     */
    public function transform($data, $transformerName = null)
    {
        $transformer = new $transformerName;

        return $transformer->response($data);
    }

}
