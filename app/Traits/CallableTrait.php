<?php

namespace App\Traits;

use Illuminate\Support\Facades\App;
use function is_array;
use function key;

/**
 * Class CallableTrait
 * @package App\Traits
 */
trait CallableTrait
{

    /**
     * @param       $class
     * @param array $runArguments
     * @param array $methods
     *
     * @return  mixed
     */
    public function call($class, $runArguments = [], $methods = [])
    {
        $action = App::make($class);

        // allows calling other methods in the class before calling the main `run` function.
        foreach ($methods as $methodInfo) {
            // if is array means it has arguments
            if (is_array($methodInfo)) {
                $method = key($methodInfo);
                $arguments = $methodInfo[$method];
                if (method_exists($action, $method)) {
                    $action->$method(...$arguments);
                }
            } else {
                // if is string means it's just the function name
                if (method_exists($action, $methodInfo)) {
                    $action->$methodInfo();
                }
            }
        }

        return $action->run(...$runArguments);
    }

}
