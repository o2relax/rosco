<?php

namespace App\Http\Controllers\User;

use App\Gateways\Actions\Orders\Custom\GetOrdersAction;
use App\Gateways\Actions\Products\GetStatusesAction;
use App\Gateways\Actions\Subscriptions\CreateSubscriptionAction;
use App\Gateways\Actions\Users\Custom\ChangePasswordUserAction;
use App\Gateways\Transformers\Orders\OrderTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Subscriptions\CreateSubscriptionRequest;
use App\Http\Requests\Users\ChangePasswordUserRequest;

class ProfileController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        return view('shop.pages.auth.profile', [
            'user' => auth()->user(),
        ]);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orders()
    {
        $orders =  $this->transform($this->call(GetOrdersAction::class, [
            auth()->user()->id
        ]), OrderTransformer::class);

        $arr = ['Jan' =>'Январь','Feb' => 'Февраль','Mar' => 'Март','Apr' => 'Апрель','May' => 'Май','Jun' => 'Июнь','Jul' => 'Июль','Aug' => 'Август','Sep' => 'Сентябрь','Oct' => 'Октябрь','Nov' => 'Ноябрь','Dec' => 'Декабрь'];
        foreach($orders as $order) {
            $order->date = $arr[substr($order->date,0,3)] . substr($order->date,3);
        }

        $statuses = $this->call(GetStatusesAction::class, ['Order']);


        return view('shop.pages.auth.orders', [
            'user' => auth()->user(),
            'orders' => $orders,
            'statuses' => $statuses,
        ]);

    }

    /**
     * @param ChangePasswordUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(ChangePasswordUserRequest $request)
    {

        $user = $this->call(ChangePasswordUserAction::class, [
            auth()->user()->id,
            $request->get('password')
        ]);

        if ($user) {
            return redirect()->back()->with('message', trans('shop/profile.messages.change_password'));
        } else {
            return redirect()->back()->with('message', trans('shop/profile.messages.change_password_fail'));
        }

    }

    public function subscribe(CreateSubscriptionRequest $request) {

        $subscribe = $this->call(CreateSubscriptionAction::class, [
            $request->get('email')
        ]);

        if ($subscribe) {
            return response()->json(['message' => trans('shop/profile.messages.subscribe')]);
        } else {
            return response()->json(['message' => trans('shop/profile.messages.subscribe_fail')]);
        }

    }

}
