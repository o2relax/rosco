<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Currencies\CreateCurrencyAction;
use App\Gateways\Actions\Currencies\DeleteCurrencyAction;
use App\Gateways\Actions\Currencies\GetCurrenciesAction;
use App\Gateways\Actions\Currencies\GetCurrencyAction;
use App\Gateways\Actions\Currencies\UpdateCurrencyAction;
use App\Gateways\Transformers\Currencies\CurrencyTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Currencies\ChangeCurrencyStatusRequest;
use App\Http\Requests\Currencies\CreateCurrencyRequest;
use App\Http\Requests\Currencies\UpdateCurrencyRequest;

class CurrencyController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        dump(currency()->format(100));
//        dump(currency()->format(100));
//        dump(currency()->format(100));
//        dump(currency()->format(100));
//        dump(currency()->format(100, 'GLD'));
//        dump(currency()->convert(100, 'EUR'));

        $currencies = $this->call(GetCurrenciesAction::class, [false]);

        return view('admin.parts.currencies.index', [
            'currencies' => $this->transform($currencies, CurrencyTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parts.currencies.create', [
            'currency' => null
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = $this->call(GetCurrencyAction::class, [
            $id
        ]);

        return view('admin.parts.currencies.edit', [
            'currency' => $this->transform($currency, CurrencyTransformer::class),
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateCurrencyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCurrencyRequest $request)
    {
        $result = $this->call(CreateCurrencyAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/currencies')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCurrencyRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCurrencyRequest $request, $id)
    {
        $result = $this->call(UpdateCurrencyAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteCurrencyAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/currencies')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/currencies')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeCurrencyStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postActive($id, ChangeCurrencyStatusRequest $request)
    {
        if ($this->call(UpdateCurrencyAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

}
