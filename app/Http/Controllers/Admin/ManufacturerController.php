<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Manufacturers\CreateManufacturerAction;
use App\Gateways\Actions\Manufacturers\DeleteManufacturerAction;
use App\Gateways\Actions\Manufacturers\GetManufacturersAction;
use App\Gateways\Actions\Manufacturers\GetManufacturerAction;
use App\Gateways\Actions\Manufacturers\Images\DeleteManufacturerImageAction;
use App\Gateways\Actions\Manufacturers\UpdateManufacturerAction;
use App\Gateways\Transformers\Manufacturers\ManufacturerTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Manufacturers\ChangeManufacturerStatusRequest;
use App\Http\Requests\Manufacturers\CreateManufacturerRequest;
use App\Http\Requests\Manufacturers\SearchManufacturerRequest;
use App\Http\Requests\Manufacturers\UpdateManufacturerRequest;

class ManufacturerController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $manufacturers = $this->call(GetManufacturersAction::class, []);

        return view('admin.parts.manufacturers.index', [
            'manufacturers' => $this->transform($manufacturers, ManufacturerTransformer::class)
        ]);
    }
    
    /**
     * Display a listing of the resource searched.
     * @param SearchManufacturerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchManufacturerRequest $request)
    {
        if (!$request->has('search')) {
            return redirect('admin/manufacturers');
        }

        $manufacturers = $this->call(GetManufacturersAction::class, [
            $request->get('search')
        ]);

        return view('admin.parts.manufacturers.index', [
            'manufacturers' => $this->transform($manufacturers, ManufacturerTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.parts.manufacturers.create', [
            'manufacturer' => null
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manufacturer = $this->call(GetManufacturerAction::class, [
            $id
        ]);

        return view('admin.parts.manufacturers.edit', [
            'manufacturer' => $this->transform($manufacturer, ManufacturerTransformer::class),
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateManufacturerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateManufacturerRequest $request)
    {
        $result = $this->call(CreateManufacturerAction::class, [
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);

        if ($result) {
            return redirect('admin/manufacturers')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateManufacturerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateManufacturerRequest $request, $id)
    {
        $result = $this->call(UpdateManufacturerAction::class, [
            $id,
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteManufacturerAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/manufacturers')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/manufacturers')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeManufacturerStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeManufacturerStatusRequest $request)
    {
        if ($this->call(UpdateManufacturerAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * Delete the image
     *
     * @param  integer $id
     *
     * @return mixed
     */
    public function deleteMainImage($id)
    {

        $image = $this->call(DeleteManufacturerImageAction::class, [$id]);

        return response()->json([$image ? true : false]);
    }

}
