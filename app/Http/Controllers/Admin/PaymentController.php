<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Currencies\GetCurrenciesAction;
use App\Gateways\Actions\Payments\CreatePaymentAction;
use App\Gateways\Actions\Payments\DeletePaymentAction;
use App\Gateways\Actions\Payments\GetPaymentsAction;
use App\Gateways\Actions\Payments\GetPaymentAction;
use App\Gateways\Actions\Payments\Images\DeletePaymentImageAction;
use App\Gateways\Actions\Payments\UpdatePaymentAction;
use App\Gateways\Transformers\Payments\PaymentTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payments\ChangePaymentStatusRequest;
use App\Http\Requests\Payments\CreatePaymentRequest;
use App\Http\Requests\Payments\UpdatePaymentRequest;

class PaymentController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $payments = $this->call(GetPaymentsAction::class, []);

        return view('admin.parts.payments.index', [
            'payments' => $this->transform($payments, PaymentTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $currencies = $this->call(GetCurrenciesAction::class, []);

        return view('admin.parts.payments.create', [
            'payment' => null,
            'currencies' => $currencies
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = $this->call(GetPaymentAction::class, [
            $id
        ]);

        $currencies = $this->call(GetCurrenciesAction::class, []);

        return view('admin.parts.payments.edit', [
            'payment' => $this->transform($payment, PaymentTransformer::class),
            'currencies' => $currencies
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreatePaymentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $result = $this->call(CreatePaymentAction::class, [
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);

        if ($result) {
            return redirect('admin/payments')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePaymentRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePaymentRequest $request, $id)
    {
        $result = $this->call(UpdatePaymentAction::class, [
            $id,
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeletePaymentAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/payments')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/payments')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangePaymentStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangePaymentStatusRequest $request)
    {
        if ($this->call(UpdatePaymentAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * Delete the image
     *
     * @param  integer $id
     *
     * @return mixed
     */
    public function deleteMainImage($id)
    {

        $image = $this->call(DeletePaymentImageAction::class, [$id]);

        return response()->json([$image ? true : false]);
    }

}
