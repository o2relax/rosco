<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Blogs\CreateBlogAction;
use App\Gateways\Actions\Blogs\DeleteBlogAction;
use App\Gateways\Actions\Blogs\GetBlogsAction;
use App\Gateways\Actions\Blogs\GetBlogAction;
use App\Gateways\Actions\Blogs\Images\DeleteBlogImageAction;
use App\Gateways\Actions\Blogs\Images\UploadBlogImageAction;
use App\Gateways\Actions\Blogs\UpdateBlogAction;
use App\Gateways\Transformers\Blogs\BlogTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blogs\ChangeBlogStatusRequest;
use App\Http\Requests\Blogs\CreateBlogRequest;
use App\Http\Requests\Blogs\UpdateBlogRequest;
use App\Http\Requests\Blogs\UploadBlogRequest;
use App\Repositories\BlogRepository;

class BlogController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $blogs = $this->call(GetBlogsAction::class, []);

        return view('admin.parts.blogs.index', [
            'blogs' => $this->transform($blogs, BlogTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parts.blogs.create', [
            'blog' => null,
            'blog_id' => app(BlogRepository::class)->getLastId()->AUTO_INCREMENT
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = $this->call(GetBlogAction::class, [
            $id
        ]);

        abort_if(!$blog, 404);

        return view('admin.parts.blogs.edit', [
            'blog' => $this->transform($blog, BlogTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateBlogRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBlogRequest $request)
    {
        $result = $this->call(CreateBlogAction::class, [
            $request->except(['_token', '_method']),
            $request->get('image')
        ]);

        if ($result) {
            return redirect('admin/blog')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBlogRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlogRequest $request, $id)
    {
        $result = $this->call(UpdateBlogAction::class, [
            $id,
            $request->except(['_token', '_method']),
            $request->get('image')
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteBlogAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/blog')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/blog')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeBlogStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeBlogStatusRequest $request)
    {
        if ($this->call(UpdateBlogAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * @param $id
     * @param UploadBlogRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload($id, UploadBlogRequest $request)
    {
        $image = $this->call(UploadBlogImageAction::class, [
            $id,
            $request->get('image')
        ]);

        return response()->json([$image ? $image : false]);
    }

    /**
     * Delete the image
     *
     * @param  integer $id
     *
     * @return mixed
     */
    public function deleteMainImage($id)
    {

        $image = $this->call(DeleteBlogImageAction::class, [$id]);

        return response()->json([$image ? true : false]);
    }

}
