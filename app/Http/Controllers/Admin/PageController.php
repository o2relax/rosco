<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Currencies\GetCurrenciesAction;
use App\Gateways\Actions\Pages\CreatePageAction;
use App\Gateways\Actions\Pages\DeletePageAction;
use App\Gateways\Actions\Pages\GetPagesAction;
use App\Gateways\Actions\Pages\GetPageAction;
use App\Gateways\Actions\Pages\Images\DeletePageImageAction;
use App\Gateways\Actions\Pages\Images\UploadPageImageAction;
use App\Gateways\Actions\Pages\UpdatePageAction;
use App\Gateways\Transformers\Pages\PageTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Pages\ChangePageStatusRequest;
use App\Http\Requests\Pages\CreatePageRequest;
use App\Http\Requests\Pages\UpdatePageRequest;
use App\Http\Requests\Pages\UploadPageRequest;
use App\Models\Page;
use App\Repositories\BaseRepository;
use App\Repositories\PageRepository;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pages = $this->call(GetPagesAction::class, []);

        return view('admin.parts.pages.index', [
            'pages' => $this->transform($pages, PageTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parts.pages.create', [
            'page' => null,
            'page_id' => app(PageRepository::class)->getLastId()->AUTO_INCREMENT
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->call(GetPageAction::class, [
            $id
        ]);

        abort_if(!$page, 404);

        return view('admin.parts.pages.edit', [
            'page' => $this->transform($page, PageTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreatePageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePageRequest $request)
    {
        $result = $this->call(CreatePageAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/pages')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePageRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePageRequest $request, $id)
    {
        $result = $this->call(UpdatePageAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeletePageAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/pages')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/pages')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangePageStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangePageStatusRequest $request)
    {
        if ($this->call(UpdatePageAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * @param $id
     * @param UploadPageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload($id, UploadPageRequest $request)
    {
        $image = $this->call(UploadPageImageAction::class, [
            $id,
            $request->get('image')
        ]);

        return response()->json([$image ? $image : false]);
    }

}
