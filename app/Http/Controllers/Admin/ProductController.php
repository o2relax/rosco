<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Attributes\GetAttributesAction;
use App\Gateways\Actions\Categories\GetCategoriesAction;
use App\Gateways\Actions\Categories\TreeCategoryAction;
use App\Gateways\Actions\Currencies\GetCurrenciesAction;
use App\Gateways\Actions\Manufacturers\GetManufacturersAction;
use App\Gateways\Actions\Products\AddProductAttributeAction;
use App\Gateways\Actions\Products\CreateProductAction;
use App\Gateways\Actions\Products\Images\DeleteProductMainImageAction;
use App\Gateways\Actions\Products\RemoveProductAttributeAction;
use App\Gateways\Transformers\Languages\LanguageTransformer;
use App\Gateways\Transformers\Products\ProductsTransformer;
use App\Gateways\Transformers\Products\ProductTransformer;
use App\Http\Requests\Products\AddProductAttributeRequest;
use App\Http\Requests\Products\CreateProductRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gateways\Actions\Products\GetStatusAction;
use App\Gateways\Actions\Products\GetProductAction;
use App\Gateways\Actions\Products\GetProductsAction;
use App\Gateways\Actions\Products\GetStatusesAction;
use App\Http\Requests\Products\RemoveProductAttributeRequest;
use App\Http\Requests\Products\UpdateProductRequest;
use App\Http\Requests\Products\SearchProductRequest;
use App\Gateways\Actions\Products\DeleteProductAction;
use App\Gateways\Actions\Languages\GetLanguagesAction;
use App\Gateways\Actions\Products\UpdateProductAction;
use App\Gateways\Transformers\Products\CategoryTransformer;
use App\Http\Requests\Products\ChangeProductStatusRequest;
use App\Gateways\Actions\Products\Images\DeleteProductImageAction;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->call(GetProductsAction::class, []);
        $statuses = $this->call(GetStatusesAction::class, ['Product']);
        $manufacturers = $this->call(GetManufacturersAction::class, [null, 'all']);
        $categories = $this->call(TreeCategoryAction::class, [
            $this->call(GetCategoriesAction::class, [null, 'all'])
        ]);

        return view('admin.parts.products.index', [
            'pagination' => $products->render(),
            'products' => $this->transform($products, ProductsTransformer::class),
            'statuses' => $statuses,
            'manufacturers' => $manufacturers,
            'categories' => $categories
        ]);
    }

    /**
     * Display a listing of the resource searched.
     * @param SearchProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchProductRequest $request)
    {
        $products = $this->call(GetProductsAction::class, [
            $request->get('search')
        ]);

        $manufacturers = $this->call(GetManufacturersAction::class, []);
        $categories = $this->call(TreeCategoryAction::class, [
            $this->call(GetCategoriesAction::class, [null,'all'])
        ]);

        $statuses = $this->call(GetStatusesAction::class, ['Product']);

        return view('admin.parts.products.index', [
            'search' => $request->get('search'),
            'pagination' => $products->appends($request->get('search'))->render(),
            'products' => $this->transform($products, ProductTransformer::class),
            'statuses' => $statuses,
            'manufacturers' => $manufacturers,
            'categories' => $categories
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $statuses = $this->call(GetStatusesAction::class, ['Product']);

        $categories = $this->call(TreeCategoryAction::class, [
            $this->call(GetCategoriesAction::class, [null,'all'])
        ]);

        $currencies = $this->call(GetCurrenciesAction::class, [false]);

        $attributes = $this->call(GetAttributesAction::class, []);

        $manufacturers = $this->call(GetManufacturersAction::class, []);

        return view('admin.parts.products.create', [
            'product' => null,
            'statuses' => $statuses,
            'categories' => $categories,
            'attributes' => $attributes,
            'manufacturers' => $manufacturers,
            'currencies' => $currencies
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = $this->call(GetProductAction::class, [
            $id
        ]);

        $currencies = $this->call(GetCurrenciesAction::class, [false]);

        $statuses = $this->call(GetStatusesAction::class, ['Product']);
        $categories = $this->call(TreeCategoryAction::class, [
            $this->call(GetCategoriesAction::class, [null,'all'])
        ]);

        $attributes = $this->call(GetAttributesAction::class, [
            ['not' => $product->attributes->keyBy('attribute_id')->keys()]
        ]);

        $manufacturers = $this->call(GetManufacturersAction::class, []);

        return view('admin.parts.products.edit', [
            'product' => $this->transform($product, ProductTransformer::class),
            'statuses' => $statuses,
            'categories' => $categories,
            'attributes' => $attributes,
            'manufacturers' => $manufacturers,
            'currencies' => $currencies
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {

        $product = $this->call(CreateProductAction::class, [
            $request->except('_token'),
            $request->file('images'),
            $request->file('image')
        ]);

        if (!$product)  return back()->with('message', trans('messages.action.create.fail'));;
        if ($product&&$product->price>0) {
            return redirect()->route('admin.products.edit', [$product->id])->with('message', trans('messages.action.create.success'));
        }

        return redirect()->route('admin.products.index')->with('message', trans('messages.action.create.fail'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateProductRequest $request)
    {

        $product = $this->call(UpdateProductAction::class, [
            $id,
            $request->except(['_token', '_method']),
            $request->file('images'),
            $request->file('image')
        ]);


        if ($product&&$product->price>0) {
            return back()->with('message', trans('messages.action.update.success'));
        } else {
            return back()->with('message', trans('messages.action.update.fail'));
        }


    }

    /**
     * @param $id
     * @param ChangeProductStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeProductStatusRequest $request)
    {
        if ($this->call(UpdateProductAction::class, [$id, $request->except('_token')])) {
            $response = $this->call(GetStatusAction::class, [$request->get('status_id')]);
        } else {
            $response = $this->call(GetStatusAction::class, [
                $this->call(GetProductAction::class, [$request->get('id')])->status_id
            ]);
        }

        return response()->json($response);
    }

    /**
     * Delete the image
     *
     * @param  integer $id
     *
     * @return mixed
     */
    public function deleteImage($id)
    {

        $image = $this->call(DeleteProductImageAction::class, [$id]);

        return response()->json([$image ? true : false]);
    }

    /**
     * Delete the image
     *
     * @param  integer $id
     *
     * @return mixed
     */
    public function deleteMainImage($id)
    {

        $image = $this->call(DeleteProductMainImageAction::class, [$id]);

        return response()->json([$image ? true : false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $product = $this->call(DeleteProductAction::class, [$id]);

        if ($product) {
            return redirect()->route('admin.products.index')->with('message', trans('messages.action.delete.success'));
        }

        return redirect()->route('admin.products.index')->with('message', trans('messages.action.delete.fail'));
    }

    public function deleteAll()
    {
        $products = $this->call(GetProductsAction::class, []);
        foreach ($products as $product) {
            $test = $this->call(DeleteProductAction::class, [$product->id]);
            if (!$test) return redirect()->route('admin.products.index')->with('message', trans('messages.action.delete.fail'));
        }

        return redirect()->route('admin.products.index')->with('message',trans('messages.action.delete.success'));
    }

    public function deleteFilteredProducts(Request $request) {
        $filters = [];

        if ($request->input('selected_category'))
            $filters['category_id'] = $request->input('selected_category');
        if ($request->input('selected_manufacturer'))
            $filters['manufacturer_id'] = $request->input('selected_manufacturer');

        if (isset($filters['category_id']) || isset ($filters['manufacturer_id'])) {
            $products = $this->call(GetProductsAction::class, [
                $filters
            ]);
            foreach ($products as $product) {
                $test = $this->call(DeleteProductAction::class, [$product->id]);
                if (!$test) return redirect()->route('admin.products.index')->with('message', trans('messages.action.delete.fail'));
            }
        }
        else {
            return redirect()->route('admin.products.index')->with('message', trans('messages.action.delete.fail'));
        }
        return redirect()->route('admin.products.index')->with('message',trans('messages.action.delete.success'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AddProductAttributeRequest $request
     * @param int $id
     * @throws
     *
     * @return \Illuminate\Http\Response
     */
    public function addAttribute($id, AddProductAttributeRequest $request)
    {

        $product = $this->call(AddProductAttributeAction::class, [
            $id,
            $request->get('attribute_id')
        ]);

        if ($product) {
            return response()->json([
                'message' => trans('messages.action.update.success'),
                'status' => 1,
                'html' => view('admin.parts.products.ajax.attributes', [
                    'product' => $product
                ])->render()
            ]);
        } else {
            return response()->json([
                'message' => trans('messages.action.attribute.duplicate'),
                'status' => 0,
            ]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param RemoveProductAttributeRequest $request
     * @param int $id | product id
     * @throws
     *
     * @return \Illuminate\Http\Response
     */

    public function deleteAttribute($id, RemoveProductAttributeRequest $request) {

        $product = $this->call(RemoveProductAttributeAction::class, [
            $id,
            $request->get('attribute_id')
        ]);

        if ($product) {
            return response()->json([
                'message' => trans('messages.action.delete.success'),
                'status' => 1,
                'html' => view('admin.parts.products.ajax.attributes', [
                    'product' => $product
                ])->render()
            ]);
        } else {
            return response()->json([
                'message' => trans('messages.action.delete.fail'),
                'status' => 0,
            ]);
        }

    }
}
