<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Customers\GetCustomerAction;
use App\Gateways\Actions\Customers\GetCustomersAction;
use App\Gateways\Actions\Customers\DeleteCustomerAction;
use App\Gateways\Actions\Orders\GetOrdersAction;
use App\Gateways\Actions\Products\GetStatusesAction;
use App\Gateways\Transformers\Orders\OrderTransformer;
use App\Http\Requests\Customers\SearchCustomerRequest;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = $this->call(GetCustomersAction::class, []);
        return view('admin.parts.customers.index', [
            'customers' => $customers
        ]);
    }

    /**
     * Display a listing of the resource searched.
     * @param SearchCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchCustomerRequest $request)
    {
        if (! $request->has('search')) {
            return redirect('admin/customers');
        }

        $customers = $this->call(GetCustomersAction::class, [
            $request->get('search')
        ]);

        return view('admin.parts.customers.index', [
        'customers' => $customers
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = $this->call(GetCustomerAction::class, [
            $id
        ]);

        abort_if(!$customer, 404);

        $orders = $this->call(GetOrdersAction::class, [
            $customer->id
        ]);

        $statuses = $this->call(GetStatusesAction::class, ['Order']);

        return view('admin.parts.customers.show', [
            'customer' => $customer,
            'orders' => $this->transform($orders, OrderTransformer::class),
            'statuses' => $statuses
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteCustomerAction::class, [
            $id
        ]);

        if ($result) {
            return redirect('admin/customers')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/customers')->with('message', trans('messages.action.delete.fail'));
    }

}
