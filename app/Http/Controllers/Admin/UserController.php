<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Users\CreateUserAction;
use App\Gateways\Actions\Users\DeleteUserAction;
use App\Gateways\Actions\Users\GetStaffAction;
use App\Gateways\Actions\Users\GetUserAction;
use App\Gateways\Actions\Users\SwitchToUserAction;
use App\Gateways\Actions\Users\UpdateUserAction;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\SearchUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->call(GetStaffAction::class, []);
        return view('admin.parts.users.index', [
            'users' => $users
        ]);
    }

    /**
     * Display a listing of the resource searched.
     * @param SearchUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchUserRequest $request)
    {
        if (! $request->has('search')) {
            return redirect('admin/users');
        }

        $users = $this->call(GetStaffAction::class, [
            $request->get('search')
        ]);

        return view('admin.parts.users.index')->with('users', $users);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInvite()
    {
        return view('admin.parts.users.invite');
    }

    /**
     * Show the form for inviting a customer.
     *
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postInvite(CreateUserRequest $request)
    {
        $result = $this->call(CreateUserAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/users')->with('message', 'Successfully created');
        }

        return back()->with('error', 'Failed to Create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->call(GetUserAction::class, [
            $id
        ]);
        return view('admin.parts.users.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUserRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $result = $this->call(UpdateUserAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteUserAction::class, [
            $id
        ]);

        if ($result) {
            return redirect('admin/users')->with('message', 'Successfully deleted');
        }

        return redirect('admin/users')->with('message', 'Failed to delete');
    }

    /**
     * Switch to a different User profile
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function switchToUser($id)
    {

        if ($this->call(SwitchToUserAction::class, [
            $id
        ])) {
            return redirect('admin.dashboard')->with('message', 'You\'ve switched users.');
        }

        return redirect('admin.dashboard')->with('message', 'Could not switch users');
    }

    /**
     * Switch back to your original user
     *
     * @return \Illuminate\Http\Response
     */
    public function switchUserBack()
    {
        if ($this->call(SwitchToUserAction::class, [])) {
            return back()->with('message', 'You\'ve switched back.');
        }

        return back()->with('message', 'Could not switch back');
    }
}
