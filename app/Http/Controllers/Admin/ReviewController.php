<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Customers\GetCustomersAction;
use App\Gateways\Actions\Products\GetProductsAction;
use App\Gateways\Actions\Reviews\CreateReviewAction;
use App\Gateways\Actions\Reviews\DeleteReviewAction;
use App\Gateways\Actions\Reviews\GetReviewsAction;
use App\Gateways\Actions\Reviews\GetReviewAction;
use App\Gateways\Actions\Reviews\UpdateReviewAction;
use App\Gateways\Transformers\Reviews\ReviewTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reviews\ChangeReviewStatusRequest;
use App\Http\Requests\Reviews\CreateReviewRequest;
use App\Http\Requests\Reviews\UpdateReviewRequest;

class ReviewController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $reviews = $this->call(GetReviewsAction::class, []);

        return view('admin.parts.reviews.index', [
            'reviews' => $this->transform($reviews, ReviewTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = $this->call(GetCustomersAction::class, []);

        $products = $this->call(GetProductsAction::class, [null, 'all']);

        return view('admin.parts.reviews.create', [
            'review' => null,
            'users' => $users,
            'products' => $products
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = $this->call(GetReviewAction::class, [
            $id
        ]);

        abort_if(!$review, 404);

        return view('admin.parts.reviews.edit', [
            'review' => $this->transform($review, ReviewTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateReviewRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateReviewRequest $request)
    {
        $result = $this->call(CreateReviewAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/reviews')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateReviewRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReviewRequest $request, $id)
    {
        $result = $this->call(UpdateReviewAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteReviewAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/reviews')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/reviews')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeReviewStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeReviewStatusRequest $request)
    {
        if ($this->call(UpdateReviewAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

}
