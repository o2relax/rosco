<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Modules\DeleteModuleFileAction;
use App\Gateways\Actions\Modules\GetModulesAction;
use App\Gateways\Actions\Modules\GetModuleAction;
use App\Gateways\Actions\Modules\UpdateModuleAction;
use App\Gateways\Actions\Products\Images\DeleteProductImageAction;
use App\Gateways\Transformers\Modules\ModuleTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Modules\ChangeModuleCacheRequest;
use App\Http\Requests\Modules\ChangeModuleStatusRequest;
use App\Http\Requests\Modules\UpdateModuleRequest;
use App\Http\Requests\Modules\UploadModuleRequest;

class ModuleController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $modules = $this->call(GetModulesAction::class, []);

        return view('admin.parts.modules.index', [
            'modules' => $this->transform($modules, ModuleTransformer::class)
        ]);
    }

    /**
     * @param $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function config($code)
    {
        $module = $this->call(GetModuleAction::class, [
            $code,
            false
        ]);

        abort_if(!$module, 404);

        return view('admin.parts.modules.edit', [
            'module' => $this->transform($module, ModuleTransformer::class)
        ]);
    }

    /**
     * @param UpdateModuleRequest $request
     * @param $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateModuleRequest $request, $code)
    {
        $result = $this->call(UpdateModuleAction::class, [
            $code,
            $request->except(['_token', '_method']),
            $request->file('file')
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * @param $id
     * @param ChangeModuleStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeModuleStatusRequest $request)
    {
        if ($this->call(UpdateModuleAction::class, [$id, $request->only('is_active')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * @param $id
     * @param ChangeModuleCacheRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCache($id, ChangeModuleCacheRequest $request)
    {
        if ($this->call(UpdateModuleAction::class, [$id, $request->only('use_cache')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function clearCache($code)
    {
        if (module()->clearCache($code)) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * @param $code
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile($code, $id)
    {

        $image = $this->call(DeleteModuleFileAction::class, [$code, $id]);

        return response()->json([$image ? true : false]);
    }

    /**
     * @param $code
     * @param int $key
     * @return string
     * @throws \Throwable
     */
    public function ajax($code, $key = 0)
    {
        return view('admin.parts.modules.custom.' . $code . '.partial', ['key' => $key])->render();
    }

}
