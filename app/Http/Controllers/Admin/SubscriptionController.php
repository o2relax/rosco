<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Subscriptions\GetSubscriptionsAction;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class SubscriptionController extends Controller
{

    protected $data;

    /**
     *
     */
    public function index()
    {


        $this->data = $this->call(GetSubscriptionsAction::class, []);
        return view('admin.parts.subscriptions.index',[
            'subscriptions' => $this->data
        ]);

//        return Excel::create('rosco-subscriptions-' . Carbon::now()->format('Y-m-d'), function($excel) {
//
//            $excel->setTitle('Подписки');
//
//            $excel->setCreator('Rosco Shop Admin Panel')
//                ->setCompany('RoscoShop');
//
//            $excel->setDescription('Список подписавшихся на рассылку почтовых ящиков');
//
//            $excel->sheet('Список', function($sheet) {
//
//                $sheet->fromArray($this->data);
//
//            });
//
//        })->download('xlsx');

    }

}
