<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Languages\CreateLanguageAction;
use App\Gateways\Actions\Languages\DeleteLanguageAction;
use App\Gateways\Actions\Languages\DeleteLanguageImageAction;
use App\Gateways\Actions\Languages\UpdateLanguageAction;
use App\Gateways\Actions\Languages\GetLanguageAction;
use App\Gateways\Actions\Languages\GetLanguagesAction;
use App\Gateways\Transformers\Languages\LanguageTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Languages\ChangeLanguageStatusRequest;
use App\Http\Requests\Languages\CreateLanguageRequest;
use App\Http\Requests\Languages\UpdateLanguageRequest;

class LanguageController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $languages = $this->call(GetLanguagesAction::class, [false]);

        return view('admin.parts.languages.index', [
            'langs' => $this->transform($languages, LanguageTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parts.languages.create', [
            'language' => null
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $language = $this->call(GetLanguageAction::class, [
            $id
        ]);

        return view('admin.parts.languages.edit', [
            'language' => $this->transform($language, LanguageTransformer::class),
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateLanguageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLanguageRequest $request)
    {
        $result = $this->call(CreateLanguageAction::class, [
            $request->except(['_token', '_method']),
            $request->file('icon')
        ]);

        if ($result) {
            return redirect('admin/languages')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateLanguageRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLanguageRequest $request, $id)
    {
        $result = $this->call(UpdateLanguageAction::class, [
            $id,
            $request->except(['_token', '_method']),
            $request->file('icon')
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteLanguageAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/languages')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/languages')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeLanguageStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postActive($id, ChangeLanguageStatusRequest $request)
    {
        if ($this->call(UpdateLanguageAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function deleteImage($id)
    {
        if ($this->call(DeleteLanguageImageAction::class, [$id])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

}
