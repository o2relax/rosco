<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Categories\CreateCategoryAction;
use App\Gateways\Actions\Categories\DeleteCategoryAction;
use App\Gateways\Actions\Categories\GetCategoriesAction;
use App\Gateways\Actions\Categories\GetCategoryAction;
use App\Gateways\Actions\Categories\Images\DeleteCategoryImageAction;
use App\Gateways\Actions\Categories\TreeCategoryAction;
use App\Gateways\Actions\Categories\UpdateCategoryAction;
use App\Gateways\Actions\Languages\GetLanguagesAction;
use App\Gateways\Transformers\Categories\CategoriesTransformer;
use App\Gateways\Transformers\Categories\CategoryTransformer;
use App\Gateways\Transformers\Languages\LanguageTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\ChangeCategoryStatusRequest;
use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\SearchCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;

class CategoryController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = $this->getCategories();

        return view('admin.parts.categories.index', [
            'categories' => $this->transform($categories, CategoriesTransformer::class)
        ]);
    }

    public function getCategories($id = null) {
        $categories = $this->call(GetCategoriesAction::class, [null,'all']);

        return $this->call(TreeCategoryAction::class, [$categories, $id]);
    }

    /**
     * Display a listing of the resource searched.
     * @param SearchCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchCategoryRequest $request)
    {
        if (!$request->has('search')) {
            return redirect('admin/categories');
        }

        $categories = $this->call(GetCategoriesAction::class, [
            $request->get('search')
        ]);

        return view('admin.parts.categories.index', [
            'categories' => $this->transform($categories, CategoryTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = $this->getCategories();

        return view('admin.parts.categories.create', [
            'category' => null,
            'categories' => $this->transform($categories, CategoryTransformer::class),
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $result = $this->call(CreateCategoryAction::class, [
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);

        if ($result) {
            return redirect('admin/categories')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->call(GetCategoryAction::class, [
            $id
        ]);

        $categories = $this->getCategories($id);

        return view('admin.parts.categories.edit', [
            'category' => $this->transform($category, CategoryTransformer::class),
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCategoryRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {

        $result = $this->call(UpdateCategoryAction::class, [
            $id,
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);


        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteCategoryAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/categories')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/categories')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeCategoryStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeCategoryStatusRequest $request)
    {

        if ($this->call(UpdateCategoryAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * Delete the image
     *
     * @param  integer $id
     *
     * @return mixed
     */
    public function deleteMainImage($id)
    {

        $image = $this->call(DeleteCategoryImageAction::class, [$id]);

        return response()->json([$image ? true : false]);
    }

}
