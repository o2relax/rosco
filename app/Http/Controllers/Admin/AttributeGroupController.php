<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Attributes\Groups\CreateAttributeGroupAction;
use App\Gateways\Actions\Attributes\Groups\DeleteAttributeGroupAction;
use App\Gateways\Actions\Attributes\Groups\GetAttributeGroupAction;
use App\Gateways\Actions\Attributes\Groups\UpdateAttributeGroupAction;
use App\Gateways\Actions\Languages\GetLanguagesAction;
use App\Gateways\Transformers\Languages\LanguageTransformer;
use App\Http\Controllers\Controller;
use App\Gateways\Actions\Attributes\Groups\GetAttributeGroupsAction;
use App\Http\Requests\Attributes\Groups\CreateAttributeGroupRequest;
use App\Http\Requests\Attributes\Groups\UpdateAttributeGroupRequest;

class AttributeGroupController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $groups = $this->call(GetAttributeGroupsAction::class, []);

        return view('admin.parts.attributes.groups.index', [
            'groups' => $groups
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.parts.attributes.groups.create', [
            'group' => null,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = $this->call(GetAttributeGroupAction::class, [
            $id
        ]);

        return view('admin.parts.attributes.groups.edit', [
            'group' => $group,
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateAttributeGroupRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAttributeGroupRequest $request)
    {
        $result = $this->call(CreateAttributeGroupAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/attributes/groups')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAttributeGroupRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttributeGroupRequest $request, $id)
    {
        $result = $this->call(UpdateAttributeGroupAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteAttributeGroupAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/attributes/groups')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/attributes/groups')->with('message', trans('messages.action.delete.fail'));
    }

}
