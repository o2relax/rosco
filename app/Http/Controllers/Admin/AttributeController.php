<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Attributes\CreateAttributeAction;
use App\Gateways\Actions\Attributes\DeleteAttributeAction;
use App\Gateways\Actions\Attributes\GetAttributeAction;
use App\Gateways\Actions\Attributes\Groups\GetAttributeGroupsAction;
use App\Gateways\Actions\Attributes\UpdateAttributeAction;
use App\Gateways\Actions\Languages\GetLanguagesAction;
use App\Gateways\Transformers\Languages\LanguageTransformer;
use App\Http\Controllers\Controller;
use App\Gateways\Actions\Attributes\GetAttributesAction;
use App\Http\Requests\Attributes\CreateAttributeRequest;
use App\Http\Requests\Attributes\UpdateAttributeRequest;

class AttributeController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $attributes = $this->call(GetAttributesAction::class, []);

        return view('admin.parts.attributes.index', [
            'attributes' => $attributes
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = $this->call(GetAttributeGroupsAction::class, []);

        return view('admin.parts.attributes.create', [
            'attribute' => null,
            'groups' => $groups
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attribute = $this->call(GetAttributeAction::class, [
            $id
        ]);

        $groups = $this->call(GetAttributeGroupsAction::class, []);

        return view('admin.parts.attributes.edit', [
            'attribute' => $attribute,
            'groups' => $groups
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateAttributeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAttributeRequest $request)
    {
        $result = $this->call(CreateAttributeAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/attributes')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAttributeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttributeRequest $request, $id)
    {
        $result = $this->call(UpdateAttributeAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteAttributeAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/attributes')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/attributes')->with('message', trans('messages.action.delete.fail'));
    }

}
