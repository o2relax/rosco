<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Currencies\GetCurrenciesAction;
use App\Gateways\Actions\Discounts\CreateDiscountAction;
use App\Gateways\Actions\Discounts\DeleteDiscountAction;
use App\Gateways\Actions\Discounts\GetDiscountsAction;
use App\Gateways\Actions\Discounts\GetDiscountAction;
use App\Gateways\Actions\Discounts\UpdateDiscountAction;
use App\Gateways\Actions\Manufacturers\GetManufacturersAction;
use App\Gateways\Actions\Products\GetProductsAction;
use App\Gateways\Transformers\Discounts\DiscountTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Discounts\ChangeDiscountStatusRequest;
use App\Http\Requests\Discounts\CreateDiscountRequest;
use App\Http\Requests\Discounts\UpdateDiscountRequest;

class DiscountController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $discounts = $this->call(GetDiscountsAction::class, []);

        return view('admin.parts.discounts.index', [
            'discounts' => $this->transform($discounts, DiscountTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.parts.discounts.create', [
            'discount' => null,
            'manufacturers'=>$manufacturers = $this->call(GetManufacturersAction::class, []),
            'products' =>    $products = $this->call(GetProductsAction::class, [null,'all'])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discount = $this->call(GetDiscountAction::class, [
            $id
        ]);

        abort_if(!$discount, 404);

        $manufacturers = $this->call(GetManufacturersAction::class, []);

        $products = $this->call(GetProductsAction::class, [null,'all']);

        $chosen_manufacturers = collect($discount->items)
            ->where('manufacturer_id', '<>', null)
            ->keyBy('manufacturer_id')
            ->keys()
            ->toArray();

        $chosen_products = collect($discount->items)
            ->where('product_id', '<>', null)
            ->keyBy('product_id')
            ->keys()
            ->toArray();

        return view('admin.parts.discounts.edit', [
            'discount' => $this->transform($discount, DiscountTransformer::class),
            'manufacturers' => $manufacturers,
            'products' => $products,
            'chosen_manufacturers' => $chosen_manufacturers,
            'chosen_products' => $chosen_products,
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateDiscountRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDiscountRequest $request)
    {
        $result = $this->call(CreateDiscountAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/discounts')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateDiscountRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDiscountRequest $request, $id)
    {
        $result = $this->call(UpdateDiscountAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteDiscountAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/discounts')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/discounts')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeDiscountStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeDiscountStatusRequest $request)
    {
        if ($this->call(UpdateDiscountAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

}
