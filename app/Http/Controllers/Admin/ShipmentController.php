<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Payments\GetPaymentsAction;
use App\Gateways\Actions\Shipments\CreateShipmentAction;
use App\Gateways\Actions\Shipments\DeleteShipmentAction;
use App\Gateways\Actions\Shipments\GetShipmentsAction;
use App\Gateways\Actions\Shipments\GetShipmentAction;
use App\Gateways\Actions\Shipments\Images\DeleteShipmentImageAction;
use App\Gateways\Actions\Shipments\UpdateShipmentAction;
use App\Gateways\Transformers\Shipments\ShipmentTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Shipments\ChangeShipmentStatusRequest;
use App\Http\Requests\Shipments\CreateShipmentRequest;
use App\Http\Requests\Shipments\UpdateShipmentRequest;

class ShipmentController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $shipments = $this->call(GetShipmentsAction::class, []);

        return view('admin.parts.shipments.index', [
            'shipments' => $this->transform($shipments, ShipmentTransformer::class)
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $payments = $this->call(GetPaymentsAction::class, []);

        return view('admin.parts.shipments.create', [
            'shipment' => null,
            'payments' => $payments
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shipment = $this->call(GetShipmentAction::class, [
            $id
        ]);

        abort_if(!$shipment, 404);

        $payments = $this->call(GetPaymentsAction::class, []);

        $chosen_payments = $shipment->payments->keyBy('id')->keys()->toArray();

        return view('admin.parts.shipments.edit', [
            'shipment' => $this->transform($shipment, ShipmentTransformer::class),
            'payments' => $payments,
            'chosen_payments' => $chosen_payments
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateShipmentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateShipmentRequest $request)
    {
        $result = $this->call(CreateShipmentAction::class, [
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);

        if ($result) {
            return redirect('admin/shipments')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateShipmentRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShipmentRequest $request, $id)
    {
        $result = $this->call(UpdateShipmentAction::class, [
            $id,
            $request->except(['_token', '_method']),
            $request->file(['image'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteShipmentAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/shipments')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/shipments')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeShipmentStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postStatus($id, ChangeShipmentStatusRequest $request)
    {
        if ($this->call(UpdateShipmentAction::class, [$id, $request->except('_token')])) {
            return response()->json(['message' => trans('messages.status.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.status.change.fail')]);
        }
    }

    /**
     * Delete the image
     *
     * @param  integer $id
     *
     * @return mixed
     */
    public function deleteMainImage($id)
    {

        $image = $this->call(DeleteShipmentImageAction::class, [$id]);

        return response()->json([$image ? true : false]);
    }

}
