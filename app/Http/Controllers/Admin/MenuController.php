<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Attributes\CreateAttributeAction;
use App\Gateways\Actions\Attributes\DeleteAttributeAction;
use App\Gateways\Actions\Attributes\GetAttributeAction;
use App\Gateways\Actions\Attributes\Groups\GetAttributeGroupsAction;
use App\Gateways\Actions\Attributes\UpdateAttributeAction;
use App\Gateways\Actions\Languages\GetLanguagesAction;
use App\Gateways\Actions\Menus\CreateMenuAction;
use App\Gateways\Actions\Menus\DeleteMenuAction;
use App\Gateways\Actions\Menus\GetMenuAction;
use App\Gateways\Actions\Menus\GetMenusAction;
use App\Gateways\Actions\Menus\Links\CreateMenuLinkAction;
use App\Gateways\Actions\Menus\Links\DeleteMenuLinkAction;
use App\Gateways\Actions\Menus\Links\GetMenuLinkAction;
use App\Gateways\Actions\Menus\Links\SortMenuLinkAction;
use App\Gateways\Actions\Menus\Links\UpdateMenuLinkAction;
use App\Gateways\Actions\Menus\UpdateMenuAction;
use App\Gateways\Transformers\Languages\LanguageTransformer;
use App\Http\Controllers\Controller;
use App\Gateways\Actions\Attributes\GetAttributesAction;
use App\Http\Requests\Attributes\CreateAttributeRequest;
use App\Http\Requests\Attributes\UpdateAttributeRequest;
use App\Http\Requests\Menus\CreateMenuRequest;
use App\Http\Requests\Menus\Links\CreateMenuLinkRequest;
use App\Http\Requests\Menus\Links\SortMenuLinkRequest;
use App\Http\Requests\Menus\Links\StatusMenuLinkRequest;
use App\Http\Requests\Menus\Links\UpdateMenuLinkRequest;
use App\Http\Requests\Menus\StatusMenuRequest;
use App\Http\Requests\Menus\UpdateMenuRequest;

class MenuController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $menus = $this->call(GetMenusAction::class, []);

        return view('admin.parts.menus.index', [
            'menus' => $menus
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.parts.menus.create', [
            'menu' => null,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = $this->call(GetMenuAction::class, [
            $id
        ]);

        return view('admin.parts.menus.edit', [
            'menu' => $menu
        ]);
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateMenuRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMenuRequest $request)
    {
        $result = $this->call(CreateMenuAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect()->route('admin.menus.edit', [$result->id])->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateMenuRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenuRequest $request, $id)
    {
        $result = $this->call(UpdateMenuAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteMenuAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/menus')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/menus')->with('message', trans('messages.action.delete.fail'));
    }

    public function active($id, StatusMenuRequest $request)
    {

        if ($this->call(UpdateMenuAction::class, [$id, $request->except(['_token'])])) {
            return response()->json(['message' => trans('messages.action.update.success')]);
        } else {
            return response()->json(['message' => trans('messages.action.update.fail')]);
        }

    }

    public function linkCreate($menu_id)
    {

        return view('admin.parts.menus.links.create', [
            'link' => null,
            'menu_id' => $menu_id
        ]);

    }

    public function linkEdit($menu_id, $link_id)
    {

        $link = $this->call(GetMenuLinkAction::class, [
            $link_id
        ]);

        return view('admin.parts.menus.links.edit', [
            'link' => $link
        ]);

    }

    public function linkStore($menu_id, CreateMenuLinkRequest $request)
    {

        $result = $this->call(CreateMenuLinkAction::class, [
            $menu_id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect()->route('admin.menus.edit', [$menu_id])->with('message', trans('messages.action.update.success'));
        }

        return redirect()->back()->with('message', trans('messages.action.update.fail'));

    }

    public function linkUpdate($menu_id, $link_id, UpdateMenuLinkRequest $request)
    {

        $result = $this->call(UpdateMenuLinkAction::class, [
            $link_id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect()->route('admin.menus.edit', [$menu_id])->with('message', trans('messages.action.update.success'));
        }

        return redirect()->back()->with('message', trans('messages.action.update.fail'));

    }

    public function linkDelete($menu_id, $id)
    {

        $result = $this->call(DeleteMenuLinkAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect()->route('admin.menus.edit', [$menu_id])->with('message', trans('messages.action.delete.success'));
        }

        return redirect()->back()->with('message', trans('messages.action.delete.fail'));
    }

    public function linkSort(SortMenuLinkRequest $request)
    {

        if ($this->call(SortMenuLinkAction::class, [$request->get('data')])) {
            return response()->json(['message' => trans('messages.sort.change.success')]);
        } else {
            return response()->json(['message' => trans('messages.sort.change.fail')]);
        }

    }

    public function linkActive(StatusMenuLinkRequest $request)
    {

        if ($this->call(UpdateMenuLinkAction::class, [$request->get('id'), $request->except(['_token'])])) {
            return response()->json(['message' => trans('messages.action.update.success')]);
        } else {
            return response()->json(['message' => trans('messages.action.update.fail')]);
        }

    }

}
