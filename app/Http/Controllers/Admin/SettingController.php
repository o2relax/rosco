<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Transformers\Settings\SettingPartTransformer;
use App\Mail\OrderCreated;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Gateways\Actions\Settings\GetSettingsAction;
use App\Gateways\Actions\Settings\UpdateSettingsAction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class SettingController extends Controller
{

    public function __construct()
    {

    }

    public function index() {

        $parts = $this->call(GetSettingsAction::class, []);

        return view('admin.parts.settings.index', [
            'parts' => $this->transform($parts, SettingPartTransformer::class)
        ]);

    }

    public function save(Request $request) {

        if($this->call(UpdateSettingsAction::class, [
            $request->except(['_token'])
        ])) {

            return redirect('admin/settings')->with('message', trans('messages.action.update.success'));

        } else {

            return redirect('admin/settings')->with('message', trans('messages.action.update.fail'));

        }

    }

}
