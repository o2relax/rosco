<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Orders\DeleteOrderAction;
use App\Gateways\Actions\Orders\GetOrderAction;
use App\Gateways\Actions\Orders\GetOrdersAction;
use App\Gateways\Actions\Orders\UpdateOrderAction;
use App\Gateways\Actions\Products\GetStatusAction;
use App\Gateways\Actions\Products\GetStatusesAction;
use App\Gateways\Transformers\Orders\OrderTransformer;
use App\Gateways\Transformers\Products\ProductListTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payments\ChangeOrderStatusRequest;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = $this->call(GetOrdersAction::class, []);

        $statuses = $this->call(GetStatusesAction::class, ['Order']);

        return view('admin.parts.orders.index', [
            'orders' => $this->transform($orders, OrderTransformer::class),
            'statuses' => $statuses,
            'pagination' => $orders->render(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->call(GetOrderAction::class, [
            $id
        ]);

        abort_if(!$order, 404);

        if (!$order->viewed) {
            $this->call(UpdateOrderAction::class, [
                $id,
                ['viewed' => Order::VIEWED]
            ]);
        }

        $statuses = $this->call(GetStatusesAction::class, ['Order']);

        foreach($order->items as $item) {
            if($item->product) $item->product = $this->transform($item->product, ProductListTransformer::class);
        }

        return view('admin.parts.orders.show', [
            'order' => $this->transform($order, OrderTransformer::class),
            'statuses' => $statuses,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteOrderAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/orders')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/orders')->with('message', trans('messages.action.delete.fail'));
    }

    /**
     * @param $id
     * @param ChangeOrderStatusRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStatus($id, ChangeOrderStatusRequest $request)
    {
        if ($this->call(UpdateOrderAction::class, [$id, $request->except('_token')])) {
            $response = $this->call(GetStatusAction::class, [$request->get('status_id')]);
        } else {
            $response = $this->call(GetStatusAction::class, [
                $this->call(GetOrderAction::class, [$request->get('id')])->status_id
            ]);
        }

        return response()->json($response);
    }

    public function deleteAll(Request $request) {
        if ($request->input('data')) {
            foreach ($request->input('data') as $order) {
                $result = $this->call(DeleteOrderAction::class,[$order,]);
                if (!$result) return redirect('admin/orders')->with('message', trans('messages.action.delete.fail'));
            }

        }
        return redirect()->back()->with('message', trans('messages.action.delete.success'));
    }

}
