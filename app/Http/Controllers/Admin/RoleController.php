<?php

namespace App\Http\Controllers\Admin;

use App\Gateways\Actions\Roles\CreateRoleAction;
use App\Gateways\Actions\Roles\DeleteRoleAction;
use App\Gateways\Actions\Roles\GetRoleAction;
use App\Gateways\Actions\Roles\GetRolesAction;
use App\Gateways\Actions\Roles\UpdateRoleAction;
use App\Http\Requests\Roles\CreateRoleRequest;
use App\Http\Requests\Roles\SearchRoleRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->call(GetRolesAction::class, []);
        return view('admin.parts.roles.index', [
            'roles' => $roles
        ]);
    }

    /**
     * Display a listing of the resource searched.
     * @param SearchRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchRoleRequest $request)
    {
        if (! $request->has('search')) {
            return redirect('admin/roles');
        }

        $roles = $this->call(GetRolesAction::class, [
            $request->get('search')
        ]);

        return view('admin.parts.roles.index', [
            'roles' => $roles
        ]);
    }

    /**
     * Show the form for inviting a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parts.roles.create');
    }

    /**
     * Show the form for inviting a customer.
     * @param CreateRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoleRequest $request)
    {
        $result = $this->call(CreateRoleAction::class, [
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return redirect('admin/roles')->with('message', trans('messages.action.create.success'));
        }

        return back()->with('error', trans('messages.action.create.fail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->call(GetRoleAction::class, [
            $id
        ]);
        return view('admin.parts.roles.edit', [
            'role' => $role
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRoleRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        $result = $this->call(UpdateRoleAction::class, [
            $id,
            $request->except(['_token', '_method'])
        ]);

        if ($result) {
            return back()->with('message', trans('messages.action.update.success'));
        }

        return back()->with('message', trans('messages.action.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->call(DeleteRoleAction::class, [
            $id,
        ]);

        if ($result) {
            return redirect('admin/roles')->with('message', trans('messages.action.delete.success'));
        }

        return redirect('admin/roles')->with('message', trans('messages.action.delete.fail'));
    }
}
