<?php

namespace App\Http\Controllers\Shop;

use App\Gateways\Actions\Categories\GetCategoryAction;
//use App\Gateways\Actions\Orders\CreateFastOrderAction;
use App\Gateways\Actions\Products\Custom\GetProductListAction;
use App\Gateways\Transformers\Categories\CategoryPageTransformer;
use App\Gateways\Transformers\Products\ProductListTransformer;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Product;

class CatalogController extends Controller
{

    public function index(Request $request)
    {
        $filters = [];

        if ($request->has('filter')) {
            $filters['filter'] = $request->get('filter');
        }

        $products = $this->call(GetProductListAction::class, [$filters]);

        return view('shop.pages.catalog', [
            'products' => $this->transform($products, ProductListTransformer::class),
        ]);
    }

    public function category($slug, Request $request)
    {
        if ($request->get('per_page')) {
            session()->put('per_page', $request->get('per_page'));
        }

        $category = $this->call(GetCategoryAction::class, [$slug]);

        abort_if(!$category, 404);

        $filters = [];

        if ($category->child->count()) {
            $filters['category_id'] = collect($category->child)->keyBy('id')->keys()->all();
            array_push($filters['category_id'], $category->id);
        } else {
            $filters['category_id'] = [$category->id];
        }

        if ($request->has('filter')) {
            $filters['filter'] = $request->get('filter');
        }

        /** @var Collection $products */
        $products = $this->call(GetProductListAction::class, [
            $filters,
            null,
            $request->get('per_page'),
            $request->get('sort'),
        ]);

        return view('shop.pages.category', [
            'products' => $this->transform($products, ProductListTransformer::class),
            'category' => $this->transform($category, CategoryPageTransformer::class),
        ]);
    }

    public function latest(Request $request)
    {
        if ($request->get('per_page')) {
            session()->put('per_page', $request->get('per_page'));
        }

        /** @var Collection $products */
        $products = $this->call(GetProductListAction::class, [null, 'getLatest', $request->get('per_page'), false]);

        return view('shop.pages.alt_category', [
            'products' => $this->transform($products, ProductListTransformer::class),
            'page'     => 'latest',
        ]);
    }

    public function popular(Request $request)
    {
        if ($request->get('per_page')) {
            session()->put('per_page', $request->get('per_page'));
        }

        /** @var Collection $products */
        $products = $this->call(GetProductListAction::class, [null, 'getPopular', $request->get('per_page'), false]);

        return view('shop.pages.alt_category', [
            'products' => $this->transform($products, ProductListTransformer::class),
            'page'     => 'popular',
        ]);
    }

    public function ajax(Request $request)
    {
        if ($request->input('data')) {
            $product = Product::published()
                              ->where('substance', 'like', '%' . $request->input('data') . '%')
                              ->orderBy('updated_at', 'asc')
                              ->get();
            $a = 0;
            $arr = [];
            foreach ($product as $item => $value) {
                $arr[$a] = [$value->substance];
                $a++;
            }

            echo json_encode($arr);
        } else {
            abort(404);
        }
    }

    public function quickBuy(Request $request)
    {
        if ($request->input('param') && $request->input('data')) {
            $order_info = [
                "name"        => $request->input('param')[2],
                "email"       => "quickbuy@gmail.com",
                "phone"       => $request->input('data'),
                "address"     => "quickbuy",
                "shipment_id" => 1,
                "payment_id"  => 4,
                "currency_id" => 2,
                "status_id"   => 4,
                "user_id"     => 1,

            ];
            $items = [
                [
                    "product_id" => $request->input('param')[0],
                    "price"      => $request->input('param')[1],
                    "quantity"   => 1,
                ],
            ];

            app(OrderRepository::class)->createOrder($order_info, $items);
        }
    }

}
