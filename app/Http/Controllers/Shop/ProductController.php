<?php

namespace App\Http\Controllers\Shop;

use App\Events\ProductViewedEvent;
use App\Gateways\Actions\Products\Custom\GetProductAttributesAction;
use App\Gateways\Actions\Products\GetProductAction;
use App\Gateways\Actions\Reviews\Custom\CreateCustomReviewAction;
use App\Gateways\Transformers\Products\ProductPageTransformer;
use App\Gateways\Transformers\Reviews\ReviewTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reviews\Custom\CreateCustomReviewRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index($slug, $tab = null, Request $request)
    {

        $product = $this->call(GetProductAction::class, [$slug]);


        $data = Product::where('is_related', '1')
            ->orderBy('updated_at','asc')
            ->get();

        abort_if(!$product, 404);

        event(new ProductViewedEvent($product, ['views' => $product->stats ? $product->stats->views + 1 : 1]));

        $attributes = $this->call(GetProductAttributesAction::class, [$product->id]);

        return view('shop.pages.product', [
            'product' => $this->transform($product, ProductPageTransformer::class),
            'reviews' => $this->transform($product->reviews, ReviewTransformer::class),
            'attributes' => $attributes,
            'tab' => $tab,
            'data' => $data
        ]);
    }

    public function postReview($slug, CreateCustomReviewRequest $request)
    {

        if ($this->call(CreateCustomReviewAction::class, [$slug, $request->except(['_token', '_method'])])) {
            return redirect()->route('shop.product', [$slug, 'review#reviews'])->with('message', trans('messages.action.review.success'));
        }

        return redirect()->route('shop.product', [$slug, 'review#reviews'])->with('message', trans('messages.action.review.fail'));

    }


}