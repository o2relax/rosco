<?php

namespace App\Http\Controllers\Shop;

use App\Gateways\Actions\Categories\GetCategoryAction;
use App\Gateways\Actions\Pages\Custom\GetCustomPageAction;
use App\Gateways\Actions\Products\Custom\GetProductListAction;
use App\Gateways\Transformers\Categories\CategoryPageTransformer;
use App\Gateways\Transformers\Pages\PageTransformer;
use App\Gateways\Transformers\Products\ProductListTransformer;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug) {

        $page = $this->call(GetCustomPageAction::class, [$slug]);

        abort_if(!$page, 404);

        return view($page->template ? 'shop.pages.custom.' . $page->template : 'shop.pages.page', [
            'page' => $this->transform($page, PageTransformer::class),
        ]);
    }

}