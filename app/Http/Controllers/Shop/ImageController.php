<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{

    public function get($disk, $id, $name, $w, $h) {

        $image = Image::cache(function($image) use ($disk, $id, $name, $w, $h){
            $image->make(storage(($id ? $id . DIRECTORY_SEPARATOR : '') . $name, $disk))->resize($w, $h, function ($constraint) {
                $constraint->aspectRatio();
            });
        }, config('cache.cache_image', 60));


        $response = response()->make($image);

        $response->header('Content-Type', 'image/png');

        return $response;
    }

    public function not($disk, $w, $h) {

        $image = Image::cache(function($image) use ($disk, $w, $h){
            $image->make(storage(config('image.no-photo'), $disk))->resize($w, $h, function ($constraint) {
                $constraint->aspectRatio();
            });
        }, config('cache.cache_image', 60));

        $response = response()->make($image);

        $response->header('Content-Type', 'image/png');

        return $response;
    }


}
