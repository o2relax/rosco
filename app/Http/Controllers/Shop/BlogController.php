<?php

namespace App\Http\Controllers\Shop;

use App\Gateways\Actions\Blogs\Custom\GetCustomBlogAction;
use App\Gateways\Actions\Blogs\Custom\GetCustomBlogsAction;
use App\Gateways\Transformers\Blogs\BlogListTransformer;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $posts = $this->call(GetCustomBlogsAction::class, []);

        return view( 'shop.pages.blog', [
            'posts' => $this->transform($posts, BlogListTransformer::class),
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item($slug) {

        $post = $this->call(GetCustomBlogAction::class, [$slug]);

        abort_if(!$post, 404);

        return view( 'shop.pages.blog_page', [
            'post' => $this->transform($post, BlogListTransformer::class),
        ]);
    }

}