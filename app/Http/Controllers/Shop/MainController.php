<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;

use App\Gateways\Actions\Products\GetProductsAction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use DB;

class MainController extends Controller
{

    public function index() {


        return view('shop.pages.main');
    }

    public function language($code) {

        locale()->set($code);

        return redirect()->back();
    }

    public function currency($code) {

        currency()->set($code);

        return redirect()->back();
    }

    public function productSearch(Request $request) {


        $term = Input::get('term');

        $search = ['name' => $term ];
        $products = $this->call(GetProductsAction::class, [$search]);
        $result_search = [];
        foreach($products as $product) {
            $result_search[] = ['label' => $product->name,'link' => $product->slug];
        }
        return $result_search;



    }
}