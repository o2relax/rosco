<?php

namespace App\Http\Controllers\Shop;

use App\Gateways\Actions\Cart\GetCartProductsAction;
use App\Gateways\Actions\Orders\CreateOrderAction;
use App\Gateways\Actions\Payments\Custom\GetCustomPaymentsAction;
use App\Gateways\Actions\Products\GetProductAction;
use App\Gateways\Actions\Shipments\Custom\GetCustomShipmentsAction;
use App\Gateways\Transformers\Cart\CartListTransformer;
use App\Gateways\Transformers\Payments\PaymentPageTransformer;
use App\Gateways\Transformers\Shipments\ShipmentPageTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cart\AddProductToCartRequest;
use App\Http\Requests\Cart\CheckoutCartRequest;
use App\Http\Requests\Cart\RemoveProductFromCartRequest;
use App\Http\Requests\Payments\Custom\GetPaymentsRequest;
use App\Models\Product;

class CartController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $items = $this->transform($this->call(GetCartProductsAction::class, []), CartListTransformer::class);
        $cart = cart()->get();

        $shipments = $this->call(GetCustomShipmentsAction::class, []);

        if (
            session()->has('shipment_id')
            &&
            $shipment = collect($shipments)->where('id', session()->get('shipment_id'))->first()
        ) {
            $payments = $this->transform(
                $shipment->payments,
                PaymentPageTransformer::class);
        } else {
            $payments = [];
        }

        return view('shop.pages.cart', [
            'items'     => $items,
            'cart'      => $cart,
            'shipments' => $this->transform($shipments, ShipmentPageTransformer::class),
            'payments'  => $payments,
        ]);
    }

    /**
     * @param AddProductToCartRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */

    public function add(AddProductToCartRequest $request)
    {
        cart()->add($request->get('product_id'), $request->get('quantity', 1));

        return response()->json([
            'count' => cart()->count(),
            'html'  => view('shop.includes.cart_modal', [
                'item' => $this->transform(
                    $this->call(GetProductAction::class, [
                            $request->get('product_id'),
                        ]
                    ),
                    CartListTransformer::class),
                'cart'    => cart()->get(),
                'data' => Product::related()->limit(5)->get()
            ])->render(),
        ]);
    }

    /**
     * @param AddProductToCartRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */

    public function quantity(AddProductToCartRequest $request)
    {
        cart()->quantity($request->get('product_id'), $request->get('quantity', 1));

        $items = $this->call(GetCartProductsAction::class, []);
        $cart = cart()->get();

        return response()->json([
            'count' => cart()->count(),
            'html'  => view('shop.includes.cart', [
                'items' => $this->transform($items, CartListTransformer::class),
                'cart'  => $cart,
            ])->render(),
        ]);
    }

    /**
     * @param RemoveProductFromCartRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \Throwable
     */
    public function delete(RemoveProductFromCartRequest $request)
    {
        cart()->delete($request->get('product_id'));

        $items = $this->call(GetCartProductsAction::class, []);
        $cart = cart()->get();

        return response()->json([
            'count' => cart()->count(),
            'html'  => view('shop.includes.cart', [
                'items' => $this->transform($items, CartListTransformer::class),
                'cart'  => $cart,
            ])->render(),
        ]);
    }

    /**
     * @param CheckoutCartRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function postCheckout(CheckoutCartRequest $request)
    {
        if (
        $order = $this->call(CreateOrderAction::class, [
            $request->get('customer'),
            $request->get('items'),
        ])
        ) {
            return redirect()
                ->route('shop.checkout.success')
                ->with([
                    'checkout' => trans('shop/checkout.success.heading', ['order_id' => $order->id]),
                    'message'  => trans('shop/checkout.success.message', [
                        'email'    => $order->email,
                        'link'     => route('shop.profile.orders'),
                        'order_id' => $order->id,
                    ]),
                ]);
        } elseif ($order == 0) {
            return redirect()->back()->withInput();
        } else {
            return redirect()
                ->route('shop.checkout.success')
                ->with([
                    'checkout' => trans('shop/checkout.fail.heading'),
                    'message'  => trans('shop/checkout.fail.message'),
                ]);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCheckout()
    {
        //if (!session('checkout')) return redirect('/');

        return view('shop.pages.checkout', [
            'heading' => session('checkout'),
            'message' => session('message'),
        ]);
    }

    /**
     * @param GetPaymentsRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPayments(GetPaymentsRequest $request)
    {
        session()->put('shipment_id', $request->get('shipment_id'));

        $payments = $this->call(GetCustomPaymentsAction::class, [explode(',', $request->get('ids'))]);

        return view('shop.includes.payments', [
            'payments' => $this->transform($payments, PaymentPageTransformer::class),
        ]);
    }

}