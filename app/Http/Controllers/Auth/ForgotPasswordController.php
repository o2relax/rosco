<?php

namespace App\Http\Controllers\Auth;

use App\Gateways\Actions\Users\Custom\ForgotPasswordUserAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\ForgotPasswordUserRequest;

class ForgotPasswordController extends Controller
{

    /**
     * ForgotPasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index() {

        return view('shop.pages.auth.forgot');

    }

    public function post(ForgotPasswordUserRequest $request) {

        $user = $this->call(ForgotPasswordUserAction::class, [
            $request->get('email')
        ]);

        if ($user) {
            return redirect()->back()->with('message', trans('shop/profile.messages.forgot_password', ['email' => $user->email]));
        } else {
            return redirect()->back()->with('message', trans('shop/profile.messages.forgot_password_fail'));
        }

    }
}
