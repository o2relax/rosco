<?php

namespace App\Http\Requests\Cart;

use App\Contracts\Requests\Request;

class CheckoutCartRequest extends Request
{
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => null,
        'roles' => null,
    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'customer.email' => 'required|email',
            'customer.address' => 'required|string',
            'customer.phone' => 'required|string',
            'customer.name' => 'required|string',
            'customer.shipment_id' => 'required|integer',
//            'customer.payment_id' => 'required|integer',
            'customer.payment_id' => 'integer',
            'items' => 'required|array'

        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'isCan',
        ]);
    }

}