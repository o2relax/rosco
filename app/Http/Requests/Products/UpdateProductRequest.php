<?php

namespace App\Http\Requests\Products;

use App\Contracts\Requests\Request;

class UpdateProductRequest extends Request
{
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => null,
        'roles' => null,
    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'images[]' => 'mimes:jpeg,bmp,png,svg,gif',
            'image' => 'mimes:jpeg,bmp,png,svg,gif',
            'currency_id' => 'required',
            'manufacturer_id' => 'required',
//            'stock' => 'required|numeric',
            'price' => 'required',
//            'article' => 'required',
            'status_id' => 'required',
            'slug' => 'nullable',
            cfg('language', 'code') . '.name' => 'required'
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'isCan',
        ]);
    }

}