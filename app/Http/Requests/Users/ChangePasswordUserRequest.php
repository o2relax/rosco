<?php

namespace App\Http\Requests\Users;

use App\Contracts\Requests\Request;
use App\Gateways\Actions\Users\Custom\GetUserAction;
use App\Traits\CallableTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ChangePasswordUserRequest extends Request
{

    use CallableTrait;
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => null,
        'roles' => null,
    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        if(!Hash::check(request()->get('old_password'), auth()->user()->getAuthPassword())) {
            return redirect()->back()->withInput()->withErrors('errors', 'old_password');
        }

        return [
            'password' => 'required|min:6|confirmed',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'isCan',
        ]);
    }

}