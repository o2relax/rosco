<?php

namespace App\Http\Requests\Reviews;

use App\Contracts\Requests\Request;

class CreateReviewRequest extends Request
{

    protected $redirect = '#reviews';

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => null,
        'roles' => null,
    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'text' => 'required',
            'name' => 'required',
            'product_id' => 'required|numeric',
            'user_id' => 'required|numeric'

        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'isCan',
        ]);
    }

}