<?php

namespace App\Http\Requests\Categories;

use App\Contracts\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateCategoryRequest extends Request
{
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => null,
        'roles' => null,
    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
    ];

    /**
     * @return  array
     */
    public function rules()
    {

        return [
            'image' => '',
            'slug' => [
                Rule::unique('categories')->ignore(request()->get('id'), 'id'),
               'nullable'
            ],
            cfg('language', 'code') . '.name' => 'required'
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'isCan',
        ]);
    }

}