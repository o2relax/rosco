<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;

class Locale
{
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function handle($request, Closure $next)
    {

        $this->app->setLocale(locale()->get());


        return $next($request);
    }
}
