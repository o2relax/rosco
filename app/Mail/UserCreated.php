<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $password;

    /**
     * UserCreated constructor.
     * @param User $user
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(cfg('admin_email'), cfg('main_title', 'langs'))
            ->subject(trans('mails.register.subject'))
            ->view('emails.user.register', [
                'user' => $this->user,
                'password', $this->password
            ]);
    }
}
