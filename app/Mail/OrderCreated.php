<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;

    /**
     * OrderCreated constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(cfg('admin_email'), cfg('main_title', 'langs'))
            ->subject(trans('mails.order.new.subject', ['order_id' => $this->order->id]))
            ->view('emails.user.orders.created', [
                'order' => $this->order
            ]);
    }
}
