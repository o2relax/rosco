<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordChanged extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $password;

    /**
     * PasswordChanged constructor.
     * @param User $user
     * @param $password
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(cfg('admin_email'), cfg('main_title', 'langs'))
            ->subject(trans('mails.password_change.subject'))
            ->view('emails.user.password_change', [
                'user' => $this->user,
                'password', $this->password
            ]);
    }
}
