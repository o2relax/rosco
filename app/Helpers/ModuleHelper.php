<?php

namespace App\Helpers;


use App\Gateways\Actions\Modules\GetModuleAction;
use App\Services\ModuleService;
use App\Traits\CacheableTrait;
use App\Traits\CallableTrait;

class ModuleHelper
{

    use CallableTrait, CacheableTrait;

    public function __construct()
    {

    }

    /**
     * @param $code
     * @return string
     * @throws \Throwable
     */

    public function show($code)
    {

        if ($this->hasCache('module_' . $code)) {
            return $this->getCache('module_' . $code);
        }

        $class = '\App\Services\Modules\\' . studly_case($code) . 'Service';

        if (class_exists($class)) {
            $data = app($class, ['code' => $code])->run();
        } else {
            $data = app(ModuleService::class, ['code' => $code])->run();
        }

        return $this->response($code, $data);

    }

    /**
     * @param $code
     * @param $data
     * @return \Illuminate\Contracts\Cache\Repository|string
     * @throws \Exception
     * @throws \Throwable
     */

    public function response($code, $data)
    {

        if (!$data['is_active'] || !view()->exists('shop.modules.' . $code)) {
            return null;
        }

        $render = view('shop.modules.' . $code, $data)->render();

        if ($data['use_cache']) {
            $this->putCache(
                'module_' . $code,
                $render,
                isset($data['config']['cache']) ? (int)$data['config']['cache'] : config('cache.cache_module', 60)
            );
        }

        return $render;
    }

    /**
     * @param $code
     * @return bool
     * @throws \Exception
     */

    public function clearCache($code)
    {
        return $this->forgetCache('module_' . $code);
    }

    /**
     * @return mixed
     * @throws \Exception
     */

    public function clearAllCache()
    {
        return cache()->tags('module')->flush();
    }

}