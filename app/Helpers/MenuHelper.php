<?php

namespace App\Helpers;

use App\Gateways\Tasks\Menus\GetMenuByCodeTask;
use App\Gateways\Transformers\Menus\MenuTransformer;
use App\Traits\CacheableTrait;
use App\Traits\CallableTrait;
use App\Traits\ResponseTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Filesystem\Cache;

class MenuHelper
{

    use CallableTrait, ResponseTrait, CacheableTrait;

    /**
     * @param $code
     * @return string
     * @throws \Throwable
     */

    public function show($code)
    {

        if ($this->hasCache('menu_' . $code)) {

            $render = $this->getCache('menu_' . $code);

        } else {

            $menu = $this->call(GetMenuByCodeTask::class, [$code]);

            $render = view(
                file_exists(resource_path('views/partials/menus/' . $code . '.blade.php')) ? 'partials.menus.' . $code : 'partials.menus.default',
                [
                    'menu' => $this->transform($menu, MenuTransformer::class)
                ]
            )->render();

            $this->putCache('menu_' . $code, $render, config('cache.cache_menu'), ['menu']);

        }

        return $render;

    }

}