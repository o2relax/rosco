<?php

namespace App\Helpers;

use App\Gateways\Actions\Settings\GetSettingsAction;

class SettingHelper
{

    protected $settings = false;
    protected $sources = false;

    /**
     * @throws \Exception
     */

    public function init()
    {

        if (!$this->settings) {

            if (cache()->has('settings')) {
                $this->settings = cache()->get('settings');
            } else {
                $this->settings = collect(app(GetSettingsAction::class)->run('list'));
                foreach($this->settings as $setting) {
                    $this->sources = new \stdClass();
                    if($setting->relation) {
                        $this->sources = $setting->source();
                        $setting->rel = $this->sources;
                    }
                }
                $this->cache();
            }

        }

    }

    /**
     * @throws \Exception
     */
    public function cache() {
        return cache()->put('settings', $this->settings, config('cache.cache_settings', 60));
    }

    /**
     * @param $code
     * @param null $mod
     * @return null
     * @throws \Exception
     */

    public function grab($code, $mod = null)
    {

        $this->init();

        if($setting = $this->settings->where('code', $code)->first()) {
            if($mod == 'langs') {
                return lang(json_decode($setting->value, true));
            } elseif ($mod) {
                return isset($setting->rel->$mod) ? $setting->rel->$mod: null;
            } else {
                return $setting->value;
            }
        } else {
            return null;
        }




    }


}