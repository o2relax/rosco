<?php

namespace App\Helpers;

use App\Gateways\Actions\Currencies\GetCurrenciesAction;
use App\Gateways\Actions\Users\UpdateUserAction;
use App\Gateways\Tasks\Currencies\GetCurrencyByCodeTask;
use App\Gateways\Tasks\Currencies\GetCurrencyByIdTask;
use App\Models\Discount;
use App\Traits\CallableTrait;
use Illuminate\Support\Facades\Cookie;

class CurrencyHelper
{

    use CallableTrait;

    protected $currency_catalog = false;
    protected $currency_shop = false;
    protected $current_currency = false;
    protected $price;
    protected $currencies = null;

    /**
     * @param $price
     * @param null $currency
     * @return mixed
     * @throws \Exception
     */

    public function convert($price, $currency = null)
    {

        $this->calculate($price, $currency);

        return $this->price;

    }

    /**
     * @param $price
     * @param int $quantity
     * @return string
     * @throws \Exception
     */

    public function price($price, $quantity = 1)
    {

        return $this->format($price, null, $quantity);

    }

    /**
     * @param $price
     * @param null $currency
     * @param int $quantity
     * @return string
     * @throws \Exception
     */
    public function format($price, $currency = null, $quantity = 1)
    {

        $this->calculate($price, $currency);

        return $this->currency_shop->prefix . number_format($this->price * $quantity, (int)$this->currency_shop->round, '.', ' ') . $this->currency_shop->postfix;

    }

    /**
     * @param $price
     * @param $currency
     * @return string
     */
    public function order($price, $currency)
    {

        return $currency->prefix . number_format($price, (int)$currency->round, '.', ' ') . $currency->postfix;

    }

    /**
     * @param $price
     * @param null $currency
     * @throws \Exception
     */
    public function calculate($price, $currency = null)
    {

        if ($currency) {
            $this->current_currency = $currency;
        } elseif (!$this->current_currency) {
            $this->current_currency = $this->get(); // iso code
        }

//        if (!$this->currency_catalog) {
//            $this->currency_catalog = app(GetCurrencyByIdTask::class)->run(cfg('currency_catalog'));
//        }

        if (!$this->currency_shop || $this->currency_shop->code != $this->current_currency) {
            if (is_numeric($this->current_currency)) {
                $this->currency_shop = app(GetCurrencyByIdTask::class)->run($this->current_currency);
            } else {
                $this->currency_shop = app(GetCurrencyByCodeTask::class)->run($this->current_currency);
            }
        }

        $this->price = round($price * $this->currency_shop->rate, $this->currency_shop->round);

    }

    /**
     * @param $price
     * @param Discount $discount
     * @return float
     * @throws \Exception
     */
    public function discount($price, Discount $discount)
    {

        if (!$this->current_currency) {
            $this->current_currency = $this->get(); // iso code
        }

//        if (!$this->currency_catalog) {
//            $this->currency_catalog = app(GetCurrencyByIdTask::class)->run(cfg('currency_catalog'));
//        }

        if (!$this->currency_shop || $this->currency_shop->code != $this->current_currency) {
            if (is_numeric($this->current_currency)) {
                $this->currency_shop = app(GetCurrencyByIdTask::class)->run($this->current_currency);
            } else {
                $this->currency_shop = app(GetCurrencyByCodeTask::class)->run($this->current_currency);
            }
        }

        if ($discount->type == '%') {
            $new_value = $price - ($price / 100 * $discount->value);
        } else {
            $new_value = $price - $discount->value;
        }

        return $new_value;

    }

    /**
     * @param $price
     * @return float
     * @throws \Exception
     */
    public function toDefault($price)
    {

        if (!$this->current_currency) {
            $this->current_currency = $this->get(); // iso code
        }

        if (!$this->currency_shop || $this->currency_shop->code != $this->current_currency) {
            $this->currency_shop = app(GetCurrencyByCodeTask::class)->run($this->current_currency);
        }

        return round($price / $this->currency_shop->rate, $this->currency_shop->round);

    }

    /**
     * @param $code
     */
    public function set($code)
    {

        $cookie = cookie('currency', $code, 60 * 24 * 31);
        session()->put('currency', $code);

        Cookie::queue($cookie);

        if (auth()->check()) {
            $this->call(UpdateUserAction::class, [
                auth()->user()->id,
                [
                    'meta' => ['currency' => $code]
                ]
            ]);
        }

    }

    /**
     * @param string $mod
     * @return array|mixed|string
     * @throws \Exception
     */
    public function get($mod = 'code')
    {

        if (auth()->check() && $currency = auth()->user()->meta->currency) {
            $code = $currency;
        } elseif (request()->cookie('currency')) {
            $code = request()->cookie('currency');
        } else {
            $code = session()->get('currency', cfg('currency_shop', 'code'));
        }

        $info = current(collect($this->list())->where('code', $code)->all());

        if (!$info) {
            $code = cfg('currency_shop', 'code');
            $this->set($code);
            $code = cfg('currency_shop', $mod);
        } else {
            $code = isset($info->$mod) ? $info->$mod : null;
        }

        return $code;
    }

    /**
     * @return \Illuminate\Contracts\Cache\Repository|null
     * @throws \Exception
     */
    public function list()
    {
        if (!cache()->has('currencies')) {
            $this->currencies = app(GetCurrenciesAction::class)->run();
            cache()->put('currencies', $this->currencies, 60 * 24);
        } else {
            $this->currencies = cache()->get('currencies');
        }

        return $this->currencies;
    }


}