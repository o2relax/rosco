<?php

namespace App\Helpers;

use App\Gateways\Actions\Languages\GetLanguagesAction;
use App\Gateways\Actions\Users\UpdateUserAction;
use App\Gateways\Transformers\Languages\LanguageTransformer;
use App\Traits\CallableTrait;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Cookie;

class LocaleHelper
{

    use CallableTrait, ResponseTrait;

    protected $languages = null;

    /**
     * @param $code
     */

    public function set($code)
    {

        $cookie = cookie('language', $code, 60 * 24 * 31);
        session()->put('language', $code);

        Cookie::queue($cookie);

        if (auth()->check()) {
            $this->call(UpdateUserAction::class, [
                auth()->user()->id,
                [
                    'meta' => ['language' => $code]
                ]
            ]);
        }

    }

    /**
     * @return array|mixed|string
     * @throws \Exception
     */
    public function get()
    {
        if (auth()->check() && $language = auth()->user()->meta->language) {
            $code = $language;
        } elseif (request()->cookie('language')) {
            $code = request()->cookie('language');
        } else {
            $code = session()->get('language', cfg('language', 'code'));
        }

        $info = collect($this->list())->where('code', $code)->all();

        if (!count($info)) {
            $code = cfg('language', 'code');
            $this->set($code);
        }

        return $code;
    }

    /**
     * @return \Illuminate\Contracts\Cache\Repository
     * @throws \Exception
     */

    public function list()
    {
        if (!cache()->has('languages')) {
            $this->languages = $this->transform(
                app(GetLanguagesAction::class)->run(),
                LanguageTransformer::class
            );
            cache()->put('languages', $this->languages, 60 * 24);
        } else {
            $this->languages = cache()->get('languages');
        }

        return $this->languages;
    }


}