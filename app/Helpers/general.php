<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use App\Helpers\SettingHelper;
use App\Helpers\CurrencyHelper;
use App\Helpers\LocaleHelper;
use App\Services\CartSessionService;
use App\Services\CartDatabaseService;
use App\Helpers\ModuleHelper;
use App\Contracts\Svg\SvgFactory;

if (!function_exists('is_route')) {
    function is_route($name)
    {
        return Route::get($name);
    }
}
if (!function_exists('is_active_route')) {
    function is_active_route($name)
    {
        return request()->is($name) || request()->is($name . '/*');
    }
}

if (!function_exists('product_image')) {
    function product_image($id, $image, $type = 'url')
    {
        return Storage::disk('products')->$type($id . DIRECTORY_SEPARATOR . $image);
    }
}

if (!function_exists('module_file')) {
    function module_file($code, $image, $type = 'url')
    {
        return Storage::disk('modules')->$type($code . DIRECTORY_SEPARATOR . $image);
    }
}

if (!function_exists('category_image')) {
    function category_image($id, $image, $type = 'path')
    {
        $file = 'categories' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $image;
        return file_exists(storage_path('app/public' . DIRECTORY_SEPARATOR . $file)) ? svg_image(str_replace('.svg', '', $file)) : null;
    }
}

if (!function_exists('blog_image')) {
    function blog_image($id, $image, $type = 'url')
    {
        return Storage::disk('blog')->$type($id . DIRECTORY_SEPARATOR . $image);
    }
}

if (!function_exists('location_hash')) {
    function location_hash($hash = null)
    {
        $string = isset(explode('#', request()->fullUrl())[1]) ? explode('#', request()->fullUrl())[1] : false;
        if ($hash) {
            return $string == $hash ? true : false;
        } else {
            return $string;
        }
    }
}

if (!function_exists('old_input')) {
    function old_input($name, $object, $default = null)
    {
        return old($name, isset($object->$name) ? $object->$name : ($default ? $default : null));
    }
}

if (!function_exists('old_input_array')) {
    function old_input_array($key, $array, $default = null)
    {
        return old($key, isset($array[$key]) ? $array[$key] : ($default ? $default : null));
    }
}

if (!function_exists('no_image')) {
    function no_image($type = 'image')
    {
        if ($type == 'svg') {
            return file_exists(storage_path('app/public/no_photo.svg')) ? svg_image('no_photo') : null;
        } else {
            return Storage::disk('public')->url('no_photo.png');
        }

    }
}

if (!function_exists('storage')) {
    function storage($file, $disk = 'public', $type = 'url')
    {
        return Storage::disk($disk)->$type($file);
    }
}

if (!function_exists('trans_exist')) {
    function trans_exist($essence, $property, $code)
    {
        return $essence && $essence->translate($code) ? $essence->translate($code)->$property : null;
    }
}

if (!function_exists('cfg')) {
    function cfg($code, $mod = null)
    {
        return app(SettingHelper::class)->grab($code, $mod);
    }
}

if (!function_exists('currency')) {
    function currency()
    {
        return app(CurrencyHelper::class);
    }
}

if (!function_exists('locale')) {
    function locale()
    {
        return app(LocaleHelper::class);
    }
}

if (!function_exists('cart')) {
    /**
     * @return CartSessionService
     */
    function cart()
    {
        if (auth()->check()) {
            return app(CartSessionService::class);
        } else {
            return app(CartSessionService::class);  // TODO: make database variant
        }

    }
}

if (!function_exists('module')) {
    function module()
    {
        return app(ModuleHelper::class);
    }
}

if (!function_exists('lang')) {
    function lang($vars, $lang = null)
    {

        if ($lang) {
            $current = $lang;
        } else {
            $current = app()->getLocale();
        }
        $default = cfg('language', 'code');
        if (isset($vars[$current])) {
            return $vars[$current];
        } elseif (isset($vars[$default])) {
            return $vars[$default];
        } else {
            return null;
        }
    }
}

if (!function_exists('svg_spritesheet')) {
    function svg_spritesheet()
    {
        return app(SvgFactory::class)->spritesheet();
    }
}

if (!function_exists('svg_image')) {
    function svg_image($icon, $class = '', $attrs = [])
    {
        return app(SvgFactory::class)->svg($icon, $class, $attrs);
    }
}

if (!function_exists('svg_icon')) {
    function svg_icon($icon, $class = '', $attrs = [])
    {
        return app(SvgFactory::class)->svg($icon, $class, $attrs);
    }
}
