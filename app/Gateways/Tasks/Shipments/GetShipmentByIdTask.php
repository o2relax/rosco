<?php

namespace App\Gateways\Tasks\Shipments;

use App\Contracts\Tasks\Task;
use App\Repositories\ShipmentRepository;

/**
 * Class GetShipmentByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetShipmentByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(ShipmentRepository::class)->find($id);
    }

}
