<?php

namespace App\Gateways\Tasks\Shipments\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class DeleteCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteShipmentImageTask extends Task
{

    /**
     * @param $image
     * @return mixed
     */
    public function run($image)
    {
        return Storage::disk('shipments')->delete($image);
    }

}
