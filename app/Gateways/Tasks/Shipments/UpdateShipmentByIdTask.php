<?php

namespace App\Gateways\Tasks\Shipments;

use App\Contracts\Tasks\Task;
use App\Repositories\ShipmentRepository;

/**
 * Class UpdateShipmentByIdTask
 * @package App\Gateways\Tasks\Shipments
 */
class UpdateShipmentByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(ShipmentRepository::class)->update($id, $data);
    }

}
