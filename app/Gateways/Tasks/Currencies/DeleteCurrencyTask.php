<?php

namespace App\Gateways\Tasks\Currencies;

use App\Contracts\Tasks\Task;
use App\Repositories\CurrencyRepository;

/**
 * Class DeleteCurrencyTask
 */
class DeleteCurrencyTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(CurrencyRepository::class)->delete($id);
    }

}
