<?php

namespace App\Gateways\Tasks\Currencies;

use App\Contracts\Tasks\Task;
use App\Repositories\CurrencyRepository;

/**
 * Class UpdateCurrencyTask
 */
class UpdateCurrencyTask extends Task
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(CurrencyRepository::class)->update($id, $data);
    }

}
