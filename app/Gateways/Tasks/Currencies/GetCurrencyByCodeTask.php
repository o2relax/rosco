<?php

namespace App\Gateways\Tasks\Currencies;

use App\Contracts\Tasks\Task;
use App\Repositories\CurrencyRepository;

/**
 * Class GetCurrencyByIdTask
 */
class GetCurrencyByCodeTask extends Task
{

    /**
     * @param $code
     * @return mixed
     */
    public function run($code)
    {
        return app(CurrencyRepository::class)->getByCode($code);
    }

}
