<?php

namespace App\Gateways\Tasks\Blogs;

use App\Contracts\Tasks\Task;
use App\Repositories\BlogRepository;

/**
 * Class UpdateBlogByIdTask
 * @package App\Gateways\Tasks\Blogs
 */
class UpdateBlogByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(BlogRepository::class)->update($id, $data);
    }

}
