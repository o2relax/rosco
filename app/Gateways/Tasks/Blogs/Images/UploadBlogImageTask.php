<?php

namespace App\Gateways\Tasks\Blogs\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class UploadBlogImageTask extends Task
{

    /**
     * @param $id
     * @param $file
     * @param null $filename
     * @return string
     */
    public function run($id, $file, $filename = null)
    {

        $file_info = pathinfo($file->getClientOriginalName());
        if($filename) {
            $file_name = str_slug($filename) . '.' . $file_info['extension'];
        } else {
            $file_name = md5($file_info['filename'] . uniqid()) . '.' . $file_info['extension'];
        }
        Storage::disk('blog')->putFileAs($id, $file, $file_name);

        if($filename) {
            return $file_name;
        } else {
            return DIRECTORY_SEPARATOR
                . 'storage' . DIRECTORY_SEPARATOR
                . 'blog' . DIRECTORY_SEPARATOR
                . $id . DIRECTORY_SEPARATOR
                . $file_name;
        }
    }

}
