<?php

namespace App\Gateways\Tasks\Blogs\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteBlogImagesTask extends Task
{

    /**
     * @param $blog_id
     * @return mixed
     */
    public function run($blog_id)
    {
        return Storage::disk('blog')->deleteDirectory($blog_id);
    }

}
