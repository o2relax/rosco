<?php

namespace App\Gateways\Tasks\Blogs\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteBlogImageTask extends Task
{

    /**
     * @param $blog_id
     * @param $image
     * @return mixed
     */
    public function run($blog_id, $image)
    {
        return Storage::disk('blog')->delete($blog_id . DIRECTORY_SEPARATOR . $image);
    }

}
