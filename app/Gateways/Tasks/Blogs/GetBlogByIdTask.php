<?php

namespace App\Gateways\Tasks\Blogs;

use App\Contracts\Tasks\Task;
use App\Repositories\BlogRepository;

/**
 * Class GetBlogByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetBlogByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(BlogRepository::class)->find($id);
    }

}
