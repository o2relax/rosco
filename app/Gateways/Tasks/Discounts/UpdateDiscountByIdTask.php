<?php

namespace App\Gateways\Tasks\Discounts;

use App\Contracts\Tasks\Task;
use App\Repositories\DiscountRepository;

/**
 * Class UpdatePaymentByIdTask
 * @package App\Gateways\Tasks\Payments
 */
class UpdateDiscountByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(DiscountRepository::class)->update($id, $data);
    }

}
