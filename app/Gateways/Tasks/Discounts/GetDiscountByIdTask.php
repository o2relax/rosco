<?php

namespace App\Gateways\Tasks\Discounts;

use App\Contracts\Tasks\Task;
use App\Repositories\DiscountRepository;

/**
 * Class GetPaymentByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetDiscountByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(DiscountRepository::class)->find($id);
    }

}
