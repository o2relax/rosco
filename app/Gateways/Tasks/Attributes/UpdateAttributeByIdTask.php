<?php

namespace App\Gateways\Tasks\Attributes;

use App\Contracts\Tasks\Task;
use App\Repositories\AttributeRepository;

/**
 * Class UpdateAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateAttributeByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(AttributeRepository::class)->update($id, $data);
    }

}
