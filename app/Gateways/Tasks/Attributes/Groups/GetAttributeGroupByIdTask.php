<?php

namespace App\Gateways\Tasks\Attributes\Groups;

use App\Contracts\Tasks\Task;
use App\Repositories\AttributeGroupRepository;

/**
 * Class GetAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetAttributeGroupByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(AttributeGroupRepository::class)->find($id);
    }

}
