<?php

namespace App\Gateways\Tasks\Attributes\Groups;

use App\Contracts\Tasks\Task;
use App\Repositories\AttributeGroupRepository;

/**
 * Class UpdateAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateAttributeGroupByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(AttributeGroupRepository::class)->update($id, $data);
    }

}
