<?php

namespace App\Gateways\Tasks\Attributes;

use App\Contracts\Tasks\Task;
use App\Repositories\AttributeRepository;

/**
 * Class GetAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetAttributeByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(AttributeRepository::class)->find($id);
    }

}
