<?php

namespace App\Gateways\Tasks\Payments\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class DeleteCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeletePaymentImageTask extends Task
{

    /**
     * @param $image
     * @return mixed
     */
    public function run($image)
    {
        return Storage::disk('payments')->delete($image);
    }

}
