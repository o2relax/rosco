<?php

namespace App\Gateways\Tasks\Payments\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class UploadPaymentImageTask extends Task
{

    /**
     * @param $file
     * @param $filename
     * @return mixed
     */
    public function run($file, $filename)
    {

        $file_info = pathinfo($file->getClientOriginalName());
        $file_name = str_slug($filename) . '.' . $file_info['extension'];
        Storage::disk('public')->putFileAs('payments', $file, $file_name);

        return $file_name;
    }

}
