<?php

namespace App\Gateways\Tasks\Payments;

use App\Contracts\Tasks\Task;
use App\Repositories\PaymentRepository;

/**
 * Class UpdatePaymentByIdTask
 * @package App\Gateways\Tasks\Payments
 */
class UpdatePaymentByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(PaymentRepository::class)->update($id, $data);
    }

}
