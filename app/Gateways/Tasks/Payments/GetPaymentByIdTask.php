<?php

namespace App\Gateways\Tasks\Payments;

use App\Contracts\Tasks\Task;
use App\Repositories\PaymentRepository;

/**
 * Class GetPaymentByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetPaymentByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(PaymentRepository::class)->find($id);
    }

}
