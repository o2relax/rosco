<?php

namespace App\Gateways\Tasks\Users;

use App\Contracts\Tasks\Task;
use App\Repositories\UserRepository;

/**
 * Class CreateUserTask
 * @package App\Gateways\Tasks\Users
 */
class CreateUserMetaTask extends Task
{

    /**
     * @param $user_id
     * @param $password
     * @param $roles
     * @param $sendMail
     * @return mixed
     */
    public function run($user_id, $password, $roles, $sendMail = false)
    {
        return app(UserRepository::class)->createMeta($user_id, $password, $roles, $sendMail);
    }

}
