<?php

namespace App\Gateways\Tasks\Users;

use App\Contracts\Tasks\Task;
use App\Repositories\UserRepository;

/**
 * Class UpdateUserByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateUserByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(UserRepository::class)->update($id, $data);
    }

}
