<?php

namespace App\Gateways\Tasks\Users;

use App\Contracts\Tasks\Task;
use App\Repositories\UserRepository;

/**
 * Class GetUserByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetUserByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(UserRepository::class)->find($id);
    }

}
