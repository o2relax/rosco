<?php

namespace App\Gateways\Tasks\Users;

use App\Contracts\Tasks\Task;
use App\Repositories\UserRepository;

/**
 * Class CreateUserTask
 * @package App\Gateways\Tasks\Users
 */
class CreateUserTask extends Task
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return app(UserRepository::class)->invite($data);
    }

}
