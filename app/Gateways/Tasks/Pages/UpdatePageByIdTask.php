<?php

namespace App\Gateways\Tasks\Pages;

use App\Contracts\Tasks\Task;
use App\Repositories\PageRepository;

/**
 * Class UpdatePageByIdTask
 * @package App\Gateways\Tasks\Pages
 */
class UpdatePageByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(PageRepository::class)->update($id, $data);
    }

}
