<?php

namespace App\Gateways\Tasks\Pages\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class UploadPageImageTask extends Task
{

    /**
     * @param $id
     * @param $file
     * @return string
     */
    public function run($id, $file)
    {

        $file_info = pathinfo($file->getClientOriginalName());
        $file_name = md5($file_info['filename'] . uniqid()) . '.' . $file_info['extension'];
        Storage::disk('pages')->putFileAs($id, $file, $file_name);

        return DIRECTORY_SEPARATOR
            . 'storage' . DIRECTORY_SEPARATOR
            . 'pages' . DIRECTORY_SEPARATOR
            . $id . DIRECTORY_SEPARATOR
            . $file_name;
    }

}
