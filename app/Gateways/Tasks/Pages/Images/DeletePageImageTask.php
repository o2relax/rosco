<?php

namespace App\Gateways\Tasks\Pages\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeletePageImageTask extends Task
{

    /**
     * @param $page_id
     * @return mixed
     */
    public function run($page_id)
    {
        return Storage::disk('pages')->deleteDirectory($page_id);
    }

}
