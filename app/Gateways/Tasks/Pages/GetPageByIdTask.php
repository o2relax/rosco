<?php

namespace App\Gateways\Tasks\Pages;

use App\Contracts\Tasks\Task;
use App\Repositories\PageRepository;

/**
 * Class GetPageByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetPageByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(PageRepository::class)->find($id);
    }

}
