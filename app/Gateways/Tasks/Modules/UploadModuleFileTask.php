<?php

namespace App\Gateways\Tasks\Modules;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class UploadModuleFileTask extends Task
{

    /**
     * @param $code
     * @param $file
     * @param $id
     * @return string
     */
    public function run($code, $file, $id)
    {

        $file_info = pathinfo($file->getClientOriginalName());
        $file_name = str_slug($id) . '.' . $file_info['extension'];
        Storage::disk('modules')->putFileAs($code, $file, $file_name);

        return $file_name;
    }

}
