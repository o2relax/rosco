<?php

namespace App\Gateways\Tasks\Modules;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class DeleteModuleFileTask
 * @package App\Gateways\Tasks\Products\Images
 */
class DeleteModuleFileTask extends Task
{

    /**
     * @param $code
     * @param $name
     * @return mixed
     */
    public function run($code, $name)
    {
        return Storage::disk('modules')->delete($code . DIRECTORY_SEPARATOR . $name);
    }

}
