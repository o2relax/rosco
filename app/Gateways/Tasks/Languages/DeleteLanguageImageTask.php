<?php

namespace App\Gateways\Tasks\Languages;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class DeleteProductImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteLanguageImageTask extends Task
{

    /**
     * @param $image
     * @return mixed
     */
    public function run($image)
    {
        return Storage::disk('languages')->delete($image);
    }

}
