<?php

namespace App\Gateways\Tasks\Languages;

use App\Contracts\Tasks\Task;
use App\Repositories\LanguageRepository;

/**
 * Class UpdateLanguageTask
 * @package App\Gateways\Tasks\Products
 */
class UpdateLanguageTask extends Task
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(LanguageRepository::class)->update($id, $data);
    }

}
