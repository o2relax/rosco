<?php

namespace App\Gateways\Tasks\Languages;

use App\Contracts\Tasks\Task;
use App\Repositories\LanguageRepository;
use App\Repositories\ProductRepository;

/**
 * Class DeleteLanguageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteLanguageTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(LanguageRepository::class)->delete($id);
    }

}
