<?php

namespace App\Gateways\Tasks\Languages;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadLanguageImageTask
 * @package App\Gateways\Tasks\Products
 */
class UploadLanguageImageTask extends Task
{

    /**
     * @param $filename
     * @param $file
     * @return mixed
     */
    public function run($file, $filename)
    {

        $file_info = pathinfo($file->getClientOriginalName());
        $file_name = $filename . '.' . $file_info['extension'];
        Storage::disk('public')->putFileAs('languages', $file, $file_name);
        return $file_name;
    }

}
