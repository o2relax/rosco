<?php

namespace App\Gateways\Tasks\Languages;

use App\Contracts\Tasks\Task;
use App\Repositories\LanguageRepository;

/**
 * Class GetLanguageByIdTask
 * @package App\Gateways\Tasks\Products
 */
class GetLanguageByIdTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(LanguageRepository::class)->find($id);
    }

}
