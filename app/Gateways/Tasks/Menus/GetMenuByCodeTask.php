<?php

namespace App\Gateways\Tasks\Menus;

use App\Contracts\Tasks\Task;
use App\Repositories\MenuRepository;

/**
 * Class GetMenuByCodeTask
 * @package App\Gateways\Tasks\Users
 */
class GetMenuByCodeTask extends Task
{

    /**
     * @param string $code
     * @return mixed
     */
    public function run($code)
    {
        return app(MenuRepository::class)->getByCode($code);
    }

}
