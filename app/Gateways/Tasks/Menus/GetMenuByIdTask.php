<?php

namespace App\Gateways\Tasks\Menus;

use App\Contracts\Tasks\Task;
use App\Repositories\MenuRepository;

/**
 * Class GetAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetMenuByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(MenuRepository::class)->find($id);
    }

}
