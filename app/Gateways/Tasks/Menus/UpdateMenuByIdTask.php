<?php

namespace App\Gateways\Tasks\Menus;

use App\Contracts\Tasks\Task;
use App\Repositories\AttributeRepository;
use App\Repositories\MenuRepository;

/**
 * Class UpdateAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateMenuByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(MenuRepository::class)->update($id, $data);
    }

}
