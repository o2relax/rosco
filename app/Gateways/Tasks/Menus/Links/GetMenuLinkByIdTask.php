<?php

namespace App\Gateways\Tasks\Menus\Links;

use App\Contracts\Tasks\Task;
use App\Repositories\MenuLinkRepository;

/**
 * Class GetAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetMenuLinkByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(MenuLinkRepository::class)->find($id);
    }

}
