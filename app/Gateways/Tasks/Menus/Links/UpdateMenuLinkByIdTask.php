<?php

namespace App\Gateways\Tasks\Menus\Links;

use App\Contracts\Tasks\Task;
use App\Repositories\AttributeGroupRepository;
use App\Repositories\MenuLinkRepository;

/**
 * Class UpdateAttributeGroupByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateMenuLinkByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(MenuLinkRepository::class)->update($id, $data);
    }

}
