<?php

namespace App\Gateways\Tasks\Categories;

use App\Contracts\Tasks\Task;
use App\Repositories\CategoryRepository;

/**
 * Class GetRoleByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetCategoryBySlugTask extends Task
{

    /**
     * @param string $slug
     * @return mixed
     */
    public function run($slug)
    {
        return app(CategoryRepository::class)->getBySlug($slug);
    }

}
