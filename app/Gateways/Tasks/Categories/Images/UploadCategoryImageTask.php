<?php

namespace App\Gateways\Tasks\Categories\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class UploadCategoryImageTask extends Task
{

    /**
     * @param $id
     * @param $file
     * @return mixed
     */
    public function run($id, $file)
    {

        $file_info = pathinfo($file->getClientOriginalName());
        $file_name = str_slug($file_info['filename']) . '.' . $file_info['extension'];
        Storage::disk('categories')->putFileAs($id, $file, $file_name);

        return $file_name;
    }

}
