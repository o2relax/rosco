<?php

namespace App\Gateways\Tasks\Categories\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class DeleteCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteCategoryImageTask extends Task
{

    /**
     * @param $category_id
     * @param $image
     * @param bool $folder
     * @return mixed
     */
    public function run($category_id, $image, $folder = false)
    {
        if($folder) {
            return Storage::disk('categories')->deleteDirectory($category_id);
        } else {
            return Storage::disk('categories')->delete($category_id . DIRECTORY_SEPARATOR . $image);
        }

    }

}
