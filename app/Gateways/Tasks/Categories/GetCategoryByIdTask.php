<?php

namespace App\Gateways\Tasks\Categories;

use App\Contracts\Tasks\Task;
use App\Repositories\CategoryRepository;

/**
 * Class GetRoleByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetCategoryByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(CategoryRepository::class)->find($id);
    }

}
