<?php

namespace App\Gateways\Tasks\Categories;

use App\Contracts\Tasks\Task;
use App\Repositories\CategoryRepository;

/**
 * Class UpdateCategoryByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateCategoryByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(CategoryRepository::class)->update($id, $data);
    }

}
