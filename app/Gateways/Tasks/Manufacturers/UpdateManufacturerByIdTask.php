<?php

namespace App\Gateways\Tasks\Manufacturers;

use App\Contracts\Tasks\Task;
use App\Repositories\CategoryRepository;
use App\Repositories\ManufacturerRepository;

/**
 * Class UpdateCategoryByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateManufacturerByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(ManufacturerRepository::class)->update($id, $data);
    }

}
