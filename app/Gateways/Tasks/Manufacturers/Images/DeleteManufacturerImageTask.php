<?php

namespace App\Gateways\Tasks\Manufacturers\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class DeleteCategoryImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteManufacturerImageTask extends Task
{

    /**
     * @param $image
     * @return mixed
     */
    public function run($image)
    {
        return Storage::disk('manufacturers')->delete($image);
    }

}
