<?php

namespace App\Gateways\Tasks\Manufacturers;

use App\Contracts\Tasks\Task;
use App\Repositories\ManufacturerRepository;

/**
 * Class GetManufacturerByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetManufacturerByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(ManufacturerRepository::class)->find($id);
    }

}
