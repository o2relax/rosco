<?php

namespace App\Gateways\Tasks\Products;

use App\Contracts\Tasks\Task;
use App\Repositories\ProductRepository;

/**
 * Class GetStatusesTask
 * @package App\Gateways\Tasks\Products
 */
class CheckDuplicateAttributeTask extends Task
{

    /**
     * @param $product_id
     * @param $attribute_id
     * @return mixed
     */
    public function run($product_id, $attribute_id)
    {
        return app(ProductRepository::class)
            ->where('id', $product_id)
            ->whereDoesntHave('attributes', function($q) use ($attribute_id) {
                $q->where('attribute_id', $attribute_id);
            })
            ->first();
    }

}
