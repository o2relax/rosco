<?php

namespace App\Gateways\Tasks\Products\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadProductImagesTask
 * @package App\Gateways\Tasks\Products
 */
class UploadProductImageTask extends Task
{

    /**
     * @param $id
     * @param $file
     * @return mixed
     */
    public function run($id, $file)
    {

        $file_info = pathinfo($file->getClientOriginalName());
        $file_name = md5(time() . uniqid()) . '.' . $file_info['extension'];
        Storage::disk('products')->putFileAs($id, $file, $file_name);

        return $file_name;
    }

}
