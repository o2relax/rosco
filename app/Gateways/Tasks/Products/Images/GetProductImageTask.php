<?php

namespace App\Gateways\Tasks\Products\Images;

use App\Contracts\Tasks\Task;
use App\Repositories\ProductImageRepository;

/**
 * Class GetProductImageTask
 * @package App\Gateways\Tasks\Products
 */
class GetProductImageTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(ProductImageRepository::class)->getById($id);
    }

}
