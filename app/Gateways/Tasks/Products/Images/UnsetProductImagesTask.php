<?php

namespace App\Gateways\Tasks\Products\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class UnsetProductImageTask
 * @package App\Gateways\Tasks\Products
 */
class UnsetProductImagesTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(ProductImageRepository::class)->deleteByProduct($id);
    }

}
