<?php

namespace App\Gateways\Tasks\Products\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class DeleteProductImageTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteProductImageTask extends Task
{

    /**
     * @param $product_id
     * @param $image
     * @param bool $folder
     * @return mixed
     */
    public function run($product_id, $image, $folder = false)
    {
        if ($folder) {
            return Storage::disk('products')->deleteDirectory($product_id);
        } else {
            return Storage::disk('products')->delete($product_id . DIRECTORY_SEPARATOR . $image);
        }

    }

}
