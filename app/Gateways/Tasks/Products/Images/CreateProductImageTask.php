<?php

namespace App\Gateways\Tasks\Products\Images;

use App\Contracts\Tasks\Task;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductImageRepository;

/**
 * Class CreateProductImageTask
 * @package App\Gateways\Tasks\Products
 */
class CreateProductImageTask extends Task
{

    /**
     * @param $id
     * @param $file_name
     * @return mixed
     */
    public function run($id, $file_name)
    {
        return app(ProductImageRepository::class)->create(['product_id' => $id, 'image' => $file_name]);
    }

}
