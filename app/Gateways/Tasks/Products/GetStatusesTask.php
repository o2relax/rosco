<?php

namespace App\Gateways\Tasks\Products;

use App\Contracts\Tasks\Task;
use App\Repositories\StatusRepository;

/**
 * Class GetStatusesTask
 * @package App\Gateways\Tasks\Products
 */
class GetStatusesTask extends Task
{

    /**
     * @param $type
     * @return mixed
     */
    public function run($type = 'All')
    {
        return app(StatusRepository::class)->getList($type);
    }

}
