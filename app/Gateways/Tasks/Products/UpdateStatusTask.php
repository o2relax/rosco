<?php

namespace App\Gateways\Tasks\Products;

use App\Contracts\Tasks\Task;
use App\Repositories\ProductRepository;

/**
 * Class GetStatusesTask
 * @package App\Gateways\Tasks\Products
 */
class UpdateStatusTask extends Task
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(ProductRepository::class)->update($id, $data);
    }

}
