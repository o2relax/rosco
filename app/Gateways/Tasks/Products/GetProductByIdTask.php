<?php

namespace App\Gateways\Tasks\Products;

use App\Contracts\Tasks\Task;
use App\Repositories\ProductRepository;

/**
 * Class GetProductByIdTask
 * @package App\Gateways\Tasks\Products
 */
class GetProductByIdTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(ProductRepository::class)->find($id);
    }

}
