<?php

namespace App\Gateways\Tasks\Products;

use App\Contracts\Tasks\Task;
use App\Repositories\ProductRepository;

/**
 * Class DeleteProductTask
 * @package App\Gateways\Tasks\Products
 */
class DeleteProductTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(ProductRepository::class)->delete($id);
    }

}
