<?php

namespace App\Gateways\Tasks\Products;

use App\Contracts\Tasks\Task;
use App\Repositories\StatusRepository;

/**
 * Class GetStatusByIdTask
 * @package App\Gateways\Tasks\Products
 */
class GetStatusByIdTask extends Task
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(StatusRepository::class)->find($id);
    }

}
