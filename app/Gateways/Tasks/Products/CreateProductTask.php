<?php

namespace App\Gateways\Tasks\Products;

use App\Contracts\Tasks\Task;
use App\Repositories\ProductRepository;

/**
 * Class CreateProductTask
 * @package App\Gateways\Tasks\Products
 */
class CreateProductTask extends Task
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {

        return app(ProductRepository::class)->create($data);
    }

}
