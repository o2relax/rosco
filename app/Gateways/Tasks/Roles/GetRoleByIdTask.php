<?php

namespace App\Gateways\Tasks\Roles;

use App\Contracts\Tasks\Task;
use App\Repositories\RoleRepository;

/**
 * Class GetRoleByIdTask
 * @package App\Gateways\Tasks\Users
 */
class GetRoleByIdTask extends Task
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {
        return app(RoleRepository::class)->find($id);
    }

}
