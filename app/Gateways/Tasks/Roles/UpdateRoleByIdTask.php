<?php

namespace App\Gateways\Tasks\Roles;

use App\Contracts\Tasks\Task;
use App\Repositories\RoleRepository;

/**
 * Class UpdateRoleByIdTask
 * @package App\Gateways\Tasks\Users
 */
class UpdateRoleByIdTask extends Task
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(RoleRepository::class)->update($id, $data);
    }

}
