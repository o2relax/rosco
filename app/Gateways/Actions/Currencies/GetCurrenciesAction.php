<?php

namespace App\Gateways\Actions\Currencies;

use App\Contracts\Actions\Action;
use App\Repositories\CurrencyRepository;

/**
 * Class GetCurrenciesAction.
 */
class GetCurrenciesAction extends Action
{

    /**
     * @param boolean $is_active
     * @return mixed
     */
    public function run($is_active = true)
    {

        if($is_active) {
            return app(CurrencyRepository::class)->getList();
        } else {
            return app(CurrencyRepository::class)->all();
        }

    }
}
