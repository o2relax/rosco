<?php

namespace App\Gateways\Actions\Currencies;

use App\Contracts\Actions\Action;
use App\Repositories\CurrencyRepository;

/**
 * Class CreateCurrencyAction.
 */
class CreateCurrencyAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return app(CurrencyRepository::class)->create($data);
    }
}
