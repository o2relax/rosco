<?php

namespace App\Gateways\Actions\Currencies;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Currencies\DeleteCurrencyTask;
use App\Gateways\Tasks\Currencies\GetCurrencyByIdTask;

/**
 * Class DeleteCurrencyAction
 */
class DeleteCurrencyAction extends Action
{

    /**
     * @param integer $id
     * @throws \Exception
     * @return mixed
     */
    public function run($id)
    {

        $currency = $this->call(GetCurrencyByIdTask::class, [$id]);

        if (!$currency) {
            throw new \Exception('Currency not found');
        }

        if (
            cfg('currency_shop') == $currency->id || cfg('currency_catalog') == $currency->id
        ) {
            return false;
        }

        return $this->call(DeleteCurrencyTask::class, [$id]);

    }
}
