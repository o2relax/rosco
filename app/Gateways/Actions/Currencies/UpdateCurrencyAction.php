<?php

namespace App\Gateways\Actions\Currencies;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Currencies\GetCurrencyByIdTask;
use App\Gateways\Tasks\Currencies\UpdateCurrencyTask;
use App\Gateways\Tasks\Languages\DeleteLanguageImageTask;
use App\Gateways\Tasks\Languages\UploadLanguageImageTask;
use App\Gateways\Tasks\Languages\GetLanguageByIdTask;
use App\Gateways\Tasks\Languages\UpdateLanguageTask;
use App\Models\Currency;
use App\Models\Language;

/**
 * Class UpdateCurrencyAction.
 */
class UpdateCurrencyAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {


        $currency = $this->call(GetCurrencyByIdTask::class, [$id]);

        if (
            array_key_exists('is_active', $data) &&
            filled($data['is_active']) &&
            $data['is_active'] == Currency::INACTIVE &&
            (cfg('currency_shop') == $currency->id || cfg('currency_catalog') == $currency->id)
        ) {
            return false;
        }

        return $this->call(UpdateCurrencyTask::class, [$id, $data]);
    }
}
