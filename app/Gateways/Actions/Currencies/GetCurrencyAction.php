<?php

namespace App\Gateways\Actions\Currencies;

use App\Contracts\Actions\Action;
use App\Repositories\CurrencyRepository;

/**
 * Class GetCurrencyAction.
 */
class GetCurrencyAction extends Action
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {

        return app(CurrencyRepository::class)->find($id);

    }
}
