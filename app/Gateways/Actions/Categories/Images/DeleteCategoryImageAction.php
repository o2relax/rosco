<?php

namespace App\Gateways\Actions\Categories\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Categories\GetCategoryByIdTask;
use App\Gateways\Tasks\Categories\Images\DeleteCategoryImageTask;
use App\Gateways\Tasks\Categories\UpdateCategoryByIdTask;
use App\Gateways\Tasks\Products\Images\DeleteProductImageTask;
use App\Gateways\Tasks\Products\Images\GetProductImageTask;
use App\Gateways\Tasks\Products\Images\UnsetProductImageTask;

/**
 * Class DeleteCategoryImageAction.
 */
class DeleteCategoryImageAction extends Action
{

    /**
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {

        $category = $this->call(GetCategoryByIdTask::class, [$id]);


//        if (is_file(category_image($category->id, $category->image, 'path'))) {
        if (is_file(storage($category->id . DIRECTORY_SEPARATOR . $category->image, 'categories', 'path'))) {
            $this->call(DeleteCategoryImageTask::class, [$category->id, $category->image]);
            $category->image = null;
            $category = $this->call(UpdateCategoryByIdTask::class, [$category->id, $category->toArray()]);
        }

        return $category;

    }
}
