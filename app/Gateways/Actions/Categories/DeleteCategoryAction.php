<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Categories\GetCategoryByIdTask;
use App\Gateways\Tasks\Categories\Images\DeleteCategoryImageTask;
use App\Repositories\CategoryRepository;

/**
 * Class DeleteCategoryAction.
 */
class DeleteCategoryAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {

        $category = $this->call(GetCategoryByIdTask::class, [$id]);

        if($category->image) $this->call(DeleteCategoryImageTask::class, [$category->id, $category->image, true]);

        return app(CategoryRepository::class)->delete($id);
    }
}
