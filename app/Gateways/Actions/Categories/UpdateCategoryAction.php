<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Categories\GetCategoryByIdTask;
use App\Gateways\Tasks\Categories\Images\DeleteCategoryImageTask;
use App\Gateways\Tasks\Categories\UpdateCategoryByIdTask;
use App\Gateways\Tasks\Categories\Images\UploadCategoryImageTask;

/**
 * Class UpdateCategoryAction.
 */
class UpdateCategoryAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run($id, array $data = [], $image = null)
    {

        if($image) {
            $category = $this->call(GetCategoryByIdTask::class, [$id]);
            if($category->image) $this->call(DeleteCategoryImageTask::class, [$category->id, $category->image]);
            $data['image'] = $this->call(UploadCategoryImageTask::class, [$id, $image]);
        }

        return $this->call(UpdateCategoryByIdTask::class, [
            $id,
            $data
        ]);
    }
}
