<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;

/**
 * Class TreeCategoryAction.
 */
class TreeCategoryAction extends Action
{

    protected $categories = [];
    protected $prefix = '';
    protected $cats = [];

    /**
     * @param array $categories
     * @param int $id
     * @return mixed
     */

    public function run($categories, $id = null)
    {

        $this->cats = [];

        foreach ($categories as $category) {
            $this->cats[$category->parent_id][$category->id] = $category;
        }

        if ($id) unset($this->cats[$id]);

        return $this->tree(0);
    }

    /**
     * @param $parent_id
     * @return array|null
     */
    public function tree($parent_id)
    {

        if (isset($this->cats[$parent_id])) {
            foreach ($this->cats[$parent_id] as $cat) {
                if ($parent_id != 0) {
                    $this->prefix = '- ';
                } else {
                    $this->prefix = '';
                }
                $cat->name = $this->prefix . ($this->prefix == '' ? mb_strtoupper($cat->name) : $cat->name);
                $this->categories[] = $cat;
                $this->tree($cat->id);
            }
        } else return null;

        return $this->categories;
    }

}
