<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;

/**
 * Class DropParentCategoryAction.
 */
class DropParentCategoryAction extends Action
{

    /**
     * @param int $category_id
     * @return mixed
     */
    public function run($category_id)
    {
            return app(CategoryRepository::class)->dropParent($category_id);
    }
}
