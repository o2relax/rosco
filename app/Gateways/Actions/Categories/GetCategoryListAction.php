<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;
use App\Repositories\CategoryRepository;

/**
 * Class GetCategoryListAction.
 */
class GetCategoryListAction extends Action
{

    /**
     * @param string $mod
     * @return mixed
     */
    public function run($mod = 'getList')
    {
        return app(CategoryRepository::class)->$mod();
    }
}
