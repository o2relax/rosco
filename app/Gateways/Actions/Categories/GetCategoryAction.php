<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Categories\GetCategoryByIdTask;
use App\Gateways\Tasks\Categories\GetCategoryBySlugTask;

/**
 * Class GetCategoryAction.
 */
class GetCategoryAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        if(is_numeric($id)) {
            return $this->call(GetCategoryByIdTask::class, [$id]);
        } else {
            return $this->call(GetCategoryBySlugTask::class, [$id]);
        }

    }
}
