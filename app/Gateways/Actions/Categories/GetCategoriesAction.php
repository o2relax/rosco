<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;
use App\Repositories\CategoryRepository;

/**
 * Class GetCategoriesAction.
 */
class GetCategoriesAction extends Action
{

    /**
     * @param array|null $search
     * @param string $mod
     * @return mixed
     */
    public function run(array $search = null, $mod = 'paginated')
    {
        if($search) {
            return app(CategoryRepository::class)->search($search);
        } else {
            return app(CategoryRepository::class)->$mod();
        }

    }
}
