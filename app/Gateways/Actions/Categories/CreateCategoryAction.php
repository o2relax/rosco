<?php

namespace App\Gateways\Actions\Categories;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Categories\Images\UploadCategoryImageTask;
use App\Repositories\CategoryRepository;

/**
 * Class CreateCategoryAction.
 */
class CreateCategoryAction extends Action
{

    /**
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run(array $data = [], $image = null)
    {

        $category = app(CategoryRepository::class)->create($data);
        if($image) {
            $data['image'] = $this->call(UploadCategoryImageTask::class, [$category->id, $image]);
        }

        return app(CategoryRepository::class)->update(isset($category->id) ? $category->id : 0, $data);
    }
}
