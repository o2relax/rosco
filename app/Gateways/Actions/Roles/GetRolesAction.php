<?php

namespace App\Gateways\Actions\Roles;

use App\Contracts\Actions\Action;
use App\Repositories\RoleRepository;

/**
 * Class GetRolesAction.
 */
class GetRolesAction extends Action
{

    /**
     * @param array|null $search
     * @return mixed
     */
    public function run(array $search = null)
    {
        if($search) {
            return app(RoleRepository::class)->search($search);
        } else {
            return app(RoleRepository::class)->paginate();
        }

    }
}
