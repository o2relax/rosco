<?php

namespace App\Gateways\Actions\Roles;

use App\Contracts\Actions\Action;
use App\Repositories\RoleRepository;

/**
 * Class CreateRoleAction.
 */
class CreateRoleAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return app(RoleRepository::class)->create($data);
    }
}
