<?php

namespace App\Gateways\Actions\Roles;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Users\GetRoleByIdTask;
use App\Gateways\Tasks\Users\UpdateRoleByIdTask;
use App\Repositories\RoleRepository;

/**
 * Class DeleteRoleAction.
 */
class DeleteRoleAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(RoleRepository::class)->delete($id);
    }
}
