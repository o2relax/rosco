<?php

namespace App\Gateways\Actions\Roles;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Roles\UpdateRoleByIdTask;

/**
 * Class UpdateRoleAction.
 */
class UpdateRoleAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return $this->call(UpdateRoleByIdTask::class, [
            $id,
            $data
        ]);
    }
}
