<?php

namespace App\Gateways\Actions\Roles;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Roles\GetRoleByIdTask;

/**
 * Class GetRoleAction.
 */
class GetRoleAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetRoleByIdTask::class, [$id]);
    }
}
