<?php

namespace App\Gateways\Actions\Modules;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Modules\DeleteModuleFileTask;

/**
 * Class DeleteModuleFileAction
 * @package App\Gateways\Actions\Modules
 */
class DeleteModuleFileAction extends Action
{

    /**
     * @param $code
     * @param $name
     * @return bool|mixed
     */
    public function run($code, $name)
    {

        if (is_file(module_file($code, $name, 'path'))) {
            return $this->call(DeleteModuleFileTask::class, [$code, $name]);
        }

        return true;

    }
}
