<?php

namespace App\Gateways\Actions\Modules;

use App\Contracts\Actions\Action;
use App\Repositories\ModuleRepository;

/**
 * Class GetModulesAction.
 */
class GetModulesAction extends Action
{

    /**
     * @return mixed
     */
    public function run()
    {
        return app(ModuleRepository::class)->paginated();
    }
}
