<?php

namespace App\Gateways\Actions\Modules;

use App\Contracts\Actions\Action;
use App\Repositories\ModuleRepository;

/**
 * Class GetModuleAction.
 */
class GetModuleAction extends Action
{

    /**
     * @param $code
     * @param $isActive
     * @return mixed
     */
    public function run($code, $isActive = true)
    {
        return app(ModuleRepository::class)->getByCode($code, $isActive);
    }
}
