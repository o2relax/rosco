<?php

namespace App\Gateways\Actions\Modules;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Modules\UploadModuleFileTask;
use App\Repositories\ModuleRepository;

/**
 * Class UpdateManufacturerAction.
 */
class UpdateModuleAction extends Action
{

    /**
     * @param $code
     * @param array $data
     * @param array|null $files
     * @return mixed
     */
    public function run($code, array $data = [], array $files = null)
    {
        if ($files) {
            foreach ($files as $key => $file) {
                $data['data'][$key]['file'] = $this->call(UploadModuleFileTask::class, [$code, $file, $key]);
            }
        }

        if (isset($data['data'])) {
            $data['data'] = json_encode($data['data']);
        }

        if (is_numeric($code)) {
            $module = app(ModuleRepository::class)->update($code, $data);
        } else {
            $module = app(ModuleRepository::class)->updateModule($code, $data);
        }

        if (isset($data['use_cache']) && $data['use_cache'] == 0) {
            module()->clearCache($module->code);
        }

        return $module;
    }
}
