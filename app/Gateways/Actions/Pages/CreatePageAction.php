<?php

namespace App\Gateways\Actions\Pages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Pages\Images\UploadPageImageTask;
use App\Repositories\PageRepository;

/**
 * Class CreatePageAction.
 */
class CreatePageAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return app(PageRepository::class)->create($data);
    }
}
