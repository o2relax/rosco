<?php

namespace App\Gateways\Actions\Pages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Pages\GetPageByIdTask;

/**
 * Class GetPageAction.
 */
class GetPageAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetPageByIdTask::class, [$id]);
    }
}
