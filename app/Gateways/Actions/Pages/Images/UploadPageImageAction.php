<?php

namespace App\Gateways\Actions\Pages\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Pages\GetPageByIdTask;
use App\Gateways\Tasks\Pages\Images\DeletePageImageTask;
use App\Gateways\Tasks\Pages\Images\UploadPageImageTask;

/**
 * Class DeletePageImageAction
 * @package App\Gateways\Actions\Pages\Images
 */
class UploadPageImageAction extends Action
{

    /**
     * @param $id
     * @param $image
     * @return mixed
     */
    public function run($id, $image)
    {
        return $this->call(UploadPageImageTask::class, [$id, $image]);
    }
}
