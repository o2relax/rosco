<?php

namespace App\Gateways\Actions\Pages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Pages\GetPageByIdTask;
use App\Gateways\Tasks\Pages\Images\DeletePageImageTask;
use App\Repositories\PageRepository;

/**
 * Class DeletePageAction
 * @package App\Gateways\Actions\Pages
 */
class DeletePageAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {

        $page = $this->call(GetPageByIdTask::class, [$id]);

        $this->call(DeletePageImageTask::class, [$page->id]);

        return app(PageRepository::class)->delete($id);
    }
}
