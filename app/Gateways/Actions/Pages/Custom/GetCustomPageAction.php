<?php

namespace App\Gateways\Actions\Pages\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\PageRepository;

/**
 * Class GetPagesAction.
 */
class GetCustomPageAction extends Action
{

    /**
     * @param $slug
     * @return mixed
     */
    public function run($slug)
    {
        return app(PageRepository::class)->getBySlug($slug);

    }
}
