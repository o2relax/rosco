<?php

namespace App\Gateways\Actions\Pages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\UploadManufacturerImageTask;
use App\Gateways\Tasks\Pages\GetPageByIdTask;
use App\Gateways\Tasks\Pages\Images\DeletePageImageTask;
use App\Gateways\Tasks\Pages\Images\UploadPageImageTask;
use App\Gateways\Tasks\Pages\UpdatePageByIdTask;

/**
 * Class UpdateManufacturerAction.
 */
class UpdatePageAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return $this->call(UpdatePageByIdTask::class, [
            $id,
            $data
        ]);
    }
}
