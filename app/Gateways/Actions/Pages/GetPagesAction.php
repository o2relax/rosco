<?php

namespace App\Gateways\Actions\Pages;

use App\Contracts\Actions\Action;
use App\Repositories\PageRepository;

/**
 * Class GetPagesAction.
 */
class GetPagesAction extends Action
{

    /**
     * @return mixed
     */
    public function run()
    {
        return app(PageRepository::class)->paginated();
    }
}
