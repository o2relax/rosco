<?php

namespace App\Gateways\Actions\Blogs;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Blogs\GetBlogByIdTask;
use App\Gateways\Tasks\Blogs\Images\DeleteBlogImagesTask;
use App\Gateways\Tasks\Blogs\Images\DeleteBlogImageTask;
use App\Repositories\BlogRepository;

/**
 * Class DeleteBlogAction
 * @package App\Gateways\Actions\Blogs
 */
class DeleteBlogAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {

        $blog = $this->call(GetBlogByIdTask::class, [$id]);

        $this->call(DeleteBlogImagesTask::class, [$blog->id]);

        return app(BlogRepository::class)->delete($id);
    }
}
