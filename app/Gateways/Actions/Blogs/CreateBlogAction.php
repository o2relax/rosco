<?php

namespace App\Gateways\Actions\Blogs;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Blogs\Images\UploadBlogImageTask;
use App\Gateways\Tasks\Pages\Images\UploadPageImageTask;
use App\Repositories\BlogRepository;

/**
 * Class CreateBlogAction.
 */
class CreateBlogAction extends Action
{

    /**
     * @param array $data
     * @param null $image
     * @return mixed
     */
    public function run(array $data = [], $image = null)
    {
        $blog = app(BlogRepository::class)->create($data);

        if ($image) {
            $data['image'] = $this->call(UploadBlogImageTask::class, [$blog->id, $image, $blog->title]);
            return app(BlogRepository::class)->update($blog->id, $data);
        } else {
            return $blog;
        }
    }
}
