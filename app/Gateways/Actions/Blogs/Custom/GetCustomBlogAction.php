<?php

namespace App\Gateways\Actions\Blogs\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\BlogRepository;

/**
 * Class GetCustomBlogAction
 * @package App\Gateways\Actions\Blogs\Custom
 */
class GetCustomBlogAction extends Action
{

    /**
     * @param $slug
     * @return mixed
     */
    public function run($slug)
    {
        return app(BlogRepository::class)->getBySlug($slug);

    }
}
