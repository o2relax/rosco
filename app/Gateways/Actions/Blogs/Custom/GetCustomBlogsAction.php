<?php

namespace App\Gateways\Actions\Blogs\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\BlogRepository;

/**
 * Class GetBlogsAction.
 */
class GetCustomBlogsAction extends Action
{

    /**
     * @param $limit
     * @return mixed
     */
    public function run($limit = 15)
    {
        return app(BlogRepository::class)->getList($limit);

    }
}
