<?php

namespace App\Gateways\Actions\Blogs;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\UploadManufacturerImageTask;
use App\Gateways\Tasks\Blogs\GetBlogByIdTask;
use App\Gateways\Tasks\Blogs\Images\DeleteBlogImageTask;
use App\Gateways\Tasks\Blogs\Images\UploadBlogImageTask;
use App\Gateways\Tasks\Blogs\UpdateBlogByIdTask;

/**
 * Class UpdateManufacturerAction.
 */
class UpdateBlogAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @param null $image
     * @return mixed
     */
    public function run($id, array $data = [], $image = null)
    {

        if($image) {
            $blog = $this->call(GetBlogByIdTask::class, [$id]);
            if($blog->image) $this->call(DeleteBlogImageTask::class, [$blog->id, $blog->image]);
            $data['image'] = $this->call(UploadBlogImageTask::class, [$blog->id, $image, $blog->title]);
        }

        return $this->call(UpdateBlogByIdTask::class, [
            $id,
            $data
        ]);
    }
}
