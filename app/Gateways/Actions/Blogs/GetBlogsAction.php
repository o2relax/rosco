<?php

namespace App\Gateways\Actions\Blogs;

use App\Contracts\Actions\Action;
use App\Repositories\BlogRepository;

/**
 * Class GetBlogsAction.
 */
class GetBlogsAction extends Action
{

    /**
     * @return mixed
     */
    public function run()
    {
        return app(BlogRepository::class)->paginated();
    }
}
