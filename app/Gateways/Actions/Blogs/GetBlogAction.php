<?php

namespace App\Gateways\Actions\Blogs;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Blogs\GetBlogByIdTask;

/**
 * Class GetBlogAction.
 */
class GetBlogAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetBlogByIdTask::class, [$id]);
    }
}
