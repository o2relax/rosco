<?php

namespace App\Gateways\Actions\Blogs\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Blogs\GetBlogByIdTask;
use App\Gateways\Tasks\Blogs\Images\DeleteBlogImageTask;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;

/**
 * Class DeleteBlogImageAction.
 */
class DeleteBlogImageAction extends Action
{

    /**
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {

        $blog = $this->call(GetBlogByIdTask::class, [$id]);


        if (is_file(blog_image($blog->id, $blog->image, 'path'))) {
            $this->call(DeleteBlogImageTask::class, [$blog->id, $blog->image]);
            $blog->image = null;
            $blog->save();
        }

        return $blog;

    }
}
