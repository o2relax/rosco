<?php

namespace App\Gateways\Actions\Blogs\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Blogs\GetBlogByIdTask;
use App\Gateways\Tasks\Blogs\Images\DeleteBlogImageTask;
use App\Gateways\Tasks\Blogs\Images\UploadBlogImageTask;

/**
 * Class DeleteBlogImageAction
 * @package App\Gateways\Actions\Blogs\Images
 */
class UploadBlogImageAction extends Action
{

    /**
     * @param $id
     * @param $image
     * @return mixed
     */
    public function run($id, $image)
    {
        return $this->call(UploadBlogImageTask::class, [$id, $image]);
    }
}
