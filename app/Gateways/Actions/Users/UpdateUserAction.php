<?php

namespace App\Gateways\Actions\Users;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Users\GetUserByIdTask;
use App\Gateways\Tasks\Users\UpdateUserByIdTask;

/**
 * Class UpdateUserAction.
 */
class UpdateUserAction extends Action
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {

        return $this->call(UpdateUserByIdTask::class, [
            $id,
            $data
        ]);

    }
}
