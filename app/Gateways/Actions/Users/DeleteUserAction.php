<?php

namespace App\Gateways\Actions\Users;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Users\DeleteUserByIdTask;

/**
 * Class DeleteUserAction.
 */
class DeleteUserAction extends Action
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {

        return $this->call(DeleteUserByIdTask::class, [$id]);

    }
}
