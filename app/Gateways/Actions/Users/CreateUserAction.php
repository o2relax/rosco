<?php

namespace App\Gateways\Actions\Users;

use App\Contracts\Actions\Action;
use App\Contracts\Exceptions\Exception;
//use App\Events\UserRegisteredEmail;
use App\Gateways\Tasks\Users\CreateUserMetaTask;
use App\Gateways\Tasks\Users\CreateUserTask;
use App\Repositories\UserRepository;

/**
 * Class CreateUserAction.
 */
class CreateUserAction extends Action
{

    /**
     * @param $data
     * @return mixed
     */
    public function run(array $data = null)
    {

        app('db')->beginTransaction();

        $data['password'] = substr(md5(rand(1111, 9999)), 0, 10);

        $user = $this->call(CreateUserTask::class, [
            $data
        ]);

        if (!$user) {
            app('db')->rollback();
        }

        $result = $this->call(CreateUserMetaTask::class, [
            $user->id,
            $data['roles'],
            true
        ]);

        if (!$result) {
            app('db')->rollback();
        }

        if (config('common.invite.send_mail')) {
//            event(new UserRegisteredEmail($user, $data['password']));
        }

        app('db')->commit();

        return $result;

    }
}
