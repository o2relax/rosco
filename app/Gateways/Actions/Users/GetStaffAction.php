<?php

namespace App\Gateways\Actions\Users;

use App\Contracts\Actions\Action;
use App\Repositories\UserRepository;

/**
 * Class GetStaffAction.
 */
class GetStaffAction extends Action
{

    /**
     * @param $search
     * @return mixed
     */
    public function run(array $search = null)
    {

        if($search) {
            return app(UserRepository::class)->search($search);
        } else {
            return app(UserRepository::class)->findStaff();
        }

    }
}
