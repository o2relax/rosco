<?php

namespace App\Gateways\Actions\Users\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\UserRepository;

/**
 * Class GetUserAction.
 */
class GetUserAction extends Action
{

    /**
     * @param $user_id
     * @param $value
     * @param string $column
     * @return mixed
     */
    public function run($user_id, $value, $column = 'id')
    {

        return app(UserRepository::class)->getByColumn($user_id, $value, $column);

    }
}
