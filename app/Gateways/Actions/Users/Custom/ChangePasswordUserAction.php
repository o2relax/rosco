<?php

namespace App\Gateways\Actions\Users\Custom;

use App\Contracts\Actions\Action;
use App\Mail\PasswordChanged;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;

/**
 * Class CreateUserAction.
 */
class ChangePasswordUserAction extends Action
{

    /**
     * @param $user_id
     * @param $password
     * @return mixed
     */
    public function run($user_id, $password)
    {
        $user = app(UserRepository::class)->update(
            $user_id,
            ['password' => bcrypt($password)]
        );

        if($user) {
            Mail::to($user->email)
                ->send(new PasswordChanged($user, $password));
        }

        return $user;
    }
}
