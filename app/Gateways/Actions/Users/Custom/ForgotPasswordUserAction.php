<?php

namespace App\Gateways\Actions\Users\Custom;

use App\Contracts\Actions\Action;
use App\Contracts\Exceptions\Exception;
use App\Mail\PasswordChanged;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;

/**
 * Class CreateUserAction.
 */
class ForgotPasswordUserAction extends Action
{

    /**
     * @param $email
     * @return mixed
     */
    public function run($email)
    {

        $user = app(UserRepository::class)->getByEmail($email);

        if(!$user) {
            return null;
        }

        $password = bcrypt(str_random(6));

        app(UserRepository::class)->update(
            $user->id,
            ['password' => $password]
        );

        if($user) {
            Mail::to($user->email)
                ->send(new PasswordChanged($user, $password));
        }

        return $user;
    }
}
