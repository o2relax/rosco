<?php

namespace App\Gateways\Actions\Users\Custom;

use App\Contracts\Actions\Action;
use App\Mail\UserCreated;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;

/**
 * Class CreateUserAction.
 */
class CreateUserAction extends Action
{

    /**
     * @param $data
     * @return mixed
     */
    public function run(array $data = null)
    {

        $user = app(UserRepository::class)->registration($data);

//        if($user) {
//            Mail::to($user->email)
//                ->send(new UserCreated($user, $data['password']));
//        }

        return $user;

    }
}
