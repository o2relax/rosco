<?php

namespace App\Gateways\Actions\Users;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Users\DeleteUserByIdTask;
use App\Gateways\Tasks\Users\GetUserByIdTask;

/**
 * Class DeleteUserAction.
 */
class SwitchToUserAction extends Action
{

    /**
     * @param int|null $id
     * @throws  \Exception
     * @return mixed
     */
    public function run($id = null)
    {

        if (!$id) $user_id = session()->pull('original_user');
        else $user_id = $id;

        try {

            $user = $this->call(GetUserByIdTask::class, [$user_id]);

            if ($id) session()->put('original_user', auth()->id());

            auth()->login($user);

        } catch (\Exception $e) {
            throw new \Exception("Error logging in as user", 1);
        }

        return true;

    }
}
