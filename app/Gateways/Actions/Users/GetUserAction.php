<?php

namespace App\Gateways\Actions\Users;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Users\GetUserByIdTask;

/**
 * Class GetUserAction.
 */
class GetUserAction extends Action
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {

        return $this->call(GetUserByIdTask::class, [$id]);

    }
}
