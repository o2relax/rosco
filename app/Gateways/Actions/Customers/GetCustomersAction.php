<?php

namespace App\Gateways\Actions\Customers;

use App\Contracts\Actions\Action;
use App\Repositories\CustomerRepository;

/**
 * Class GetStaffAction.
 */
class GetCustomersAction extends Action
{

    /**
     * @param $search
     * @return mixed
     */
    public function run(array $search = null)
    {

        if($search) {
            return app(CustomerRepository::class)->search($search);
        } else {
            return app(CustomerRepository::class)->getList();
        }

    }
}
