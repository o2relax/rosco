<?php

namespace App\Gateways\Actions\Customers;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Customers\GetCustomerByIdTask;
use App\Repositories\CustomerRepository;

/**
 * Class GetCustomerAction.
 */
class GetCustomerAction extends Action
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {

        return app(CustomerRepository::class)->getById($id);

    }
}
