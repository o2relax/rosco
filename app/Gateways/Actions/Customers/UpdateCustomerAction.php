<?php

namespace App\Gateways\Actions\Customers;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Customers\GetCustomerByIdTask;
use App\Gateways\Tasks\Customers\UpdateCustomerByIdTask;

/**
 * Class UpdateCustomerAction.
 */
class UpdateCustomerAction extends Action
{

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {

        return $this->call(UpdateCustomerByIdTask::class, [
            $id,
            $data
        ]);

    }
}
