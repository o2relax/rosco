<?php

namespace App\Gateways\Actions\Customers;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Customers\DeleteCustomerByIdTask;

/**
 * Class DeleteCustomerAction.
 */
class DeleteCustomerAction extends Action
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {

        return $this->call(DeleteCustomerByIdTask::class, [$id]);

    }
}
