<?php

namespace App\Gateways\Actions\Shipments\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\ShipmentRepository;

/**
 * Class GetPaymentsAction.
 */
class GetCustomShipmentsAction extends Action
{

    /**
     * @param array|null $ids
     * @return mixed
     */
    public function run(array $ids = null)
    {
        if($ids) {
            return app(ShipmentRepository::class)->getByIds($ids);
        } else {
            return app(ShipmentRepository::class)->getList();
        }

    }
}
