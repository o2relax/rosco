<?php

namespace App\Gateways\Actions\Shipments;

use App\Contracts\Actions\Action;
use App\Repositories\ShipmentRepository;

/**
 * Class GetShipmentsAction.
 */
class GetShipmentsAction extends Action
{

    /**
     * @param array|null $search
     * @param string $mod
     * @return mixed
     */
    public function run(array $search = null, $mod = 'paginated')
    {
        if($search) {
            return app(ShipmentRepository::class)->search($search);
        } else {
            return app(ShipmentRepository::class)->$mod();
        }

    }
}
