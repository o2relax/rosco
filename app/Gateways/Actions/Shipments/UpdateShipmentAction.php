<?php

namespace App\Gateways\Actions\Shipments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\UploadManufacturerImageTask;
use App\Gateways\Tasks\Shipments\GetShipmentByIdTask;
use App\Gateways\Tasks\Shipments\Images\DeleteShipmentImageTask;
use App\Gateways\Tasks\Shipments\Images\UploadShipmentImageTask;
use App\Gateways\Tasks\Shipments\UpdateShipmentByIdTask;

/**
 * Class UpdateManufacturerAction.
 */
class UpdateShipmentAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run($id, array $data = [], $image = null)
    {

        if($image) {
            $shipment = $this->call(GetShipmentByIdTask::class, [$id]);
            if($shipment->image) $this->call(DeleteShipmentImageTask::class, [$shipment->image]);
            $data['image'] = $this->call(UploadShipmentImageTask::class, [$image, $shipment->code]);
        }

        return $this->call(UpdateShipmentByIdTask::class, [
            $id,
            $data
        ]);
    }
}
