<?php

namespace App\Gateways\Actions\Shipments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Shipments\GetShipmentByIdTask;

/**
 * Class GetShipmentAction.
 */
class GetShipmentAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetShipmentByIdTask::class, [$id]);
    }
}
