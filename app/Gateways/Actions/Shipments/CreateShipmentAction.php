<?php

namespace App\Gateways\Actions\Shipments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Shipments\Images\UploadShipmentImageTask;
use App\Repositories\ShipmentRepository;

/**
 * Class CreateShipmentAction.
 */
class CreateShipmentAction extends Action
{

    /**
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run(array $data = [], $image = null)
    {

        $shipment = app(ShipmentRepository::class)->create($data);

        if($image) {
            $data['image'] = $this->call(UploadShipmentImageTask::class, [$image, $shipment->code]);
        }

        return app(ShipmentRepository::class)->update($shipment->id, $data);
    }
}
