<?php

namespace App\Gateways\Actions\Shipments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Shipments\GetShipmentByIdTask;
use App\Gateways\Tasks\Shipments\Images\DeleteShipmentImageTask;
use App\Repositories\ShipmentRepository;

/**
 * Class DeleteShipmentAction
 * @package App\Gateways\Actions\Shipments
 */
class DeleteShipmentAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        $shipment = $this->call(GetShipmentByIdTask::class, [$id]);
        if ($shipment->image) $this->call(DeleteShipmentImageTask::class, [$shipment->image]);

        return app(ShipmentRepository::class)->delete($id);
    }
}
