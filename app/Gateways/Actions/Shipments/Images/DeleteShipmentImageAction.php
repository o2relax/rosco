<?php

namespace App\Gateways\Actions\Shipments\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;
use App\Gateways\Tasks\Shipments\GetShipmentByIdTask;
use App\Gateways\Tasks\Shipments\Images\DeleteShipmentImageTask;

/**
 * Class DeleteShipmentImageAction
 * @package App\Gateways\Actions\Shipments\Images
 */
class DeleteShipmentImageAction extends Action
{

    /**
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {

        $shipment = $this->call(GetShipmentByIdTask::class, [$id]);


        if (is_file(storage($shipment->image, 'shipments', 'path'))) {
            $this->call(DeleteShipmentImageTask::class, [$shipment->image]);
            $shipment->image = null;
            $shipment->save();
        }

        return $shipment;

    }
}
