<?php

namespace App\Gateways\Actions\Attributes\Groups;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeGroupRepository;

/**
 * Class CreateAttributeGroupAction.
 */
class CreateAttributeGroupAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return app(AttributeGroupRepository::class)->create($data);
    }
}
