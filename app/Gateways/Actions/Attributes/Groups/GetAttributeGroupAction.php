<?php

namespace App\Gateways\Actions\Attributes\Groups;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Attributes\Groups\GetAttributeGroupByIdTask;
use App\Gateways\Tasks\Categories\GetCategoryByIdTask;

/**
 * Class GetCategoryAction.
 */
class GetAttributeGroupAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetAttributeGroupByIdTask::class, [$id]);
    }
}
