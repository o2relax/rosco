<?php

namespace App\Gateways\Actions\Attributes\Groups;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeGroupRepository;
use App\Repositories\CategoryRepository;

/**
 * Class GetAttributeGroupsAction.
 */
class GetAttributeGroupsAction extends Action
{

    /**
     * @param array|null $search
     * @return mixed
     */
    public function run(array $search = null)
    {
        if($search) {
            return app(AttributeGroupRepository::class)->search($search);
        } else {
            return app(AttributeGroupRepository::class)->all();
        }

    }
}
