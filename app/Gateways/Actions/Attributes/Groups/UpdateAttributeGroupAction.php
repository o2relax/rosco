<?php

namespace App\Gateways\Actions\Attributes\Groups;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Attributes\Groups\UpdateAttributeGroupByIdTask;

/**
 * Class UpdateAttributeGroupAction.
 */
class UpdateAttributeGroupAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {

        return $this->call(UpdateAttributeGroupByIdTask::class, [$id, $data]);
    }
}
