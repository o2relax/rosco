<?php

namespace App\Gateways\Actions\Attributes\Groups;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeGroupRepository;

/**
 * Class DeleteCategoryAction.
 */
class DeleteAttributeGroupAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(AttributeGroupRepository::class)->delete($id);
    }
}
