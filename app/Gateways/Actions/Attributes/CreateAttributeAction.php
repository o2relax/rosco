<?php

namespace App\Gateways\Actions\Attributes;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeRepository;

/**
 * Class CreateAttributeAction.
 */
class CreateAttributeAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return app(AttributeRepository::class)->create($data);
    }
}
