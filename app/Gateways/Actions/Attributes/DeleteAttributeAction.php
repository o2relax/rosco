<?php

namespace App\Gateways\Actions\Attributes;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeRepository;

/**
 * Class DeleteCategoryAction.
 */
class DeleteAttributeAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(AttributeRepository::class)->delete($id);
    }
}
