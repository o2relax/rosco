<?php

namespace App\Gateways\Actions\Attributes;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Attributes\Groups\UpdateAttributeGroupByIdTask;
use App\Gateways\Tasks\Attributes\UpdateAttributeByIdTask;
use App\Gateways\Tasks\Categories\GetCategoryByIdTask;
use App\Gateways\Tasks\Categories\Images\DeleteCategoryImageTask;
use App\Gateways\Tasks\Categories\UpdateCategoryByIdTask;
use App\Gateways\Tasks\Categories\Images\UploadCategoryImageTask;

/**
 * Class UpdateAttributeGroupAction.
 */
class UpdateAttributeAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {

        return $this->call(UpdateAttributeByIdTask::class, [$id, $data]);
    }
}
