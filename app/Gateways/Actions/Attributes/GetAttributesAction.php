<?php

namespace App\Gateways\Actions\Attributes;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeRepository;

/**
 * Class GetAttributesAction.
 */
class GetAttributesAction extends Action
{

    /**
     * @param array|null $search
     * @return mixed
     */
    public function run(array $search = null)
    {
        if($search) {
            return app(AttributeRepository::class)->search($search);
        } else {
            return app(AttributeRepository::class)->all();
        }

    }
}
