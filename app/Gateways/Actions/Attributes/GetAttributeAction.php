<?php

namespace App\Gateways\Actions\Attributes;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Attributes\GetAttributeByIdTask;

/**
 * Class GetAttributeAction.
 */
class GetAttributeAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetAttributeByIdTask::class, [$id]);
    }
}
