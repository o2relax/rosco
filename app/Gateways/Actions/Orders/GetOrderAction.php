<?php

namespace App\Gateways\Actions\Orders;

use App\Contracts\Actions\Action;
use App\Repositories\OrderRepository;

/**
 * Class CreateProductAction.
 */
class GetOrderAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(OrderRepository::class)->find($id);
    }
}
