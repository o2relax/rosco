<?php

namespace App\Gateways\Actions\Orders;

use App\Contracts\Actions\Action;
use App\Repositories\OrderRepository;

/**
 * Class CreateProductAction.
 */
class UpdateOrderAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(OrderRepository::class)->update($id, $data);
    }
}
