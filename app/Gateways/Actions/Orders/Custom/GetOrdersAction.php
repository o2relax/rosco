<?php

namespace App\Gateways\Actions\Orders\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\OrderRepository;

/**
 * Class CreateProductAction.
 */
class GetOrdersAction extends Action
{

    /**
     * @param $user_id
     * @return mixed
     */
    public function run($user_id)
    {
        return app(OrderRepository::class)->getByUser($user_id);
    }
}
