<?php

namespace App\Gateways\Actions\Orders;

use App\Contracts\Actions\Action;
use App\Gateways\Actions\Cart\GetCartProductsAction;
use App\Gateways\Tasks\Payments\GetPaymentByIdTask;
use App\Gateways\Tasks\Products\UpdateProductTask;
use App\Gateways\Tasks\Users\UpdateUserByIdTask;
use App\Gateways\Transformers\Cart\CartListTransformer;
use App\Mail\OrderCreated;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Mail;

/**
 * Class CreateProductAction.
 */
class CreateOrderAction extends Action
{

    use ResponseTrait;

    /**
     * @param array $customer
     * @param array $items
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function run(array $customer, array $items)
    {

        app('db')->beginTransaction();

        $products = $this->call(GetCartProductsAction::class, ['total']);

        $order_items = [];

//        $payment = $this->call(GetPaymentByIdTask::class, [$customer['payment_id']]);
        if (isset($customer['payment_id'])) {
            $payment = ($this->call(GetPaymentByIdTask::class, [$customer['payment_id']]));
        } else {
            $payment = null;
        }

        $customer['phone'] = preg_replace('/[^0-9]/', '', $customer['phone']);
        $customer['currency_id'] = $payment ? $payment->currency_id : currency()->get('id');

        foreach ($products as $product) {
            $product = $this->transform($product, CartListTransformer::class);
            $order_items[] = [
                'product_id' => $product->id,
                'price' => currency()->convert($product->price, $customer['currency_id']),
                'quantity' => $items[$product->id]
            ];
        }

        $customer['status_id'] = Order::NEW;

        session()->put('email', $customer['name']);
        session()->put('email', $customer['email']);
        session()->put('phone', $customer['phone']);
        session()->put('address', $customer['address']);

        if (auth()->check()) {
            $customer['user_id'] = auth()->user()->id;
            if (!auth()->user()->meta->phone) {
                $this->call(UpdateUserByIdTask::class, [
                    $customer['user_id'],
                    [
                        'meta' => [
                            'phone' => $customer['phone'],
                        ]
                    ]
                ]);
            }
        }

        $order = app(OrderRepository::class)->createOrder(
            $customer,
            $order_items
        );

        foreach ($products as $product) {
            if (!$product->use_stock) {
                continue;
            }
            $stock = $product->stock - $items[$product->id];
            if ($stock < 0) {
                app('db')->rollBack();
                return 0;
            }
            $this->call(UpdateProductTask::class, [
                $product->id,
                [
                    'stock' => $stock,
                    'status_id' => 3
                ]
            ]);
        }

        if ($order) {
            app('db')->commit();
            cart()->clear();
            if (cfg('notice_email_user_order')) {
                //Mail::to($order->email)
                //    ->send(new OrderCreated($order));
            }
            if (cfg('notice_email_admin_order')) {
                //Mail::to(cfg('admin_email'))
                //    ->send(new OrderCreated($order));
            }
        } else {
            app('db')->rollBack();
        }

        return $order;

    }
}
