<?php

namespace App\Gateways\Actions\Orders;

use App\Contracts\Actions\Action;
use App\Repositories\OrderRepository;

/**
 * Class CreateProductAction.
 */
class CreateFastOrderAction extends Action
{

    /**
     * @param array $order_info
     * @param array $items
     * @return mixed
     */
    public function run(array $order_info = [],array $items = [])
    {
        return app(OrderRepository::class)->createOrder($order_info,$items);
    }
}
