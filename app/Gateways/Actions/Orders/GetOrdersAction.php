<?php

namespace App\Gateways\Actions\Orders;

use App\Contracts\Actions\Action;
use App\Repositories\OrderRepository;

/**
 * Class CreateProductAction.
 */
class GetOrdersAction extends Action
{

    /**
     * @param null $user_id
     * @return mixed
     */
    public function run($user_id = null)
    {
        if($user_id) {
            return app(OrderRepository::class)->getByUser($user_id);
        } else {
            return app(OrderRepository::class)->paginated();
        }

    }
}
