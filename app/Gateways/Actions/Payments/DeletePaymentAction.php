<?php

namespace App\Gateways\Actions\Payments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Payments\GetPaymentByIdTask;
use App\Gateways\Tasks\Payments\Images\DeletePaymentImageTask;
use App\Repositories\PaymentRepository;

/**
 * Class DeletePaymentAction
 * @package App\Gateways\Actions\Payments
 */
class DeletePaymentAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {

        $shipment = $this->call(GetPaymentByIdTask::class, [$id]);
        if($shipment->image) $this->call(DeletePaymentImageTask::class, [$shipment->image]);

        return app(PaymentRepository::class)->delete($id);
    }
}
