<?php

namespace App\Gateways\Actions\Payments;

use App\Contracts\Actions\Action;
use App\Repositories\PaymentRepository;

/**
 * Class GetPaymentsAction.
 */
class GetPaymentsAction extends Action
{

    /**
     * @param array|null $search
     * @param string $mod
     * @return mixed
     */
    public function run(array $search = null, $mod = 'paginated')
    {
        if($search) {
            return app(PaymentRepository::class)->search($search);
        } else {
            return app(PaymentRepository::class)->$mod();
        }

    }
}
