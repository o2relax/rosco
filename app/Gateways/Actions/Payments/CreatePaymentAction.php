<?php

namespace App\Gateways\Actions\Payments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Payments\Images\UploadPaymentImageTask;
use App\Repositories\PaymentRepository;

/**
 * Class CreatePaymentAction.
 */
class CreatePaymentAction extends Action
{

    /**
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run(array $data = [], $image = null)
    {

        $shipment = app(PaymentRepository::class)->create($data);

        if($image) {
            $data['image'] = $this->call(UploadPaymentImageTask::class, [$image, $shipment->code]);
        }

        return app(PaymentRepository::class)->update($shipment->id, $data);
    }
}
