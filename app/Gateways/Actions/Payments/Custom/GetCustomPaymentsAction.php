<?php

namespace App\Gateways\Actions\Payments\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\PaymentRepository;

/**
 * Class GetPaymentsAction.
 */
class GetCustomPaymentsAction extends Action
{

    /**
     * @param array|null $ids
     * @return mixed
     */
    public function run(array $ids = null)
    {
        if($ids) {
            return app(PaymentRepository::class)->getByIds($ids);
        } else {
            return app(PaymentRepository::class)->getList();
        }

    }
}
