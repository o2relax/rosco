<?php

namespace App\Gateways\Actions\Payments\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Payments\GetPaymentByIdTask;
use App\Gateways\Tasks\Payments\Images\DeletePaymentImageTask;

/**
 * Class DeletePaymentImageAction
 * @package App\Gateways\Actions\Payments\Images
 */
class DeletePaymentImageAction extends Action
{

    /**
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {

        $shipment = $this->call(GetPaymentByIdTask::class, [$id]);


        if (is_file(storage($shipment->image, 'payments', 'path'))) {
            $this->call(DeletePaymentImageTask::class, [$shipment->image]);
            $shipment->image = null;
            $shipment->save();
        }

        return $shipment;

    }
}
