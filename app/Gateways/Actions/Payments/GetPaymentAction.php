<?php

namespace App\Gateways\Actions\Payments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Payments\GetPaymentByIdTask;

/**
 * Class GetPaymentAction.
 */
class GetPaymentAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetPaymentByIdTask::class, [$id]);
    }
}
