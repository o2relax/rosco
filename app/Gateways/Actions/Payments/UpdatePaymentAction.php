<?php

namespace App\Gateways\Actions\Payments;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\UploadManufacturerImageTask;
use App\Gateways\Tasks\Payments\GetPaymentByIdTask;
use App\Gateways\Tasks\Payments\Images\DeletePaymentImageTask;
use App\Gateways\Tasks\Payments\Images\UploadPaymentImageTask;
use App\Gateways\Tasks\Payments\UpdatePaymentByIdTask;

/**
 * Class UpdateManufacturerAction.
 */
class UpdatePaymentAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run($id, array $data = [], $image = null)
    {

        if($image) {
            $shipment = $this->call(GetPaymentByIdTask::class, [$id]);
            if($shipment->image) $this->call(DeletePaymentImageTask::class, [$shipment->image]);
            $data['image'] = $this->call(UploadPaymentImageTask::class, [$image, $shipment->code]);
        }

        return $this->call(UpdatePaymentByIdTask::class, [
            $id,
            $data
        ]);
    }
}
