<?php

namespace App\Gateways\Actions\Subscriptions;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Shipments\Images\UploadShipmentImageTask;
use App\Repositories\ShipmentRepository;
use App\Repositories\SubscriptionRepository;

/**
 * Class CreateShipmentAction.
 */
class CreateSubscriptionAction extends Action
{

    /**
     * @param $email
     * @return mixed
     */
    public function run($email)
    {

        return app(SubscriptionRepository::class)->create(['email' => $email]);
    }
}
