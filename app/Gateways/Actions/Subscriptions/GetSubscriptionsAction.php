<?php

namespace App\Gateways\Actions\Subscriptions;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Shipments\Images\UploadShipmentImageTask;
use App\Repositories\ShipmentRepository;
use App\Repositories\SubscriptionRepository;

/**
 * Class CreateShipmentAction.
 */
class GetSubscriptionsAction extends Action
{

    /**
     * @return mixed
     */
    public function run()
    {

        return app(SubscriptionRepository::class)->all();
    }
}
