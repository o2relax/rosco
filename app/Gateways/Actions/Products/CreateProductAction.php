<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Products\CreateProductTask;
use App\Gateways\Tasks\Products\Images\CreateProductImageTask;
use App\Gateways\Tasks\Products\UpdateProductTask;
use App\Gateways\Tasks\Products\Images\UploadProductImageTask;

/**
 * Class CreateProductAction.
 */
class CreateProductAction extends Action
{

    /**
     * @param array $data
     * @param array|null $files
     * @param $image
     * @return mixed
     */
    public function run(array $data = [], array $files = null, $image = null)
    {

        $product = $this->call(CreateProductTask::class, [$data]);
        if (!$product) return null;
        if($image) {
            $data['image'] = $this->call(UploadProductImageTask::class, [$product->id, $image]);
        }

        $this->call(UpdateProductTask::class, [$product->id, $data]);

        if($files) {
            foreach ($files as $file) {
                $file_name = $this->call(UploadProductImageTask::class, [$product->id, $file]);
                $this->call(CreateProductImageTask::class, [$product->id, $file_name]);
            }
        }

        return $product;

    }
}
