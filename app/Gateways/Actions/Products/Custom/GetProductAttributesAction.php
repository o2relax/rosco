<?php

namespace App\Gateways\Actions\Products\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeRepository;
use App\Repositories\ProductAttributeRepository;
use App\Repositories\ProductRepository;

/**
 * Class GetProductAttributesAction.
 */
class GetProductAttributesAction extends Action
{

    /**
     * @param $product_id
     * @return mixed
     */
    public function run($product_id)
    {

        return app(ProductAttributeRepository::class)->getByProduct($product_id);

    }
}
