<?php

namespace App\Gateways\Actions\Products\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeRepository;
use App\Repositories\ProductAttributeRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductStatRepository;

/**
 * Class GetProductAttributesAction.
 */
class UpdateProductStatsAction extends Action
{

    /**
     * @param $product
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function run($product, array $data = [])
    {

        cache()->tags('module_product_popular')->flush();

        return $product->stats ? $product->stats()->update($data) : $product->stats()->create($data);

    }
}
