<?php

namespace App\Gateways\Actions\Products\Custom;

use App\Contracts\Actions\Action;
use App\Repositories\ProductRepository;

/**
 * Class GetProductsAction.
 */
class GetProductListAction extends Action
{

    /**
     * @param array|null $filters
     * @param null       $mod
     * @param null       $per_page
     * @param bool       $module
     *
     * @return mixed
     */
    public function run(array $filters = null, $mod = null, $per_page = null, $module = true)
    {

        if(!$per_page) {
            $per_page = session()->get('per_page', cfg('catalog_pagination'));
        }

        if($mod) {
            return app(ProductRepository::class)->$mod(
                $per_page,
                $module ? 'get' : 'paginate'
            );
        }

        if(isset($filters['filter'])) {
            return app(ProductRepository::class)->getFiltered(
                $filters['filter'],
                $per_page
            );
        } else {
            return app(ProductRepository::class)->getPublished(
                isset($filters['category_id']) ? $filters['category_id'] : null,
                $per_page
            );
        }



    }
}
