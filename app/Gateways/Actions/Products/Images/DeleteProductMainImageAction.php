<?php

namespace App\Gateways\Actions\Products\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Products\GetProductByIdTask;
use App\Gateways\Tasks\Products\Images\DeleteProductImageTask;
use App\Gateways\Tasks\Products\Images\GetProductImageTask;
use App\Gateways\Tasks\Products\UpdateProductTask;

/**
 * Class DeleteProductMainImageAction.
 */
class DeleteProductMainImageAction extends Action
{

    /**
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {

        $product = $this->call(GetProductByIdTask::class, [$id]);

        if (is_file(product_image($product->id, $product->image, 'path'))) {
            $this->call(DeleteProductImageTask::class, [$product->id, $product->image]);
            $product->image = null;
            $product->save();
//            $product = $this->call(UpdateProductTask::class, [$id, $product->toArray()]);
        }

        return $product;

    }
}
