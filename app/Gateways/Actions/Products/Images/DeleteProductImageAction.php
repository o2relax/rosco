<?php

namespace App\Gateways\Actions\Products\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Products\Images\DeleteProductImageTask;
use App\Gateways\Tasks\Products\Images\GetProductImageTask;
use App\Gateways\Tasks\Products\Images\UnsetProductImageTask;

/**
 * Class DeleteProductImageAction.
 */
class DeleteProductImageAction extends Action
{

    /**
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {

        $image = $this->call(GetProductImageTask::class, [$id]);

        if($image) {
            if (is_file(product_image($image->product_id, $image->image, 'path'))) {
                $this->call(DeleteProductImageTask::class, [$image->product_id, $image->image]);
            }
            $this->call(UnsetProductImageTask::class, [$image->id]);
        }

        return $image;

    }
}
