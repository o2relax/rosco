<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Products\CheckDuplicateAttributeTask;
use App\Gateways\Tasks\Products\GetProductByIdTask;
use App\Gateways\Tasks\Products\Images\CreateProductImageTask;
use App\Gateways\Tasks\Products\Images\DeleteProductImageTask;
use App\Gateways\Tasks\Products\UpdateProductTask;
use App\Gateways\Tasks\Products\Images\UploadProductImageTask;

/**
 * Class UpdateProductAction.
 */
class AddProductAttributeAction extends Action
{

    /**
     * @param integer $id
     * @param integer $attribute_id
     * @return mixed
     */
    public function run($id, $attribute_id)
    {

        if($product = $this->call(CheckDuplicateAttributeTask::class, [$id, $attribute_id])) {

            $product->attributes()->create(['attribute_id' => $attribute_id]);

            return $product;

        }

        return false;

    }
}
