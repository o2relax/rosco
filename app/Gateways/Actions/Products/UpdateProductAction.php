<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Products\GetProductByIdTask;
use App\Gateways\Tasks\Products\Images\CreateProductImageTask;
use App\Gateways\Tasks\Products\Images\DeleteProductImageTask;
use App\Gateways\Tasks\Products\UpdateProductTask;
use App\Gateways\Tasks\Products\Images\UploadProductImageTask;

/**
 * Class UpdateProductAction.
 */
class UpdateProductAction extends Action
{

    /**
     * @param integer $id
     * @param array $data
     * @param array|null $files
     * @param $image
     * @return mixed
     */
    public function run($id, array $data = [], array $files = null, $image = null)
    {

        if($image) {
            $product = $this->call(GetProductByIdTask::class, [$id]);
            if($product->image) $this->call(DeleteProductImageTask::class, [$product->id, $product->image]);
            $data['image'] = $this->call(UploadProductImageTask::class, [$id, $image]);
        }

        $product = $this->call(UpdateProductTask::class, [$id, $data]);

        if($files) {
            foreach ($files as $file) {
                $file_name = $this->call(UploadProductImageTask::class, [$id, $file]);
                $this->call(CreateProductImageTask::class, [$id, $file_name]);
            }
        }

        return $product;

    }
}
