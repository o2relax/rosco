<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Products\GetStatusesTask;

/**
 * Class GetStatusesAction.
 */
class GetStatusesAction extends Action
{

    /**
     * @param $type
     * @return mixed
     */
    public function run($type = 'All')
    {

        return $this->call(GetStatusesTask::class, [$type]);

    }
}
