<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Repositories\ProductRepository;

/**
 * Class GetProductAction
 * @package App\Gateways\Actions\Products
 */
class GetProductAction extends Action
{

    /**
     * @param $id | or slug
     * @return mixed
     */
    public function run($id)
    {

        if(is_numeric($id)) {
            return app(ProductRepository::class)->find($id);
        } else {
            return app(ProductRepository::class)->getBySlug($id);
        }

    }
}
