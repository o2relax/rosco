<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Repositories\ProductAttributeRepository;

/**
 * Class RemoveProductAttributeAction.
 */
class RemoveProductAttributeAction extends Action
{

    /**
     * @param integer $product_id
     * @param integer $attribute_id
     * @return mixed
     */
    public function run($product_id, $attribute_id)
    {

        return app(ProductAttributeRepository::class)->remove($attribute_id, $product_id);

    }
}
