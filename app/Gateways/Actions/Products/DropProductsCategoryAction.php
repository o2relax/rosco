<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Repositories\ProductRepository;

/**
 * Class DropProductsCategoryAction.
 */
class DropProductsCategoryAction extends Action
{

    /**
     * @param int $category_id
     * @return mixed
     */
    public function run($category_id)
    {
            return app(ProductRepository::class)->dropCategory($category_id);
    }
}
