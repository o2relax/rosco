<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Repositories\ProductRepository;

/**
 * Class GetProductsAction.
 */
class GetProductsAction extends Action
{

    /**
     * @param $search
     * @param $per_page
     * @return mixed
     */
    public function run(array $search = null, $per_page = null)
    {

        if ($search) {
            return app(ProductRepository::class)->search(
                $search,
                $per_page ? $per_page : config('site.pagination', 25)
            );
        } elseif ($per_page == 'all') {
            return app(ProductRepository::class)->all();
        } else {
            return app(ProductRepository::class)->paginated(
                $per_page ? $per_page : config('site.pagination', 25)
            );
        }

    }
}
