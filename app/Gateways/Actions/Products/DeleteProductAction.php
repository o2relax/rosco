<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Contracts\Exceptions\Exception;
use App\Gateways\Tasks\Products\DeleteProductTask;
use App\Gateways\Tasks\Products\GetProductByIdTask;
use App\Gateways\Tasks\Products\Images\DeleteProductImageTask;
use App\Gateways\Tasks\Products\Images\UnsetProductImagesTask;
use App\Gateways\Tasks\Products\Images\UnsetProductImageTask;
use App\Gateways\Tasks\Products\UpdateProductTask;
use App\Gateways\Tasks\Products\Images\UploadProductImageTask;

/**
 * Class DeleteProductAction.
 */
class DeleteProductAction extends Action
{

    /**
     * @param integer $id
     * @throws \Exception
     * @return mixed
     */
    public function run($id)
    {

        $product = $this->call(GetProductByIdTask::class, [$id]);

        if(!$product) {
            throw new \Exception('Product not found');
        }

        $this->call(DeleteProductImageTask::class, [$product->id, null, true]);

        $this->call(DeleteProductTask::class, [$id]);

        return $product;

    }
}
