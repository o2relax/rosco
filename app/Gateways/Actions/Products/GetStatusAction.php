<?php

namespace App\Gateways\Actions\Products;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Products\GetStatusByIdTask;

/**
 * Class GetStatusAction.
 */
class GetStatusAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {

        return $this->call(GetStatusByIdTask::class, [$id]);

    }
}
