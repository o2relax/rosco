<?php

namespace App\Gateways\Actions\Reviews;

use App\Contracts\Actions\Action;
use App\Repositories\ProductReviewRepository;

/**
 * Class DeleteReviewAction
 * @package App\Gateways\Actions\Reviews
 */
class DeleteReviewAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {

        return app(ProductReviewRepository::class)->delete($id);
    }
}
