<?php

namespace App\Gateways\Actions\Reviews\Custom;

use App\Contracts\Actions\Action;
use App\Gateways\Actions\Products\GetProductAction;
use App\Models\User;
use App\Repositories\ProductReviewRepository;

/**
 * Class CreateProductAction.
 */
class CreateCustomReviewAction extends Action
{

    /**
     * @param $slug
     * @param array $data
     * @return mixed
     */
    public function run($slug, array $data = [])
    {

        $product = $this->call(GetProductAction::class, [$slug]);

        if (auth()->check()) {
            $data['product_id'] = $product->id;
            $data['user_id'] = auth()->user()->id;
            $data['name'] = auth()->user()->name;
            if (auth()->user()->hasRole(User::ADMIN)) {
                $data['is_active'] = 1;
            } else {
                $data['is_active'] = 0;
            }
        } else {
            $data['user_id'] = 0;
            $data['is_active'] = 0;
        }

        return app(ProductReviewRepository::class)->create($data);

    }
}
