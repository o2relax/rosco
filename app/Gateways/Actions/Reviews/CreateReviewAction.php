<?php

namespace App\Gateways\Actions\Reviews;

use App\Contracts\Actions\Action;
use App\Gateways\Actions\Products\GetProductAction;
use App\Models\User;
use App\Repositories\ProductReviewRepository;

/**
 * Class CreateProductAction.
 */
class CreateReviewAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        $data['viewed'] = 1;

        if (!$data['created_at']) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

        return app(ProductReviewRepository::class)->create($data);

    }
}
