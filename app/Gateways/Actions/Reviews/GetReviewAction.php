<?php

namespace App\Gateways\Actions\Reviews;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Reviews\GetReviewByIdTask;
use App\Repositories\ProductReviewRepository;

/**
 * Class GetReviewAction.
 */
class GetReviewAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        $review = app(ProductReviewRepository::class)->getById($id);

        if (!$review->veiwed) {
            app(ProductReviewRepository::class)->update($review->id, ['viewed' => 1]);
        }

        return $review;
    }
}
