<?php

namespace App\Gateways\Actions\Reviews;

use App\Contracts\Actions\Action;
use App\Repositories\ProductReviewRepository;

/**
 * Class GetReviewsAction
 * @package App\Gateways\Actions\Reviews
 */
class GetReviewsAction extends Action
{

    /**
     * @param null $product_id
     * @return mixed
     */
    public function run($product_id = null)
    {
        if($product_id) {
            return app(ProductReviewRepository::class)->getByProduct($product_id);
        } else {
            return app(ProductReviewRepository::class)->paginated();
        }

    }
}
