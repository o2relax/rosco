<?php

namespace App\Gateways\Actions\Reviews;

use App\Contracts\Actions\Action;
use App\Repositories\ProductReviewRepository;

/**
 * Class UpdateReviewAction
 * @package App\Gateways\Actions\Reviews
 */
class UpdateReviewAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {
        return app(ProductReviewRepository::class)->update($id, $data);
    }
}
