<?php

namespace App\Gateways\Actions\Languages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Languages\DeleteLanguageImageTask;
use App\Gateways\Tasks\Languages\DeleteLanguageTask;
use App\Gateways\Tasks\Languages\GetLanguageByIdTask;
use App\Gateways\Tasks\Languages\UpdateLanguageTask;

/**
 * Class DeleteLanguageImageAction.
 */
class DeleteLanguageImageAction extends Action
{

    /**
     * @param integer $id
     * @throws \Exception
     * @return mixed
     */
    public function run($id)
    {

        $language = $this->call(GetLanguageByIdTask::class, [$id]);

        if (!$language) {
            throw new \Exception('Language not found');
        }

        if ($language->icon) {
            $this->call(DeleteLanguageImageTask::class, [$language->icon]);
            $language->icon = null;
            $language->save();
        }

        return $language;

    }
}
