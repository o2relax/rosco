<?php

namespace App\Gateways\Actions\Languages;

use App\Contracts\Actions\Action;
use App\Repositories\LanguageRepository;

/**
 * Class GetLanguagesAction.
 */
class GetLanguageAction extends Action
{

    /**
     * @param int $id
     * @return mixed
     */
    public function run($id)
    {

        return app(LanguageRepository::class)->find($id);

    }
}
