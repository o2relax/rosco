<?php

namespace App\Gateways\Actions\Languages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Languages\DeleteLanguageImageTask;
use App\Gateways\Tasks\Languages\DeleteLanguageTask;
use App\Gateways\Tasks\Languages\GetLanguageByIdTask;

/**
 * Class DeleteLanguageAction.
 */
class DeleteLanguageAction extends Action
{

    /**
     * @param integer $id
     * @throws \Exception
     * @return mixed
     */
    public function run($id)
    {

        $language = $this->call(GetLanguageByIdTask::class, [$id]);

        if(!$language) {
            throw new \Exception('Language not found');
        }

        if(cfg('language') == $language->id) {
            return false;
        }

        if($language->icon) {
            $this->call(DeleteLanguageImageTask::class, [$language->icon]);
        }

        return $this->call(DeleteLanguageTask::class, [$id]);

    }
}
