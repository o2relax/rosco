<?php

namespace App\Gateways\Actions\Languages;

use App\Contracts\Actions\Action;
use App\Repositories\LanguageRepository;

/**
 * Class GetLanguagesAction.
 */
class GetLanguagesAction extends Action
{

    /**
     * @param boolean $is_active
     * @return mixed
     */
    public function run($is_active = true)
    {

        if($is_active) {
            return app(LanguageRepository::class)->getList();
        } else {
            return app(LanguageRepository::class)->all();
        }

    }
}
