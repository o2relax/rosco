<?php

namespace App\Gateways\Actions\Languages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Languages\UploadLanguageImageTask;
use App\Repositories\LanguageRepository;

/**
 * Class CreateLanguageAction.
 */
class CreateLanguageAction extends Action
{

    /**
     * @param array $data
     * @param $icon
     * @return mixed
     */
    public function run(array $data = [], $icon = null)
    {
        if($icon) {
            $data['icon'] = $this->call(UploadLanguageImageTask::class, [$icon, $data['code']]);
        }
        return app(LanguageRepository::class)->create($data);
    }
}
