<?php

namespace App\Gateways\Actions\Languages;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Languages\DeleteLanguageImageTask;
use App\Gateways\Tasks\Languages\UploadLanguageImageTask;
use App\Gateways\Tasks\Languages\GetLanguageByIdTask;
use App\Gateways\Tasks\Languages\UpdateLanguageTask;
use App\Models\Language;

/**
 * Class UpdateLanguageAction.
 */
class UpdateLanguageAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @param $icon
     * @return mixed
     */
    public function run($id, array $data = [], $icon = null)
    {


        $language = $this->call(GetLanguageByIdTask::class, [$id]);

        if($icon) {
//            if($language->icon) $this->call(DeleteLanguageImageTask::class, [$language->icon]);
            $data['icon'] = $this->call(UploadLanguageImageTask::class, [$icon, $language->code]);
        }

        if(filled($data['is_active']) && $data['is_active'] == Language::INACTIVE && cfg('language') == $language->id) {
            return false;
        }

        return $this->call(UpdateLanguageTask::class, [$id, $data]);
    }
}
