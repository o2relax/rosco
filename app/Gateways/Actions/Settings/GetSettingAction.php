<?php

namespace App\Gateways\Actions\Settings;

use App\Contracts\Actions\Action;
use App\Repositories\SettingRepository;

/**
 * Class GetSettingAction.
 */
class GetSettingAction extends Action
{

    /**
     * @param $code
     * @return mixed
     */
    public function run($code)
    {

        return app(SettingRepository::class)->getOne($code);

    }
}
