<?php

namespace App\Gateways\Actions\Settings;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Languages\DeleteLanguageImageTask;
use App\Gateways\Tasks\Languages\UploadLanguageImageTask;
use App\Gateways\Tasks\Languages\GetLanguageByIdTask;
use App\Gateways\Tasks\Languages\UpdateLanguageTask;
use App\Models\Language;
use App\Repositories\SettingRepository;

/**
 * Class UpdateSettingsAction.
 */
class UpdateSettingsAction extends Action
{

    /**
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function run(array $data = [])
    {


        foreach($data as $key => $value) {
            if($key == 'catalog_per_page' || $key == 'catalog_order' || $key == 'catalog_sort') {
                if(cfg($key) != $value) cache()->tags('products')->flush();
            }
            app(SettingRepository::class)->save($key, $value);
        }

        return true;


    }
}
