<?php

namespace App\Gateways\Actions\Settings;

use App\Contracts\Actions\Action;
use App\Repositories\SettingRepository;

/**
 * Class GetSettingsAction.
 */
class GetSettingsAction extends Action
{

    /**
     * @param $mod
     * @return mixed
     */
    public function run($mod = null)
    {

        if($mod == 'list') {
            return app(SettingRepository::class)->cfg();
        } else {
            return app(SettingRepository::class)->getList();
        }

    }
}
