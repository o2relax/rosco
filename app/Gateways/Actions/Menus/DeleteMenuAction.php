<?php

namespace App\Gateways\Actions\Menus;

use App\Contracts\Actions\Action;
use App\Repositories\MenuRepository;

/**
 * Class DeleteMenuAction.
 */
class DeleteMenuAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(MenuRepository::class)->delete($id);
    }
}
