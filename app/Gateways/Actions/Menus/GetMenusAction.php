<?php

namespace App\Gateways\Actions\Menus;

use App\Contracts\Actions\Action;
use App\Repositories\MenuRepository;

/**
 * Class GetMenusAction.
 */
class GetMenusAction extends Action
{

    /**
     * @return mixed
     */
    public function run()
    {
        return app(MenuRepository::class)->all();
    }
}
