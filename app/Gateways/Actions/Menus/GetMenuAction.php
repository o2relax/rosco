<?php

namespace App\Gateways\Actions\Menus;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Attributes\GetAttributeByIdTask;
use App\Gateways\Tasks\Menus\GetMenuByIdTask;

/**
 * Class GetAttributeAction.
 */
class GetMenuAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetMenuByIdTask::class, [$id]);
    }
}
