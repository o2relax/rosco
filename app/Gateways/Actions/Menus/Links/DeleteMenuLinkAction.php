<?php

namespace App\Gateways\Actions\Menus\Links;

use App\Contracts\Actions\Action;
use App\Repositories\AttributeGroupRepository;
use App\Repositories\MenuLinkRepository;

/**
 * Class DeleteMenuLinkAction.
 */
class DeleteMenuLinkAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(MenuLinkRepository::class)->delete($id);
    }
}
