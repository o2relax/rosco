<?php

namespace App\Gateways\Actions\Menus\Links;

use App\Contracts\Actions\Action;
use App\Repositories\MenuLinkRepository;

/**
 * Class SortMenuLinkAction
 * @package App\Gateways\Actions\Menus\Links
 */

class SortMenuLinkAction extends Action
{

    /**
     * @param string $data
     * @return mixed
     */
    public function run($data)
    {

        $data = json_decode($data);

        if(count($data)) {
            return app(MenuLinkRepository::class)->sortLinks($data);
        } else {
            return false;
        }
    }
}
