<?php

namespace App\Gateways\Actions\Menus\Links;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Menus\GetMenuByIdTask;
use App\Repositories\MenuLinkRepository;

/**
 * Class CreateMenuLinkAction.
 */
class CreateMenuLinkAction extends Action
{

    /**
     * @param $menu_id
     * @param array $data
     * @return mixed
     */
    public function run($menu_id, array $data = [])
    {

        $data['menu_id'] = $menu_id;

        $menu = $this->call(GetMenuByIdTask::class, [$menu_id]);

        $data['sort'] = $menu->links->count() + 1;

        return app(MenuLinkRepository::class)->create($data);
    }
}
