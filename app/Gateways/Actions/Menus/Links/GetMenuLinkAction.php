<?php

namespace App\Gateways\Actions\Menus\Links;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Menus\Links\GetMenuLinkByIdTask;

/**
 * Class GetMenuLinkAction.
 */
class GetMenuLinkAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetMenuLinkByIdTask::class, [$id]);
    }
}
