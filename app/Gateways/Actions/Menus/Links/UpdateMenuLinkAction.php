<?php

namespace App\Gateways\Actions\Menus\Links;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Menus\Links\UpdateMenuLinkByIdTask;

/**
 * Class UpdateMenuLinkAction
 * @package App\Gateways\Actions\Menus\Links
 */

class UpdateMenuLinkAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {

        return $this->call(UpdateMenuLinkByIdTask::class, [$id, $data]);
    }
}
