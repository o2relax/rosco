<?php

namespace App\Gateways\Actions\Menus;

use App\Contracts\Actions\Action;
use App\Repositories\MenuRepository;

/**
 * Class CreateMenuAction.
 */
class CreateMenuAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return app(MenuRepository::class)->create($data);
    }
}
