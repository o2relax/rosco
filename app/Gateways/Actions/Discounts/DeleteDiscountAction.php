<?php

namespace App\Gateways\Actions\Discounts;

use App\Contracts\Actions\Action;
use App\Repositories\DiscountRepository;

/**
 * Class DeleteDiscountAction
 * @package App\Gateways\Actions\Discounts
 */
class DeleteDiscountAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return app(DiscountRepository::class)->delete($id);
    }
}
