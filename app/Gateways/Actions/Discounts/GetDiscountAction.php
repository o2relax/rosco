<?php

namespace App\Gateways\Actions\Discounts;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Discounts\GetDiscountByIdTask;

/**
 * Class GetDiscountAction.
 */
class GetDiscountAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetDiscountByIdTask::class, [$id]);
    }
}
