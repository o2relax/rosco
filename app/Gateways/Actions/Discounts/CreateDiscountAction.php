<?php

namespace App\Gateways\Actions\Discounts;

use App\Contracts\Actions\Action;
use App\Repositories\DiscountRepository;

/**
 * Class CreateDiscountAction
 * @package App\Gateways\Actions\Discounts
 */
class CreateDiscountAction extends Action
{

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {

        $period = explode(' - ', $data['period']);
        $data['date_start'] = $period[0];
        if (isset($period[1])) {
            $data['date_end'] = $period[1];
        }

        return app(DiscountRepository::class)->create($data);

    }
}
