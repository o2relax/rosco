<?php

namespace App\Gateways\Actions\Discounts;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Discounts\UpdateDiscountByIdTask;

/**
 * Class UpdateDiscountAction
 * @package App\Gateways\Actions\Discounts
 */
class UpdateDiscountAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function run($id, array $data = [])
    {

        $period = explode(' - ', $data['period']);
        $data['date_start'] = $period[0];
        if (isset($period[1])) {
            $data['date_end'] = $period[1];
        }

        return $this->call(UpdateDiscountByIdTask::class, [
            $id,
            $data
        ]);
    }
}
