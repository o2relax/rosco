<?php

namespace App\Gateways\Actions\Discounts;

use App\Contracts\Actions\Action;
use App\Repositories\DiscountRepository;

/**
 * Class GetDiscountsAction.
 */
class GetDiscountsAction extends Action
{

    /**
     * @param string $mod
     * @return mixed
     */
    public function run($mod = 'paginated')
    {
        return app(DiscountRepository::class)->$mod();
    }
}
