<?php

namespace App\Gateways\Actions\Cart;

use App\Contracts\Actions\Action;
use App\Repositories\ProductRepository;

/**
 * Class GetCartProductsAction.
 */
class GetCartProductsAction extends Action
{

    /**
     * @param null $mod
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function run($mod = null)
    {
            return app(ProductRepository::class)->getByIds(
                array_keys(cart()->get()),
                $mod
            );
    }
}
