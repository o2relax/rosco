<?php

namespace App\Gateways\Actions\Manufacturers;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\UploadManufacturerImageTask;

/**
 * Class UpdateManufacturerAction.
 */
class UpdateManufacturerAction extends Action
{

    /**
     * @param $id
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run($id, array $data = [], $image = null)
    {

        if($image) {
            $manufacturer = $this->call(GetManufacturerByIdTask::class, [$id]);
            if($manufacturer->image) $this->call(DeleteManufacturerImageTask::class, [$manufacturer->image]);
            $data['image'] = $this->call(UploadManufacturerImageTask::class, [$image, $manufacturer->name]);
        }

        return $this->call(UpdateManufacturerByIdTask::class, [
            $id,
            $data
        ]);
    }
}
