<?php

namespace App\Gateways\Actions\Manufacturers\Images;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Gateways\Tasks\Manufacturers\UpdateManufacturerByIdTask;

/**
 * Class DeleteManufacturerImageAction.
 */
class DeleteManufacturerImageAction extends Action
{

    /**
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {

        $manufacturer = $this->call(GetManufacturerByIdTask::class, [$id]);

        
        if (is_file(storage($manufacturer->image, 'manufacturers', 'path'))) {
            $this->call(DeleteManufacturerImageTask::class, [$manufacturer->image]);
            $manufacturer->image = null;
            $manufacturer->save();
        }

        return $manufacturer;

    }
}
