<?php

namespace App\Gateways\Actions\Manufacturers;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;
use App\Gateways\Tasks\Manufacturers\Images\DeleteManufacturerImageTask;
use App\Repositories\ManufacturerRepository;

/**
 * Class DeleteManufacturerAction.
 */
class DeleteManufacturerAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {

        $manufacturer = $this->call(GetManufacturerByIdTask::class, [$id]);
        if ($manufacturer->image) $this->call(DeleteManufacturerImageTask::class, [$manufacturer->image]);

        return app(ManufacturerRepository::class)->delete($id);
    }
}
