<?php

namespace App\Gateways\Actions\Manufacturers;

use App\Contracts\Actions\Action;
use App\Repositories\ManufacturerRepository;

/**
 * Class GetManufacturersAction.
 */
class GetManufacturersAction extends Action
{

    /**
     * @param array|null $search
     * @param string $mod
     * @return mixed
     */
    public function run(array $search = null, $mod = 'paginated')
    {
        if($search) {
            return app(ManufacturerRepository::class)->search($search);
        } else {
            return app(ManufacturerRepository::class)->$mod();
        }

    }
}
