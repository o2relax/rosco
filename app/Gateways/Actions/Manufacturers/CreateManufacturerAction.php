<?php

namespace App\Gateways\Actions\Manufacturers;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\Images\UploadManufacturerImageTask;
use App\Repositories\ManufacturerRepository;

/**
 * Class CreateManufacturerAction.
 */
class CreateManufacturerAction extends Action
{

    /**
     * @param array $data
     * @param $image
     * @return mixed
     */
    public function run(array $data = [], $image = null)
    {

        $manufacturer = app(ManufacturerRepository::class)->create($data);

        if ($image) {
            $data['image'] = $this->call(UploadManufacturerImageTask::class, [$image, $manufacturer->name]);
            return app(ManufacturerRepository::class)->update($manufacturer->id, $data);
        } else {
            return $manufacturer;
        }

    }
}
