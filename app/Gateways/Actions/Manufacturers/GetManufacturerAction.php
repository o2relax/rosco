<?php

namespace App\Gateways\Actions\Manufacturers;

use App\Contracts\Actions\Action;
use App\Gateways\Tasks\Manufacturers\GetManufacturerByIdTask;

/**
 * Class GetManufacturerAction.
 */
class GetManufacturerAction extends Action
{

    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->call(GetManufacturerByIdTask::class, [$id]);
    }
}
