<?php

namespace App\Gateways\Transformers\Products;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Intervention\Image\Facades\Image;

/**
 * Class ProductListTransformer
 * @package App\Gateways\Transformers
 */
class ProductListTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {

        if ($item->image) {
            $item->image = route('shop.image.get', ['products', $item->id, $item->image, cfg('image_list_width'), cfg('image_list_height')]);
        } else {
            $item->image = route('shop.image.not', ['products', cfg('image_list_width'), cfg('image_list_height')]);
        }
        $item->short_description = mb_substr(strip_tags($item->short_description,''), 0, cfg('product_list_description_length'));
        $item->link = route('shop.product', [$item->slug]);

        if (count($item->discounts)) {
            if ($item->currency->code == currency()->get()) {
                $item->old_price = currency()->price($item->price/$item->currency->rate);

                $prices = [];
                foreach ($item->discounts as $discount) {
                    $prices[] = currency()->discount($item->price, $discount->info);
                }

                $item->price = $item->currency->prefix . round(min($prices),$item->currency->round) . $item->currency->postfix;
            } else {
                $item->old_price = currency()->price($item->price/$item->currency->rate);

                $prices = [];
                foreach ($item->discounts as $discount) {
                    $prices[] = currency()->discount($item->price, $discount->info);
                }

                $item->price = currency()->price(round(min($prices)/$item->currency->rate,$item->currency->round));
            }
        } elseif (count($item->discountByManufacturer)) {
            if ($item->currency->code == currency()->get()) {
                $item->old_price = currency()->price($item->price/$item->currency->rate);

                $prices = [];
                foreach ($item->discountByManufacturer as $discount) {
                    $prices[] = currency()->discount($item->price, $discount->info);
                }

                $item->price = $item->currency->prefix . round(min($prices),$item->currency->round) . $item->currency->postfix;
            } else {
                $item->old_price = currency()->price($item->price/$item->currency->rate);

                $prices = [];
                foreach ($item->discountByManufacturer as $discount) {
                    $prices[] = currency()->discount($item->price, $discount->info);
                }

                $item->price = currency()->price(min($prices)/$item->currency->rate);
            }
        } else {
            if($item->currency->code == currency()->get()) {
                $item->price = $item->currency->prefix . round($item->price,$item->currency->round) . $item->currency->postfix;
                //$item->price = currency()->order($item->price,currency()->get());
                $item->old_price = null;
            } else {
                $item->price = currency()->price(round($item->price/$item->currency->rate,$item->currency->round));
                $item->old_price = null;
            }
        }

        return $item;
    }

}
