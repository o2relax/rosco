<?php

namespace App\Gateways\Transformers\Products;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;

/**
 * Class ProductTransformer
 * @package App\Gateways\Transformers
 */
class ProductTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->image = $item->image ? product_image($item->id, $item->image) : null;

        return $item;
    }

}
