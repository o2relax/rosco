<?php

namespace App\Gateways\Transformers\Manufacturers;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class ManufacturerTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->def_image = $item->image ? storage($item->image, 'manufacturers') : null;
        $item->image = $item->image ? storage($item->image, 'manufacturers') : no_image();

        return $item;
    }

}
