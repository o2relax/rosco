<?php

namespace App\Gateways\Transformers\Blogs;

use App\Contracts\Transformers\Transformer;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class BlogListTransformer extends Transformer
{

    /**
     * @param $item
     *
     * @return object
     */
    public function transform($item)
    {
        if (!$item->seo_title) $item->seo_title = $item->title;
        $item->image = $item->image ? blog_image($item->id, $item->image) : no_image();
        if (!$item->short_description) $item->short_description = str_limit(strip_tags($item->description));
        $item->link = route('shop.blog.post', [$item->slug]);

        return $item;
    }

}
