<?php

namespace App\Gateways\Transformers\Blogs;

use App\Contracts\Transformers\Transformer;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class BlogTransformer extends Transformer
{

    /**
     * @param $item
     *
     * @return object
     */
    public function transform($item)
    {
        if(!$item->seo_title) $item->seo_title = $item->title;
        $item->def_image = $item->image ? blog_image($item->id, $item->image) : null;
        $item->image = $item->image ? blog_image($item->id, $item->image) : no_image();

        return $item;
    }

}
