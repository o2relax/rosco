<?php

namespace App\Gateways\Transformers\Shipments;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class ShipmentTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->def_image = $item->image ? storage($item->image, 'shipments') : null;
        $item->image = $item->image ? storage($item->image, 'shipments') : no_image();

        return $item;
    }

}
