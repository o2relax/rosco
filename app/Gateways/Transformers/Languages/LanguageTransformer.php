<?php

namespace App\Gateways\Transformers\Languages;

use App\Contracts\Transformers\Transformer;
use App\Models\Language;

/**
 * Class LanguageTransformer
 * @package App\Gateways\Transformers
 */
class LanguageTransformer extends Transformer
{

    /**
     * @param Language $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->icon = storage($item->icon, 'languages');

        return $item;
    }

}
