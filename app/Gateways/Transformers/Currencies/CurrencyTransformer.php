<?php

namespace App\Gateways\Transformers\Currencies;

use App\Contracts\Transformers\Transformer;
use App\Models\Language;

/**
 * Class CurrencyTransformer
 * @package App\Gateways\Transformers
 */
class CurrencyTransformer extends Transformer
{

    /**
     * @param Language $item
     *
     * @return object
     */
    public function transform($item)
    {
        return $item;
    }

}
