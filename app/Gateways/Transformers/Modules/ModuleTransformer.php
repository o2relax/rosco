<?php

namespace App\Gateways\Transformers\Modules;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class ModuleTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->data = json_decode($item->data, true);

        return $item;
    }

}
