<?php

namespace App\Gateways\Transformers\Settings;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
/**
 * Class ProductListTransformer
 * @package App\Gateways\Transformers
 */
class SettingTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        if(mb_strpos($item->type, 'langs') !== false) {
            $item->value = json_decode($item->value, true);
        }

        return $item;
    }

}
