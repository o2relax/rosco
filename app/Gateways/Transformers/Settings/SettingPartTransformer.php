<?php

namespace App\Gateways\Transformers\Settings;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
/**
 * Class ProductListTransformer
 * @package App\Gateways\Transformers
 */
class SettingPartTransformer extends Transformer
{

    protected $relations = [
        'settings'
    ];

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        return $item;
    }

    public function relationSettings($item) {

        return $this->item($item, SettingTransformer::class);

    }

}
