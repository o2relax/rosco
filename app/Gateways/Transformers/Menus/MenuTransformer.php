<?php

namespace App\Gateways\Transformers\Menus;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class MenuTransformer extends Transformer
{

    protected $relations = [
        'links'
    ];

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        return $item;
    }

    public function relationLinks($item) {

        return $this->item($item, MenuLinkTransformer::class);

    }

}
