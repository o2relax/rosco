<?php

namespace App\Gateways\Transformers\Menus;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class MenuLinkTransformer extends Transformer
{

    protected $relations = [
        'links'
    ];

    /**
     * @param Product $item
     *
     * @return object
     */

    public function transform($item) {

        $item->url = url($item->url ? $item->url : '#');

        return $item;

    }

}
