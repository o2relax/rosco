<?php

namespace App\Gateways\Transformers\Orders;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class OrderTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->date = Carbon::parse($item->created_at)->toFormattedDateString();
        //$item->total = currency()->order($item->items->sum('price'), $item->currency);
        foreach ($item->items as $q) {
            $item->total += $q->price*$q->quantity;
        }
        $item->total = currency()->order($item->total,$item->currency);
        return $item;
    }

}
