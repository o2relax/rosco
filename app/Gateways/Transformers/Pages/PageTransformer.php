<?php

namespace App\Gateways\Transformers\Pages;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class PageTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        if(!$item->seo_title) $item->seo_title = $item->title;

        return $item;
    }

}
