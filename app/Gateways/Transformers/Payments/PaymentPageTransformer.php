<?php

namespace App\Gateways\Transformers\Payments;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class PaymentPageTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->image = route('shop.image.get', ['payments', 0, $item->image, config('image.shipment.width'), config('image.shipment.height')]);

        return $item;
    }

}
