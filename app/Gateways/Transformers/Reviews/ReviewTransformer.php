<?php

namespace App\Gateways\Transformers\Reviews;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class ReviewTransformer
 * @package App\Gateways\Transformers\Pages
 */
class ReviewTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->date = Carbon::parse($item->created_at)->toFormattedDateString();
        return $item;
    }

}
