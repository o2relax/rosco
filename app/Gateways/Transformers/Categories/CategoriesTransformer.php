<?php

namespace App\Gateways\Transformers\Categories;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class CategoriesTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->short = str_limit($item->description, 100);
        $item->date = Carbon::parse($item->created_at)->toFormattedDateString();
        $item->image = $item->image ? category_image($item->id, $item->image) : no_image('svg');
        $item->color = $item->color ? $item->color : '#8bac53';
        $item->link = route('shop.category', [$item->slug]);

        return $item;
    }

}
