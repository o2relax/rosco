<?php

namespace App\Gateways\Transformers\Categories;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class CategoryListTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->link = route('shop.category', [$item->slug]);
        $item->image = $item->image ? category_image($item->id, $item->image) : no_image('svg');
        $item->color = $item->color ? $item->color : '#8bac53';

        return $item;
    }

}
