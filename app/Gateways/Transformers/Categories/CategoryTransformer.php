<?php

namespace App\Gateways\Transformers\Categories;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class CategoryTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {
        $item->short = str_limit($item->description, 100);
        $item->date = Carbon::parse($item->created_at)->toFormattedDateString();
        $item->image = $item->image ? storage($item->id . DIRECTORY_SEPARATOR . $item->image, 'categories') : null;
        $item->link = route('shop.category', [$item->slug]);
        $item->color = $item->color ? $item->color : '#8bac53';

        return $item;
    }

}
