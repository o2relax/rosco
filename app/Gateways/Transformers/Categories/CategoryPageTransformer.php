<?php

namespace App\Gateways\Transformers\Categories;

use App\Contracts\Transformers\Transformer;
use App\Models\Product;
use Carbon\Carbon;

/**
 * Class CategoryTransformer
 * @package App\Gateways\Transformers
 */
class CategoryPageTransformer extends Transformer
{

    /**
     * @param Product $item
     *
     * @return object
     */
    public function transform($item)
    {

        $item->seo_title = $item->seo_title ? $item->seo_title : $item->name;

        return $item;
    }

}
