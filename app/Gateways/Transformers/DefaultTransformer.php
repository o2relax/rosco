<?php

namespace App\Gateways\Transformers;

use App\Contracts\Transformers\Transformer;

/**
 * Class DefaultTransformer
 * @package App\Gateways\Transformers
 */
class DefaultTransformer extends Transformer
{

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @var  array
     */
    protected $defaultIncludes = [
    ];

    /**
     * @param array $data
     *
     * @return array
     */
    public function transform(array $data = [])
    {
        return $data;
    }
}
