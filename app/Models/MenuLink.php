<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class MenuLink extends BaseModel
{

    use TranslatableTrait;

    const PARENT = 0;

    protected $fillable = [
        'menu_id',
        'parent_id',
        'url',
        'sort',
        'is_active'
    ];

    public $translatedAttributes = [
        'title'
    ];

    public function menu() {
        return $this->belongsTo(Menu::class, 'menu_id', 'id');
    }

    public function child() {
        return app(MenuLink::class)->where('menu_id', $this->menu_id)->where('parent_id', $this->id)->orderBy(MenuLink::ORDER, MenuLink::SORT)->get();
    }

}
