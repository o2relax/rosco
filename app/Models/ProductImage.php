<?php

namespace App\Models;

class ProductImage extends BaseModel
{

    protected $fillable = [
        'product_id',
        'image',
        'main',
    ];

    public function link() {
        return product_image($this->product_id, $this->image);
    }

}
