<?php

namespace App\Models;

class Setting extends BaseModel
{

    protected $fillable = [
        'part_id',
        'name',
        'code',
        'value',
        'options',
        'type',
        'sort'
    ];

    protected $source = false;

    public function part()
    {
        return $this->hasOne(SettingPart::class, 'id', 'part_id');
    }

    public function source()
    {

        if (!$this->relation) return null;

        if(blank($this->source[$this->id])) {
            $source = new $this->relation;
            $this->source[$this->id] = $source->where('id', $this->value)->first();
        }

        return $this->source[$this->id];

    }

}