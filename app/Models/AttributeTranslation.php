<?php

namespace App\Models;

class AttributeTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'description',
    ];

    public $timestamps = false;

}
