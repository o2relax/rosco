<?php

namespace App\Models;

class Currency extends BaseModel
{

    protected $fillable = [
        'name',
        'code',
        'prefix',
        'postfix',
        'rate',
        'round',
        'is_active'
    ];

    const ACTIVE = 1;
    const INACTIVE = 0;

}