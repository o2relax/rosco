<?php

namespace App\Models;

class PageTranslation extends BaseModel
{

    protected $fillable = [
        'title',
        'description',
        'seo_h1',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public $timestamps = false;

}
