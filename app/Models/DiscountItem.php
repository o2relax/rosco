<?php

namespace App\Models;


class DiscountItem extends BaseModel
{

    public $fillable = [
        'discount_id',
        'product_id',
        'manufacturer_id',
    ];

    public function info() {
        return $this->belongsTo(Discount::class, 'discount_id', 'id');
    }

}
