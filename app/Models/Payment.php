<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class Payment extends BaseModel
{

    use TranslatableTrait;

    public $fillable = [
        'code',
        'currency_id',
        'is_active',
        'image'
    ];

    public $translatedAttributes = [
        'name',
        'description'
    ];

    public function currency() {
        return $this->belongsTo(Currency::class);
    }

    public function shipments()
    {
        return $this
            ->belongsToMany(Shipment::class, 'payment_to_shipments', 'payment_id', 'shipment_id')
            ->using(PaymentToShipment::class);
    }

}
