<?php

namespace App\Models;

class ShipmentTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'description'
    ];

    public $timestamps = false;

}
