<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class Attribute extends BaseModel
{

    use TranslatableTrait;

    public $fillable = [
        'attribute_group_id',
        'sort',
    ];

    public $translatedAttributes = [
        'name',
        'description',
    ];

    public function group() {

        return $this->belongsTo(AttributeGroup::class, 'attribute_group_id', 'id');

    }
}
