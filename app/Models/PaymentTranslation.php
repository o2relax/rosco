<?php

namespace App\Models;

class PaymentTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'description'
    ];

    public $timestamps = false;

}
