<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;


class PaymentToShipment extends Pivot
{
    public $timestamps = false;

    protected $fillable = [
        'payment_id',
        'shipment_id'
    ];

    public function payment() {
        return $this->belongsTo(Payment::class);
    }

    public function shipment() {
        return $this->belongsTo(Shipment::class);
    }
}
