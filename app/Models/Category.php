<?php

namespace App\Models;

use App\Services\Site\FileService;
use App\Services\Shop\CartService;
use App\Services\Shop\ProductService;
use App\Traits\TranslatableTrait;

class Category extends BaseModel
{

    use TranslatableTrait;

    const MAIN_CATEGORY = 0;

    public $fillable = [
        'parent_id',
        'is_active',
        'slug',
        'image',
        'sort',
        'color'
    ];

    public $translatedAttributes = [
        'name',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public function child() {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parent() {
        return $this->hasMany(Category::class, 'id', 'parent_id');
    }

}
