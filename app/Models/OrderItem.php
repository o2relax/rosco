<?php

namespace App\Models;

class OrderItem extends BaseModel
{

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price'
    ];

    public function order() {
        return $this->belongsTo(OrderItem::class, 'order_id', 'id');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

}
