<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{

    use \Laravel\Cashier\Billable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'phone',
        'is_active',
        'language',
        'currency',
        'activation_token',
    ];

    /**
     * User
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
