<?php

namespace App\Models;

class Menu extends BaseModel
{

    protected $fillable = [
        'code',
        'name',
        'is_active'
    ];

    public function parents() {
        return app(MenuLink::class)->where('menu_id', $this->id)->where('parent_id', MenuLink::PARENT)->orderBy(MenuLink::ORDER, MenuLink::SORT)->get();
    }

    public function links() {
        return $this->hasMany(MenuLink::class, 'menu_id', 'id');
    }
}
