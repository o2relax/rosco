<?php

namespace App\Models;

class CategoryTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public $timestamps = false;

}
