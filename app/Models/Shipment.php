<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class Shipment extends BaseModel
{

    use TranslatableTrait;

    public $fillable = [
        'code',
        'price',
        'free_from',
        'is_active',
        'image'
    ];

    public $translatedAttributes = [
        'name',
        'description'
    ];

    public function payments()
    {
        return $this
            ->belongsToMany(Payment::class, 'payment_to_shipments', 'shipment_id', 'payment_id')
            ->using(PaymentToShipment::class);
    }

}
