<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class ProductAttribute extends BaseModel
{

    use TranslatableTrait;

    protected $fillable = [
        'product_id',
        'attribute_id'
    ];

    public $translatedAttributes = [
        'text'
    ];

    public $timestamps = false;

    public function attribute() {
        return $this->belongsTo(Attribute::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }


}
