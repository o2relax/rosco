<?php

namespace App\Models;

class ProductStat extends BaseModel
{

    public $fillable = [
        'views',
        'orders',
        'carts',
        'product_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
