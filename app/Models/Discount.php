<?php

namespace App\Models;


class Discount extends BaseModel
{

    public $fillable = [
        'name',
        'value',
        'type',
        'date_start',
        'date_end',
        'is_active'
    ];

    public function items() {
        return $this->hasMany(DiscountItem::class);
    }

}
