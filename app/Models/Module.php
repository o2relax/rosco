<?php

namespace App\Models;

class Module extends BaseModel
{

    protected $fillable = [
//        'name',
//        'code',
        'data',
        'is_active',
        'use_cache'
    ];

    const ACTIVE = 1;
    const INACTIVE = 0;

}