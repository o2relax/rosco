<?php

namespace App\Models;

class SettingPart extends BaseModel
{

    protected $fillable = ['name'];

    public $timestamps = false;

    public function settings() {
        return $this->hasMany(Setting::class, 'part_id', 'id');
    }

}
