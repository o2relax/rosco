<?php

namespace App\Models;


class Subscription extends BaseModel
{

    public $fillable = [
        'email',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'email', 'email');
    }

}
