<?php

namespace App\Models;

class ProductAttributeTranslation extends BaseModel
{

    protected $fillable = [
        'text'
    ];

    public $timestamps = false;

}
