<?php

namespace App\Models;

class ProductTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'short_description',
        'full_description',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public $timestamps = false;

}
