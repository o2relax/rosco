<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class AttributeGroup extends BaseModel
{

    use TranslatableTrait;

    public $fillable = [
        'sort',
    ];

    public $translatedAttributes = [
        'name',
        'description',
    ];

    public function attributes() {

        return $this->hasMany(Attribute::class);

    }
}
