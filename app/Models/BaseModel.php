<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class BaseModel extends Model
{

    use Rememberable;

    const ACTIVE = 1;
    const INACTIVE = 0;

    const ORDER = 'sort';
    const SORT = 'ASC';

}
