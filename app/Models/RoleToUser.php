<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;


class RoleToUser extends Pivot
{
    public $timestamps = false;

    public $table = 'role_to_user';

    protected $fillable = [
        'role_id',
        'user_id'
    ];

    public function role() {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
