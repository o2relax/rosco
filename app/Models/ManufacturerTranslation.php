<?php

namespace App\Models;

class ManufacturerTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'short_description',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public $timestamps = false;

}
