<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class Status extends BaseModel
{

    use TranslatableTrait;

    protected $fillable = [
        'code',
        'name',
        'type'
    ];

    public $translatedAttributes = [
        'name',
        'description'
    ];


}
