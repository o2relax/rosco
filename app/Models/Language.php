<?php

namespace App\Models;

class Language extends BaseModel
{

    protected $fillable = [
        'name',
        'code',
        'is_active',
        'icon'
    ];

    const ACTIVE = 1;
    const INACTIVE = 0;

}
