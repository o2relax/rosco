<?php

namespace App\Models;

class StatusTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'description',
    ];

    public $timestamps = false;

}
