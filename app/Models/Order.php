<?php

namespace App\Models;

class Order extends BaseModel
{

    const VIEWED = 1;

    protected $fillable = [
        'user_id',
        'name',
        'email',
        'address',
        'phone',
        'shipment_id',
        'payment_id',
        'currency_id',
        'status_id',
        'viewed'
    ];

    const NEW = 4;

    public function items() {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function currency() {
        return $this->belongsTo(Currency::class);
    }

    public function status()
    {
        return app(Status::class)->where('id', $this->status_id)->where('type', 'Order')->withTranslation()->first();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function shipment() {
        return $this->belongsTo(Shipment::class);
    }
    public function payment() {
        return $this->belongsTo(Payment::class);
    }


}
