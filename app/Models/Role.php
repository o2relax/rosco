<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'label',
        'permissions',
        'style'
    ];

    /**
     * A Roles users
     *
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'role_to_user', 'role_id', 'user_id')->using(RoleToUser::class);
    }

    /**
     * Find a role by name
     *
     * @param  string $name
     * @return Role
     */
    public static function findByName($name)
    {
        return Role::where('name', $name)->firstOrFail();
    }
}
