<?php

namespace App\Models;

class AttributeGroupTranslation extends BaseModel
{

    protected $fillable = [
        'name',
        'description',
    ];

    public $timestamps = false;

}
