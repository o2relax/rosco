<?php

namespace App\Models;

class MenuLinkTranslation extends BaseModel
{

    protected $fillable = [
        'menu_link_id',
        'title'
    ];

    public $timestamps = false;

}
