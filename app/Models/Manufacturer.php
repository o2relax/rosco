<?php

namespace App\Models;

use App\Services\Site\FileService;
use App\Services\Shop\CartService;
use App\Services\Shop\ProductService;
use App\Traits\TranslatableTrait;

class Manufacturer extends BaseModel
{

    use TranslatableTrait;

    public $fillable = [
        'slug',
        'image',
        'is_active'
    ];

    public $translatedAttributes = [
        'name',
        'short_description',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    const ACTIVE = 1;
    const INACTIVE = 0;

}
