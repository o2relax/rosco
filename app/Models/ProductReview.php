<?php

namespace App\Models;

class ProductReview extends BaseModel
{

    public $fillable = [
        'user_id',
        'name',
        'text',
        'product_id',
        'is_active',
        'created_at',
        'viewed'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
