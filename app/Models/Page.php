<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class Page extends BaseModel
{

    use TranslatableTrait;

    public $fillable = [
        'slug',
        'is_active'
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'seo_h1',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

}
