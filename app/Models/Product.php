<?php

namespace App\Models;

use App\Traits\TranslatableTrait;

class Product extends BaseModel
{

    use TranslatableTrait;

    const UNSTOCK = 0;

    public $table = 'products';

    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = [
        'manufacturer_id',
        'category_id',
        'slug',
        'article',
        'substance',
        'price',
        'image',
        'stock',
        'is_published',
        'is_related',
        'is_latest',
        'is_popular',
        'status_id',
        'showed_at',
        'use_stock',
        'currency_id',
    ];

    public $translatedAttributes = [
        'name',
        'short_description',
        'full_description',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function status()
    {
        return app(Status::class)->where('id', $this->status_id)->where('type', 'Product')->withTranslation()->first();
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    public function stats()
    {
        return $this->hasOne(ProductStat::class);
    }

    public function translation()
    {
        return $this->hasOne(ProductTranslation::class)->where('lang_code', app()->getLocale());
    }

    public function discountByManufacturer()
    {
        return $this->hasMany(DiscountItem::class, 'manufacturer_id', 'manufacturer_id');
    }

    public function discounts()
    {
        return $this->hasMany(DiscountItem::class);
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }

    public function scopePopular($query)
    {
        return $query->where('is_popular', 1);
    }

    public function scopeLatest($query)
    {
        return $query->where('is_latest', 1);
    }

    public function scopePublished($query)
    {
        return $query->where('is_published', self::ACTIVE);
    }

    public function scopeRelated($query)
    {
        return $query->where('is_related', 1);
    }

}
