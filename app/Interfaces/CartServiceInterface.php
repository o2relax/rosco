<?php

namespace App\Interfaces;

interface CartServiceInterface
{
    /**
     * CartServiceInterface constructor.
     */
    public function __construct();

    /**
     * @return int
     */
    public function count();

    /**
     * @param $id
     * @return bool
     */
    public function check($id);

    /**
     * @return float
     */
    public function total();

    /**
     * @return mixed
     */
    public function get();

    /**
     * @param $product_id
     * @param int $quantity
     * @return mixed
     */
    public function add($product_id, $quantity = 1);

    /**
     * @param $product_id
     * @return mixed
     */
    public function delete($product_id);

    /**
     * @param $product_id
     * @param int $quantity
     * @return mixed
     */
    public function quantity($product_id, $quantity = 1);

    /**
     * @param $product
     * @param string $class
     * @return mixed
     */
    public function addToCartBtn($product, $class = '');

    /**
     * @param $product
     * @param string $class
     * @return mixed
     */
    public function deleteFromCartBtn($product, $class = '');
}
