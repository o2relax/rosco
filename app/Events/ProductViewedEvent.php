<?php

namespace App\Events;

use App\Models\Product;
use App\Traits\CallableTrait;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductViewedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels, CallableTrait;

    public $data;
    public $product;

    /**
     * ProductViewedEvent constructor.
     * @param $product
     * @param array $data
     */
    public function __construct($product, array $data = [])
    {
        $this->data = $data;
        $this->product = $product;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
