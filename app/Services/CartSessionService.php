<?php

namespace App\Services;

use App\Contracts\Cart\Cart;
use App\Gateways\Actions\Cart\GetCartProductsAction;
use App\Traits\CallableTrait;
use App\Gateways\Transformers\Cart\CartListTransformer;

/**
 * Class CartService
 * @package App\Services
 */
class CartSessionService extends Cart
{

    use CallableTrait;

    protected $count = null;
    protected $total = null;

    public function __construct()
    {
        if (!session()->has('cart')) session()->put('cart', []);
    }

    /**
     * @return int
     */

    public function count()
    {

        if($this->count) return $this->count;

        $this->count = 0;

        foreach($this->get() as $item) {
            $this->count += $item['quantity'];
        }

        return $this->count;
    }

    /**
     * @param $id
     * @return bool
     */
    public function check($id) {
        return array_key_exists($id, $this->get());
    }

    /**
     * @return float
     */

    public function total()
    {

        $cart = $this->get();

        $products = $this->call(GetCartProductsAction::class, []);

        foreach($products as $product) {
            $this->total = $this->total + ((app(CartListTransformer::class)->transform($product))->price * $cart[$product->id]['quantity']);
        }

        return $this->total;
    }

    /**
     * @return mixed
     */

    public function get()
    {
        return session()->get('cart', []);
    }

    /**
     * @param $product_id
     * @param int $quantity
     * @return bool|mixed
     */

    public function add($product_id, $quantity = 1)
    {

        $cart = $this->get();

        if (array_key_exists($product_id, $cart)) {
            $cart[$product_id] = [
                'quantity' => $cart[$product_id]['quantity'] + $quantity
            ];
        } else {
            $cart[$product_id] = [
                'quantity' => $quantity
            ];
        }

        $this->count = null;

        session()->put('cart', $cart);

        $this->count();

        return true;

    }

    /**
     * @param $product_id
     * @param int $quantity
     * @return bool|mixed
     */

    public function quantity($product_id, $quantity = 1)
    {

        $cart = $this->get();

        $cart[$product_id] = [
            'quantity' => $quantity
        ];

        $this->count = null;

        session()->put('cart', $cart);

        $this->count();

        return true;

    }

    /**
     * @param $product_id
     * @return bool|mixed
     */

    public function delete($product_id)
    {

        $cart = $this->get();

        unset($cart[$product_id]);

        $this->count = null;

        session()->put('cart', $cart);

        $this->count();

        return true;

    }

    public function clear() {

        session()->forget('cart');

        return true;

    }

}
