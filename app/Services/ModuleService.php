<?php

namespace App\Services;

use App\Gateways\Actions\Modules\GetModuleAction;
use App\Traits\CallableTrait;

/**
 * Class SharingService | for sharing variables to app
 * @package App\Services
 */
class ModuleService
{

    use CallableTrait;

    public $module_info = null;

    /**
     * ModuleService constructor.
     * @param null $code
     */
    public function __construct($code = null)
    {
        if($code) {
            $this->module_info = $this->call(GetModuleAction::class, [$code]);
        }
    }

    /**
     * @return array
     */
    public function run()
    {
        return $this->response();
    }

    /**
     * @return mixed|null
     */
    public function config()
    {
        return isset($this->module_info->data) ? json_decode($this->module_info->data, true) : null;
    }

    /**
     * @param array $data
     * @return array
     */
    public function response($data = [])
    {
        return [
            'data' => $data,
            'config' => $this->config(),
            'code' => isset($this->module_info) ? $this->module_info->code : null,
            'use_cache' => $this->useCache(),
            'is_active' => $this->isActive()
        ];

    }

    /**
     * @return bool
     */
    public function useCache()
    {
        return isset($this->module_info) ? boolval($this->module_info->use_cache) : false;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return isset($this->module_info) ? boolval($this->module_info->is_active) : false;
    }

}
