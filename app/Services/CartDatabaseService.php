<?php

namespace App\Services;

use App\Contracts\Cart\Cart;

/**
 * Class CartService
 * @package App\Services
 */

class CartDatabaseService extends Cart
{

    protected $total = null;

    public function __construct()
    {
        if (!session()->has('cart')) session()->put('cart', []);
    }

    /**
     * @return int
     */

    public function count()
    {

        if($this->total) return $this->total;

        $this->total = 0;

        foreach($this->get() as $item) {
            $this->total += $item['quantity'];
        }

        return $this->total;
    }

    /**
     * @return mixed
     */

    public function get()
    {
        return session()->get('cart', []);
    }

    /**
     * @param $product_id
     * @param int $quantity
     * @return mixed|void
     */

    public function add($product_id, $quantity = 1)
    {

        $cart = $this->get();

        if (array_key_exists($product_id, $cart)) {
            $cart[$product_id] = [
                'quantity' => $cart[$product_id]['quantity'] + $quantity
            ];
        } else {
            $cart[$product_id] = [
                'quantity' => $quantity
            ];
        }

        $this->total = null;

        $this->count();

        session()->put('cart', $cart);

    }

    /**
     * @param $product_id
     * @param int $quantity
     */

    public function quantity($product_id, $quantity = 1)
    {

        $cart = $this->get();

        $cart[$product_id] = [
            'quantity' => $quantity
        ];

        $this->total = null;

        $this->count();

        session()->put('cart', $cart);

    }

    /**
     * @param $product_id
     * @return mixed|void
     */

    public function delete($product_id)
    {

        $cart = $this->get();

        unset($cart[$product_id]);

        $this->total = null;

        $this->count();

        session()->put('cart', $cart);

    }

}
