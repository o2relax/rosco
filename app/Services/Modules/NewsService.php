<?php

namespace App\Services\Modules;

use App\Gateways\Actions\Blogs\Custom\GetCustomBlogsAction;
use App\Gateways\Transformers\Blogs\BlogListTransformer;
use App\Services\ModuleService;
use App\Traits\ResponseTrait;

/**
 * Class SharingService | for sharing variables to app
 * @package App\Services
 */
class NewsService extends ModuleService
{

    use ResponseTrait;

    /**
     * @return array
     */

    public function run()
    {

        $posts = $this->call(GetCustomBlogsAction::class, [3]);

        return $this->response(
            $this->transform($posts, BlogListTransformer::class)
        );

    }

}
