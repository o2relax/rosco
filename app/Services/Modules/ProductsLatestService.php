<?php

namespace App\Services\Modules;

use App\Gateways\Actions\Products\Custom\GetProductListAction;
use App\Gateways\Transformers\Products\ProductListTransformer;
use App\Services\ModuleService;
use App\Traits\ResponseTrait;

/**
 * Class SharingService | for sharing variables to app
 * @package App\Services
 */
class ProductsLatestService extends ModuleService
{

    use ResponseTrait;

    /**
     * @return array
     */

    public function run()
    {

        $products = $this->call(GetProductListAction::class, [null, 'getLatest', 9]);

        return $this->response(
            $this->transform($products, ProductListTransformer::class)
        );

    }

}
