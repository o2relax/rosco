<?php

namespace App\Services\Modules;

use App\Gateways\Actions\Categories\GetCategoryListAction;
use App\Gateways\Actions\Categories\TreeCategoryAction;
use App\Gateways\Transformers\Categories\CategoryListTransformer;
use App\Services\ModuleService;
use App\Traits\ResponseTrait;

/**
 * Class SharingService | for sharing variables to app
 * @package App\Services
 */
class CategoryBlocksService extends ModuleService
{

    use ResponseTrait;

    /**
     * @return array
     */

    public function run()
    {

        $categories = $this->call(GetCategoryListAction::class, ['getParents']);

        return $this->response(
            $this->transform($categories, CategoryListTransformer::class)
        );

    }

}
