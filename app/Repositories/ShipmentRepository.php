<?php

namespace App\Repositories;

use App\Models\Shipment;

class ShipmentRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(Shipment $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * @param int $per_page
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function paginated($per_page = 25, $sort = 'created_at', $order = 'desc')
    {
        return $this->model->with(['payments' => function($q) {
            $q->withTranslation();
        }])->withTranslation()->orderBy($sort, $order)->paginate($per_page);
    }

    /**
     * Stores manufacturer into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {

        $data = $this->checkData($data);

        $model = parent::create(array_only($data, $this->fillable));

        $model->fill(array_except($data, $this->fillable));

        $model->save();

        if (isset($data['payments'])) {
            foreach ($data['payments'] as $payment) {
                $model->payments()->attach(['payment_id' => $payment]);
            }
        }

        return $model;
    }

    /**
     * Updates manufacturer in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {

        $data = $this->checkData($data);

        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();
        $model->payments()->detach();
        if (isset($data['payments'])) {
            foreach ($data['payments'] as $payment) {
                $model->payments()->attach(['payment_id' => $payment]);
            }
        }

        return $model;
    }

    /**
     * delete manufacturer.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->deleteTranslations();
        $model->payments()->detach();
        $model->delete();

        return $model;
    }

    public function getList() {
        return $this->model
            ->withTranslation()
            ->where('is_active', Shipment::ACTIVE)
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags('shipments')
            ->get();
    }

}
