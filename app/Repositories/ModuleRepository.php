<?php

namespace App\Repositories;

use App\Models\Module;

class ModuleRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(Module $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * @param $code
     * @param bool $isActive
     * @return mixed
     */
    public function getByCode($code, $isActive = true)
    {
        $module = $this->model->where('code', $code);

        if ($isActive) {
            $module->where('is_active', Module::ACTIVE);
        }

        return $module->first();
    }

    /**
     * @param int $per_page
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function paginated($per_page = 10, $sort = 'name', $order = 'asc')
    {
        return $this->model->orderBy($sort, $order)->paginate($per_page);
    }

    /**
     * @param $code
     * @param array $data
     * @return mixed
     */
    public function updateModule($code, array $data = [])
    {
        $module = $this->getByCode($code, false);
        $module->fill(array_only($data, $this->fillable));
        $module->save();

        return $module;
    }

}
