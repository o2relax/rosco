<?php

namespace App\Repositories;

use App\Models\Setting;
use App\Models\SettingPart;

class SettingRepository extends BaseRepository
{

    protected $fillable;

    public $part;

    public function __construct(Setting $model, SettingPart $part)
    {
        $this->fillable = $model->getFillable();
        $this->part = $part;
        return parent::__construct($model);
    }

    public function cfg()
    {
        return $this->model->select(['code', 'value', 'relation'])->get();
    }

    public function getList()
    {
        return $this->part->with(['settings' => function($q) {
            $q->orderBy(SettingPart::ORDER, SettingPart::SORT);
        }])->get();
    }

    public function getOne($code)
    {
        return $this->model->where('code', $code)->first();
    }

    public function save($code, $value)
    {
        $setting = $this->getOne($code);
        if(is_array($value)) {
            $setting->value = json_encode($value);
        } else {
            $setting->value = $value;
        }
        $setting->save();

        return $setting;
    }

}
