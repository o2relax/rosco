<?php

namespace App\Repositories;

use App\Models\Subscription;

class SubscriptionRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(Subscription $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * delete manufacturer.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->delete();

        return $model;
    }

}
