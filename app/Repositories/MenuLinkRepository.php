<?php

namespace App\Repositories;

use App\Models\MenuLink;

class MenuLinkRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(MenuLink $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * Stores category into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {

        $model = parent::create(array_only($data, $this->fillable));

        $model->fill(array_except($data, $this->fillable));

        $model->save();

        return $model;
    }

    /**
     * Updates category in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {

        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * delete category.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->deleteTranslations();
        $model->delete();

        return $model;
    }

    public function sortLinks(array $arData = []) {

        $sort = 0;

        if(count($arData)) {
            foreach ($arData as $parent) {
                $item = $this->model->find($parent->id);
                $item->update(['parent_id' => 0,'sort' => $sort]);
                if(isset($parent->children)) {
                    foreach($parent->children as $child) {
                        $item2 = $this->model->find($child->id);
                        $item2->update(['parent_id' => $parent->id, 'sort' => $sort]);
                        $sort++;
                    }
                }
                $sort++;
            }
            return true;
        } else return false;

    }

}
