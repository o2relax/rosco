<?php

namespace App\Repositories;

use App\Models\ProductAttribute;

class ProductAttributeRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(ProductAttribute $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    public function remove($attribute_id, $product_id) {

        $attribute = $this->model->where('attribute_id', $attribute_id)->where('product_id', $product_id)->firstOrFail();

        $product = $attribute->product;

        $attribute->deleteTranslations();
        $attribute->delete();

        return $product;

    }

    public function getByProduct($product_id) {
        return $this->model
            ->withTranslation()
            ->where('product_id', $product_id)
            ->with(['attribute' => function($q) {
                $q->withTranslation()->with(['group'])->orderBy(ProductAttribute::ORDER, ProductAttribute::SORT);
            }])
            ->get();
    }


}
