<?php

namespace App\Repositories;

use App\Models\Discount;

class DiscountRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(Discount $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * @param int $per_page
     * @param string $sort
     * @param string $order
     * @return mixed
     */

    public function paginated($per_page = 25, $sort = 'created_at', $order = 'desc')
    {
        return $this->model->with(['items'])->orderBy($sort, $order)->paginate($per_page);
    }

    /**
     * Stores manufacturer into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {

        $data = $this->checkData($data);

        if(!isset($data['type'])) {
            $data['type'] = '';
        }

        $model = parent::create(array_only($data, $this->fillable));

        $model->fill(array_except($data, $this->fillable));

        $model->save();

        if(isset($data['manufacturers']) && count($data['manufacturers'])) {
            foreach($data['manufacturers'] as $key) {
                $model->items()->create([
                    'manufacturer_id' =>  $key
                ]);
            }
        }

        if(isset($data['products']) && count($data['products'])) {
            foreach($data['products'] as $key) {
                $model->items()->create([
                    'product_id' =>  $key
                ]);
            }
        }

        return $model;
    }

    /**
     * Updates manufacturer in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {

        $data = $this->checkData($data);

        if(!isset($data['type'])) {
            $data['type'] = '';
        }

        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();

        $model->items()->delete();

        if(isset($data['manufacturers']) && count($data['manufacturers'])) {
            foreach($data['manufacturers'] as $key) {
                $model->items()->create([
                   'manufacturer_id' =>  $key
                ]);
            }
        }

        if(isset($data['products']) && count($data['products'])) {
            foreach($data['products'] as $key) {
                $model->items()->create([
                    'product_id' =>  $key
                ]);
            }
        }

        return $model;
    }

    /**
     * delete manufacturer.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->items()->delete();
        $model->delete();

        return $model;
    }

    /**
     * @return mixed
     */

    public function getList() {
        return $this->model
            ->where('is_active', Discount::ACTIVE)
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags('discounts')
            ->get();
    }

}
