<?php

namespace App\Repositories;

use App\Models\ProductReview;

class ProductReviewRepository extends BaseRepository
{

    protected $fillable;
    protected $translatedAttributes;

    /**
     * ProductStatRepository constructor.
     * @param ProductReview $model
     */
    public function __construct(ProductReview $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * @param int $per_page
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function paginated($per_page = 25, $sort = 'created_at', $order = 'desc')
    {
        return $this->model->with(['user', 'product'])->orderBy($sort, $order)->paginate($per_page);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getById($id)
    {
        return $this->model->with(['user', 'product'])->where('id', $id)->first();
    }

}
