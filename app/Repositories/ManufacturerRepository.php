<?php

namespace App\Repositories;

use App\Models\Manufacturer;

class ManufacturerRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(Manufacturer $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * @param string $sort
     * @param string $order
     * @return mixed
     */

    public function all($sort = 'created_at', $order = 'desc')
    {
        return $this->model->withTranslation()->orderBy($sort, $order)->get();
    }

    /**
     * Search the manufacturer
     *
     * @param array $search
     * @param $per_page
     * @return mixed
     */
    public function search($search, $per_page = 25)
    {

        $query = $this->model->orderBy('created_at', 'desc');

        foreach($this->fillable as $key => $fillable) {
            if(isset($search[$fillable])) $query->orWhere($fillable, 'LIKE', '%' . $search[$fillable] . '%');
        }

        foreach ($this->translatedAttributes as $attribute) {
            if(isset($search[$attribute])) $query->orWhereTranslationLike($attribute, '%' . $search[$attribute] . '%');
        }

        return $query->paginate($per_page);
    }

    /**
     * Stores manufacturer into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {

        $data = $this->checkData($data);

        $model = parent::create(array_only($data, $this->fillable));

        $model->fill(array_except($data, $this->fillable));

        $model->save();

        return $model;
    }

    /**
     * Updates manufacturer in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {

        $data = $this->checkData($data);

        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * delete manufacturer.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->deleteTranslations();
        $model->delete();

        return $model;
    }

    /**
     * @return mixed
     */
    public function getList()
    {
        return $this->model
            ->withTranslation()
            ->where('is_active', Manufacturer::ACTIVE)
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags('manufacturers')
            ->get();
    }

}
