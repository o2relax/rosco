<?php

namespace App\Repositories;

use App\Models\OrderItem;

class OrderItemRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(OrderItem $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }
    
}
