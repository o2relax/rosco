<?php

namespace App\Repositories;

use App\Models\Blog;

class BlogRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(Blog $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * @param int $per_page
     * @param string $sort
     * @param string $order
     * @return mixed
     */

    public function paginated($per_page = 25, $sort = 'created_at', $order = 'desc')
    {
        return $this->model->withTranslation()->orderBy($sort, $order)->paginate($per_page);
    }

    /**
     * Stores manufacturer into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {

        $data = $this->checkData($data);

        $model = parent::create(array_only($data, $this->fillable));

        $model->fill(array_except($data, $this->fillable));

        $model->save();

        return $model;
    }

    /**
     * Updates manufacturer in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {

        $data = $this->checkData($data);

        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * delete manufacturer.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->deleteTranslations();
        $model->delete();

        return $model;
    }

    /**
     * @param null $per_page
     * @return mixed
     */
    public function getList($per_page = null) {
        return $this->model
            ->withTranslation()
            ->where('is_active', Blog::ACTIVE)
            ->orderBy('created_at', 'desc')
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags(['blog'])
            ->paginate($per_page ? $per_page : cfg('blog_pagination'));
    }

    /**
     * @param $slug
     * @return mixed
     */

    public function getBySlug($slug) {
        return $this->model
            ->withTranslation()
            ->where('slug', $slug)
            ->where('is_active', Blog::ACTIVE)
//            ->remember(config('cache.cache_query', 1))
//            ->cacheDriver('redis')
//            ->cacheTags(['blog'])
            ->first();
    }

}
