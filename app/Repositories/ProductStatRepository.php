<?php

namespace App\Repositories;

use App\Models\ProductStat;
use Illuminate\Support\Facades\Schema;

class ProductStatRepository extends BaseRepository
{

    protected $fillable;
    protected $translatedAttributes;

    /**
     * ProductStatRepository constructor.
     * @param ProductStat $model
     */
    public function __construct(ProductStat $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }


}
