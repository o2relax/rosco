<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\UserMeta;
use DB;
use Exception;
use App\Models\User;

class CustomerRepository extends BaseRepository
{

    protected $fillable;

    protected $role;
    protected $meta;

    protected $roleUser;

    public function __construct(User $model, Role $role, UserMeta $meta)
    {
        $this->fillable = $model->getFillable();
        $this->roleUser = $model::USER;
        $this->role = $role;
        $this->meta = $meta;
        return parent::__construct($model);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getById($id)
    {
        return $this->model->with('meta')->where('id', $id)->first();
    }

    /**
     * Find a user by email
     *
     * @param  string $email
     * @return User
     */
    public function getByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }


    public function getList()
    {
        return $this->model->whereHas('roles', function ($q) {
                $q->where('name', $this->roleUser);
            })->get();
    }

    /**
     * Search the users
     *
     * @param  array $inputs
     * @return mixed
     */
    public function search(array $inputs = [])
    {

        $query = $this->model->orderBy('created_at', 'desc');

        $columns = $this->fillable;

        foreach ($inputs as $key => $value) {
            if (!$value) continue;
            if (in_array($key, $columns)) {
                $query->where($key, 'LIKE', '%' . $value . '%');
            } elseif (in_array($key, $this->model->available)) {
                $query->whereHas($key, function ($q) use ($value) {
                    $q->where('name', 'LIKE', '%' . $value . '%');
                });
            }
        }

        return $query->whereHas('roles', function ($q) {
            $q->where('name', $this->roleUser);
        })->paginate(config('common.paginate', 25));
    }


    /**
     * Destroy the profile
     *
     * @param  int $id
     * @return bool
     */

    public function delete($id)
    {
        $user = $this->model->find($id);
        $user->meta()->delete();
        $user->roles()->detach();
        return parent::delete($id);

    }

    /**
     * Assign a role to the user
     *
     * @param  string $roleName
     * @param  integer $userId
     * @return void
     */
    public function assignRole($roleName, $userId)
    {

        $role = $this->role->findByName($roleName);
        $user = $this->model->find($userId);

        $user->roles()->attach($role);
    }

    /**
     * UnAssign a role from the user
     *
     * @param  string $roleName
     * @param  integer $userId
     * @return void
     */
    public function unAssignRole($roleName, $userId)
    {
        $role = $this->role->findByName($roleName);
        $user = $this->model->find($userId);

        $user->roles()->detach($role);
    }

    /**
     * UnAssign all roles from the user
     *
     * @param  $userId
     */
    public function unAssignAllRoles($userId)
    {
        $user = $this->model->find($userId);
        $user->roles()->detach();
    }
}
