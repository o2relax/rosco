<?php

namespace App\Repositories;

use App\Models\ProductStatus;

class ProductStatusRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(ProductStatus $model)
    {
        $this->fillable['base'] = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * Returns all items.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getSelect()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    /**
     * Returns all one item.
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

}
