<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(Category $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function all($sort = 'created_at', $order = 'desc')
    {
        return $this->model->withTranslation()->orderBy($sort, $order)->get();
    }

    /**
     * Search the roles
     *
     * @param array $search
     * @param $per_page
     * @return mixed
     */
    public function search($search, $per_page = 25)
    {

        $query = $this->model->orderBy('created_at', 'desc');

        foreach ($this->fillable as $key => $fillable) {
            if (isset($search[$fillable])) $query->orWhere($fillable, 'LIKE', '%' . $search[$fillable] . '%');
        }

        foreach ($this->translatedAttributes as $attribute) {
            if (isset($search[$attribute])) $query->orWhereTranslationLike($attribute, '%' . $search[$attribute] . '%');
        }

        return $query->paginate($per_page);
    }

    public function getBySlug($slug)
    {
        return $this->model
            ->withTranslation()
            ->with('child')
            ->where('slug', $slug)
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags('category_' . $slug)
            ->first();
    }

    public function getList()
    {
        return $this->model
            ->withTranslation()
            ->where('is_active', Category::ACTIVE)
            ->orderBy(Category::ORDER, Category::SORT)
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags('categories')
            ->get();
    }

    public function getParents()
    {
        return $this->model
            ->withTranslation()
            ->where('is_active', Category::ACTIVE)
            ->where('parent_id', Category::MAIN_CATEGORY)
            ->orderBy(Category::ORDER, Category::SORT)
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags('categories')
            ->get();
    }

    public function getChild()
    {
        return $this->model
            ->withTranslation()
            ->where('is_active', Category::ACTIVE)
            ->where('parent_id', '!=', Category::MAIN_CATEGORY)
            ->orderBy(Category::ORDER, Category::SORT)
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags('categories')
            ->get();
    }

    /**
     * Stores category into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {
	if(!isset($data['is_active'])) $data['is_active'] = 0;
	elseif ($data['is_active'] == "on") $data['is_active'] = 1;
        $data = $this->checkData($data);
        $query = $this->model->where('slug', $data['slug'])->count();
        if ($query) {
            return redirect()->route('admin.categories.index');
        }
        $model = parent::create(array_only($data, $this->fillable));
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * Updates category in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        if ($id) {
            if (!isset($data['is_active'])) $data['is_active'] = 0;
            elseif ($data['is_active'] == "on") $data['is_active'] = 1;
            $data = $this->checkData($data);

//            $query = $this->model->where('slug', $data['slug'])->count();
//        if($query || $this->model->where('slug',++$data['slug'])->count())
//        {
//            $query += 1;
//            $data['slug'] = $data['slug'] . $query;
//        }
            $model = $this->model->find($id);
            $model->fill($data);

            $model->save();
            return $model;
        }
    }

    /**
     * delete category.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->deleteTranslations();
        $model->delete();

        return $model;
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function dropParent($category_id)
    {
        return $this->model
            ->where('parent_id', $category_id)
            ->update([
                'parent_id' => Category::MAIN_CATEGORY
            ]);
    }

}
