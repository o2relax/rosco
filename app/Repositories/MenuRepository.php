<?php

namespace App\Repositories;

use App\Models\Menu;
use App\Models\MenuLink;

class MenuRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(Menu $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * @param $code
     * @return mixed
     */

    public function getByCode($code)
    {
        return $this->model->where('code', $code)->with(['links' => function ($q) {
            $q->orderBy(MenuLink::ORDER, MenuLink::SORT)
                ->where('is_active', MenuLink::ACTIVE);
        }])->first();
    }

    /**
     * Stores category into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {

        $model = parent::create(array_only($data, $this->fillable));

        $model->fill(array_except($data, $this->fillable));

        $model->save();

        return $model;
    }

    /**
     * Updates category in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {

        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * delete category.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->links()->delete();
        $model->delete();

        return $model;
    }

}
