<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductReview;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;

class ProductRepository extends BaseRepository
{

    /** @var Product */
    protected $model;
    protected $fillable;
    protected $translatedAttributes;

    /**
     * ProductRepository constructor.
     *
     * @param Product $model
     */
    public function __construct(Product $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;

        return parent::__construct($model);
    }

    /**
     * @param int    $per_page
     * @param string $sort
     * @param string $order
     *
     * @return mixed
     */
    public function paginated($per_page = 15, $sort = 'id', $order = 'desc')
    {
        return $this->model->withTranslation()->with([
            'manufacturer' => function($q) {
                $q->withTranslation();
            },
            'category'     => function($q) {
                $q->withTranslation();
            },
            'currency',
        ])->orderBy($sort, $order)->paginate($per_page);
    }

    /**
     * @param array $ids
     * @param null  $mod
     *
     * @return mixed
     */
    public function getByIds(array $ids, $mod = null)
    {
        if ($mod == 'total') {
            return $this->model->whereIn('id', $ids)->get();
        } else {
            return $this->model
                ->withTranslation()
                ->with([
                    'manufacturer'           => function($q) {
                        $q->withTranslation();
                    },
                    'discountByManufacturer' => function($q) {
                        $q->whereHas('info', function($qq) {
                            $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                               ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
                        });
                    },
                    'discounts'              => function($q) {
                        $q->whereHas('info', function($qq) {
                            $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                               ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
                        });
                    },
                    'currency',
                ])->whereIn('id', $ids)->get();
        }
    }

    /**
     * Search Product.
     *
     * @param array $search
     * @param       $per_page
     *
     * @return Product
     */
    public function search($search, $per_page)
    {
        $query = $this->model->orderBy('created_at', 'desc');

        foreach ($this->fillable as $key => $fillable) {
            if (isset($search[$fillable])) {
                $query->Where($fillable, 'LIKE', $search[$fillable]);
            }
        }

        foreach ($this->translatedAttributes as $attribute) {
            if (isset($search[$attribute])) {
                $query->orWhereTranslationLike($attribute, '%' . $search[$attribute] . '%');
            }
        }

        return $query->paginate($per_page);
    }

    public function searchAll($search)
    {
        $query = $this->model->orderBy('created_at', 'desc');

        foreach ($this->fillable as $key => $fillable) {
            if (isset($search[$fillable])) {
                $query->Where($fillable, 'LIKE', $search[$fillable]);
            }
        }

        foreach ($this->translatedAttributes as $attribute) {
            if (isset($search[$attribute])) {
                $query->orWhereTranslationLike($attribute, '%' . $search[$attribute] . '%');
            }
        }

        return $query->paginate(7);
    }

    /**
     * @param      $filters
     * @param      $per_page
     *
     * @return mixed
     */
    public function getFiltered($filters, $per_page)
    {
        $query = $this->model->with('currency');

        foreach ($this->fillable as $key => $fillable) {
            if (
                isset($filters[$fillable]) &&
                $fillable != 'price' &&
                $fillable != 'category_id' &&
                $fillable != 'manufacturer_id'
            ) {
                $query->orWhere($fillable, 'LIKE', $filters[$fillable]);
            }
        }

        //            if (isset($filters[$fillable]) && $fillable != 'price') {
        //                $query->where(function($q)
        //                {
        //                    foreach($this->fillable as $key => $fillable) {
        //                        $q->orWhere($fillable , 'LIKE' , $filters[$fillable]);
        //                    }
        //                })
        //                    ->get();
        //            }

        if (isset($filters['manufacturer_id'])) {
            $manufacturer_pieces = explode(",", $filters['manufacturer_id']);
            $query->whereIn('manufacturer_id', $manufacturer_pieces);
        }

        //        foreach ($this->translatedAttributes as $attribute) {
        //            if (isset($filters[$attribute])) $query->orWhereTranslationLike($attribute, '%' . $filters[$attribute] . '%');
        //        }

        //        if (isset($filters['category_id'])) {
        //            $category_pieces = explode(",", $filters['category_id']);
        //            foreach ($category_pieces as $item) {
        //                $query->orWhere('category_id', $item);
        //            }
        //        }

        if (isset($filters['category_id'])) {
            $category_pieces = explode(",", $filters['category_id']);
            $query->whereIn('category_id', $category_pieces);
        }

        if (isset($filters['price_from'])) {
            $query->where('price', '>=', currency()->toDefault($filters['price_from']));
        }

        if (isset($filters['substance_from'])) {
            $query->orWhere('substance', 'LIKE', '%' . $filters['substance_from'] . '%');
        }

        if (isset($filters['price_to'])) {
            $query->where('price', '<=', currency()->toDefault($filters['price_to']));
        }

        $products = $query
            ->withTranslation()
            ->published();

        if (request()->get('sort')) {
            $products = $this->sortable($products);
        }

        return $this->getForPublic($products)
                    ->paginate($per_page);
    }

    /**
     * @param     $category_id
     * @param int $per_page
     *
     * @return mixed
     */
    public function getPublished($category_id, $per_page = 16)
    {
        $products = $this->model
            ->with('currency')
            ->withTranslation()
            ->where('is_published', 1);

        if ($category_id) {
            $products->whereIn('category_id', $category_id);
        }

        if (request()->get('sort')) {
            $products = $this->sortable($products);
        }

        return $this->getForPublic($products)
//                    ->remember(config('cache.cache_query', 1))
//                    ->cacheDriver('redis')
//                    ->cacheTags('products')
                    ->paginate($per_page);
    }

    public function sortable(Builder $products)
    {
        if ($sort = request()->get('sort')) {
            switch ($sort) {
                case 'date_add_desc':
                    $products->latest();
                    break;
                case 'date_add_asc':
                    $products->oldest();
                    break;
                case 'price_asc':
                    $products->orderBy('price');
                    break;
                case 'price_desc':
                    $products->orderByDesc('price');
                    break;
                case 'name_asc':
                    $products->with([
                        'translation' => function(Relation $query) {
                            $query->orderBy('name');
                        },
                    ]);
                    break;
                case 'name_desc':
                    $products->with([
                        'translation' => function(Relation $query) {
                            $query->orderByDesc('name');
                        },
                    ]);
                    break;
                case 'stock_desc':
                    $products->orderByDesc('stock');
                    break;
                case 'stock_asc':
                    $products->orderBy('stock');
                    break;
            }

            return $products;
        }

        return $products->orderBy(cfg('catalog_order'), cfg('catalog_sort'));
    }

    /**
     * Stores Product into database.
     *
     * @param array $data
     *
     * @return Product
     */
    public function create(array $data = [])
    {
        $data = $this->checkData($data);
        $test = $this->model->where('slug', 'LIKE', $data['slug'])->count();

        if ($test) {
            return null;
        }

        $product = parent::create(array_only($data, $this->fillable));

        $product->fill(array_except($data, $this->fillable));

        if (isset($data['attributes'])) {
            foreach ($data['attributes'] as $key => $attr) {
                $attribute = $product->attributes()->create(['attribute_id' => $key]);
                $attribute->fill($attr);
                $attribute->save();
            }
        }

        $product->save();

        return $product;
    }

    /**
     * Updates Product in the database.
     *
     * @param int   $id
     * @param array $data
     *
     * @return Product
     */
    public function update($id, array $data = [])
    {
        if (!isset($data['is_published'])) {
            $data['is_published'] = 0;
        }

        $data = $this->checkData($data);

        $product = $this->model->find($id);
        $product->fill($data);
        if (isset($data['attributes'])) {
            foreach ($data['attributes'] as $key => $attr) {
                $attribute = $product->attributes()->firstOrCreate(['attribute_id' => $key]);
                $attribute->fill($attr);
                $attribute->save();
            }
        }
        $product->save();

        return $product;
    }

    /**
     * @param $slug
     *
     * @return mixed
     */
    public function getBySlug($slug)
    {
        return $this->model
            ->published()
            ->where('slug', $slug)
            ->withTranslation()
            ->with([
                'manufacturer'           => function($q) {
                    $q->withTranslation();
                },
                'stats',
                'images',
                'reviews'                => function($q) {
                    $q->where('is_active', ProductReview::ACTIVE)
                      ->orderBy('created_at', 'desc');
                },
                'category'               => function($q) {
                    $q->withTranslation();
                },
                'discountByManufacturer' => function($q) {
                    $q->whereHas('info', function($qq) {
                        $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                           ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
                    });
                },
                'discounts'              => function($q) {
                    $q->whereHas('info', function($qq) {
                        $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                           ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
                    });
                },
            ])
            ->remember(config('cache.cache_query', 1))
            ->cacheDriver('redis')
            ->cacheTags(['products', 'product_' . $slug])
            ->first();
    }

    /**
     * @param $products
     *
     * @return mixed
     */
    public function getForPublic($products)
    {
        return $products
            ->with([
                'discountByManufacturer' => function($q) {
                    $q->whereHas('info', function($qq) {
                        $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                           ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
                    })->with('info');
                },
                'discounts'              => function($q) {
                    $q->whereHas('info', function($qq) {
                        $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                           ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
                    })->with('info');
                },
            ]);
    }

    /**
     * @param int    $per_page
     * @param string $module
     *
     * @return mixed
     */
    public function getLatest($per_page = 8, $module = 'get')
    {
        $products = $this->model
            ->withTranslation()
            ->published()
            ->latest();

        return $this->getForPublic($products)
                    ->orderBy('created_at', 'desc')
                    ->limit($per_page)
                    ->$module();
    }

    /**
     * Get all published products.
     *
     * @param $per_page
     *
     * @return \Illuminate\Support\Collection|null|static|Product
     */
    public function getPopular($per_page = 6)
    {
        $products = $this->model
            ->withTranslation()
            //            ->whereHas('stats', function($q) {
            //                $q->orderBy('views', 'desc');
            //            })
            ->published()
            ->popular();

        return $this->getForPublic($products)
                    ->orderBy('created_at', 'desc')
                    ->limit($per_page)
                    ->get();
    }

    /**
     * @param int $per_page
     *
     * @return mixed
     */
    public function getDiscounts($per_page = 6)
    {
        $products = $this->model
            ->withTranslation()
            ->where('is_published', 1);

        return $products->whereHas('discountByManufacturer', function($q) {
            $q->whereHas('info', function($qq) {
                $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                   ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
            })
              ->with('info');
        })
                        ->orwhereHas('discounts', function($q) {
                            $q->whereHas('info', function($qq) {
                                $qq->whereDate('date_start', '<=', Carbon::now()->format('Y-m-d'))
                                   ->whereDate('date_end', '>=', Carbon::now()->format('Y-m-d'));
                            })
                              ->with('info');
                        })
                        ->orderBy(cfg('catalog_order'), cfg('catalog_sort'))
            //            ->remember(config('cache.cache_query', 1))
            //            ->cacheDriver('redis')
            //            ->cacheTags(['products', 'products_discount'])
                        ->limit($per_page)
                        ->get();
    }

    /**
     * delete product.
     *
     * @param $id
     *
     * @return \Illuminate\Support\Collection|null|static|Product
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->images()->delete();
        $model->attributes()->delete();
        $model->discounts()->delete();
        $model->deleteTranslations();
        $model->delete();

        return $model;
    }

    public function dropCategory($category_id)
    {
        return $this->model->where('category_id', $category_id)->update(['category_id' => null]);
    }

}
