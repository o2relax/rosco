<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{

    protected $fillable;

    protected $roleAdmin;
    protected $roleManager;
    protected $roleUser;

    public function __construct(Role $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    public function all($order = 'id', $sort = 'desc') {
        return $this->model->all();
    }


    /**
     * Search the roles
     *
     * @param  array $inputs
     * @return mixed
     */
    public function search(array $inputs = [])
    {

        $query = $this->model->orderBy('id', 'desc');

        $columns = $this->fillable;

        foreach ($inputs as $key => $value) {
            if(!$value) continue;
            if (in_array($key, $columns)) $query->where($key, 'LIKE', '%' . $value . '%');
        }

        return $query->paginate(config('common.paginate', 25));
    }

    /**
     * Destroy the role
     *
     * @param  int $id
     * @return bool
     */

    public function delete($id)
    {
        $role = $this->model->find($id);
        $role->users()->detach();
        return parent::delete($id);

    }

    public function create(array $data = []) {
        $data['permissions'] = implode(',', $data['permissions']);
        return parent::create($data);
    }

    public function update($id, array $data = [])
    {

        $data = $this->checkData($data);
        if (isset($data["permissions"])) {
            $data["permissions"] = implode(",", $data["permissions"]);
        } else {
            $data["permissions"] = NULL;
        }
        $model = $this->model->find($id);
        $model->fill($data);

        $model->save();

        return $model;
    }

}
