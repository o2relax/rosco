<?php

namespace App\Repositories;

use App\Models\ProductImage;

class ProductImageRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(ProductImage $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * Returns one item.
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function deleteByProduct($product_id) {
        return $this->model->where('product_id', $product_id)->delete();
    }

}
