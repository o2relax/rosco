<?php

namespace App\Repositories;

use App\Models\Currency;

class CurrencyRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(Currency $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * Returns active Language.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList()
    {
        return $this->model->where('is_active', Currency::ACTIVE)->get();
    }

    /**
     * Returns by code Language.
     * @param $code
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByCode($code)
    {
        return $this->model->where('code', $code)->first();
    }

}
