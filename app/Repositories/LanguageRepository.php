<?php

namespace App\Repositories;

use App\Models\Language;

class LanguageRepository extends BaseRepository
{

    protected $fillable;

    public function __construct(Language $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * Returns active Language.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList()
    {
        return $this->model->where('is_active', Language::ACTIVE)->get();
    }

}
