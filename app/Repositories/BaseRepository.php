<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\ValidationException;

class BaseRepository
{
    /**
     * @var Model|null
     */
    protected $model;

    /**
     * @var array
     */
    protected $fillable;

    /**
     * @var array
     */
    protected $validationRules = [];

    /**
     * BaseRepository constructor.
     * @param Model|null $model
     */
    public function __construct(Model $model = null)
    {
        $this->model = $model;
        $this->fillable = $model->getFillable();
    }

    /**
     * @return array
     */
    public function validatorAttributeNames()
    {
        return [];
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param $array
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function with($array)
    {
        return $this->model->with($array);
    }

    /**
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function all($sort = 'created_at', $order = 'desc')
    {
        return $this->model->orderBy($sort, $order)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->model->query();
    }

    /**
     * @return mixed
     */
    public function count()
    {
        return $this->model->count();
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function instance(array $attributes = [])
    {
        $model = $this->model;

        return new $model($attributes);
    }

    /**
     * @param $key
     * @param $where
     * @return mixed
     */
    public function where($key, $where)
    {
        return $this->model->where($key, $where);
    }

    /**
     * @param $key
     * @param $whereIn
     * @return mixed
     */
    public function whereIn($key, $whereIn)
    {
        return $this->model->whereIn($key, $whereIn);
    }

    /**
     * @param null $perPage
     * @return mixed
     */
    public function paginate($perPage = null)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * @param array $data
     * @param null $rules
     * @param array $messages
     * @param array $customAttributes
     * @return mixed
     */
    public function validator(array $data = [], $rules = null, array $messages = [], array $customAttributes = [])
    {
        if (is_null($rules)) {
            $rules = $this->validationRules;
        }

        return Validator::make($data, $rules, $messages, $customAttributes);
    }

    /**
     * @param array $data
     * @param null $rules
     * @param array $messages
     * @param array $customAttributes
     * @return bool
     * @throws ValidationException
     */
    public function validate(array $data = [], $rules = null, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->validator($data, $rules, $messages, $customAttributes);

        return $this->_validate($validator);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data = [])
    {
        return $this->model->create(array_only($data, $this->fillable));
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        $instance = $this->findOrFail($id);
        $instance->update(array_only($data, $this->fillable));

        return $instance;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->delete();

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->delete($id);
    }

    /**
     * @param \Illuminate\Validation\Validator $validator
     * @return bool
     * @throws ValidationException
     */
    protected function _validate(\Illuminate\Validation\Validator $validator)
    {
        if (!empty($attributeNames = $this->validatorAttributeNames())) {
            $validator->setAttributeNames($attributeNames);
        }

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return true;
    }

    /**
     * @param int $per_page
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function paginated($per_page = 25, $sort = 'created_at', $order = 'desc')
    {
        return $this->model->orderBy($sort, $order)->paginate($per_page);
    }

    /**
     * @param array $data
     * @return array
     */
    public function checkData(array $data) {

        if(array_key_exists('slug', $data) && (is_null($data['slug']) || (isset($data['slug']) && $data['slug'] == ''))) {
            if(isset($data[cfg('language', 'code')]['name'])) {
                $data['slug'] = str_slug($data[cfg('language', 'code')]['name']);
            } elseif(isset($data[cfg('language', 'code')]['title'])) {
                $data['slug'] = str_slug($data[cfg('language', 'code')]['title']);
            }
        }

        if(array_key_exists('code', $data) && (is_null($data['code']) || (isset($data['code']) && $data['code'] == ''))) {
            $data['code'] = str_slug(isset($data[cfg('language', 'code')]['name']) ? $data[cfg('language', 'code')]['name'] : null);
        }

        if(array_key_exists('sort', $data) && is_null($data['sort'])) {
            unset($data['sort']);
        }

        return $data;
    }

    /**
     * @return mixed
     */
    public function getLastId() {
        return DB::table('information_schema.tables')
            ->where('table_name', $this->model->getTable())
            ->whereRaw('table_schema = DATABASE()')
            ->select('AUTO_INCREMENT')->first();
    }
}