<?php

namespace App\Repositories;

use App\Models\Status;

class StatusRepository extends BaseRepository
{

    protected $fillable;

    /**
     * StatusRepository constructor.
     * @param Status $model
     */

    public function __construct(Status $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * @param $type
     * @return mixed
     */

    public function getList($type) {
        return $this->model->withTranslation()->where('type', $type)->get();
    }

}
