<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\UserMeta;
use DB;
use Exception;
use App\Models\User;

class UserRepository extends BaseRepository
{

    protected $fillable;

    protected $role;
    protected $meta;

    protected $roleAdmin;
    protected $roleManager;
    protected $roleUser;

    public function __construct(User $model, Role $role, UserMeta $meta)
    {
        $this->fillable = $model->getFillable();
        $this->roleAdmin = $model::ADMIN;
        $this->roleManager = $model::MANAGER;
        $this->roleUser = $model::USER;
        $this->role = $role;
        $this->meta = $meta;
        return parent::__construct($model);
    }

    /**
     * Find a user by email
     *
     * @param  string $email
     * @return User
     */
    public function getByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }

    /**
     * Find by Role ID
     * @param  integer $id
     * @return mixed
     */
    public function findByRoleID($id)
    {

        $users = $this->model->whereHas('roles', function ($q) use ($id) {
            $q->where('id', $id);
        });

        return $users;
    }

    /**
     * Find by Role ID
     * @return mixed
     */
    public function findStaff()
    {

        $users = $this->model
            ->whereRoles([$this->roleAdmin, $this->roleManager])
            ->paginate(config('common.paginate', 25));

        return $users;
    }

    /**
     * Search the users
     *
     * @param  array $inputs
     * @return mixed
     */
    public function search(array $inputs = [])
    {

        $query = $this->model->orderBy('created_at', 'desc');

        $columns = $this->fillable;

        foreach ($inputs as $key => $value) {
            if (!$value) continue;
            if (in_array($key, $columns)) $query->where($key, 'LIKE', '%' . $value . '%');
            elseif (in_array($key, $this->model->available)) {
                $query->whereHas($key, function ($q) use ($value) {
                    $q->where('name', 'LIKE', '%' . $value . '%');
                });
            }
        }

        return $query->paginate(config('common.paginate', 25));
    }

    /**
     * Invite a new member
     * @param  array $data
     * @return mixed
     */
    public function invite($data)
    {

        return parent::create([
            'email' => $data['email'],
            'name' => $data['name'],
            'password' => bcrypt($data['password'])
        ]);

    }

    public function registration(array $data = [])
    {

        $user = $this->invite($data);
        $this->createMeta($user->id);

        return $user;
    }

    /**
     * Create a user's profile
     *
     * @param  $user_id
     * @param  string $role the role of this user
     * @return User
     */
    public function createMeta($user_id, $role = 'user')
    {

        $user = $this->model->find($user_id);

        $this->meta->create([
            'user_id' => $user_id
        ]);

        $this->assignRole($role, $user_id);

        return $user;
    }

    /**
     * Destroy the profile
     *
     * @param  int $id
     * @return bool
     */

    public function delete($id)
    {
        $user = $this->model->find($id);
        $user->meta()->delete();
        $user->roles()->detach();
        return parent::delete($id);

    }

    /**
     * Update a user's profile
     *
     * @param  int $id
     * @param  array $data
     * @throws Exception
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        try {
            return DB::transaction(function () use ($id, $data) {
                $user = $this->model->find($id);

                if (isset($data['meta'])) {
                    $user->meta->update($data['meta']);
                }

                $user->update($data);

                if (isset($data['roles'])) {
                    $user->roles()->detach();
                    $this->assignRole($data['roles'], $id);
                }

                return $user;
            });
        } catch (Exception $e) {
            throw new Exception("We were unable to update your profile", 1);
        }
    }

    /**
     * Assign a role to the user
     *
     * @param  string $roleName
     * @param  integer $userId
     * @return void
     */
    public function assignRole($roleName, $userId)
    {

        $role = $this->role->findByName($roleName);
        $user = $this->model->find($userId);

        $user->roles()->attach($role);
    }

    /**
     * UnAssign a role from the user
     *
     * @param  string $roleName
     * @param  integer $userId
     * @return void
     */
    public function unAssignRole($roleName, $userId)
    {
        $role = $this->role->findByName($roleName);
        $user = $this->model->find($userId);

        $user->roles()->detach($role);
    }

    /**
     * UnAssign all roles from the user
     *
     * @param  $userId
     */
    public function unAssignAllRoles($userId)
    {
        $user = $this->model->find($userId);
        $user->roles()->detach();
    }

    /**
     * @param $user_id
     * @param $value
     * @param string $column
     * @return mixed
     */
    public function getByColumn($user_id, $value, $column = 'id')
    {
        return $this->model->where('id', $user_id)->where($column, $value)->first();
    }
}
