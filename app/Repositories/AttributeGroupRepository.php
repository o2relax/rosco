<?php

namespace App\Repositories;

use App\Models\AttributeGroup;

class AttributeGroupRepository extends BaseRepository
{

    protected $fillable;

    protected $translatedAttributes;

    public function __construct(AttributeGroup $model)
    {
        $this->fillable = $model->getFillable();
        $this->translatedAttributes = $model->translatedAttributes;
        return parent::__construct($model);
    }

    /**
     * Search the roles
     *
     * @param array $search
     * @param $per_page
     * @return mixed
     */
    public function search($search, $per_page = 25)
    {

        $query = $this->model->orderBy('created_at', 'desc');

        foreach($this->fillable as $key => $fillable) {
            if(isset($search[$fillable])) $query->orWhere($fillable, 'LIKE', '%' . $search[$fillable] . '%');
        }

        foreach ($this->translatedAttributes as $attribute) {
            if(isset($search[$attribute])) $query->orWhereTranslationLike($attribute, '%' . $search[$attribute] . '%');
        }

        return $query->paginate(config('common.paginate', $per_page));
    }

    /**
     * Stores category into database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {

        $data = $this->checkData($data);

        $model = parent::create(array_only($data, $this->fillable));

        $model->fill(array_except($data, $this->fillable));

        $model->save();

        return $model;
    }

    /**
     * Updates category in the database.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {

        $data = $this->checkData($data);

        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * delete category.
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->deleteTranslations();
        $model->attributes()->delete();
        $model->delete();

        return $model;
    }

}
