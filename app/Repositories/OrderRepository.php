<?php

namespace App\Repositories;

use App\Models\Order;

class OrderRepository extends BaseRepository
{

    protected $fillable;

    /**
     * OrderRepository constructor.
     * @param Order $model
     */
    public function __construct(Order $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    /**
     * @param array $order_info
     * @param array $items
     * @return mixed
     */
    public function createOrder(array $order_info = [], array $items = [])
    {

        $order = parent::create($order_info);

        foreach ($items as $item) {
            $order->items()->create($item);
        }

        return $order;

    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->items()->delete();
        $model->delete();

        return $model;
    }

    /**
     * @param int $per_page
     * @param string $sort
     * @param string $order
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|mixed
     */
    public function paginated($per_page = 25, $sort = 'created_at', $order = 'desc')
    {
        return $this->model
            ->with(['items' => function ($q) {
                $q->with(['product' => function ($qq) {
                    $qq->withTranslation();
                }]);
            }, 'currency', 'user'])
            ->orderBy($sort, $order)
            ->paginate();
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getByUser($user_id)
    {
        return $this->model->where('user_id', $user_id)
            ->with(['items' => function ($q) {
                $q->with(['product' => function ($qq) {
                    $qq->withTranslation();
                }]);
            }, 'currency', 'user'])
            ->orderBy('created_at', 'desc')
            ->paginate(10);
    }

}
