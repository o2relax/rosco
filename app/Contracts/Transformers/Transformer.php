<?php

namespace App\Contracts\Transformers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class Transformer
 * @package App\Contracts\Transformers
 */
abstract class Transformer
{

    /**
     * @var
     */
    public $response;

    /**
     * @var null
     */
    protected $relations = null;

    /**
     * @param $data
     * @return mixed|null
     */
    public function response($data)
    {

        if ($data instanceof Model) {
            $this->response = $this->item($data);
        } elseif ($data instanceof LengthAwarePaginator || $data instanceof Collection || is_array($data)) {
            $this->response = $this->collection($data);
        }
        return $this->response;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function relations($value)
    {
        if ($this->relations) {
            foreach ($this->relations as $relation) {

                $func = 'relation' . ucfirst($relation);

                if (!method_exists($this, $func) || !$value->$relation) continue;

                $rel = [];

                foreach ($value->$relation as $key => $item) {
                    $rel[$key] = $this->$func($item);
                }
                $value->$relation = $rel;
            }
        }

        return $this->transform($value);
    }

    /**
     * @param $data
     * @param null $transformerName
     * @return mixed
     */
    public function collection($data, $transformerName = null)
    {
        foreach ($data as $key => $value) {
            if ($transformerName) {
                $class = new $transformerName;
                $value = $class->response($value);
            } else {
                $value = $this->relations($value);
            }
        }
        return $data;
    }

    /**
     * @param $data
     * @param null $transformerName
     * @return mixed|null
     */
    public function item($data, $transformerName = null)
    {
        $item = null;
        if ($transformerName) {
            $class = new $transformerName;
            $item = $class->response($data);
        } else {
            $item = $this->relations($data);
        }
        return $item;
    }

    /**
     * @param $object
     * @return mixed
     */
    public function transform($object)
    {
        return $object;
    }

}
