<?php

namespace App\Contracts\Cart;

use App\Interfaces\CartServiceInterface;
use App\Traits\CallableTrait;

/**
 * Class Action
 * @package App\Contracts\Actions
 */
abstract class Cart implements CartServiceInterface
{
    use CallableTrait;

    /**
     * @param $product
     * @param string $class
     * @return string
     * @throws \Throwable
     */

    public function addToCartBtn($product, $class = '') {
        return view('shop.includes.cart_btn', ['product_id' => $product, 'class' => $class])->render();
    }

    /**
     * @param $product
     * @param string $class
     * @return string
     * @throws \Throwable
     */
    public function deleteFromCartBtn($product, $class = '') {
        return view('shop.includes.cart_delete', ['product_id' => $product, 'class' => $class])->render();
    }
}
