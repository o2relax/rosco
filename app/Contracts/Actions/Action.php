<?php

namespace App\Contracts\Actions;

use App\Traits\CallableTrait;

/**
 * Class Action
 * @package App\Contracts\Actions
 */
abstract class Action
{
    use CallableTrait;

    /**
     * @return mixed
     */
}
