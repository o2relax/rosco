<?php

namespace App\Contracts\Tasks;

use Illuminate\Support\Facades\App;

/**
 * Class Task
 * @package App\Contracts\Tasks
 */
abstract class Task
{


    /**
     * @return mixed
     */

    /**
     *  Format error
     *
     * @param $e
     * @param $text
     * @return string
     */
    public function format($e, $text)
    {
        return '[' . basename(get_class($this)) . '@' . debug_backtrace()[1]['function']. ']: ' . $text . $e->getMessage() . 'on line ' . $e->getLine() . ' in file ' . $e->getFile();
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $runArguments
     * @param array $relations
     * @param array $methods
     *
     * @return  mixed
     */
    public function call($class, $method,  $runArguments = [], $relations = [], $methods = [])
    {
        $action = App::make($class);

        // allows calling other methods in the class before calling the main `run` function.
        foreach ($methods as $methodInfo) {
            // if is array means it has arguments
            if (is_array($methodInfo)) {
                $method = key($methodInfo);
                $arguments = $methodInfo[$method];
                if (method_exists($action, $method)) {
                    $action->$method(...$arguments);
                }
            } else {
                // if is string means it's just the function name
                if (method_exists($action, $methodInfo)) {
                    $action->$methodInfo();
                }
            }
        }

        return $action->run($method, $runArguments, $relations);
    }

}
